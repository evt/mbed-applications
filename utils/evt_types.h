/*
 * file: evt_types.h
 * purpose: Define some aliased types in common use through EVT embedded applications.
 */

#pragma once

#include "evt_protocols_common.h"
#include "evt_time.h"
/*
 * Useful for callback functions. Takes no parameters and returns none. Can be
 * used to set global flags.
 */
using void_function_ptr_t = void (*)();

constexpr static uint32_t CAN_BUFFER_LENGTH = 1250;

struct CanLog
{
    CanLog(): index(0), isFull_A(false), isFull_B(false), context(0)
    {
        // Empty constructor
    }

    // We're keeping two copies of each so one buffer can get populated while
    // the other is getting processed.
    Time::Clock timestamp_A[CAN_BUFFER_LENGTH];
    Time::Clock timestamp_B[CAN_BUFFER_LENGTH];

    PROTOCOL::CanMessage messages_A[CAN_BUFFER_LENGTH];
    PROTOCOL::CanMessage messages_B[CAN_BUFFER_LENGTH];

    uint32_t index;

    bool isFull_A;
    bool isFull_B;
    uint8_t context;

};
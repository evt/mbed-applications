/* 
 *  evt_bit_utils.h
 *
 *  General purpose utility functions and/or macros for
 *  easy bit manipulation for improved code readability.
 */
#ifndef EVT_BIT_UTILS_H
#define EVT_BIT_UTILS_H


/*
 * Operations for 16 bit data
 */
constexpr static inline uint8_t getHighByte(uint16_t data)
{
    return static_cast<uint8_t>((data & 0xFF00) >> 8);
}

constexpr static inline uint8_t getLowByte(uint16_t data)
{
    return static_cast<uint8_t>(data & 0x00FF);
}

/*
 * Operations for 32 bit data
 */
constexpr static inline uint8_t getHighestByte(uint32_t data)
{
    return static_cast<uint8_t>((data & 0xFF000000) >> 24);
}

constexpr static inline uint8_t getMiddleHighByte(uint32_t data)
{
    return static_cast<uint8_t>((data & 0x00FF0000) >> 16);
}

constexpr static inline uint8_t getMiddleLowByte(uint32_t data)
{
    return static_cast<uint8_t>((data & 0x0000FF00) >> 8);
}

constexpr static inline uint8_t getLowestByte(uint32_t data)
{
    return static_cast<uint8_t>((data & 0x000000FF));
}




#endif

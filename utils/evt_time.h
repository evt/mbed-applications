/**
 * Used to provide a common set of data structures for interacting
 * with time on the boards. 
 */

#pragma once

#include <cstdint>

namespace Time
{
    enum class WeekDay
    {
        MONDAY    = 1,
        TUESDAY   = 2,
        WEDNESDAY = 3,
        THURSDAY  = 4,
        FRIDAY    = 5,
        SATURDAY  = 6,
        SUNDAY    = 7
    };

    enum class Month
    {
        JANUARY     = 1,
        FEBRUARY    = 2,
        MARCH       = 3,
        APRIL       = 4,
        MAY         = 5,
        JUNE        = 6,
        JULY        = 7,
        AUGUST      = 8,
        SEPTEMBER   = 9,
        OCTOBER     = 10,
        NOVEMBER    = 11,
        DECEMBER    = 12
    };

    typedef struct Date
    {
        WeekDay weekDay;
        Month month;
        uint8_t day;
        uint8_t year;
    } Date;

    typedef struct Clock
    {
        uint8_t hours;
        uint8_t minutes;
        uint8_t seconds;
        uint8_t subseconds;
    } Clock;
}
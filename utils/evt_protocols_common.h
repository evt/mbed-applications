/*
 * file: evt_protocols_common.h
 *
 * Common protocol-specific data structures and information
 * that can be shared across architecture layers for ease of use.
 *
 */

#pragma once

namespace PROTOCOL
{

constexpr static uint8_t CAN_MESSAGE_MAX_LENGTH = 8;

struct CanMessage
{
    CanMessage(uint32_t _id, uint8_t * _packet, uint8_t _dlc)
        : id(_id), dataLengthCode(_dlc)
    {
        for(unsigned int i = 0; i < _dlc; i++)
            packet[i] = _packet[i];
    }

    CanMessage() : id(0), dataLengthCode(0)
    {
        // Empty default constructor
    }

    uint32_t id;
    uint8_t dataLengthCode;
    uint8_t packet[CAN_MESSAGE_MAX_LENGTH];
};

}

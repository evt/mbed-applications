import os

EVT_TARGETS = {

    'imu':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'task_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('boards', 'imu')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'sgm':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'task_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('boards', 'sgm')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'bms':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'task_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('boards', 'bms')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_app_sgm':
        {
            'sources': ['app', 'mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_app_sgm')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_analogIn':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', os.path.join('test_analog', 'test_analogIn'))],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_analogOut':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', os.path.join('test_analog', 'test_analogOut'))],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_bare_metal_can':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_bare_metal_can')],
            'hw_target': 'NUCLEO_F302R8' # should be EVT_BMS_4_1 at some point
        },
    'test_blink':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_blink')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_bno055':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_bno055')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_error_manager':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_error_manager')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_i2c':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_i2c')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_nucleo_can':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_nucleo_can')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_ltc2440':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_ltc2440')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_evtCan':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_evtCan')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_evt_gpio_mux':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_evt_gpio_mux')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_pwm':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_pwm')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_gpio_toggle_time':
        {
            'sources': ['app', 'mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_gpio_toggle_time')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_spi_mux':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_spi_mux')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_spi':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_spi')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_stwd100':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_stwd100')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_task_manager':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'task_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_task_manager')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_uart':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_uart')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_imu':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', 'app', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_imu')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_ltc6811':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_ltc6811')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_cellVoltages':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_cellVoltages')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_appTemperatures':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_appTemperatures')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_sim100':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_sim100')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_f3xx_adc':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_adc', 'test_f3xx_adc')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_f4xx_adc':
        {
             'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_adc', 'test_f4xx_adc')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_blink_gub':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_blink_gub')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_f4xx_can':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_f4xx_can')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_f3xx_flash':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_f3xx_flash')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_f3xx_gpio':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_f3xx_gpio')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_f4xx_rtc':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_f4xx_rtc')],
            'hw_target' : 'NUCLEO_F446RE'
        },
    'test_fatfs':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('third_party', os.path.join('FatFs', 'src')), os.path.join('testing', 'test_fatfs')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_can_logging':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev','app', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('third_party', os.path.join('FatFs', 'src')), os.path.join('testing', 'test_canLogging')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_temp_sensors':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('testing', 'test_temp_sensors')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'gub':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'task_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('third_party', os.path.join('FatFs', 'src')), os.path.join('boards', 'gub')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'tms':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'app', 'error_manager', 'task_manager', 'utils', os.path.join('third_party', os.path.join('FatFs', 'inc')), os.path.join('boards', 'tms')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_f4xx_uart':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('testing', 'test_f4xx_uart')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_f4xx_gpio':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('testing', 'test_f4xx_gpio')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_f4xx_i2c':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('testing', 'test_f4xx_i2c')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_f3xx_uart':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('testing', 'test_f3xx_uart')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_f3xx_dac':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('testing', 'test_f3xx_dac')],
            'hw_target': 'NUCLEO_F302R8'
        },
    'test_f4xx_dac':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f4xx'), os.path.join('io', 'common'), os.path.join('io', 'f4xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('testing', 'test_f4xx_dac')],
            'hw_target': 'NUCLEO_F446RE'
        },
    'test_f3xx_i2c':
        {
            'sources': ['mbed-platform', os.path.join('targets', 'common'), os.path.join('targets', 'f3xx'), 'mc', os.path.join('io', 'common'), os.path.join('io', 'f3xx'), os.path.join('io', 'mbed'), 'dev', 'error_manager', 'utils', os.path.join('testing', 'test_f3xx_i2c')],
            'hw_target': 'NUCLEO_F302R8'
        }
}

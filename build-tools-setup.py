"""
    file: build-tools-setup.py
    purpose: A simple Python 2.7 script for reconfiguring some mbed-cli build
             options. This script should be run every time the files need to
             be changed or if a local repository has just been cloned.
"""

from __future__ import print_function
import os
import shutil


REPO_DIR = os.path.dirname(os.path.abspath(__file__))
BUILD_CONFIG_DIR = os.path.join(REPO_DIR, 'custom-build-files')

CUSTOM_BUILD_FILES = {'targets.json' : os.path.join(REPO_DIR, 'mbed-os', 'targets'),
                      'debug.json' : os.path.join(REPO_DIR, 'mbed-os', 'tools', 'profiles'),
                      'develop.json' : os.path.join(REPO_DIR, 'mbed-os', 'tools', 'profiles'),
                      'release.json' : os.path.join(REPO_DIR, 'mbed-os', 'tools', 'profiles')}


if os.path.exists(os.path.join(REPO_DIR, 'mbed-os')):
    # overwrite the mbed-os files with the current files
    for k in CUSTOM_BUILD_FILES.keys():
        shutil.copy(os.path.join(BUILD_CONFIG_DIR, k), CUSTOM_BUILD_FILES[k])
else:
    print("You have not yet initialized the mbed-cli build system.")

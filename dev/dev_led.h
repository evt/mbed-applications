/*
 * file: dev_led.h
 * purpose: Interface for LED objects.
 */

#ifndef DEV_LED_H
#define DEV_LED_H

#include "io_gpio_intf.h"

namespace DEV
{

class LED
{
public:

    // Determining whether the LED is active high or active low
    enum class ACTIVE_STATE
    {
        HIGH    = 0u,
        LOW     = 1u,
    };

    LED(IO::GpioIntf & gpio_pin, ACTIVE_STATE configuration = ACTIVE_STATE::HIGH);
    ~LED();

    void toggle();

    IO::GpioIntf::STATE getCurrentState();

    void setCurrentState(IO::GpioIntf::STATE state);

private:

    IO::GpioIntf & my_gpio_pin;
    ACTIVE_STATE my_active_state;
};

} // namespace DEV

#endif  // DEV_LED_H

/**
 * Represents the interface with the Sendyne. Implements the CAN interfacing using the protocall documented below.
 * 
 * http://sendyne.com/Datasheets/SIM100%20CAN%20protocol%20v0_4.pdf
 * 
 * @author Collin Bolles
 **/


#ifndef DEV_SENDYNE_SIM100
#define DEV_SENDYNE_SIM100

#include <cstdint>
#include "io_can_intf.h"

namespace DEV
{
class Sim100
{
    public:

    /**
     * Represents the status response from the Sendyne
     * see datasheet page 4 figure 7
     **/
    struct Status
    {
        bool hardwareError;
        bool noNewEstimates;
        bool highUncertainty;
        bool highBatteryVoltage;
        bool lowBatteryVoltage;
        bool isoltationWarning;
        bool isolationFault;
    };

    /**
     * Represents the state of the Isolation as reported from the Sendyne
     * see datasheet page 5 figure 8
     **/
    struct IsolationState
    {
        struct Status status;
        uint16_t electricIsolation;
        uint8_t isolationUncertainty;
        uint16_t energyStored;
        uint8_t storedUncertainty;
    };

    /**
     * Represents the state of the resistance as reported from the Sendyne
     * see datasheet page 6 figure 9
     **/
    struct IsolationResistances
    {
        struct Status status;
        uint16_t resistanceP;
        uint8_t resistancePUncertainty;
        uint16_t resistanceN;
        uint8_t resistanceNUncertainty;
    };

    /**
     * Represents the state of the capacitances as reported from the Sendyne
     * see datasheet page 6 figure 10
     **/
    struct IsolationCapacitances
    {
        struct Status status;
        uint16_t capacitanceP;
        uint8_t capacitancePUncertainty;
        uint16_t capacitanceN;
        uint8_t capacitanceNUncertainty;
    };

    /**
     * Represnts the state of the voltages as reported from the Sendyne
     * see datasheet page 6 figure 11
     **/
    struct IsolationVoltages
    {
        struct Status status;
        uint16_t voltageP;
        uint8_t voltagePUncertainty;
        uint16_t voltageN;
        uint8_t voltageNUncertainty;
    };

    /**
     * Represents the state of the battery voltages as reported from the Sendyne
     * see datasheet page 7 figure 12
     **/
    struct BatteryVoltages
    {
        struct Status status;
        uint16_t batteryVoltage;
        uint8_t batteryVoltageUncertainty;
        uint16_t batteryVoltageMax;
        uint8_t batteryVoltageMaxUncertainty;
    };

    /**
     * Represents the error flags as reported from the Sendyne
     * see datasheet page 8 figure 13
     **/
    struct ErrorFlags
    {
        struct Status status;
        bool vx2Broken;
        bool vx1Broken;
        bool chassisConnectionBroken;
        bool vx1Vx2Reversed;
        bool voltageOutOfSpecs;
        bool powerSupplyOutOfRange;
    };

    /**
     * Set the values in the status structure to the binary representation of the data
     * 
     * @param status Status structure which represents the status of the Sendyne board
     * @param data The data value that was retrieved from the Sendyne 
     **/
    void setStatus(Status & status, uint8_t data);

    /**
     * Set the values in the isolation state structure to the response from the Sendyne
     * 
     * @param state The IsolationState structure to set the values to
     **/
    void readIsolationState(IsolationState & state);

    /**
     * Set the values in the resistance structure to the response from the Sendyne
     * 
     * @param resistances The IsolationResistances structure to set the values to
     **/
    void readIsolationResistances(IsolationResistances & resistances);

    /**
     * Set the values in the capacitances structure to the response from the Sendyne
     * 
     * @param capacitances The IsolationCapacitances structure to set the values to
     **/
    void readIsolationCapacitances(IsolationCapacitances & capacitances);

    /**
     * Set the values in the isolation voltages structure to the response from the Sendyne
     * 
     * @param voltages The IsolationVoltages structure to set the values to
     **/
    void readIsolationVoltages(IsolationVoltages & voltages);

    /**
     * Set the values in the battery voltages structure to the response from the Sendyne
     * 
     * @param voltages The BatteryVoltages structure to set the values to
     **/
    void readBatteryVoltages(BatteryVoltages & voltages);

    /**
     * Set the values in the error flags structure to the response from the Sendyne
     * 
     * @param errorFlag The ErrorFlags structure to set the values to
     **/
    void readErrorFlags(ErrorFlags & errorFlag);

    /**
     * Get the part name from the Sendyne
     * 
     * @param partName Pointer where the partname will be accessed
     **/
    void readPartName(char partName[]);

    /**
     * Write the maximum voltage that the battery should reach to the Sendyne
     * 
     * @param maxVoltage The maximum voltage the battery should be
     **/
    void writeMaxVoltage(uint16_t maxVoltage);

    /**
     * Send request to the board to reset the registers
     **/
    void resetBoard();


    // Constructor and destructor
    Sim100(IO::CanIntf & can);
    ~Sim100();

    private:
    // Interface values
    IO::CanIntf & my_canIntf;

    // Read operation codes see datasheet page 3 figure 5
    constexpr static uint32_t TRANSMIT_CAN_ID            = 0xA100101;
    constexpr static uint32_t RECEIEVE_CAN_ID            = 0xA100100;
    constexpr static uint8_t READ_ISOLATION_STATE        = 0xE0;
    constexpr static uint8_t READ_ISOLATION_RESISTANCE   = 0xE1;
    constexpr static uint8_t READ_ISOLATION_CAPACITANCES = 0xE2;
    constexpr static uint8_t READ_VOLTAGES_POLE_CHASSIS  = 0xE3;
    constexpr static uint8_t READ_BATTERY_VOLTAGE        = 0xE4;
    constexpr static uint8_t READ_ERROR_FLAGS            = 0xE5;

    // Manufacturer Data
    constexpr static uint8_t READ_PART_NAME_0 = 0x01;
    constexpr static uint8_t READ_PART_NAME_1 = 0x02;
    constexpr static uint8_t READ_PART_NAME_2 = 0x03;
    constexpr static uint8_t READ_PART_NAME_3 = 0x04;

    //Write operation codes see datasheet page 9
    constexpr static uint8_t WRITE_MAX_VOLTAGE = 0xF0;
    constexpr static uint8_t RESET_BOARD = 0xC1;

}; // class Sim100

} // namespace

#endif

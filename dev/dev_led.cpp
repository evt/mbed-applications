/*
 * file: dev_led.cpp
 * purpose: Driver for LED objects using the mbed framework.
 */

#include "dev_led.h"

namespace DEV
{

LED::LED(IO::GpioIntf & gpio_pin, ACTIVE_STATE configuration) : my_gpio_pin(gpio_pin), my_active_state(configuration)
{
    this->my_gpio_pin.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    this->setCurrentState(IO::GpioIntf::STATE::LOW);
}


LED::~LED()
{
    // Empty destructor
}


void LED::toggle()
{
    IO::GpioIntf::STATE current_state = this->my_gpio_pin.readPin();

    if (IO::GpioIntf::STATE::LOW == current_state)
    {
        this->my_gpio_pin.writePin(IO::GpioIntf::STATE::HIGH);
    }
    else
    {
        this->my_gpio_pin.writePin(IO::GpioIntf::STATE::LOW);
    }
}


IO::GpioIntf::STATE LED::getCurrentState()
{
    return this->my_gpio_pin.readPin();
}


void LED::setCurrentState(IO::GpioIntf::STATE state)
{
    // if the LED is active high, it's state follows from pin setting
    if (this->my_active_state == ACTIVE_STATE::HIGH)
    {
        this->my_gpio_pin.writePin(state);
    }
    // otherwise, it is reversed
    else
    {
        if (state == IO::GpioIntf::STATE::HIGH)
        {
            this->my_gpio_pin.writePin(IO::GpioIntf::STATE::LOW);
        }
        else
        {
            this->my_gpio_pin.writePin(IO::GpioIntf::STATE::HIGH);
        }
    }
}

} // namespace DEV

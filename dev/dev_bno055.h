/*
 * file: dev_bno055.h
 * purpose: Interface for the BNO055 IMU.
 */

#ifndef DEV_BNO055_H
#define DEV_BNO055_H

#include <cstdint>

namespace IO
{
class i2cIntf;
class GpioIntf;
} // namespace IO

namespace DEV
{

class bno055
{
public:

    // System status from the status register
    enum class Status
    {
        IDLE        = 0u,
        ERROR       = 1u,
        PERIPH_INIT = 2u,
        SYSTEM_INIT = 3u,
        SELF_TEST   = 4u,
        FUSION_RUN  = 5u,
        SYSTEM_RUN  = 6u
    };

    // System error status from the error register
    enum class Error
    {
        NO_ERROR            = 0x0,
        PERIPH_INIT_ERROR   = 0x1,
        SYSTEM_INIT_ERROR   = 0x2,
        SELF_TEST_FAILED    = 0x3,
        VALUE_OUT_OF_RANGE  = 0x4,
        ADDR_OUT_OF_RANGE   = 0x5,
        REG_WRITE_ERROR     = 0x6,
        LOW_PWR_UNAVAILABLE = 0x7,
        ACC_PWR_UNAVAILABLE = 0x8,
        ALGO_CONFIG_ERROR   = 0x9,
        SENSOR_CONFIG_ERROR = 0xA
    };

    // Operation mode from the OPR_MODE register
    enum class OperationMode
    {
        CONFIG_MODE             = 0x0,
        ACCELEROMETER_ONLY      = 0x1,
        MAGNOMETER_ONLY         = 0x2,
        GYROSCOPE_ONLY          = 0x3,
        ACCELEROMETER_MAGNOMETER = 0x4,
        ACCELEROMETER_GYROSCOPE = 0x5,
        MAGNOMETER_GYROSCOPE    = 0x6,
        ACCELEROMETER_GYROSCOPE_MAGNOMETER = 0x7,
        INERTIAL_MEASUREMENT_UNIT = 0x8,
        COMPASS                 = 0x9,
        MAGNOMETER_FOR_GYROSCOPE = 0xA,
        NINE_DEGREES_OF_FREEDOM_FAST_MAGNOMETER_CALIBRATION_OFF = 0xB,
        NINE_DEGREES_OF_FREEDOM = 0xC

    };

    // Sensor offsets to account for drift and other inaccuracies
    struct CalibrationInfo
    {
        int16_t accel_x;
        int16_t accel_y;
        int16_t accel_z;

        int16_t mag_x;
        int16_t mag_y;
        int16_t mag_z;

        int16_t gyro_x;
        int16_t gyro_y;
        int16_t gyro_z;

        int16_t accel_radius;
        int16_t mag_radius;
    };

    // Status of overall calibration level for the system/
    // sensor fusion algorithm as well as the 3 individual sensors
    // Values for each range from 0->3. (0 = Bad, 3 = Good)
    // If sysCal == 0 the data obtained from the bno055 should be ignored!
    struct CalibrationStatus
    {
        uint8_t sysCal;
        uint8_t accelCal;
        uint8_t magCal;
        uint8_t gyroCal;
    };

    // getStatus - Returns the contents of the status register.
    //              Logs an error if the status is ERROR.
    Status getStatus();

    // getAcceleromter_()
    //  Returns the value produced by the raw acceleromter for
    //  the specified axis. Units are in cm/s^2
    int16_t getAccelerometerX();
    int16_t getAccelerometerY();
    int16_t getAccelerometerZ();

    // getQuaternionPosition_()
    //  Returns the calculated value of the quaternion position
    //  for the specified axis. Units are in "Quaternion Units"
    //  (That's actually what the datasheet says)
    int16_t getQuaternionPositionW();
    int16_t getQuaternionPositionX();
    int16_t getQuaternionPositionY();
    int16_t getQuaternionPositionZ();

    // getLinearAcceleration_()
    //  Returns the calculated value of the linear acceleration
    //  for the specified axis. Units are in cm/s^2
    int16_t getLinearAccelerationX();
    int16_t getLinearAccelerationY();
    int16_t getLinearAccelerationZ();

    // getGravityVector_()
    //  Returns the calculated value of the gravity vector
    //  for the specified axis. Units are in cm/s^2
    int16_t getGravityVectorX();
    int16_t getGravityVectorY();
    int16_t getGravityVectorZ();

    bno055(IO::i2cIntf &i2c, IO::GpioIntf &ps0, IO::GpioIntf &ps1);
    ~bno055();

    // loadCalibration - Programs the bno055 sensor cal offsets
    //  cal            - Reference to calibration structure with the desired settings
    void loadCalibration(const CalibrationInfo & cal);

    // readCalibration - Obtains the current bno055 sensor cal offsets
    //  cal            - Reference to calibration structure, will be populated with current settings
    void readCalibration(CalibrationInfo & cal);

    // getCalibrationStatus - Reads the current system calibration state
    //  calStat             - Reference to status structure, will be populated with current cal status
    void getCalibrationStatus(CalibrationStatus & calStat);

    // readReg -    Read the contents of registers directly.
    //  reg_addr -  The address of the first register.
    //  buf -       The buffer to store the read data in.
    //  len -       The number of register to read.
    void readReg(uint8_t reg_addr, uint8_t *buf, unsigned len);

    // writeReg -   Write to the contents of a register directly.
    //  reg_addr -  The address of the register.
    //  data -      The data to write to the register.
    void writeReg(uint8_t reg_addr, uint8_t data);

    // changePage - Change the current memory page.
    //  page -      Desired memory page (0 or 1).
    void changePage(uint8_t page);

private:

    IO::i2cIntf & my_i2c;
    IO::GpioIntf & my_ps0;
    IO::GpioIntf & my_ps1;

    uint8_t my_curPage;
    Status my_status;

    int16_t my_accelerometerX;
    int16_t my_accelerometerY;
    int16_t my_accelerometerZ;

    int16_t my_quaternionPositionW;
    int16_t my_quaternionPositionX;
    int16_t my_quaternionPositionY;
    int16_t my_quaternionPositionZ;

    int16_t my_linearAccelerationX;
    int16_t my_linearAccelerationY;
    int16_t my_linearAccelerationZ;

    int16_t my_gravityVectorX;
    int16_t my_gravityVectorY;
    int16_t my_gravityVectorZ;

    constexpr static uint8_t I2C_ADDR = 0x28;
    constexpr static uint8_t I2C_ADDR_ALT = 0x29;

    constexpr static uint16_t EXPECTED_SW_REV = 0x0311;
    constexpr static uint8_t EXPECTED_BL_REV = 0x15;

    // OPR_MODE register values
    constexpr static uint8_t OPR_MODE_CONFIG = 0x0;
    constexpr static uint8_t OPR_MODE_NDOF = 0xC;

    // Constant memory addresses
    constexpr static uint8_t PAGE_ID_ADR = 0x07;        // The current page ID

    // Memory page 0 addresses
    constexpr static uint8_t SW_REV_ID_LSB_ADR = 0x04;  // software version LSB
    constexpr static uint8_t SW_REV_ID_MSB_ADR = 0x05;  // software version MSB
    constexpr static uint8_t BL_REV_ID_ADR = 0x06;      // Bootloader version

    constexpr static uint8_t ACC_DATA_X_LSB_ADR = 0x08; // Accelerometer X LSB
    constexpr static uint8_t ACC_DATA_X_MSB_ADR = 0x09; // Accelerometer X MSB
    constexpr static uint8_t ACC_DATA_Y_LSB_ADR = 0x0A; // Accelerometer Y LSB
    constexpr static uint8_t ACC_DATA_Y_MSB_ADR = 0x0B; // Accelerometer Y MSB
    constexpr static uint8_t ACC_DATA_Z_LSB_ADR = 0x0C; // Accelerometer Z LSB
    constexpr static uint8_t ACC_DATA_Z_MSB_ADR = 0x0D; // Accelerometer Z MSB

    constexpr static uint8_t QUA_DATA_W_LSB_ADR = 0x20; // Quaternion W LSB
    constexpr static uint8_t QUA_DATA_W_MSB_ADR = 0x21; // Quaternion W MSB
    constexpr static uint8_t QUA_DATA_X_LSB_ADR = 0x22; // Quaternion X LSB
    constexpr static uint8_t QUA_DATA_X_MSB_ADR = 0x23; // Quaternion X MSB
    constexpr static uint8_t QUA_DATA_Y_LSB_ADR = 0x24; // Quaternion Y LSB
    constexpr static uint8_t QUA_DATA_Y_MSB_ADR = 0x25; // Quaternion Y MSB
    constexpr static uint8_t QUA_DATA_Z_LSB_ADR = 0x26; // Quaternion Z LSB
    constexpr static uint8_t QUA_DATA_Z_MSB_ADR = 0x27; // Quaternion Z MSB

    constexpr static uint8_t LIA_DATA_X_LSB_ADR = 0x28; // Linear Accel X LSB
    constexpr static uint8_t LIA_DATA_X_MSB_ADR = 0x29; // Linear Accel X MSB
    constexpr static uint8_t LIA_DATA_Y_LSB_ADR = 0x2A; // Linear Accel Y LSB
    constexpr static uint8_t LIA_DATA_Y_MSB_ADR = 0x2B; // Linear Accel Y MSB
    constexpr static uint8_t LIA_DATA_Z_LSB_ADR = 0x2C; // Linear Accel Z LSB
    constexpr static uint8_t LIA_DATA_Z_MSB_ADR = 0x2D; // Linear Accel Z MSB

    constexpr static uint8_t GRV_DATA_X_LSB_ADR = 0x2E; // Grav Vector X LSB
    constexpr static uint8_t GRV_DATA_X_MSB_ADR = 0x2F; // Grav Vector X MSB
    constexpr static uint8_t GRV_DATA_Y_LSB_ADR = 0x30; // Grav Vector Y LSB
    constexpr static uint8_t GRV_DATA_Y_MSB_ADR = 0x31; // Grav Vector Y MSB
    constexpr static uint8_t GRV_DATA_Z_LSB_ADR = 0x32; // Grav Vector Z LSB
    constexpr static uint8_t GRV_DATA_Z_MSB_ADR = 0x33; // Grav Vector Z MSB

    constexpr static uint8_t CALIB_STAT_ADDR = 0x35;    // Calibration status
    constexpr static uint8_t ST_RESULT_ADR = 0x36;      // Self-test result
    constexpr static uint8_t SYS_STATUS_ADR = 0x39;     // System Status value
    constexpr static uint8_t SYS_ERROR_ADR = 0x3A;      // System Error value
    constexpr static uint8_t OPR_MODE_ADR = 0x3D;       // The operation mode

    /* Accelerometer Offset registers */
    constexpr static uint8_t ACCEL_OFFSET_X_LSB_ADDR = 0X55;
    constexpr static uint8_t ACCEL_OFFSET_X_MSB_ADDR = 0X56;
    constexpr static uint8_t ACCEL_OFFSET_Y_LSB_ADDR = 0X57;
    constexpr static uint8_t ACCEL_OFFSET_Y_MSB_ADDR = 0X58;
    constexpr static uint8_t ACCEL_OFFSET_Z_LSB_ADDR = 0X59;
    constexpr static uint8_t ACCEL_OFFSET_Z_MSB_ADDR = 0X5A;

    /* Magnetometer Offset registers */
    constexpr static uint8_t MAG_OFFSET_X_LSB_ADDR = 0X5B;
    constexpr static uint8_t MAG_OFFSET_X_MSB_ADDR = 0X5C;
    constexpr static uint8_t MAG_OFFSET_Y_LSB_ADDR = 0X5D;
    constexpr static uint8_t MAG_OFFSET_Y_MSB_ADDR = 0X5E;
    constexpr static uint8_t MAG_OFFSET_Z_LSB_ADDR = 0X5F;
    constexpr static uint8_t MAG_OFFSET_Z_MSB_ADDR = 0X60;

    /* Gyroscope Offset register s*/
    constexpr static uint8_t GYRO_OFFSET_X_LSB_ADDR = 0X61;
    constexpr static uint8_t GYRO_OFFSET_X_MSB_ADDR = 0X62;
    constexpr static uint8_t GYRO_OFFSET_Y_LSB_ADDR = 0X63;
    constexpr static uint8_t GYRO_OFFSET_Y_MSB_ADDR = 0X64;
    constexpr static uint8_t GYRO_OFFSET_Z_LSB_ADDR = 0X65;
    constexpr static uint8_t GYRO_OFFSET_Z_MSB_ADDR = 0X66;

    /* Radius registers */
    constexpr static uint8_t ACCEL_RADIUS_LSB_ADDR = 0X67;
    constexpr static uint8_t ACCEL_RADIUS_MSB_ADDR = 0X68;
    constexpr static uint8_t MAG_RADIUS_LSB_ADDR = 0X69;
    constexpr static uint8_t MAG_RADIUS_MSB_ADDR = 0X6A;

};

} // namespace DEV

#endif  // DEV_BNO055_H

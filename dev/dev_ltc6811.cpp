#include "dev_ltc6811.h"
#include "error_manager.h"
#include "target_base.h"
#include "evt_bit_utils.h"
#include "io_gpio_intf.h"
#include "io_spi_intf.h"


namespace DEV
{

Ltc6811::Ltc6811(IO::SpiIntf& spiModule, IO::GpioIntf& chipSelect, uint8_t address, uint8_t numCells): 
    my_spiModule(spiModule), my_chipSelect(chipSelect), my_address(address), my_numCells(numCells)
{

    this->initPecTable();
}

Ltc6811::~Ltc6811()
{

	// Empty destructor
}

/* 
 * readCellVoltages()
 *
 * Read voltage values from the primary registers with RDCV, format shown below
 *
 *  a3 -> a0: 4 bit slave address
 *
 *  | -------------------CMD[0]-------------------------- | -----------------CMD[1]---------------------- |
 *  |  15  |  14  |  13  |  12  |  11  |  10  |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
 *  -------------------------------------------------------------------------------------------------------
 *  |  1   |  a3  |  a2  |  a1  |  a0  |  0   |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  0  |  0  |
 */
void Ltc6811::readCellVoltages(uint16_t * voltages)
{
    uint16_t cmd = static_cast<uint16_t>(Ltc6811::Command::READ_CELLS_A); // Start at A
    uint16_t pec, readPec;
    uint8_t commandArray[COMMAND_LENGTH];            // Command and PEC codes
    uint8_t registerRaw[4*REGISTER_GROUP_LENGTH];     // Raw data from LTC registers
    uint8_t pecRegister[2];                         // Dummy placeholder for PEC bytes
    const char * errorMessage;
    target::ErrorLevel level;

    /* Build the command to read voltages */
    commandArray[0] = getHighByte(cmd);         // Bits 0 thru 2 for upper 3 command bits
    commandArray[0] |= (1 << 7) | (this->my_address << 3);  // Bit 7 = 1 for addressed mode
    commandArray[1] = getLowByte(cmd);          // Lower byte of read command

    /* Read all cells at once to avoid hogging the SPI bus when "twiddling your bits" */
    wakeSerial();

    for(uint8_t i = 0; i < 4; i++) // Iterate through groups A -> D
    {
        /* Set the PEC bytes */
        pec = this->pecCalc(2, commandArray);
        commandArray[2] = getHighByte(pec); // PEC[0]
        commandArray[3] = getLowByte(pec); // PEC[1]

        /* Read a single register group */
        this->my_spiModule.setChipSelectLow(this->my_chipSelect);
        this->my_spiModule.write(commandArray, COMMAND_LENGTH);
        this->my_spiModule.read(&registerRaw[(REGISTER_GROUP_LENGTH*i)], REGISTER_GROUP_LENGTH);
        this->my_spiModule.read(pecRegister, PEC_BYTES_LENGTH);
        this->my_spiModule.setChipSelectHigh(this->my_chipSelect);
        
        // Re-compute the PEC code from the read data
        readPec = (pecRegister[0] << 8) + (pecRegister[1]);
        if(readPec != pecCalc(REGISTER_GROUP_LENGTH, &registerRaw[(REGISTER_GROUP_LENGTH*i)]))
        {
            // If the PEC doesn't match, don't save the data, just log the error
            // and keep the last known good value
            errorMessage = "LTC6811 CELL VOLTAGE REGISTER PEC MISMATCH";
            level = target::ErrorLevel::FATAL;
            error_manager::logError(errorMessage, level);
        }
        else
        {
            // There are three 16-bit voltage values to be stored per register bank
            // divided into six individual 8-bit values per register. This loop takes care
            // of one register bank
            uint8_t * registerTemp = &registerRaw[(REGISTER_GROUP_LENGTH*i)];
            for(uint8_t j = 0; j < (CELLS_PER_GROUP); j++)
            {
                voltages[(CELLS_PER_GROUP*i)+j] = registerTemp[0] + (static_cast<uint16_t>(registerTemp[1]) << 8);
                registerTemp += 2;
            }
        }

        /* Move to next register group */
        commandArray[1] += 2;
    } // for(i)

    return;
}

/* 
 * readAuxVoltages()
 *
 * Read voltage values from the auxiliary registers with RDAUX, format shown below
 *
 *  a3 -> a0: 4 bit slave address
 *
 *  | -------------------CMD[0]-------------------------- | -----------------CMD[1]---------------------- |
 *  |  15  |  14  |  13  |  12  |  11  |  10  |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
 *  -------------------------------------------------------------------------------------------------------
 *  |  1   |  a3  |  a2  |  a1  |  a0  |  0   |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  0  |  0  |
 */
void Ltc6811::readAuxVoltages(uint16_t * voltages)
{
    uint16_t cmd = static_cast<uint16_t>(Command::READ_AUX_A);
    uint16_t pec, readPec;
    uint8_t rdaux[COMMAND_LENGTH];
    uint8_t registerRaw[2*REGISTER_GROUP_LENGTH];
    uint8_t pecRegister[2];
    const char * errorMessage;
    target::ErrorLevel level;


    wakeSerial();

    /* Rebuild command for current address starting at group A */
    rdaux[0] = getHighByte(cmd);           // Upper command byte (really just 3 bits)
    rdaux[0] |= (1 << 7) | ((this->my_address) << 3);// Set new address
    rdaux[1] = getLowByte(cmd);           // Lower byte of read command

    for(uint8_t i = 0; i < 2; i++)
    {
        pec = pecCalc(2, rdaux);
        rdaux[2] = getHighByte(pec);
        rdaux[3] = getLowByte(pec);

        this->my_spiModule.setChipSelectLow(this->my_chipSelect);
        this->my_spiModule.write(rdaux, 4);
        this->my_spiModule.read(&registerRaw[(REGISTER_GROUP_LENGTH*i)], REGISTER_GROUP_LENGTH);
        this->my_spiModule.read(pecRegister, PEC_BYTES_LENGTH);
        this->my_spiModule.setChipSelectHigh(this->my_chipSelect);

        // Re-compute the PEC code from the read data
        readPec = (pecRegister[0] << 8) + (pecRegister[1]);
        if(readPec != pecCalc(REGISTER_GROUP_LENGTH, &registerRaw[(REGISTER_GROUP_LENGTH*i)]))
        {
            // If the PEC doesn't match, don't save the data, just log the error
            // and keep the last known good value
            errorMessage = "LTC6811 AUX VOLTAGE REGISTER PEC MISMATCH";
            level = target::ErrorLevel::FATAL;
            error_manager::logError(errorMessage, level);
        }
        else
        {
            // There are three 16-bit voltage values to be stored per register bank
            // divided into six individual 8-bit values per register. This loop takes care
            // of one register bank
            uint8_t * registerTemp = &registerRaw[(REGISTER_GROUP_LENGTH*i)];
            for(uint8_t j = 0; j < (CELLS_PER_GROUP); j++)
            {
                voltages[(CELLS_PER_GROUP*i)+j] = registerTemp[0] + (static_cast<uint16_t>(registerTemp[1]) << 8);
                registerTemp += 2;
            }
        }

        /* Move to next register group */
        rdaux[1] += 2;

    } // for(i)

    return;
}

/* 
 * startADC()
 *
 * Start ADC conversions with the ADCV command, with the following format:
 *
 *  | -------------------CMD[0]-------------------------- | -----------------CMD[1]---------------------- |
 *  |  15  |  14  |  13  |  12  |  11  |  10  |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
 *  -------------------------------------------------------------------------------------------------------
 *  |  1   |  a3  |  a2  |  a1  |  a0  |  0   |  1  | md1 | md0 |  1  |  1  |  dp |  0  | ch2 | ch1 | ch0 |
 */
void Ltc6811::startADC(AdcMode md, DischargePermitted dp, CellSelect ch)
{

    uint16_t cmd = static_cast<uint16_t>(Ltc6811::Command::START_ADCONV);
    uint16_t pec;
    uint8_t adcv[COMMAND_LENGTH];

    cmd |= (static_cast<uint16_t>(md) << 7);
    cmd |= (static_cast<uint16_t>(dp) << 4);
    cmd |= static_cast<uint16_t>(ch);

    adcv[0] = getHighByte(cmd); // CMD[0]
    adcv[0] |= (1 << 7) | (this->my_address << 3);
    adcv[1] = getLowByte(cmd); // CMD[1]
    
    pec = this->pecCalc(2, adcv);

    adcv[2] = getHighByte(pec); // PEC[0]
    adcv[3] = getLowByte(pec); // PEC[1]

    wakeSerial();
    this->my_spiModule.setChipSelectLow(this->my_chipSelect);
    this->my_spiModule.write(adcv, COMMAND_LENGTH);
    this->my_spiModule.setChipSelectHigh(this->my_chipSelect);

    return;

}

/* 
 * pollADC()
 *
 * Poll a board's conversion status with PLADC, format shown below
 *
 *  a3 -> a0: 4 bit slave address
 *
 *  | -------------------CMD[0]-------------------------- | -----------------CMD[1]---------------------- |
 *  |  15  |  14  |  13  |  12  |  11  |  10  |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
 *  -------------------------------------------------------------------------------------------------------
 *  |  1   |  a3  |  a2  |  a1  |  a0  |  1   |  1  |  1  |  0  |  0  |  0  |  1  |  0  |  1  |  0  |  0  |
 */
Ltc6811::AdcStat Ltc6811::pollADC()
{

    uint16_t cmd = static_cast<uint16_t>(Ltc6811::Command::POLL_ADC);
    uint8_t pladc[4];
    uint16_t pec;
    uint8_t result = 0;
    AdcStat status;

    pladc[0] = getHighByte(cmd);
    pladc[0] |= (1 << 7) | (this->my_address << 3);
    pladc[1] = getLowByte(cmd);

    pec = this->pecCalc(2, pladc);

    pladc[2] = getHighByte(pec);
    pladc[3] = getLowByte(pec);

    wakeSerial();
    this->my_spiModule.setChipSelectLow(this->my_chipSelect);
    this->my_spiModule.write(pladc, COMMAND_LENGTH);
    this->my_spiModule.read(&result, 1);
    this->my_spiModule.setChipSelectHigh(this->my_chipSelect);

    // Reading all 0's from the bus means the LTC has pulled the MISO line low,
    // indicating that conversions are done
    status = (result == 0x00) ? (AdcStat::NOT_BUSY) : (AdcStat::BUSY);

    return status;
}

/* 
 * startADCAux()
 *
 * Start auxiliary ADC conversions with the ADAX command, with the following format:
 *
 *  | -------------------CMD[0]-------------------------- | -----------------CMD[1]---------------------- |
 *  |  15  |  14  |  13  |  12  |  11  |  10  |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
 *  -------------------------------------------------------------------------------------------------------
 *  |  1   |  a3  |  a2  |  a1  |  a0  |  1   |  0  | md1 | md0 |  1  |  1  |  0  |  0  | ch2 | ch1 | ch0 |
 */
void Ltc6811::startADCAux(Ltc6811::AdcMode md, Ltc6811::GpioSelect chg)
{
    uint16_t cmd = static_cast<uint16_t>(Ltc6811::Command::START_ADCONV_AUX);
    uint16_t pec;
    uint8_t adax[4];

    cmd |= (static_cast<uint16_t>(md) << 7);
    cmd |= static_cast<uint16_t>(chg);

    adax[0] = getHighByte(cmd);
    adax[0] |= (1 << 7) | (this->my_address << 3);
    adax[1] = getLowByte(cmd);

    pec = this->pecCalc(2, adax);

    adax[2] = getHighByte(pec);
    adax[3] = getLowByte(pec);

    wakeSerial();
    this->my_spiModule.setChipSelectLow(this->my_chipSelect);
    this->my_spiModule.write(adax, COMMAND_LENGTH);
    this->my_spiModule.setChipSelectHigh(this->my_chipSelect);

    return;
}

/* 
 * readConfig()
 *
 * Read the configuration register from a single slave board
 *
 *  a3 -> a0: Bits for slave address
 *
 *  | -------------------CMD[0]-------------------------- | -----------------CMD[1]---------------------- |
 *  |  15  |  14  |  13  |  12  |  11  |  10  |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
 *  -------------------------------------------------------------------------------------------------------
 *  |  1   |  a3  |  a2  |  a1  |  a0  |  0   |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  0  |
 */
void Ltc6811::readConfig(ConfigRegister * config)
{
    uint16_t command = static_cast<uint16_t>(Ltc6811::Command::READ_CONFIG_A);
    uint16_t pec, readPec;
    uint8_t conf[REGISTER_GROUP_LENGTH];
    uint8_t pecRegister[PEC_BYTES_LENGTH];
    uint8_t rdcfg[4];
    const char * errorMessage;
    target::ErrorLevel level;

    // Build the read command to send over SPI
    rdcfg[0] = getHighByte(command);
    rdcfg[0] |= (1 << 7) | (this->my_address << 3);
    rdcfg[1] = getLowByte(command);
    
    pec = this->pecCalc(2, rdcfg);

    rdcfg[2] = getHighByte(pec); // PEC[0]
    rdcfg[3] = getLowByte(pec); // PEC[1]


    // Read the config over SPI
    wakeSerial();
    this->my_spiModule.setChipSelectLow(this->my_chipSelect);
    this->my_spiModule.write(rdcfg, 4);              // Send the command
    this->my_spiModule.read(conf, REGISTER_GROUP_LENGTH);   // Perform read operation
    this->my_spiModule.read(pecRegister, PEC_BYTES_LENGTH);
    this->my_spiModule.setChipSelectHigh(this->my_chipSelect);

    // Re-calculate PEC on read data
    pec = pecCalc(REGISTER_GROUP_LENGTH, conf);
    readPec = (pecRegister[0] << 8) + pecRegister[1];

    if(pec != readPec)
    {
        // If there was a PEC mismatch, don't update the local copies and log an error
        errorMessage = "LTC6811 CONFIG REGISTER PEC MISMATCH";
        level = target::ErrorLevel::FATAL;
        error_manager::logError(errorMessage, level);
    }
    else
    {
        // Update the configuraiton stored locally...sorry this isn't very readable
        // See datasheet Table 40 for the description of this register
        this->my_configuration.GPIO   = (conf[0] & 0xF8) >> 3;
        this->my_configuration.REFON  = (conf[0] & 0x04) >> 2;
        this->my_configuration.DTEN   = (conf[0] & 0x02) >> 1;
        this->my_configuration.ADCOPT = (conf[0] & 0x01);
        this->my_configuration.VUV    = (conf[1]) + ((static_cast<uint16_t>(conf[2]) & 0x0F) << 8);
        this->my_configuration.VOV    = ((conf[2] & 0xF0) >> 4) + (static_cast<uint16_t>(conf[3]) << 4); 
        this->my_configuration.DCC    = (conf[4]) + ((static_cast<uint16_t>(conf[5]) & 0x0F) << 8);
        this->my_configuration.DCTO   = ((conf[5] & 0xF0) > 4);

        /* Passing info to app layer */
        config->GPIO   = this->my_configuration.GPIO;
        config->REFON  = this->my_configuration.REFON;
        config->DTEN   = this->my_configuration.DTEN;
        config->ADCOPT = this->my_configuration.ADCOPT;
        config->VUV    = this->my_configuration.VUV;
        config->VOV    = this->my_configuration.VOV;
        config->DCC    = this->my_configuration.DCC;
        config->DCTO   = this->my_configuration.DCTO;   
    }

    return;
    
}

/* 
 * writeConfig()
 *
 * Write the configuration register to a single slave board or broadcast write a global config
 *
 *  a3 -> a0: Bits for slave address (optional)
 *
 *  | -------------------CMD[0]-------------------------- | -----------------CMD[1]---------------------- |
 *  |  15  |  14  |  13  |  12  |  11  |  10  |  9  |  8  |  7  |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
 *  -------------------------------------------------------------------------------------------------------
 *  |  1   |  a3  |  a2  |  a1  |  a0  |  0   |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |
 */
void Ltc6811::writeConfig(Ltc6811::ConfigRegister * config)
{
    uint16_t command = static_cast<uint16_t>(Ltc6811::Command::WRITE_CONFIG_A);
    uint16_t pec;
    uint8_t conf[REGISTER_GROUP_LENGTH + 2];
    uint8_t wrcfg[4];

    wrcfg[0] = getHighByte(command);
    wrcfg[0] |= (1 << 7) | (this->my_address << 3);
    wrcfg[1] = getLowByte(command);

    pec = this->pecCalc(2, wrcfg);

    wrcfg[2] = getHighByte(pec); // PEC[0]
    wrcfg[3] = getLowByte(pec); // PEC[1]

    conf[0] = (config->GPIO  << 3) +
              (config->REFON << 2) +
              (config->DTEN  << 1) +
              (config->ADCOPT);

    conf[1] = static_cast<uint8_t>(config->VUV & 0x00FF);
    
    conf[2] = static_cast<uint8_t>((config->VUV & 0x0F00) >> 8)+
              static_cast<uint8_t>((config->VOV & 0x000F) << 4);

    conf[3] = static_cast<uint8_t>((config->VOV & 0x0FF0) >> 4);
    
    conf[4] = static_cast<uint8_t>(config->DCC & 0x00FF);
    
    conf[5] = static_cast<uint8_t>((config->DCC & 0x0F00) >> 8) +
               static_cast<uint8_t>(config->DCTO << 4);

    pec = this->pecCalc(6, conf);

    conf[6] = getHighByte(pec); // PEC[0]
    conf[7] = getLowByte(pec); // PEC[1]

    // Write over SPI
    wakeSerial();
    this->my_spiModule.setChipSelectLow(this->my_chipSelect);
    this->my_spiModule.write(wrcfg, 4);                       // Send command
    this->my_spiModule.write(conf, REGISTER_GROUP_LENGTH+2);  // Perform write operation
    this->my_spiModule.setChipSelectHigh(this->my_chipSelect);

    readConfig(config); // Read it back to make sure operation was successful (ERRORS LOGGED HERE)
                        // If successful the copy of the config register will be updated locally
    return;

}


void Ltc6811::toggleGPIO(uint8_t gpio)
{

    this->my_configuration.GPIO = gpio | 0x01; // GPIO1 needs to always be high to let it function as an ADC input
                                               // as per the current BMS slave implementation of the GPIOs
    writeConfig(&this->my_configuration);

    return;
}


void Ltc6811::wakeSerial()
{
    uint8_t dummyByte = 0;
    this->my_spiModule.setChipSelectLow(this->my_chipSelect);
    this->my_spiModule.write(&dummyByte, 1);
    this->my_spiModule.setChipSelectHigh(this->my_chipSelect);

    return;
}

uint8_t Ltc6811::getNumCells()
{
    return this->my_numCells;
}

/* Function provided by LTC6811 datasheet */
void Ltc6811::initPecTable()
{
    uint16_t remainder = 0;
    for (int i = 0; i < 256; i++)
    {
        remainder = i << 7;
        for (int bit = 8; bit > 0; --bit)
        {
            if (remainder & 0x4000)
            {
                remainder = ((remainder << 1));
                remainder = (remainder ^ CRC15_POLY);
            }
            else
            {
                remainder = ((remainder << 1));
            }
        }
        pec15Table[i] = remainder&0xFFFF;
    }
}

void Ltc6811::setAddress(uint8_t address)
{
    this->my_address = address;
}


/* Function provided by LTC6811 datasheet */
uint16_t Ltc6811::pecCalc(uint8_t len, uint8_t *data)
{
    uint16_t remainder,address;
    
    remainder = 16;//initialize the PEC
    for(uint8_t i = 0; i < len; i++) // loops for each byte in data array
    {
        address = ((remainder >> 7) ^ data[i]) & 0xff;//calculate PEC table address 
        remainder = (remainder << 8) ^ pec15Table[address];
    }
    return(remainder*2);//The CRC15 has a 0 in the LSB so the remainder must be multiplied by 2
}

} // namespace

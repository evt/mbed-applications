/*
 * dev_ltc2440.h
 * header file for the ltc2440
 */

#ifndef DEV_LTC2440_H
#define DEV_LTC2440_H

#include <cstdint>

/*
 * Forward Declarations
 */
namespace IO
{
class SpiIntf;
class GpioIntf;
} // namespace IO

namespace DEV
{

class Ltc2440
{
public:

    enum class Status
    {
        VIN_GREATER_THAN_HALF_VREF = 0x30,       //0011
        VIN_LESS_THAN_HALF_VREF = 0x20,          //0010
        NEG_VIN_GREATER_THAN_HALF_VREF = 0x10,   //0001
        NEG_VIN_LESS_THAN_HALF_VREF = 0x00,      //0000
        UNKNOWN = 0xFF,  //Should never get here
    };

    struct AdcCode
    {
        uint8_t status;
        int32_t value;
    };

    Status read(AdcCode *adcCode, uint8_t *status = nullptr); //reads adc value and returns the status
    float adcCodeToVoltage(int32_t adcCode, float vRef);    //*Not use 
    int8_t eocTimeout(uint16_t misoTimeout);

    Ltc2440(IO::SpiIntf &spiModule, IO::GpioIntf &ChipSelect);
    ~Ltc2440();

private:
    IO::SpiIntf &mySpiModule;
    IO::GpioIntf &myChipSelect;

};
}
#endif
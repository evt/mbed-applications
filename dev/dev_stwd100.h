/*
 * file: dev_stwd100.h
 * purpose: header for the TMS watchdog
 */

#ifndef DEV_STWD100_H
#define DEV_STWD100_H

namespace IO
{
class GpioIntf;
} // namespace IO

namespace DEV
{

class Stwd100
{
public:

    Stwd100(IO::GpioIntf & gpio_pin);  // Constructor
    ~Stwd100(); // Destructor

    void pet(); // Keeps WD from timing out by toggling GPIO pin

private:

    IO::GpioIntf & my_gpio_pin;    // Should be the WDI
};
} // namespace DEV

#endif  // DEV_STWD100_H

 /*
 * file: sn74cb3q3253.cpp
 * purpose: multiplexer cpp for the sn74cb3q3253 that will select isoSpi for the sgm master
 */

#include "dev_sn74cb3q3253.h"
#include "io_gpio_intf.h"
#include "error_manager.h"

namespace DEV
{

/*
 * Intiatializes the selects as outputs and sets them them low (PORT0)
 */
Sn74cb3q3253::Sn74cb3q3253(IO::GpioIntf & muxSel1, IO::GpioIntf & muxSel0) : 
    my_muxSel1(muxSel1), my_muxSel0(muxSel0)
{
    this->my_muxSel1.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    this->my_muxSel0.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    this->my_muxSel1.writePin(IO::GpioIntf::STATE::LOW);
    this->my_muxSel0.writePin(IO::GpioIntf::STATE::LOW);
    this->my_current_state = SPI_PORT::PORT0;
}


Sn74cb3q3253::~Sn74cb3q3253()
{
    // Empty destructor
}


/*
 * checks which port was passed in and sets the selects appropriately
 */
void Sn74cb3q3253::selectSPI(SPI_PORT spi)
{
    switch (spi)
    {
        case SPI_PORT::PORT0 :
            this->my_muxSel1.writePin(IO::GpioIntf::STATE::LOW);
            this->my_muxSel0.writePin(IO::GpioIntf::STATE::LOW);
            this->my_current_state = SPI_PORT::PORT0;
            break;

        case SPI_PORT::PORT1 :
            this->my_muxSel1.writePin(IO::GpioIntf::STATE::LOW);
            this->my_muxSel0.writePin(IO::GpioIntf::STATE::HIGH);
            this->my_current_state = SPI_PORT::PORT1;
            break;

        case SPI_PORT::PORT2 :
            this->my_muxSel1.writePin(IO::GpioIntf::STATE::HIGH);
            this->my_muxSel0.writePin(IO::GpioIntf::STATE::LOW);
            this->my_current_state = SPI_PORT::PORT2;
            break;

        case SPI_PORT::PORT3 :
            this->my_muxSel1.writePin(IO::GpioIntf::STATE::HIGH);
            this->my_muxSel0.writePin(IO::GpioIntf::STATE::HIGH);
            this->my_current_state = SPI_PORT::PORT3;
            break;
    }

}


Sn74cb3q3253::SPI_PORT Sn74cb3q3253::getCurrentState()
{

    return this->my_current_state;
}

} // namespace DEV

/*
 * file: dev_bno055.cpp
 * purpose: Driver for the BNO055 IMU over I2C.
 */

#include "dev_bno055.h"
#include "error_manager.h"
#include "io_gpio_intf.h"
#include "io_i2c_intf.h"
#include "evt_bit_utils.h"

namespace DEV
{

bno055::bno055(IO::i2cIntf & i2c, IO::GpioIntf & ps0, IO::GpioIntf & ps1) : 
    my_i2c(i2c), 
    my_ps0(ps0),
    my_ps1(ps1),
    my_curPage(0),
    my_status(Status::IDLE),
    my_accelerometerX(0),
    my_accelerometerY(0),
    my_accelerometerZ(0),
    my_quaternionPositionW(0),
    my_quaternionPositionX(0),
    my_quaternionPositionY(0),
    my_quaternionPositionZ(0),
    my_linearAccelerationX(0),
    my_linearAccelerationY(0),
    my_linearAccelerationZ(0),
    my_gravityVectorX(0),
    my_gravityVectorY(0),
    my_gravityVectorZ(0)
{
    // used to store read from bno055
    uint8_t rx_buf;

    my_ps0.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    my_ps1.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);

    // Set the bno055 to use the I2C interface
    my_ps0.writePin(IO::GpioIntf::STATE::LOW);
    my_ps1.writePin(IO::GpioIntf::STATE::LOW);

    // Get the current page address
    readReg(bno055::PAGE_ID_ADR, &my_curPage, 1);

    // Make sure the page is zero so that we can change the operating mode
    if (my_curPage != 0)
    {
        changePage(0);
    }

    // Wait for the bno055 to initialize
    do
    {
        readReg(bno055::SYS_STATUS_ADR, &rx_buf, 1);
        my_status = static_cast<bno055::Status>(rx_buf);

        if (my_status == Status::ERROR)
        {
            error_manager::logError(
                    "BNO055 Encountered an error on init. Might just mean the BNO was already running",
                    target::ErrorLevel::WARNING);
            
            // getStatus will log the actual error based on the error register
            getStatus();
            break;
        }

        if (my_status == Status::FUSION_RUN || my_status == Status::SYSTEM_RUN)
        {
            // This probably shouldn't happen unless the code gets reset
            // without the bno055 getting reset.
            error_manager::logError(
                    "BNO055 is already running.",
                    target::ErrorLevel::INFO);
            break;
        }
    } while (my_status != Status::IDLE);

    // Set the operation mode of the bno.
    writeReg(bno055::OPR_MODE_ADR, OPR_MODE_NDOF);

    error_manager::logError(
            "BNO055 initialization complete.",
            target::ErrorLevel::DEBUG);
}

bno055::~bno055()
{
    // Empty destructor
}

int16_t bno055::getAccelerometerX()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(ACC_DATA_X_LSB_ADR, rx_data, 2);
    my_accelerometerX = (rx_data[1] << 8) + rx_data[0];
    return my_accelerometerX;
}
int16_t bno055::getAccelerometerY()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(ACC_DATA_Y_LSB_ADR, rx_data, 2);
    my_accelerometerY = (rx_data[1] << 8) + rx_data[0];
    return my_accelerometerY;
}
int16_t bno055::getAccelerometerZ()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(ACC_DATA_Z_LSB_ADR, rx_data, 2);
    my_accelerometerZ = (rx_data[1] << 8) + rx_data[0];
    return my_accelerometerZ;
}

int16_t bno055::getQuaternionPositionW()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(QUA_DATA_W_LSB_ADR, rx_data, 2);
    my_quaternionPositionW = (rx_data[1] << 8) + rx_data[0];
    return my_quaternionPositionW;
}
int16_t bno055::getQuaternionPositionX()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(QUA_DATA_X_LSB_ADR, rx_data, 2);
    my_quaternionPositionX = (rx_data[1] << 8) + rx_data[0];
    return my_quaternionPositionX;
}
int16_t bno055::getQuaternionPositionY()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(QUA_DATA_Y_LSB_ADR, rx_data, 2);
    my_quaternionPositionY = (rx_data[1] << 8) + rx_data[0];
    return my_quaternionPositionY;
}
int16_t bno055::getQuaternionPositionZ()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(QUA_DATA_Z_LSB_ADR, rx_data, 2);
    my_quaternionPositionZ = (rx_data[1] << 8) + rx_data[0];
    return my_quaternionPositionZ;
}

int16_t bno055::getLinearAccelerationX()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(LIA_DATA_X_LSB_ADR, rx_data, 2);
    my_linearAccelerationX = (rx_data[1] << 8) + rx_data[0];
    return my_linearAccelerationX;
}
int16_t bno055::getLinearAccelerationY()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(LIA_DATA_Y_LSB_ADR, rx_data, 2);
    my_linearAccelerationY = (rx_data[1] << 8) + rx_data[0];
    return my_linearAccelerationY;
}
int16_t bno055::getLinearAccelerationZ()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(LIA_DATA_Z_LSB_ADR, rx_data, 2);
    my_linearAccelerationZ = (rx_data[1] << 8) + rx_data[0];
    return my_linearAccelerationZ;
}

int16_t bno055::getGravityVectorX()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(GRV_DATA_X_LSB_ADR, rx_data, 2);
    my_gravityVectorX = (rx_data[1] << 8) + rx_data[0];
    return my_gravityVectorX;
}
int16_t bno055::getGravityVectorY()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(GRV_DATA_Y_LSB_ADR, rx_data, 2);
    my_gravityVectorY = (rx_data[1] << 8) + rx_data[0];
    return my_gravityVectorY;
}
int16_t bno055::getGravityVectorZ()
{
    uint8_t rx_data[2];
    if (my_curPage != 0)
    {
        changePage(0);
    }
    readReg(GRV_DATA_Z_LSB_ADR, rx_data, 2);
    my_gravityVectorZ = (rx_data[1] << 8) + rx_data[0];
    return my_gravityVectorZ;
}

void bno055::loadCalibration(const CalibrationInfo & cal)
{
    // Change to config mode to modify the calibration
    writeReg(bno055::OPR_MODE_ADR, OPR_MODE_CONFIG);

    // Accelerometer
    writeReg(ACCEL_OFFSET_X_LSB_ADDR, getLowByte(cal.accel_x));
    writeReg(ACCEL_OFFSET_X_MSB_ADDR, getHighByte(cal.accel_x));

    writeReg(ACCEL_OFFSET_Y_LSB_ADDR, getLowByte(cal.accel_y));
    writeReg(ACCEL_OFFSET_Y_MSB_ADDR, getHighByte(cal.accel_y));

    writeReg(ACCEL_OFFSET_Z_LSB_ADDR, getLowByte(cal.accel_z));
    writeReg(ACCEL_OFFSET_Z_MSB_ADDR, getHighByte(cal.accel_z));

    // Magnetometer
    writeReg(MAG_OFFSET_X_LSB_ADDR, getLowByte(cal.mag_x));
    writeReg(MAG_OFFSET_X_MSB_ADDR, getHighByte(cal.mag_x));

    writeReg(MAG_OFFSET_Y_LSB_ADDR, getLowByte(cal.mag_y));
    writeReg(MAG_OFFSET_Y_MSB_ADDR, getHighByte(cal.mag_y));

    writeReg(MAG_OFFSET_Z_LSB_ADDR, getLowByte(cal.mag_z));
    writeReg(MAG_OFFSET_Z_MSB_ADDR, getHighByte(cal.mag_z));

    // Gyroscope
    writeReg(GYRO_OFFSET_X_LSB_ADDR, getLowByte(cal.gyro_x));
    writeReg(GYRO_OFFSET_X_MSB_ADDR, getHighByte(cal.gyro_x));

    writeReg(GYRO_OFFSET_Y_LSB_ADDR, getLowByte(cal.gyro_y));
    writeReg(GYRO_OFFSET_Y_MSB_ADDR, getHighByte(cal.gyro_y));

    writeReg(GYRO_OFFSET_Z_LSB_ADDR, getLowByte(cal.gyro_z));
    writeReg(GYRO_OFFSET_Z_MSB_ADDR, getHighByte(cal.gyro_z));

    // Radius
    writeReg(ACCEL_RADIUS_LSB_ADDR, getLowByte(cal.accel_radius));
    writeReg(ACCEL_RADIUS_MSB_ADDR, getHighByte(cal.accel_radius));

    writeReg(MAG_RADIUS_LSB_ADDR, getLowByte(cal.mag_radius));
    writeReg(MAG_RADIUS_MSB_ADDR, getHighByte(cal.mag_radius));

    // Back to NDOF mode
    writeReg(bno055::OPR_MODE_ADR, OPR_MODE_NDOF);
}

void bno055::readCalibration(CalibrationInfo & cal)
{
    uint8_t tmp = 0;

    // Change to config mode
    writeReg(bno055::OPR_MODE_ADR, OPR_MODE_CONFIG);

    // Accelerometer
    readReg(ACCEL_OFFSET_X_LSB_ADDR, &tmp, 1);
    cal.accel_x = static_cast<uint16_t>(tmp);
    readReg(ACCEL_OFFSET_X_MSB_ADDR, &tmp, 1);
    cal.accel_x |= static_cast<uint16_t>(tmp) << 8;

    readReg(ACCEL_OFFSET_Y_LSB_ADDR, &tmp, 1);
    cal.accel_y = static_cast<uint16_t>(tmp);
    readReg(ACCEL_OFFSET_Y_MSB_ADDR, &tmp, 1);
    cal.accel_y |= static_cast<uint16_t>(tmp) << 8;

    readReg(ACCEL_OFFSET_Z_LSB_ADDR, &tmp, 1);
    cal.accel_z = static_cast<uint16_t>(tmp);
    readReg(ACCEL_OFFSET_Z_MSB_ADDR, &tmp, 1);
    cal.accel_z |= static_cast<uint16_t>(tmp) << 8;

    // Magnetometer
    readReg(MAG_OFFSET_X_LSB_ADDR, &tmp, 1);
    cal.mag_x = static_cast<uint16_t>(tmp);
    readReg(MAG_OFFSET_X_MSB_ADDR, &tmp, 1);
    cal.mag_x |= static_cast<uint16_t>(tmp) << 8;

    readReg(MAG_OFFSET_Y_LSB_ADDR, &tmp, 1);
    cal.mag_y = static_cast<uint16_t>(tmp);
    readReg(MAG_OFFSET_Y_MSB_ADDR, &tmp, 1);
    cal.mag_y |= static_cast<uint16_t>(tmp) << 8;

    readReg(MAG_OFFSET_Z_LSB_ADDR, &tmp, 1);
    cal.mag_z = static_cast<uint16_t>(tmp);
    readReg(MAG_OFFSET_Z_MSB_ADDR, &tmp, 1);
    cal.mag_z |= static_cast<uint16_t>(tmp) << 8;

    // Gyroscope
    readReg(GYRO_OFFSET_X_LSB_ADDR, &tmp, 1);
    cal.gyro_x = static_cast<uint16_t>(tmp);
    readReg(GYRO_OFFSET_X_MSB_ADDR, &tmp, 1);
    cal.gyro_x |= static_cast<uint16_t>(tmp) << 8;

    readReg(GYRO_OFFSET_Y_LSB_ADDR, &tmp, 1);
    cal.gyro_y = static_cast<uint16_t>(tmp);
    readReg(GYRO_OFFSET_Y_MSB_ADDR, &tmp, 1);
    cal.gyro_y |= static_cast<uint16_t>(tmp) << 8;

    readReg(GYRO_OFFSET_Z_LSB_ADDR, &tmp, 1);
    cal.gyro_z = static_cast<uint16_t>(tmp);
    readReg(GYRO_OFFSET_Z_MSB_ADDR, &tmp, 1);
    cal.gyro_z |= static_cast<uint16_t>(tmp) << 8;

    // Radius
    readReg(ACCEL_RADIUS_LSB_ADDR, &tmp, 1);
    cal.accel_radius = static_cast<uint16_t>(tmp);
    readReg(ACCEL_RADIUS_MSB_ADDR, &tmp, 1);
    cal.accel_radius |= static_cast<uint16_t>(tmp) << 8;

    readReg(MAG_RADIUS_LSB_ADDR, &tmp, 1);
    cal.mag_radius = static_cast<uint16_t>(tmp);
    readReg(MAG_RADIUS_MSB_ADDR, &tmp, 1);
    cal.mag_radius |= static_cast<uint16_t>(tmp) << 8;

    writeReg(bno055::OPR_MODE_ADR, OPR_MODE_NDOF);
}

void bno055::getCalibrationStatus(CalibrationStatus & calStat)
{
    uint8_t status = 0;
    readReg(CALIB_STAT_ADDR, &status, 1);

    calStat.sysCal = (status >> 6) & 0x03;
    calStat.gyroCal = (status >> 4) & 0x03;
    calStat.accelCal = (status >> 2) & 0x03;
    calStat.magCal = (status) & 0x03;

}


bno055::Status bno055::getStatus()
{
    // Make sure the memory page is 0
    if (my_curPage != 0)
    {
        changePage(0);
    }

    // Read the self-test register and return an inverted result
    uint8_t rx_buf;
    readReg(bno055::SYS_STATUS_ADR, &rx_buf, 1);

    my_status = static_cast<bno055::Status>(rx_buf);
    
    if (my_status == Status::ERROR)
    {
        const char * msg;
        target::ErrorLevel level;

        readReg(bno055::SYS_ERROR_ADR, &rx_buf, 1);
        switch (static_cast<Error>(rx_buf))
        {
            case Error::NO_ERROR:
                msg = "BNO055 reported an error, but the error register is clear.";
                level = target::ErrorLevel::WARNING;
                break;
            case Error::PERIPH_INIT_ERROR:
                msg = "BNO055 peripheral initialization error.";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::SYSTEM_INIT_ERROR:
                msg = "BNO055 system initialization error.";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::SELF_TEST_FAILED:
                msg = "BNO055 self-test failed.";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::VALUE_OUT_OF_RANGE:
                msg = "Attempted to write to a BNO055 register with an invalid value";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::ADDR_OUT_OF_RANGE:
                msg = "Attempted to write to an invalid BNO055 register address";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::REG_WRITE_ERROR:
                msg = "Error writing to the BNO055";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::LOW_PWR_UNAVAILABLE:
                msg = "BNO055 low power mode unavailable for current operation mode.";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::ACC_PWR_UNAVAILABLE:
                msg = "BNO055 accelerometer power mode not available.";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::ALGO_CONFIG_ERROR:
                msg = "BNO055 fusion algorithm configuration error.";
                level = target::ErrorLevel::ERROR;
                break;
            case Error::SENSOR_CONFIG_ERROR:
                msg = "BNO055 sensor configuration error.";
                level = target::ErrorLevel::ERROR;
                break;
            default:
                msg = "BNO055 unrecognized error.";
                level = target::ErrorLevel::ERROR;
                break;
        }

        error_manager::logError(msg, level);
    }

    return my_status;
}

void bno055::readReg(uint8_t reg_addr, uint8_t *buf, unsigned len)
{
    my_i2c.write(bno055::I2C_ADDR_ALT, (char *) &reg_addr, 1);
    my_i2c.read(bno055::I2C_ADDR_ALT, (char *) buf, len);
}

void bno055::writeReg(uint8_t reg_addr, uint8_t data)
{
    uint8_t tx_data[2];
    tx_data[0] = reg_addr;
    tx_data[1] = data;
    my_i2c.write(bno055::I2C_ADDR_ALT, (char *) tx_data, 2);
}

void bno055::changePage(uint8_t page)
{
    writeReg(bno055::PAGE_ID_ADR, page);
    readReg(bno055::PAGE_ID_ADR, &my_curPage, 1);
    
    if (my_curPage != page)
    {
        error_manager::logError(
            "BNO055 failed to change memory pages.",
            target::ErrorLevel::ERROR);
    }
}

} // namespace DEV


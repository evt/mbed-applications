/*
 * dev_ltc2440.cpp
 * cpp file for the ltc2440 driver
 */

#include <cstdint>
#include "dev_ltc2440.h"
#include "error_manager.h"
#include "evt_bit_utils.h"
#include "io_spi_intf.h"
#include "io_gpio_intf.h"

namespace DEV
{

Ltc2440::Ltc2440(IO::SpiIntf &spiModule, IO::GpioIntf &chipSelect) : mySpiModule(spiModule), myChipSelect(chipSelect)
{
    //empty constructor
}

Ltc2440::~Ltc2440()
{
    // Empty destructor
}

// Reads from LTC2440 ADC that accepts an 8 bit configuration and returns a 24 bit output word.
Ltc2440::Status Ltc2440::read(AdcCode *adcCode, uint8_t *status)
{
    
    uint8_t adc8[4];
    this->mySpiModule.setChipSelectLow((this->myChipSelect));    //Pull CS low

    this->mySpiModule.read(adc8, 4);
    this->mySpiModule.setChipSelectHigh((this->myChipSelect));    //Pull CS high

    adcCode->value = adc8[0]<<24;
    adcCode->value |= adc8[1]<<16;
    adcCode->value |= adc8[2]<<8;
    adcCode->value |= adc8[3];

    adcCode->status=adc8[0]&0xF0;    //masks off everything but the status

    /*
     * The error logs are hidden for now as the log error slows run time due to wait statements
     */
    switch (static_cast<Status>(adcCode->status))
    {
        case Status::VIN_GREATER_THAN_HALF_VREF :
           
            error_manager::logError("Vin is greater than half of Vref", target::ErrorLevel::DEBUG);
            
            return Status::VIN_GREATER_THAN_HALF_VREF;

        case Status::VIN_LESS_THAN_HALF_VREF :
           
            error_manager::logError("Vin is less than half of Vref", target::ErrorLevel::DEBUG);
            
            return Status::VIN_LESS_THAN_HALF_VREF;

        case Status::NEG_VIN_GREATER_THAN_HALF_VREF :
           
            error_manager::logError("Vin and Vref are negative. Vin is greater than half of Vref", target::ErrorLevel::DEBUG);
            
            return Status::NEG_VIN_GREATER_THAN_HALF_VREF;

        case Status::NEG_VIN_LESS_THAN_HALF_VREF :
           
            error_manager::logError("Vin and Vref are negative. Vin is less than half of Vref", target::ErrorLevel::DEBUG);
            
            return Status::NEG_VIN_LESS_THAN_HALF_VREF;

        default :
            error_manager::logError("Unknown status", target::ErrorLevel::ERROR);
    }

    if (status!=nullptr)
    {
        *status=adcCode->status;
    }
    return Status::UNKNOWN;
}
/*
 * Currently not in use as it converts the adc code incorrectly
 */
#if 0
// Calculates the voltage corresponding to an adc code, given the reference voltage (in volts)
float Ltc2440::adcCodeToVoltage(int32_t adcCode, float vRef)
{
    float voltage;

    #ifndef CHECK_ZERO_CODE
    if (adcCode == 0x00000000)
    {
        adcCode = 0x20000000;
    }
    #endif

    adcCode -= 0x20000000;    //Converts offset binary to binary

    voltage=(float)adcCode;
    voltage = voltage / 0x20000000;    //This calculates the input as a fraction of the reference voltage (dimensionless)
    voltage = voltage * vRef;    //Multiply fraction by Vref to get the actual voltage at the input (in volts)
    return(voltage);
}
#endif
}   //namespace DEV
#ifndef DEV_LTC6811_H
#define DEV_LTC6811_H

#include <cstdint>

/*
 * Forward Declarations
 */
namespace IO
{
class SpiIntf;
class GpioIntf;
} // namespace IO


namespace DEV
{

class Ltc6811
{
public:

    enum class AdcMode
    {
        MODE_OTHER    = 0u, // Need another name for this
        MODE_FAST     = 1u,
        MODE_NORMAL   = 2u,
        MODE_FILTERED = 3u,
    };

    enum class DischargePermitted
    {
        NO  = 0u,
        YES = 1u,
    };

    enum class CellSelect
    {
        ALL_CELLS  = 0u,
        CELLS_1_7  = 1u,
        CELLS_2_8  = 2u,
        CELLS_3_9  = 3u,
        CELLS_4_10 = 4u,
        CELLS_5_11 = 5u,
        CELLS_6_12 = 6u,
    };

    enum class GpioSelect
    {
        ALL_GPIO = 0u,
        GPIO_1   = 1u,
        GPIO_2   = 2u,
        GPIO_3   = 3u,
        GPIO_4   = 4u,
        GPIO_5   = 5u,
        SECOND_REF  = 6u,
    };

    enum class AdcStat
    {
        BUSY,
        NOT_BUSY,
    };


    /* 
     * configRegister bitfield descriptions:
     *
     * GPIO (5 bits)  - GPIO pin control (LSB = GPIO1, MSB = GPIO5)
     *
     * REFON (1 bit)  - 0: Reference shuts down after A/D conversions (default)
     *                  1: Reference remains on until watchdog timeout (A/D conversions
     *                       are quicker, no longer need to account for power-up time t_refup).
     *
     * ADCOPT (1 bit) - Defines conversion speeds used with AdcMode
     *                  0: 27kHz, 7kHz, 422Hz, or 26Hz (Fast, normal, other, filtered)
     *                  1: 14kHz, 3kHz, 1kHz, or 2kHz
     * 
     * VUV (12 bits)  - Under-voltage threshold - Comparison voltage = (VUV + 1)*16*100uV
     *
     * VOV (12 bits)  - Over-voltage threshold  - Comparison voltage = (VOV)*16*100uV
     *
     * DCC (12 bits)  - Discharge cell x (LSB = cell_1, MSB = cell_12)
     *
     * DCTO (4 bits)  - Discharge timeout - *NOT USED* - Keep at 0x0.
     *                  Can tell cells to discharge for certain amount of time between 30sec. 
     *                  and 120min. according to parameters set in S_control and PWM_control registers.
     *                  See datasheet for more details.
     */
    struct ConfigRegister
    {
        uint8_t  GPIO  :5;
        uint8_t  REFON :1;
        uint8_t  DTEN  :1;
        uint8_t  ADCOPT:1;
        uint16_t VUV   :12;
        uint16_t VOV   :12;
        uint16_t DCC   :12;
        uint16_t DCTO  :4;

    };


    /*
     * readCellVoltages()
     *
     * Read cell voltage values from CV register groups A -> D
     *
     * param - voltages* - Pointer to array of cell voltages, 12 bytes long
     */
    void readCellVoltages(uint16_t * voltages);


    /*
     * readAuxVoltages()
     *
     * Read voltages on GPIO lines (aux registers) groups A & B
     * 
     * param - voltages* - Pointer to array of auxiliary voltages, 5 bytes long
     */
    void readAuxVoltages(uint16_t * voltages);

    /*
     * readConfig()
     *
     * Read the configuration from the LTC config registers
     *
     * param - config* - Pointer to config to store the read data
     *
     */
    void readConfig(ConfigRegister * config);

    /*
     * writeConfig()
     *
     * Update the configuration register of a slave device.
     * Changes are reflected in private variable "myConfiguration"
     *
     * param - config* - Pointer to config data to be sent
     *
     */ 
    void writeConfig(Ltc6811::ConfigRegister * config);

    /*
     *startADC()
     *
     * Starts performing A/D conversions on the selected cells.
     *
     * param - md - ADC mode used for conversion speed - 
     *              *NORMAL mode recommended for now, 7kHz (See ADCTOPT in config register)
     *       - dp - Tells whether cell discharge is permitted while performing conversions
     *       - ch - Channels to measure, for now we're using ALL_CELLS
     */
    void startADC(AdcMode md, DischargePermitted dp, CellSelect ch);


    /*
     * pollADC()
     * 
     * Poll the A/D conversion status of a given device
     *
     */
    AdcStat pollADC();


    /*
     * startADCAux()
     *
     * Starts A/D conversions on auxillary GPIO pins.
     *
     * param - md - ADC mode used for conversion speed -
     *              *NORMAL mode recommended for now, 7kHz (See ADCOPT in config register)
     *       - chg- GPIO channels to measure
     *
     */
    void startADCAux(AdcMode md, GpioSelect chg);


    /*
     * toggleGPIO()
     *
     * Toggles a GPIO with the LTC6811 on a slave board
     *
     * param - gpio - Gpio bit configuration of addresed slave board (Bit 0 = GPIO1, Bit 1 = GPIO2...)
     *
     */
    void toggleGPIO(uint8_t gpio);

    /*
     * getNumCells()
     *
     * Returns the number of cells managed by a particular slave
     *
     */
    uint8_t getNumCells();

    /*
     * setAddress()
     *
     * Sets the hardware address of the current LTC6811 object
     *
     * WARNING: FOR TESTING PURPOSES ONLY - DO NOT USE IN APPLICATION CODE
     *          HARDWARE ADDRESSES ALWAYS REMAIN THE SAME, AND CHANGING THE
     *          ADDRESS HERE WILL QUERY A DIFFERENT BOARD
     */
    void setAddress(uint8_t address);

    // Constructor and destructor
    Ltc6811(IO::SpiIntf & spiModule, IO::GpioIntf & ChipSelect, uint8_t address, uint8_t numCells);
    ~Ltc6811();

private:

    IO::SpiIntf & my_spiModule;
    IO::GpioIntf & my_chipSelect;

    ConfigRegister my_configuration;
    uint8_t my_address;
    uint8_t my_numCells;

    uint16_t pec15Table[256];

    /* Basic commands for now */
    enum class Command
    {
        /* Condifuration register */
        WRITE_CONFIG_A  = 0x001, // WRCFGA
        WRITE_CONFIG_B  = 0x024, // WRCFGB
        READ_CONFIG_A   = 0x002, // RDCFGA
        READ_CONFIG_B   = 0x026, // RDCFGB

        /* Main and auxillary register banks for voltages */
        READ_CELLS_A    = 0x004, // RDCVA
        READ_CELLS_B    = 0x006, // RDCVB
        READ_CELLS_C    = 0x008, // RDCVC
        READ_CELLS_D    = 0x00A, // RDCVD
        READ_CELLS_E    = 0x009, // RDCVE
        READ_CELLS_F    = 0x00B, // RDCVF
        READ_AUX_A      = 0x00C, // RDAUXA
        READ_AUX_B      = 0x00E, // RDAUXB
        READ_AUX_C      = 0x00D, // RDAUXC
        READ_AUX_D      = 0x00F, // RDAUXD

        /* Status registers */
        READ_STATUS_A   = 0x010, // RDSTATA
        READ_STATUS_B   = 0x012, // RDSTATB

        /* Analog to digital conversions */
        START_ADCONV    = 0x260, // ADCV
        START_ADCONV_AUX= 0x460, // ADAX
        POLL_ADC        = 0x714, // PLADC
        
        /* We don't use these but may need them later
           to implement discharging */
        WRITE_S_CONTROL   = 0x014, // WRSCTRL
        WRITE_PWM_CONTROL = 0x020, // WRPWM
        READ_S_CONTROL    = 0x016, // RDSCTRL
        READ_PWM_CONTROL  = 0x022, // RDPWM

    };

    // Byte lengths for registers and commands
    constexpr static uint8_t REGISTER_GROUP_LENGTH = 6u;
    constexpr static uint8_t CELLS_PER_GROUP = 3u;
    constexpr static uint8_t COMMAND_LENGTH = 4u;
    constexpr static uint8_t PEC_BYTES_LENGTH = 2u;

    // Used to generate the PEC table
    constexpr static uint16_t CRC15_POLY = 0x4599;
    
    /* These two functions were provided directly from the LTC6811 datasheet */
    // TODO: Get the PEC table to initialize at compile time (once we get pipelines running with the proper version of the compiler)
    void initPecTable();
    uint16_t pecCalc(uint8_t len, uint8_t * data);

    void wakeSerial();

}; // class Ltc6811

} // namespace

#endif

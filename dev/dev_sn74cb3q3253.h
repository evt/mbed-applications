 /*
 * file: sn74cb3q3253.h
 * purpose: multiplexer header for the sn74cb3q3253 that will select isoSpi for the sgm master
 */

#ifndef DEV_SN74CB3Q3253_H
#define DEV_SN74CB3Q3253_H

namespace IO
{
class GpioIntf;
} // namespace IO


namespace DEV
{

class Sn74cb3q3253
{
public:

    /*
     * This enum is to keep track of which SPI port is current selected
     */
    enum class SPI_PORT
    {
        PORT0,
        PORT1,
        PORT2,
        PORT3
    };

    Sn74cb3q3253(IO::GpioIntf & muxSel1, IO::GpioIntf & muxSel0);
    ~Sn74cb3q3253();

    /*
     * selects the SPI port to be used by taking in a enum of which port
     */
    void selectSPI(SPI_PORT spi);

    /*
     * gets the current SPI port selected by returning an enum of the port
     */
    Sn74cb3q3253::SPI_PORT getCurrentState();

private:

    IO::GpioIntf & my_muxSel1; //multiplexer select 1  (MSB)
    IO::GpioIntf & my_muxSel0;  //multiplexer select 0 (LSB)
    Sn74cb3q3253::SPI_PORT my_current_state;

};

} // namespace DEV

#endif  // DEV_SN74CB3Q3253_H

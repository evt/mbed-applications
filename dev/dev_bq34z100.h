/*
 * file: dev_bq34z100.h
 * purpose: Defines a class which manages interfacing to the TI BQ34Z100 fuel
 *          gauge integrated circuit for reading information on the battery
 *          pack.
 */

#ifndef DEV_BQ34Z100_H
#define DEV_BQ34Z100_H

#include <cstdint>
#include "io_i2c_intf.h"

using MilliAmpHour = uint32_t;

namespace DEV
{

class Bq34z100
{
private:

    MilliAmpHour my_fullCharge;
    MilliAmpHour my_currentCharge;

public:

    /*
     * Quantifies the state of charge as a percentage full with a percentage
     * error.
     */
    struct CHARGE_PERCENTAGE
    {
        float charge;
        float error;
    };

    /*
     * Constructor for the fuel gauge device taking in a driver for the I2C bus
     * peripheral and the address that the chip has on the bus.
     *
     * i2cDriver: The driver for the I2C bus.
     * i2cAddress: The bus address of the chip.
     * inputDivisionFactor: A ratio of the pack voltage seen by the fuel gauge
     *      to the actual pack voltage. This parameter is optional.
     */
    Bq34z100(IO::i2cIntf & i2cDriver, uint8_t i2cAddress, float inputDivisionFactor = 1.0);

    // Delete default constructor
    Bq34z100() = delete;
    // Delete copy constructors
    Bq34z100(Bq34z100 const &) = delete;
    Bq34z100 & operator=(Bq34z100 const &) = delete;
    // Delete move constructors
    Bq34z100(Bq34z100 &&) = delete;
    Bq34z100 & operator=(Bq34z100 &&) = delete;
    
    ~Bq34z100();

    /*
     * Requests the state of charge of the battery as a percentage. How much is
     * "left in the tank?"
     *
     * charge: Pointer to struct to insert data read from the chip into.
     */
    void getStateOfCharge(CHARGE_PERCENTAGE * const charge);

    /*
     * Requests the charge left on the battery in milliamp hours.
     */
    MilliAmpHour getRemainingCharge();

    /*
     * Requests the voltage of the battery pack monitored by the fuel gauge.
     *
     * Returns: A battery voltage represented as a floating point number.
     */
    float getPackVoltage();

}; // class Bq34z100

} // namespace DEV

#endif // DEV_BQ34Z100_H

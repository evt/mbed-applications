/*
 * file: dev_stwd100.cpp
 * purpose: contains functions necessary to toggle the watchdog
 */

#include "dev_stwd100.h"
#include "io_gpio_intf.h"
#include "mbed.h"

namespace DEV
{

/*
 * Sets the specified WDI pin to be an output and initiallizes it low
 */
Stwd100::Stwd100(IO::GpioIntf & gpio_pin) : my_gpio_pin(gpio_pin)
{
    this->my_gpio_pin.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    this->my_gpio_pin.writePin(IO::GpioIntf::STATE::LOW);
}


Stwd100::~Stwd100()
{
    // Empty Destructor
}


/*
 * Toggles the GPIO pin to keep the WD from timing out.
 * Safest maximum time between pets is 1 second
 */
void Stwd100::pet()
{
    this->my_gpio_pin.writePin(IO::GpioIntf::STATE::HIGH);
    wait_us(1);
    this->my_gpio_pin.writePin(IO::GpioIntf::STATE::LOW);
}

} // namespace DEV


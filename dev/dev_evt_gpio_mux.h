 /*
 * file: dev_evt_gpio_mux.h
 * purpose: multiplexer header for a generic mux 
 */
#ifndef DEV_EVT_GPIO_MUX_H
#define DEV_EVT_GPIO_MUX_H

#include <cstdint>
#include "io_gpio_intf.h"

namespace DEV
{

template <unsigned int NUMBER_OF_MUX_SELS>  //templated to take in the number of selects to use

class EvtGpioMux
{

public:

    // Constructor
    EvtGpioMux(IO::GpioIntf ** muxSels)
    {
        for (unsigned int i = 0; i < NUMBER_OF_MUX_SELS; i++)
        {
            myMuxSels[i] = muxSels[i];
            myMuxSels[i]->setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);  //selects are always outputs
            myMuxSels[i]->writePin(IO::GpioIntf::STATE::LOW);    //defaults all select pins to low
        }
    }

    // Empty Destructor
    ~EvtGpioMux() {}

    /*
     * selects the SPI port to be used by taking in a the port number to select and then
     * drives the selects accordingly
     */
    void choosePort(uint8_t portNumber)
    {
        for (unsigned int i = 0; i < NUMBER_OF_MUX_SELS; i++)
        {
            uint8_t muxSelectBit = portNumber & 0x01;      // masks all bits except the LSb tell the mux which way to drive a select
            portNumber >>= 1;  // Shifts portNum by one to compare next bit to drive

            if (!muxSelectBit)
            {
                myMuxSels[i]->writePin(IO::GpioIntf::STATE::LOW);  // drive pin low
            }
            else
            {
                myMuxSels[i]->writePin(IO::GpioIntf::STATE::HIGH); // drive pin high
            }
        }
    }

private:

    IO::GpioIntf * myMuxSels[NUMBER_OF_MUX_SELS];  // array of mux selects

};
}   // namespace DEV

#endif  //DEV_EVT_GPIO_MUX
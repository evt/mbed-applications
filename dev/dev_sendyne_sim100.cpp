#include "dev_sendyne_sim100.h"
#include "error_manager.h"
#include "io_can_intf.h"
#include <string.h>

namespace DEV
{

Sim100::Sim100(IO::CanIntf & can) : my_canIntf(can)
{
    // Empty Constructor
}

void Sim100::setStatus(Status & status, uint8_t data)
{
    status.isoltationWarning    = data & 1;
    status.isolationFault       = (data >> 1) & 1;
    status.lowBatteryVoltage    = (data >> 2) & 1;
    status.highBatteryVoltage   = (data >> 3) & 1;
    status.highUncertainty      = (data >> 5) & 1;
    status.noNewEstimates       = (data >> 6) & 1;
    status.hardwareError        = (data >> 7) & 1;
}

void Sim100::readIsolationState(IsolationState & state)
{	
    uint8_t data[] = { READ_ISOLATION_STATE };
    PROTOCOL::CanMessage requestMessage(TRANSMIT_CAN_ID, data, 1);
    my_canIntf.transmit(requestMessage);


    PROTOCOL::CanMessage * response = my_canIntf.receive(RECEIEVE_CAN_ID);

    setStatus(state.status, response->packet[1]);

    state.electricIsolation = (static_cast<uint16_t>(response->packet[2]) << 8) | response->packet[3];
    state.isolationUncertainty = response->packet[4];
    state.energyStored = (static_cast<uint16_t>(response->packet[5]) << 8) | response->packet[6];
    state.storedUncertainty = response->packet[7];
}

void Sim100::readIsolationResistances(IsolationResistances & resistances)
{
    uint8_t data[] = { READ_ISOLATION_RESISTANCE };
    PROTOCOL::CanMessage requestMessage(TRANSMIT_CAN_ID, data, 1);
    my_canIntf.transmit(requestMessage);

    PROTOCOL::CanMessage * response = my_canIntf.receive(RECEIEVE_CAN_ID);

    setStatus(resistances.status, response->packet[1]);

    resistances.resistanceP = (static_cast<uint16_t>(response->packet[2]) << 8) | response->packet[3];
    resistances.resistancePUncertainty = response->packet[4];
    resistances.resistanceN = (static_cast<uint16_t>(response->packet[5]) << 8) | response->packet[6];
    resistances.resistanceNUncertainty = response->packet[7];
}

void Sim100::readIsolationCapacitances(IsolationCapacitances & capacitances)
{
    uint8_t data[] = { READ_ISOLATION_CAPACITANCES };
    PROTOCOL::CanMessage requestMessage(TRANSMIT_CAN_ID, data, 1);
    my_canIntf.transmit(requestMessage);

    PROTOCOL::CanMessage * response = my_canIntf.receive(RECEIEVE_CAN_ID);

    setStatus(capacitances.status, response->packet[1]);

    capacitances.capacitanceP = (static_cast<uint16_t>(response->packet[2]) << 8) | response->packet[3];
    capacitances.capacitancePUncertainty = response->packet[4];
    capacitances.capacitanceN = (static_cast<uint16_t>(response->packet[5]) << 8) | response->packet[6];
    capacitances.capacitanceNUncertainty = response->packet[7];
}

void Sim100::readIsolationVoltages(IsolationVoltages & voltages)
{
    uint8_t data[] = { READ_VOLTAGES_POLE_CHASSIS };
    PROTOCOL::CanMessage requestMessage(TRANSMIT_CAN_ID, data, 1);
    my_canIntf.transmit(requestMessage);

    PROTOCOL::CanMessage * response = my_canIntf.receive(RECEIEVE_CAN_ID);

    setStatus(voltages.status, response->packet[1]);

    voltages.voltageP = (static_cast<uint16_t>(response->packet[2]) << 8) | response->packet[3];
    voltages.voltagePUncertainty = response->packet[4];
    voltages.voltageN = (static_cast<uint16_t>(response->packet[5]) << 8) | response->packet[6];
    voltages.voltageNUncertainty = response->packet[7];
}

void Sim100::readBatteryVoltages(BatteryVoltages & batteryVoltage)
{
    uint8_t data[] = { READ_BATTERY_VOLTAGE };
    PROTOCOL::CanMessage requestMessage(TRANSMIT_CAN_ID, data, 1);
    my_canIntf.transmit(requestMessage);

    PROTOCOL::CanMessage * response = my_canIntf.receive(RECEIEVE_CAN_ID);

    setStatus(batteryVoltage.status, response->packet[1]);

    batteryVoltage.batteryVoltage = (static_cast<uint16_t>(response->packet[2]) << 8) | response->packet[3];
    batteryVoltage.batteryVoltageUncertainty = response->packet[4];
    batteryVoltage.batteryVoltageMax = (static_cast<uint16_t>(response->packet[5]) << 8) | response->packet[6];
    batteryVoltage.batteryVoltageMaxUncertainty = response->packet[7];
}

void Sim100::readErrorFlags(ErrorFlags & errorFlags)
{
    uint8_t data[] = { READ_ERROR_FLAGS };
    PROTOCOL::CanMessage requestMessage(TRANSMIT_CAN_ID, data, 1);
    my_canIntf.transmit(requestMessage);

    PROTOCOL::CanMessage * response = my_canIntf.receive(RECEIEVE_CAN_ID);

    setStatus(errorFlags.status, response->packet[1]);

    errorFlags.powerSupplyOutOfRange    = (response->packet[2] >> 2) & 1;
    errorFlags.voltageOutOfSpecs        = (response->packet[2] >> 3) & 1;
    errorFlags.vx1Vx2Reversed           = (response->packet[2] >> 4) & 1;
    errorFlags.chassisConnectionBroken  = (response->packet[2] >> 5) & 1;
    errorFlags.vx1Broken                = (response->packet[2] >> 6) & 1;
    errorFlags.vx2Broken                = (response->packet[2] >> 7) & 1;
}

void Sim100::readPartName(char partName[])
    {
    uint8_t requestData_0[] = { READ_PART_NAME_0 };
    PROTOCOL::CanMessage requestMessage_0(TRANSMIT_CAN_ID, requestData_0, 1);
    my_canIntf.transmit(requestMessage_0);

    PROTOCOL::CanMessage * response_0 = my_canIntf.receive(RECEIEVE_CAN_ID);

    for(int i = 0; i < 4; i++) 
    {
        partName[i] = response_0->packet[i + 1];
    }

    #if 0
    my_canIntf.transmit(requestMessage_1);
    IO::CanMessage response_1(0, requestData_1, 8);
    my_canIntf.receive(response_1);

    for(int i = 0; i < 4; i++) 
    {
        partName[i+4] = response_1.packet[i + 1];
    }

    for(int i = 0; i < 4; i++) 
    {
        partName[i+4] = response.packet[i + 1];
    }

    requestData[0] = READ_PART_NAME_2;
    my_canIntf.transmit(canRequest);
    my_canIntf.receive(canResponse);
    const uint8_t * data_2 = canResponse.packet;
    for(int i = 0; i < 4; i++) 
    {
        partName[i+8] = data_2[i + 1];
    }

    requestData[0] = READ_PART_NAME_3;
    my_canIntf.transmit(canRequest);
    my_canIntf.receive(canResponse);
    const uint8_t * data_3 = canResponse.packet;
    for(int i = 0; i < 4; i++) 
    {
        partName[i+12] = data_3[i + 1];
    }
    #endif
    partName[17] = '\0';
}

void Sim100::writeMaxVoltage(uint16_t maxVoltage)
{
    uint8_t first = (maxVoltage >> 8) & 0xff;
    uint8_t second = maxVoltage & 0xff;
    uint8_t requestData[] = { WRITE_MAX_VOLTAGE, first, second };
    PROTOCOL::CanMessage canMessage(TRANSMIT_CAN_ID, requestData, 3);
    my_canIntf.transmit(canMessage);
}

void Sim100::resetBoard()
{
    uint8_t requestData[] = { RESET_BOARD };
    PROTOCOL::CanMessage canMessage(TRANSMIT_CAN_ID, requestData, 1);
    my_canIntf.transmit(canMessage);
}
Sim100::~Sim100()
{
	// Empty destructor
}
}

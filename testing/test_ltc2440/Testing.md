# Test Plan for Dev-Ltc2440 Driver

## Features
* Read a code output as an int32_t from the ltc2440
* Eventually will convert code to a Voltage

## Required Hardware for Test
* Anything with SPI port (A BMS 4.1 was used)
* A SGM slave
* Saleae (if reading SPI headers)
* STProgrammer
* USB to TTL adapter or Saleae to read MOSI from SPI

## Additional Required Software
* STM32CubeProgrammer
* Terminal Emulator (i.e. Putty, minicom, screen) OR Saleae's "Logic"

## Steps to Complete the Test if using UART
1. Connect USB to TTL's RX probe to transmit of the master being programmed
2. Connect STProgrammer to JTAG header of the master
3. Power the master and the SGM slave
4. Don't blow up the slave 
5. Program the master with STM32Programmer
6. Start your terminal emulator
7. Look at the ADC codes
8. To convert them to Volts, subtract 0x20000000, divide by 0x20000000 and multiply by Vref (4.096 in this case)

## Steps to Complete the Test if using Saleae
1. Connect probes of Saleae to SPI header on the master
2. Connect STProgrammer to JTAG header of the master
3. Power the master and the SGM slave
4. Don't blow up the slave 
5. Program the master with STM32Programmer
6. Start Saleae's "Logic" program
7. Look ADC output from MOSI
8. The first output should be 0x20 which is a status code for Vin is greater than half of Vref
9. The second output will be the ADC code
10. To convert to Volts, subtract 0x20000000, divide by 0x20000000 and multiply by Vref (4.096 in this case)
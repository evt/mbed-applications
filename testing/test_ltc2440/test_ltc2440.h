#ifndef TEST_LTC2440
#define TEST_LTC2440

#include "io_gpio_mbed_adapter.h"
#include "io_spi_mbed_adapter.h"
#include "dev_ltc2440.h"

/* Pin for chip select */
constexpr IO::PIN CS_PIN = IO::PIN::MC_PB12;

/* Pins for SPI2 */
constexpr PinName MOSI_PIN = PinName::PB_15;
constexpr PinName MISO_PIN = PinName::PB_14;
constexpr PinName SCLK_PIN = PinName::PB_13;

/* Spi settings */
constexpr IO::SpiMbedAdapter::BusMode BUS_MODE = IO::SpiMbedAdapter::BusMode::MODE_00;
constexpr IO::SpiMbedAdapter::BitOrder BIT_ORDER = IO::SpiMbedAdapter::BitOrder::DEFAULT_FIRST;



#endif
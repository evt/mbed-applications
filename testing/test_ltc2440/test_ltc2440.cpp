/*
 * test_ltc2440.cpp
 * file to test read driver of the ltc2440 then convert the adc code to voltage.
 */

#include "test_ltc2440.h"
#include "evt_bit_utils.h"
#include "target_bms.h"
#include "mbed.h"

namespace
{
/*
 * This function ismeant to calculate the resistance of what is being put through the sgm slave.
 * However, the gain was recently changed and INTERCEPT_VOLTAGE and GAIN will need to be derived again.
 */
#if 0
double calcResist(double voltage)
{
    constexpr double GAIN = -332.8;
    constexpr double INTERCEPT_VOLTAGE = 111574;    //in millivolts
    return (voltage-INTERCEPT_VOLTAGE)/GAIN;    //in ohms
}
#endif
}

int main()
{


    IO::SpiMbedAdapter &spiModule = IO::SpiMbedAdapter::getInstance<MOSI_PIN, MISO_PIN, SCLK_PIN>();
    IO::GpioMbedAdapter &chipSelect = IO::GpioMbedAdapter::getInstance<CS_PIN>();
    spiModule.configureSpi(BUS_MODE, BIT_ORDER);
    chipSelect.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    chipSelect.writePin(IO::GpioIntf::STATE::HIGH);

    target::Bms &my_target = target::Bms::getInstance();
    IO::uartIntf & uart = my_target.getUART();

    DEV::Ltc2440 ltc(spiModule, chipSelect);

    DEV::Ltc2440::AdcCode adcCode;
    uint8_t adc8[4];

    while(1)
    {

        ltc.read(&adcCode);

        adc8[0] = getHighestByte(adcCode.value);
        adc8[1] = getMiddleHighByte(adcCode.value);
        adc8[2] = getMiddleLowByte(adcCode.value);
        adc8[3] = getLowestByte(adcCode.value);

        uart.printf("Status is %d\n\r", (adcCode.status));
        uart.printf("ADC code is 0x%X\n\r", adcCode.value);

        spiModule.write(&adcCode.status,1);
        wait(.01);
        spiModule.write(adc8, 4);
        wait_ms(1000);
    }

} // main
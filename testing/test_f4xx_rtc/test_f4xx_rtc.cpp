#include "target_nucleo_f446re.h"
#include "io_rtc_f4xx_hal_adapter.h"
#include "io_uart_intf.h"
#include <cstdint>

int main() 
{
    // Initialize values
    target::nucleo_f446re & my_target = target::nucleo_f446re::getInstance();
    IO::uartIntf & my_uart = my_target.getUART();
    IO::RtcIntf & my_rtc = IO::RtcF4xxHalAdapter::getInstance();

    Time::Date initDate = { Time::WeekDay::MONDAY, Time::Month::APRIL, 5, 19 };
    Time::Clock initClock = { 12, 12, 12, 12 };

    // Read values
    Time::Date date;
    Time::Clock clock;

    char timestamp[60];

    char input;

    while(true)
    {
        my_uart.printf(">> ");
        input = my_uart.getc();
        my_uart.putc(input);
        my_uart.printf("\n\r\n");

        switch(input)
        {
            case 't':
            case 'T':
                my_rtc.readClock(clock);
                my_uart.printf("Hour: %d ", clock.hours);
                my_uart.printf("Minute: %d ", clock.minutes);
                my_uart.printf("Seconds: %d ", clock.seconds);
                my_uart.printf("Subseconds: %d\n\r", clock.subseconds);
                break;
            case 'd':
            case 'D':
                my_rtc.readDate(date);
                my_uart.printf("%d/%d/%d\n\r", date.month, date.day, date.year);
                break;
            case 'i':
            case 'I':
                my_rtc.initTime(initDate, initClock);
                my_uart.printf("Initializing time... %c\n\r", input);
                break;
            case 's':
            case 'S':
                my_rtc.getTimestamp(timestamp);
                my_uart.printf("%s\n\r", timestamp);
                break;
            case '\n':
                break;
            default:
                my_uart.printf("Unknown command %c\n\r", input);
                break;
        }        
    }
}


#include "mbed.h"
#include "target_nucleo_f302r8.h"

int main()
{
    char temp;

    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();

    IO::uartIntf &uartDriver = my_target.getUART();
    DEV::LED my_led = my_target.getLED();

    // Start by turning the LED off
    my_led.setCurrentState(IO::GpioIntf::STATE::LOW);

    
    uartDriver.puts("Welcome to UART!\r\n");
    // Make sure that printf works.
    uartDriver.printf("printf test!\r\n");
    uartDriver.printf("printf test %d!\r\n>", 2);

    for (;;) // Loop forever and echo the characters as the user types them.
    {
        temp = uartDriver.getc();
        uartDriver.putc(temp);
        my_led.toggle();

        if (temp == '\r') // If they hit enter, print a new line and new prompt.
        {
            uartDriver.puts("\n>");
        }
    }
}

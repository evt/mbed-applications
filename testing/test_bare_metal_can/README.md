# Testing MC CAN

## Hardware Requirements
* STM32F302 Nucleo board
* Female-to-Female Jumper
* Saleae Logic Analyzer / Oscilloscope
* USB/TTL FTDI Cable

1. Hook up the female-to-female wire to pins B8 and B9.
2. Connect your USB adapter to Pins C10 and C11.
3. Set the protocol of your logic analyzer to CAN at 250 kbps.
4. Verify that the CANTX pin is writing data.
    1. Message identifier: 1337
    2. Data: 0xAA, 0x55, 0xAA, 0x55
5. Check the UART output.
    1. A message "Transmission requested." indicates transmission is working.
    2. A message "Message received!" indicates reception is working.
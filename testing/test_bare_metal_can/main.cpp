/*
 * file: main.cpp
 * purpose: Test the bare metal MC layer implementation of the MC layer.
 */

#include "mbed.h"
#include "mc_can.h"
#include "target_nucleo_f302r8.h"


namespace
{

uint8_t  data[] = {0xAA, 0x55, 0xAA, 0x55, 0xDE, 0xAD, 0xBE, 0xEF};
uint32_t id = 1337;
uint8_t  dlc = 8;

PROTOCOL::CanMessage const transmitMessage(id, data, dlc);

uint8_t  rx_data[8];
uint32_t rx_id;
uint8_t  rx_dlc;

PROTOCOL::CanMessage receivedMessage(rx_id, rx_data, rx_dlc);

// Need to explore this more, just playing around with values for now.
MC::CanFilter my_filter(id << 3,
                        id >> 16,
                        id << 3,
                        id >> 16,
                         MC::CanFilterPosition::POS_0,
                         MC::CanRxMailbox::RX_0,
                         MC::CanFilterMode::ID_MASK_32_BIT);


} // namespace

// Global flag
bool message_received(false);

int main()
{
    target::nucleo_f302r8 & my_target = target::nucleo_f302r8::getInstance();
    IO::uartIntf & uartDriver = my_target.getUART();
    DEV::LED & my_led = my_target.getLED();

    MC::can_initializeGpioPins(MC::Pin::B8, MC::Pin::B9);
    MC::can_initialize(250000, 36000000);

    if(!MC::can_configFilter(my_filter))
    {
        uartDriver.printf("Unable to configure message Rx filter\r\n");
    }

    uartDriver.printf("CAN Initialized.\r\n");

    MC::can_setMode(MC::CanMode::NORMAL, true);
    if (MC::CanMode::NORMAL != MC::can_getMode())
    {
        while (1)
        {
            uartDriver.printf("setMode Failed: %d\r\n",
                              static_cast<uint8_t>(MC::can_getMode()));
            wait_ms(100);
        }
    }

    // Reading the message clears the interrupt
    MC::can_enableInterrupt(MC::CanIrq::RECEIVE, [](){ message_received = MC::can_read(&receivedMessage, MC::CanRxMailbox::RX_0); });

    // Loop forever
    while (true)
    {
        // Send a message
        bool status = MC::can_write(&transmitMessage, MC::CanTxMailbox::TX_0);
        wait_ms(50);
        if (MC::CanMode::NORMAL != MC::can_getMode())
        {
            while (true)
            {
                uartDriver.printf("CAN left Normal Mode: %d\r\n",
                                  static_cast<uint8_t>(MC::can_getMode()));
                wait_ms(100);
            }
        }
        if (MC::can_getErrorState())
        {
            while (true)
            {
                uartDriver.printf("Error state on transmit %lu\r\n", MC::can_getErrorState());
                wait_ms(100);
            }
        }
        else if (MC::can_previousTransmitSuccessful(MC::CanTxMailbox::TX_0))
        {
            uartDriver.printf("It seems to be OK.\r\n");
        }
        else
        {
            while (true)
            {
                uartDriver.printf("I don't know what to think anymore\r\n");
                wait_ms(100);
            }
        }

        if (status)
        {
            uartDriver.printf("Transmission requested.\r\n");
        }
        else
        {
            uartDriver.printf("Transmission request failed\r\n");
        }
        wait_ms(250);

        // Assuming our interrupt worked...
        if (message_received)
        {
            uartDriver.printf("Message received!\r\n");
            uartDriver.printf("    ID : %u\n\r", receivedMessage.id);
            uartDriver.printf("    DLC: %u\n\r", receivedMessage.dataLengthCode);
            for(uint8_t i = 0; i < receivedMessage.dataLengthCode; i++)
                uartDriver.printf("    data[%u]: %X\n\r", i, receivedMessage.packet[i]);
        }
        else
        {
            uartDriver.printf("But I can't see it\r\n");
        }
        wait_ms(250);
        my_led.toggle();
    }
}
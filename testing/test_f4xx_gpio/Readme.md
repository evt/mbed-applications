# Test Plan for GPIO F4xx HAL Adapter

## Features
* Tests the IO wrapper for the STM32 F4xx HAL GPIO driver

## Required Hardware for Test
* STM32 NUCLEO_F446RE
* Jumper wire(s)
* Saleae (Optional)

## Additional Required Software
* Minicom, gtkterm, putty or your favourite serial terminal emulator
* Saleae Logic (Optional)

## Steps to Complete Test
1. Connect a jumper between pins PC9 and PH1.
2. Build the test executable 'python mbed_build.py test_f4xx_gpio'.
3. Flash the Nucleo board with 'test_f4xx_gpio.bin'.
4. Open your terminal emulator with 115200 8N1 baud settings.
5. A welcome message should be printed, as well as a message stating an IRQ was registered to PC9.
6. (Optional) Probe PA6 with the Saleae and verify a 5Hz square wave with 50% duty cycle.
7. In the terminal emulator, verify that the rising edge counter increments ~5 times per second.

/*
 * file: test_f4xx_gpio.cpp
 * purpose: Test procedure for the IO adapter for the F4xx HAL GPIO drivers
 */

#include "target_nucleo_f446re.h"
#include "io_gpio_f4xx_hal_adapter.h"
#include "io_uart_intf.h"
#include "mbed.h"

uint32_t risingEdgeCount = 0;
target::nucleo_f446re & my_target = target::nucleo_f446re::getInstance();
IO::uartIntf & my_uart = my_target.getUART();
int main()
{
    // We're going to send a square wave from PH1 to PC9 to test rising edge detection.
    IO::GpioF4xxHalAdapter & my_gpio1 = IO::GpioF4xxHalAdapter::getInstance<IO::PIN::MC_PC9>();
    IO::GpioF4xxHalAdapter & my_gpio2 = IO::GpioF4xxHalAdapter::getInstance<IO::PIN::MC_PH1>();

    // Technically not needed for gpio1, registering the IRQ for rising edge detection does this under the hood
    my_gpio1.setPinDirection(IO::GpioIntf::DIRECTION::INPUT);
    my_gpio2.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);

    my_uart.baud(115200);

    my_uart.printf("Test beginning...\r\n");

    // Let's count the number of rising edges on PC9; register the following lambda with the GPIO adapter:
    my_gpio1.registerIrq(IO::GpioF4xxHalAdapter::TriggerEdge::RISING,
                        [](){ my_uart.printf("Rising edge detected! %d\r\n", risingEdgeCount++); });

    my_uart.printf("Registered IRQ to PC9.\r\n");

    // 5Hz "square wave"
    while(true)
    {
        my_gpio2.writePin(IO::GpioIntf::STATE::LOW);
        wait(0.1);
        my_gpio2.writePin(IO::GpioIntf::STATE::HIGH);
        wait(0.1);
    }
}
# Test Plan for IO_I2C Adapter

## Features
* Write to I2C slave.
* Read from I2C slave.

## Required Hardware for Test
* STM32F302R8 Nucleo Development Board
* Arduino Uno
* Two Pullup Resistors (About 4K Ohms)
* Breadboard and Wires for Hookup

## Required Additional Software for Test
* Arduino IDE

## Steps to Complete the Test
1. Connect the SDA, SCL, and GND lines of the two boards together, using the two pullup resistors to tie the SDA and SCL lines to the 3.3V line.
2. Program the Arduino with the 'i2c_slave' sketch.
3. Build the executable with _python mbed_build.py -t test_i2c_
4. Drop the compiled 'test_i2c.bin' into the Nucleo's file system.
5. Power on both devices.
6. Open the Arduino serial monitor.
7. Look for the messages. You should see "I2C Test Message!" if the Arduino received data from the Nucleo and "Data Received!" if the Nucleo was able to read data from the Arduino.

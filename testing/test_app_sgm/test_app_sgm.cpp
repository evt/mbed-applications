/*
 * test_app_sgm.cpp
 * file to test if everything SGM works
 */

#include "mbed.h"
#include "app_sgm.h"
#include "target_sgm.h"
#include "mc_can.h"

int main()
{
    target::Sgm & my_target = target::Sgm::getInstance();
    APP::Sgm my_sgm_app(my_target.getEvtCan(),
                        my_target.getLtc2440().first,
                        my_target.getMUX(), 1);

    my_sgm_app.init();
    
    IO::EvtCan &can = my_target.getEvtCan();

    // Set into loopback mode for purposes of bench test
    MC::can_setMode(MC::CanMode::NORMAL, true);

    while(1)
    {

        my_sgm_app.run();
        wait(0.1);  
    }
}


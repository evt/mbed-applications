/*
 * file: test_can_logging.cpp
 * purpose: Test GUB app to log CAN messages to SD card
 */

#include "app_gub_logger.h"
#include "target_gub.h"
#include "io_can_intf.h"
#include "io_uart_intf.h"
#include "mbed.h"
#include "io_gpio_mbed_adapter.h"
#include "evt_types.h"
#include "app_gfd.h"

int main()
{
    target::Gub & my_target = target::Gub::getInstance();
    IO::uartIntf & uart = my_target.getUART();
    IO::RtcIntf & rtc = my_target.getRTC();
    APP::AppGubLogger my_app = APP::AppGubLogger(my_target.getCAN1(), my_target.getCAN2(), rtc, 1);
    APP::GroundFault my_gfd_app = APP::GroundFault(my_target.getGFD(), 1);

    Time::Date initDate = { Time::WeekDay::MONDAY, Time::Month::APRIL, 5, 19 };
    Time::Clock initClock = { 12, 12, 12, 12 };
    char timestamp[25];

    uart.baud(115200);
    rtc.initTime(initDate, initClock);
    my_app.init();


    uart.printf("Waiting...\r\n");
    rtc.getTimestamp(timestamp);
    uart.printf("Current Time: %s\r\n", timestamp);
    uint8_t ret;

    uint32_t count = 0;

    while(1)
    {
        ret = my_app.run();

        if(count++ == 200)
        {
            my_gfd_app.run();
            count = 0;
        }

        wait_us(1000);

    }
}

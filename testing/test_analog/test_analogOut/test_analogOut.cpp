/*
 * file: test_analogOut.cpp
 * purpose: perform sample write sequence to test the io_analogOut module
 */

#include "io_analogOut.h"
#include "io_uart_mbed_adapter.h"
#include <cstdint>
#include "mbed.h"
#include "target_nucleo_f302r8.h"

int main()
{
    uint16_t test_output_voltage = 1500; // an output voltage test value
    constexpr PinName AOUTPUT_PIN = PinName::PA_4;

    // getInstance block
    IO::analogOut &analogOutTest = IO::analogOut::getInstance<AOUTPUT_PIN>();
    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();

    IO::uartIntf &uartDriver = my_target.getUART(); 

    uartDriver.puts("Begin Testing AnalogOut!\n");

    // write the value
    analogOutTest.write(test_output_voltage);

    while (1)
    {
        // begin testing output
        uartDriver.printf("%s", "Written Voltage: ");
        uartDriver.printf("%d\n\r", test_output_voltage);
        wait(.1);
    }
}
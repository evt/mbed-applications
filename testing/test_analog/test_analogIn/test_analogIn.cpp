/*
 * file: test_analogIn.cpp
 * purpose: perform sample read sequence to test the io_analogIn module
 */

#include <cstdint>
#include "mbed.h"
#include "io_adc_mbed_adapter.h"
#include "io_uart_mbed_adapter.h"
#include "target_nucleo_f302r8.h"

inline float VOLTS_TO_MILLIVOLTS(float inputVolts)
{
    return inputVolts * 1000;
}

int main()
{
    const auto adcPin = IO::PIN::MC_PB0;
    auto adcDriver = IO::AdcMbedAdapter::getInstance<adcPin>();
    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();

    IO::uartIntf &uartDriver = my_target.getUART(); 

    uartDriver.puts("Begin Testing ADC!\n");

    while (true) 
    {
        // begin testing input
        uint32_t inputMillivolts = static_cast<uint32_t>(VOLTS_TO_MILLIVOLTS(adcDriver.read()));
        uartDriver.printf("Voltage: ");
        uartDriver.printf("%d mV\n\r", inputMillivolts);
        wait(1);
    }
}
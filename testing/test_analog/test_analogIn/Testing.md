# Test Plan for IO_ANALOGIN Driver

## Features
* Read an Analog voltage.

## Required Hardware for Test
* STM32F302R8 Nucleo Development Board
* One Potentiometer
* One USB to UART cable
* Breadboard and Wires for Hookup

## Required Additional Software for Test
* Minicom on Linux / Putty on Windows

## Steps to Complete the Test
1. Connect the jumpers to the specified pin locations (found in the testing files) and place one pullup resistor as well as a potentiometer to the breadboard then connect the UART to USB to the UART pins on the Nucleo.
2. Build the executable with python mbed_build.py test_analogIn
3. Drop the compiled 'test_analogIn.bin' into the Nucleo's file system.
4. Power on the board.
5. Open minicom/putty.
6. Look for the messages. You should see a stream of voltage values in the form "Voltage: *value* mV" The value is of type uint16_t so the value will not be a decimal however the value preserves 3 decimal places in the voltage.

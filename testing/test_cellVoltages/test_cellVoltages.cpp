
#include "mbed.h"
#include "app_cellVoltage.h"
#include "target_bms.h"
#include "mc_can.h"

int main()
{
    target::Bms & my_target = target::Bms::getInstance();
    APP::CellVoltage my_app(my_target.getLtc6811().first,
                            my_target.getEvtCan(),
                            my_target.getLtc6811().second,
                            1);

    MC::can_setMode(MC::CanMode::NORMAL, true);

    my_app.init();

    while (1)
    {
        wait(0.1);
        my_app.run();
    }
}

#include "target_nucleo_f302r8.h"
#include "io_uart_f3xx_hal_adapter.h"
#include "dev_led.h"
#include "io_uart_intf.h"

#define STR_LEN 5
char buf[STR_LEN];

int main()
{
    char temp;

    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();

    IO::uartIntf &uartDriver = my_target.getUART();
    DEV::LED my_led = my_target.getLED();

    // Start by turning off the LED
    my_led.setCurrentState(IO::GpioIntf::STATE::LOW);

    // Checks puts function
    uartDriver.puts("Welcome to UART!\r\n");
    // Checks putc function
    uartDriver.putc('T');
    uartDriver.putc('E');
    uartDriver.putc('S');
    uartDriver.putc('T');
    uartDriver.putc('\r');
    uartDriver.putc('\n');
    // Checks printf function
    uartDriver.printf("printf test!\r\n");
    uartDriver.printf("printf test %d!\r\n", 2);

    for (;;) // Infinite loop echoing characters as the user types
    {
        uartDriver.printf("\n\r> ");
        uartDriver.gets(&buf[0], STR_LEN);
        uartDriver.printf("\n\r%s\n\r", buf);
        uartDriver.printf("\n\r");
        for (int i = 0; i < STR_LEN; i++){
            uartDriver.printf("buf[%d]:0x%x\r\n", i, buf[i]);
        }
        uartDriver.printf("\n\r");
    }
}
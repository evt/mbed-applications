# Test Plan for io_adc_f4xx_hal_adapter

## Features
* Continuous conversion of multiple ADC channels
* Instantaneous sample of multiple ADC channels
* DMA for efficient data transfer between ADC peripheral and memory

## Required Hardware for Test
* STM32F446RE Nucleo Development Board
* Serial (UART) to USB adapter
* Breadboard
* Potentiometer
* Misc. jumper wires

## Required Additional Software for Test
* Serial terminal emulator of choice (minicom, gtkterm, putty, etc.)

## Steps to Complete Test
1. Connect GND, Tx and Rx of the serial adapter to the Nucleo
2. Connect ADC channels on PC1 and PC0 to the center tap of the potentiometer on the breadboard
3. Connect the 3.3V rail and GND on the Nucleo to the outer pins of the potentiometer
4. Compile the test program with _python mbed_build.py test_f4xx_adc_ and copy 'test_f4xx_adc.bin' into the Nucleo's file system
5. Configure the serial terminal emulator with 115200 8N1 baud settings
6. The terminal should be displaying the contents of the DMA buffer as well as the instantaneously-sampled ADC values
7. Adjust the potentiometer and verify that the ADC readings update appropriately.
8. Remove one of the channels from the potentiometer and tie it to 3.3V and verify the ADC readings match
9. Tie the same channel to GND and verify the ADC readings match
10. Repeat steps 8-9 with the second channel.

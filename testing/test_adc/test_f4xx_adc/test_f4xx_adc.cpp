/*
 * file: test_f4xx_adc.cpp
 *
 * Test for the IO-layer wrapper for STM32F4xx_HAL_ADC.
 *
 * Configures the ADC with two channels with continous conversion.
 * Uses the DMA controller to retrieve the data and prints the results.
 */

#include "target_nucleo_f446re.h"
#include "io_adc_f4xx_hal_adapter.h"
#include "mbed.h"
#include "io_pin_names.h"
#include "io_uart_intf.h"

inline float VOLTS_TO_MILLIVOLTS(float inputVolts)
{
    return inputVolts * 1000;
}

int main()
{

    constexpr float VDD = 3.3;
    constexpr uint16_t DMA_BUFFER_SIZE = 5;
    constexpr uint8_t NUM_CHANNELS = 2;
    constexpr auto ADC_PIN1 = IO::PIN::MC_PC0;
    constexpr auto ADC_PIN2 = IO::PIN::MC_PC1;

    target::nucleo_f446re &my_target = target::nucleo_f446re::getInstance();
    IO::uartIntf &my_uart = my_target.getUART(); 

    my_uart.printf("Welcome to the ADC test.\r\n");
    my_uart.printf("Enabling DMA...\r\n");

    auto & my_adc = IO::AdcF4xxHalAdapter::getInstance<NUM_CHANNELS, DMA_BUFFER_SIZE>({ADC_PIN1, ADC_PIN2});
    uint16_t * my_buffer = my_adc.getBuffer();

    my_adc.startDMA();

    while(true)
    {
        // Wait to get some conversions (probably overkill)
        wait(0.5);

        // Stop the DMA so we can print the buffer without our data getting stomped on
        my_adc.stopDMA();

        my_uart.printf("DMA Buffer:\r\n");

        my_uart.printf("------\r\n");
        for (uint32_t i = 0; i < DMA_BUFFER_SIZE; i++)
            my_uart.printf("%u\r\n", my_buffer[i]);

        my_uart.printf("Manually reading each channel:\r\n");

        uint16_t val1 = static_cast<uint32_t>(VOLTS_TO_MILLIVOLTS(VDD * my_adc.readChannel(ADC_PIN1)));
        uint16_t val2 = static_cast<uint32_t>(VOLTS_TO_MILLIVOLTS(VDD * my_adc.readChannel(ADC_PIN2)));
        uint16_t raw1 = my_adc.readChannelRaw(ADC_PIN1);
        uint16_t raw2 = my_adc.readChannelRaw(ADC_PIN2);

        my_uart.printf("Channel 1: %u mV | %u raw\r\n", val1, raw1);
        my_uart.printf("Channel 2: %u mV | %u raw\r\n", val2, raw2);

        my_adc.startDMA();
    }
}

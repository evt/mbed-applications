/*
 * file: test_blink.cpp
 * purpose: Tests blinking an LED on the STM32 Nucleo development board using
 *          the Mbed peripheral adapters.
 */

#include "mbed.h"
#include "target_nucleo_f302r8.h"

int main()
{
    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();

    DEV::LED my_led = my_target.getLED();

    auto toggle_lambda = [& my_led]()
    {
        static bool state = false;
        state = !state;
        my_led.setCurrentState(static_cast<IO::GpioIntf::STATE>(state));
        return;
    };

    while (true)
    {
        toggle_lambda();
        wait(0.5);
    }
}

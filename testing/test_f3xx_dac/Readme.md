# Test Plan for F3xx DAC

## Features
* HAL Adapter for the STM32F3xx DAC peripheral

## Required Hardware for Test
* STM32 Nucleo F302R8
* Oscilliscope or other analyzer (Saleae was used)

## Additional Required Software
* Saleae Logic (if using Saleae)

## Steps to Complete the Test
1. Compile the test program with the target "test_f3xx_dac" and program the Nucleo
2. Probe pin PA4 on the Nucleo with the oscilloscope or Saleae in ANALOG mode
3. Observe a steady and cyclic ramp-up and ramp-down from 0 to 3.3V and vice versa.
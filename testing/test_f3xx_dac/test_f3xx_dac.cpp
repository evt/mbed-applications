/*
 * file: test_f3xx_dac.cpp
 */

#include "target_nucleo_f302r8.h"
#include "io_dac_intf.h"
#include "io_dac_f3xx_hal_adapter.h"

constexpr uint16_t VDD_MILLIVOLTS = 3300;
constexpr float VDD_VOLTS = 3.3;

int main()
{
    target::nucleo_f302r8 & my_target = target::nucleo_f302r8::getInstance();
    IO::DacIntf & my_dac = my_target.getDAC();

    while(1)
    {
        // Ramp up and ramp down
        for(uint16_t i = 0; i <= VDD_MILLIVOLTS; i++)
        {
            my_dac.write((static_cast<float>(i)/VDD_MILLIVOLTS)*VDD_VOLTS);
            wait_us(100);
        }

        for(uint16_t i = VDD_MILLIVOLTS; i > 0; i--)
        {
            my_dac.write((static_cast<float>(i)/VDD_MILLIVOLTS)*VDD_VOLTS);
            wait_us(100);
        }
    }

}
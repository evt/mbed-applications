/*
 * file: test_fatfs.cpp
 *
 * Runs basic file read and write tests to the SD
 * card on GUB 2.0
 */

#include "target_nucleo_f446re.h"
#include "io_uart_intf.h"

#include "fatfs.h"
#include "sd_diskio.h"

void SystemClock_Config();
void Error_Handler();
void configureSDIO();

SD_HandleTypeDef hsd;

int main(void)
{
    target::nucleo_f446re & Gub = target::nucleo_f446re::getInstance();
    IO::uartIntf & uart = Gub.getUART();

#if 0
    // Leftover from HAL
    HAL_Init();

    SystemClock_Config();
#endif

    uart.baud(115200);
    
    configureSDIO();

    MX_FATFS_Init();

    char fileReadBuffer[100];
    char fileWriteBuffer[100];
    unsigned int numBytesRead=0;

    uart.printf("\r\nHello\r\n");

    unsigned int wbytes;
    unsigned int ret;

    if (retSD == 0)
    {
        ret = f_mount(&SDFatFS, SDPath, 1);
        if (ret == FR_OK)
        {
            uart.printf("\r\nMount successful. Path: %s  \r\n", SDPath);

            ret = f_open(&SDFile, "STM32.txt", FA_CREATE_ALWAYS|FA_WRITE);

            if (ret == FR_OK)
            {
                uart.printf("\r\n(WRITE) Open successful\r\n");

                sprintf(fileWriteBuffer, "Hello. I am the STM32.\n");
                ret = f_write(&SDFile, fileWriteBuffer, 23, &wbytes);

                if (ret == FR_OK)
                {
                    uart.printf("\r\nWrite successful\r\n");
                    f_close(&SDFile);
                }
                else
                {
                    uart.printf("\r\nWrite fail code: %x wbytes: %u\r\n", ret, wbytes);
                }
            }
            else
            {
                uart.printf("\r\nOpen fail code: %x\r\n", ret);
            }

            ret = f_open(&SDFile, "SDCard.txt", FA_READ);

            if (ret == FR_OK)
            {
                
                uart.printf("\r\n(READ) Open successful\r\n");

                ret = f_read(&SDFile, fileReadBuffer, 100, &numBytesRead);

                if (ret == FR_OK)
                {
                    uart.printf("\r\n=== File Contents ===\r\n");
                    uart.printf(fileReadBuffer);
                }
                else
                {
                    uart.printf("\r\nFailed to read file\r\n");
                }
            }
            else
            {
                uart.printf("\r\nFailed to open SDCard.txt\r\n");
            }
        }
        else
        {
            uart.printf("\r\nMount fail code: %x\r\n", ret);
        }

    }
}


void configureSDIO()
{
    hsd.Instance = SDIO;
    hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
    hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
    hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
    hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
    hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
    hsd.Init.ClockDiv = 5;


    GPIO_InitTypeDef GPIO_InitStruct = {0};
    
    /* SDIO clock enable */
    __HAL_RCC_SDIO_CLK_ENABLE();
  
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    /**SDIO GPIO Configuration    
    PC8     ------> SDIO_D0
    PC9     ------> SDIO_D1
    PC10     ------> SDIO_D2
    PC11     ------> SDIO_D3
    PC12     ------> SDIO_CK
    PD2     ------> SDIO_CMD 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_SDIO;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_SDIO;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}

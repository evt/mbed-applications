
#include "mbed.h"
#include "target_bms.h"
#include "io_uart_intf.h"

constexpr uint8_t SLAVE_INDEX = 0u;

int main()
{

    target::Bms & testBms = target::Bms::getInstance(); // Grab BMS target
    DEV::Ltc6811 * ltc = testBms.getLtc6811().first;   // Array of LTCs, 1st element of std::pair

    IO::uartIntf & uart = testBms.getUART();

    DEV::Ltc6811::ConfigRegister configRegister;

    configRegister.GPIO   = 1;
    configRegister.REFON  = 1;
    configRegister.DTEN   = 0;
    configRegister.ADCOPT = 0;
    // Dummy under and over voltage values
    configRegister.VUV    = 812; // 1.3V
    configRegister.VOV    = 2625;// 4.2V
    configRegister.DCC    = 0;
    configRegister.DCTO   = 0;

    uint16_t cellVoltages[12];

    DEV::Ltc6811::AdcStat result = DEV::Ltc6811::AdcStat::BUSY;
    
    while(1)
    {

        uart.printf("Starting LTC6811 Test Suite.\r\n");
        uart.printf("Any errors logged will appear here.\r\n");
        uart.printf("Enter the slave address in hex (0x0 - 0xF)\r\n");

        uint8_t slaveAddr = uart.getc();

        if(('0' <= slaveAddr) && ('9' >= slaveAddr))
        {
            slaveAddr -= '0';
        }
        else if(('a' <= slaveAddr) && ('f' >= slaveAddr))
        {
            slaveAddr -= 'a';
        }
        else if(('A' <= slaveAddr) && ('F' >= slaveAddr))
        {
            slaveAddr -= 'A';
        }
        else
        {
            uart.printf("Invalid address\r\n");
            continue;
        }
        ltc[SLAVE_INDEX].setAddress(slaveAddr);

        uart.printf("Starting Step 1...writing to config registers\r\n");

        // STEP 1
        // Write to config register. This function logs errors automatically.
        ltc[SLAVE_INDEX].writeConfig(&configRegister);
        uart.printf("Step 1 done. Starting step 2...toggling GPIOs\r\n");

        // STEP 2
        // Toggle some GPIOs
        uint8_t gpio = 0x01;
        for(uint8_t i = 0; i < 5; i++)
        {
            uart.printf("Turning on GPIO%d...", i+1);
            ltc[SLAVE_INDEX].toggleGPIO(gpio);
            ltc[SLAVE_INDEX].readConfig(&configRegister);

            if(configRegister.GPIO == gpio)
            {
                uart.printf("SUCCESS\r\n");
            }
            else
            {
                uart.printf("FAIL %d\r\n", configRegister.GPIO);
            }

            gpio |= (1 << (i+1));
        }

        uart.printf("Turning off GPIOs...");
        gpio = 0x00;
        ltc[SLAVE_INDEX].toggleGPIO(gpio);
        ltc[SLAVE_INDEX].readConfig(&configRegister);
        
        if(configRegister.GPIO == gpio)
        {
            uart.printf("SUCCESS\r\n");
        }
        else
        {
            uart.printf("FAIL\r\n");
        }

        uart.printf("Step 2 done. Starting step 3...measuring cells\r\n");
        uart.printf("A/D conversions started...Attempt limit 5\r\n");

        // STEP 3
        // Start A/D conversions
        ltc[SLAVE_INDEX].startADC(DEV::Ltc6811::AdcMode::MODE_NORMAL,
                     DEV::Ltc6811::DischargePermitted::NO,
                     DEV::Ltc6811::CellSelect::ALL_CELLS);


        // Poll the ADC if conversions are done
        uint8_t attempt = 1;
        while(DEV::Ltc6811::AdcStat::BUSY == result)
        {
            if(attempt > 5)
            {
                uart.printf("Maximum polling attempts exceeded.\r\n");
                break;
            }
            else
            {
                result = ltc[SLAVE_INDEX].pollADC();
            }
            attempt++;
            wait(0.001);
        }
        result = DEV::Ltc6811::AdcStat::BUSY;
        uart.printf("AD Conversions Done. Reading cell voltages...\r\n");
        ltc[SLAVE_INDEX].readCellVoltages(cellVoltages);

        for(uint8_t i = 0; i < 12; i++)
        {
            uart.printf("Cell %d:  %d\r\n", i, cellVoltages[i]);
        }


        uart.printf("Step 3 done. Starting step 4...reading voltage data from thermistors\r\n");

        uint16_t thermistorRead[5];
        //Loop through each thermistor for temperature data
        for(uint8_t i = 0; i < 13; i++)
        {
            gpio = (i+2) << 1; //GPIOs [5:2]
            ltc[SLAVE_INDEX].toggleGPIO(gpio);

            ltc[SLAVE_INDEX].startADCAux(DEV::Ltc6811::AdcMode::MODE_NORMAL,
                                         DEV::Ltc6811::GpioSelect::GPIO_1);

            uint8_t attempt = 1;
            while(DEV::Ltc6811::AdcStat::BUSY == result)
            {
                if(attempt > 5)
                {
                    uart.printf("Maximum polling attempts exceeded for GPIO%d.\r\n", 0);
                    break;
                }
                else
                {
                    result = ltc[SLAVE_INDEX].pollADC();
                }
                attempt++;
                wait(0.001);
            }
            result = DEV::Ltc6811::AdcStat::BUSY;
            ltc[SLAVE_INDEX].readAuxVoltages(thermistorRead);

            uart.printf("Thermistor %d: :%d\r\n", ((gpio>>1)-2), thermistorRead[0]);
        }

        uart.printf("Done.\r\n");

        wait(0.5);
    }


} // main





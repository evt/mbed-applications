# Test Procedure for LTC6811

## Required Hardware
* BMS Master Board
* BMS Slave Board (Address 0 preferred, else modify the address constexpr at the top of the test file)
* Wire harness for slave board (ISO SPI and cell voltages - 
    either from battery pack or 12-resistor voltage divider cicuit (~1.2k each) with 32V DC power supply)
* UART to USB adapter
* STLink for programming
* Saleae / logic analyzer (optional)

## Required Software
* Terminal emulator for viewing UART
* STM32CubeProgrammer for flashing boards
* Saleae Logic Software (optional)

## Procedure
1. Connect isoSPI between BMS master and slave, STLink to JTAG header, cell voltage harness to slave board
2. BMS external power can come from an Arduino or USB 5V breakout cable
3. Probe UART Tx and Rx at pins A9 and A10, respectively
4. Build the code with the following command ```python mbed_build.py test_ltc6811```
5. Flash with STM32CubeProgrammer
6. Observe output of UART on your terminal - errors and statuses will be logged here.
7. Verify cell and thermistor voltages with a multimeter
8. (Optional) Probe the master-side SPI bus and examine the data as you see fit.
    Commands are two bytes followed by two PEC bytes, data is read/written in 6 byte intervals
    followed by two PEC bytes. Command values are described in the dev_ltc6811.h header.

## Notes
* Any errors in PEC codes will appear as logged by the error manager as FATAL
* If polling the ADC fails, cell voltages should read 65535 (0xFFFF) for all cells
* Cell voltage units are in 100uV
* Cell voltages for the hobby battery packs are ~3.8 to 4.2V
* Thermistor voltage should read ~2.0V at room temperature




#include "target_nucleo_f302r8.h"
#include "io_i2c_f3xx_hal_adapter.h"
#include "io_i2c_intf.h"

int main()
{
    constexpr uint8_t SLAVE_ADDR = 4;
    char msg_1[] = "I2C Initial Test!";         // 17 chars
    char msg_2[] = "Requesting Data...";        // 19 chars
    char msg_success[] = "Data Received!";      // 14 chars
    char msg_failure[] = "Data Not Received!";  // 18 chars
    char buf[2];    // Space for the "OK" response

    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();
    IO::i2cIntf &i2cDriver = my_target.getI2C();

    i2cDriver.write(SLAVE_ADDR, msg_1, 17);
    i2cDriver.write(SLAVE_ADDR, msg_2, 19);
    i2cDriver.read(SLAVE_ADDR, buf, 2); // Try to read "OK"

    if(buf[0] == 'O' && buf[1] == 'K')
    {
        i2cDriver.write(SLAVE_ADDR, msg_success, 14);
    }
    else
    {
        i2cDriver.write(SLAVE_ADDR, msg_failure, 18);
    }

    for(;;) // Test over. Wait forever.
    {
        ; // null statement
    }
    
}
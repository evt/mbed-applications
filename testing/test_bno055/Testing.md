# Test Procedure for BNO055

## Features
* Initialize the BNO055
* Read and write BNO055 registers
* Get accelerometer data
* Get linear acceleration data
* Get gravity vector data
* Get quaternion position data

## Required Hardware for Test
* STM32F302R8 Nucleo Development Board
* Adafruit BNO055 board
* Two Pullup Resistors (About 4K Ohms)
* UART to USB dongle
* Breadboard and Wires for Hookup

## Required Additional Software for Test
* Terminal emulator (or some other way of reading/writing to UART)

## Steps to Complete the Test
1. Hook up the Adafruit BNO055 board according to the connections.txt file
2. Connect the UART to USB dongle
3. Build the executable with _python mbed_build.py -t test_bno055_
4. Drop the compiled 'test_bno055.bin' into the Nucleo's file system.
5. Open up the terminal emulator with 9600 8n1
6. Type "help" to get a list of commands.
7. Play around with the REPL and make sure that everything works.

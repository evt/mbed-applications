/*
 * file: test_bno055.cpp
 * purpose: 
 */

#include <cstring>
#include "mbed.h"
#include "dev_bno055.h"
#include "target_imu.h"
#include "io_gpio_mbed_adapter.h"

void REPL(DEV::bno055&);
void runCommand(char*, IO::uartIntf&, DEV::bno055&);
uint8_t hexToInt(char*);
uint8_t strToInt(char*);

namespace 
{
    constexpr int MAX_LINE = 100;
}

int main()
{
    target::Imu & my_target = target::Imu::getInstance();
    IO::uartIntf &uartDriver = my_target.getUART();

    // We have had issues with bno055 initialization in the past
    // So print a message before and after initializing the bno.
    uartDriver.puts("Starting bno055 test...\n\r");
    DEV::bno055 & my_bno = my_target.getBno055();
    uartDriver.puts("bno055 initialized.\n\r");

    // Once the bno055 is initialized, check the status to make
    // sure everything is good.
    uartDriver.puts("Checking the BNO055 status...\n\r");
    DEV::bno055::Status result = my_bno.getStatus();
    if (DEV::bno055::Status::ERROR == result)
    {
        uartDriver.puts("Error encountered. Attempting to run REPL regardless...\n\r");
    }
    else
    {
        uartDriver.puts("No error detected! Moving on to REPL...\n\r");
    }

    REPL(my_bno);

    for (;;) // Wait forever.
    {
    }
}

void REPL(DEV::bno055& bno)
{
    // The maximum characters per line
    char buf[MAX_LINE+1];
    int index = 0;

    target::Imu &target = target::Imu::getInstance();
    IO::uartIntf &uart = target.getUART();

    uart.printf("Entering the REPL!");
    uart.puts("\r\n>");

    while (1)
    {
        buf[index] = uart.getc();
        if (buf[index] == 0x08) // backspace
        {
            if (index > 0)
            {
                uart.putc(0x08);
                uart.putc(' ');
                uart.putc(0x08);
                --index;
            }
        }
        else if ((buf[index] == '\r') || (buf[index] == '\n'))
        {
            buf[index] = '\0';
            uart.puts("\r\n");
            index = 0;
            runCommand(buf, uart, bno);
            uart.puts(">");
        }
        else if (buf[index] == 0x1B) // An escape sequence
        {
            // Make sure it's an actual escape sequence
            if ('[' == uart.getc())
            {
                uart.putc(buf[index]);
                uart.putc('[');
                uart.putc(uart.getc());
            }
        }
        else if (index < MAX_LINE)
        {
            uart.putc(buf[index]);
            ++index;
        }
    }
}

void runCommand(char *cmd, IO::uartIntf &uart, DEV::bno055& bno)
{
    int16_t bno_data[4];
    char *tmp = strtok(cmd, " ");

    // Create a gpio to toggle for testing
    constexpr IO::PIN test_gpio = IO::PIN::MC_PC9;
    IO::GpioIntf &test_pin = IO::GpioMbedAdapter::getInstance<test_gpio>();
    test_pin.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    test_pin.writePin(IO::GpioIntf::STATE::LOW);

    if (0 == strncmp(tmp, "help", MAX_LINE))
    {
        uart.puts("List of valid commands:\n\r");
        uart.puts("  help  - Show this menu.\n\r");
        uart.puts("  acc   - Get raw accelerometer data.\n\r");
        uart.puts("  grv   - Get gravity vector data.\n\r");
        uart.puts("  lia   - Get linear accelerometer data.\n\r");
        uart.puts("  qua   - Get quaternion position data.\n\r");
        uart.puts("  cal   - Run calibration routine.\n\r");
        uart.puts("  dmc   - Writes dummy calibration.\n\r");
        uart.puts("  r     - Read values from the BNO055 registers.\n\r");
        uart.puts("  w     - Write values to the BNO055 registers.\n\r");
        uart.puts("  panic - Use in case of emergency.\n\r");
        uart.puts("\n\r");
    }
    // Get the accleration data
    else if (0 == strncmp(tmp, "acc", MAX_LINE))
    {
        bno_data[0] = bno.getAccelerometerX();
        bno_data[1] = bno.getAccelerometerY();
        bno_data[2] = bno.getAccelerometerZ();

        uart.printf("Accelerometer Data      X: %6d, Y: %6d, Z: %6d\n\r",
                    bno_data[0], bno_data[1], bno_data[2]);
    }
    // Get the gravity vector data
    else if (0 == strncmp(tmp, "grv", MAX_LINE))
    {
        bno_data[0] = bno.getGravityVectorX();
        bno_data[1] = bno.getGravityVectorY();
        bno_data[2] = bno.getGravityVectorZ();

        uart.printf("Gravity Vector Data     X: %6d, Y: %6d, Z: %6d\n\r",
                    bno_data[0], bno_data[1], bno_data[2]);
    }
    // Get the linear acceleration data
    else if (0 == strncmp(tmp, "lia", MAX_LINE))
    {
        bno_data[0] = bno.getLinearAccelerationX();
        bno_data[1] = bno.getLinearAccelerationY();
        bno_data[2] = bno.getLinearAccelerationZ();

        uart.printf("Linear Accleration Data X: %6d, Y: %6d, Z: %6d\n\r",
                    bno_data[0], bno_data[1], bno_data[2]);
    }
    // Get the quaternion position data
    else if (0 == strncmp(tmp, "qua", MAX_LINE))
    {
        bno_data[0] = bno.getQuaternionPositionW();
        bno_data[1] = bno.getQuaternionPositionX();
        bno_data[2] = bno.getQuaternionPositionY();
        bno_data[3] = bno.getQuaternionPositionZ();

        uart.printf("Quaternion Data         X: %6d, Y: %6d, Z: %6d, W: %6d\n\r",
                    bno_data[1], bno_data[2], bno_data[3], bno_data[0]);
    }
    // Perform calibration procedure.
    // Stops when all calibrations are at quality level 3.
    else if (0 == strncmp(tmp, "cal", MAX_LINE))
    {
        DEV::bno055::CalibrationStatus calStat = {0};
        DEV::bno055::CalibrationInfo calInfo = {0};

        while((calStat.sysCal != 3) || (calStat.accelCal != 3)
              || (calStat.magCal != 3) || (calStat.gyroCal != 3))
        {
            bno.readCalibration(calInfo);
            bno.getCalibrationStatus(calStat);

            uart.printf("Calibration Settings:\n\r");
            uart.printf("  Accel X : %d\n\r", calInfo.accel_x);
            uart.printf("  Accel Y : %d\n\r", calInfo.accel_y);
            uart.printf("  Accel Z : %d\n\r", calInfo.accel_z);
            uart.printf("\n\r");
            uart.printf("  Mag X : %d\n\r", calInfo.mag_x);
            uart.printf("  Mag Y : %d\n\r", calInfo.mag_y);
            uart.printf("  Mag Z : %d\n\r", calInfo.mag_z);
            uart.printf("\n\r");
            uart.printf("  Gyro X : %d\n\r", calInfo.gyro_x);
            uart.printf("  Gyro Y : %d\n\r", calInfo.gyro_y);
            uart.printf("  Gyro Z : %d\n\r", calInfo.gyro_z);
            uart.printf("\n\r");
            uart.printf("  Accel Radius : %d\n\r", calInfo.accel_radius);
            uart.printf("  Mag Radius : %d\n\r", calInfo.mag_radius);
            uart.printf("\n\r");
            uart.printf("\n\r");

            uart.printf("Cal Status: Sys: %u Accel: %u Mag: %u, Gyro: %u\n\r",
                        calStat.sysCal, calStat.accelCal, calStat.magCal, calStat.gyroCal);
            wait(0.5);
        }
    }
    // Write some dummy calibration data
    else if (0 == strncmp(tmp, "dmc", MAX_LINE))
    {
        DEV::bno055::CalibrationInfo calInfo1;

        calInfo1.accel_x = -1;
        calInfo1.accel_y = -2;
        calInfo1.accel_z = -3;

        calInfo1.mag_x = 100;
        calInfo1.mag_y = 200;
        calInfo1.mag_z = 300;

        calInfo1.gyro_x = -1234;
        calInfo1.gyro_y = 1111;
        calInfo1.gyro_z = 5;

        calInfo1.accel_radius = 1111;
        calInfo1.mag_radius = -234;

        bno.loadCalibration(calInfo1);

    }
    // Read from a register
    else if (0 == strncmp(tmp, "r", MAX_LINE))
    {
        test_pin.writePin(IO::GpioIntf::STATE::HIGH);
        uint8_t addr = 0;
        uint8_t len = 0;

        tmp = strtok(NULL, " ");
        if (NULL == tmp)
        {
            uart.puts("Usage: r addr len\n\r");
            uart.puts("  addr - register address in 0x__ format.\n\r");
            uart.puts("  len  - The number of bytes to read from the BNO055.\n\r");
            return;
        }

        addr = hexToInt(tmp);

        tmp = strtok(NULL, " ");
        if (NULL != tmp)
        {
            len = strToInt(tmp);
            if (len > MAX_LINE)
            {
                uart.printf("ERROR: len must be less than %d!\n\r", MAX_LINE);
                return;
            }
        }
        else
        {
            uart.puts("Usage: r addr len\n\r");
            uart.puts("  addr - register address in 0x__ format.\n\r");
            uart.puts("  len  - The number of bytes to read from the BNO055.\n\r");
        }

        bno.readReg(addr, (uint8_t *) cmd, len);

        for (int i = 0; i != len; ++i)
        {
            uart.printf("0x%02X: 0x%02X\n\r", i+addr, *cmd);
            ++cmd;
        }
        test_pin.writePin(IO::GpioIntf::STATE::LOW);
    }
    // Write to a register
    else if (0 == strncmp(tmp, "w", MAX_LINE))
    {
        test_pin.writePin(IO::GpioIntf::STATE::HIGH);
        uint8_t addr = 0;
        uint8_t value = 0;

        tmp = strtok(NULL, " ");
        if (NULL == tmp)
        {
            uart.puts("Usage: w addr value\n\r");
            uart.puts("  addr  - register address in 0x__ format.\n\r");
            uart.puts("  value - The byte to write in 0x__ format.\n\r");
            return;
        }

        addr = hexToInt(tmp);

        tmp = strtok(NULL, " ");
        if (NULL == tmp)
        {
            uart.puts("Usage: w addr value\n\r");
            uart.puts("  addr  - register address in 0x__ format.\n\r");
            uart.puts("  value - The byte to write in 0x__ format.\n\r");
            return;
        }
        
        value = hexToInt(tmp);

        bno.writeReg(addr, value);
        bno.readReg(addr, &value, 1);

        uart.printf("0x%02X: 0x%02X\n\r", addr, value);
        test_pin.writePin(IO::GpioIntf::STATE::LOW);
    }
    else if (0 == strncmp(tmp, "panic", MAX_LINE))
    {
        uart.puts("You got this! \\(^.^)/\n\r");
    }
    else
    {
        uart.printf("Unrecognized command: %s\n\r", cmd);
    }
}

// hex must be in 0x__ format
uint8_t hexToInt(char *str)
{
    uint8_t result = 0;
    str += 2;

    if ('0' <= *str && *str <= '9')
    {
        result += (*str - '0') << 4;
    }
    else if ('a' <= *str && *str <= 'f')
    {
        result += ((*str - 'a') + 10) << 4;
    }
    else if ('A' <= *str && *str <= 'F')
    {
        result += ((*str - 'A') + 10) << 4;
    }
    str += 1;
    if ('0' <= *str && *str <= '9')
    {
        result += (*str - '0');
    }
    else if ('a' <= *str && *str <= 'f')
    {
        result += ((*str - 'a') + 10);
    }
    else if ('A' <= *str && *str <= 'F')
    {
        result += ((*str - 'A') + 10);
    }
    return result;
}

uint8_t strToInt(char *str)
{
    uint8_t result = 0;
    char *tmp = str;

    while (('0' <= *str) && (*str <= '9'))
    {
        result *= 10;
        result += (*tmp - '0');
        ++str;
    }
    return result;
}

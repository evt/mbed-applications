# Test Plan for GPIO F3xx HAL Adapter

## Features
* Tests the IO wrapper for the STM32 F3xx HAL GPIO driver

## Required Hardware for Test
* STM32 NUCLEO_F302R8
* Jumper wire(s)
* Saleae (Optional)

## Additional Required Software
* Minicom, gtkterm, putty or your favourite serial terminal emulator
* Saleae Logic (Optional)

## Steps to Complete Test
1. Connect a jumper between pins PA5 and PA6.
2. Build the test executable 'python mbed_build.py test_f3xx_gpio'.
3. Flash the Nucleo board with 'test_f3xx_gpio.bin'.
4. Open your terminal emulator with 115200 8N1 baud settings.
5. A welcome message should be printed, as well as a message stating an IRQ was registered to PA5.
6. (Optional) Probe PA6 with the Saleae and verify a 5Hz square wave with 50% duty cycle.
7. In the terminal emulator, verify that the rising edge counter increments ~5 times per second.

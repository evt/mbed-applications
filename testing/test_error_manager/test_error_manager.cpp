/*
 * file: test_error_manager.cpp
 * purpose: test functionality of the error_manager
 */

#include "mbed.h"
#include "target_nucleo_f302r8.h"
#include "error_manager.h"

int main()
{
    //get nucleo target
    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();
    
    //create an error manager with the target
    error_manager::setTarget(&my_target);

    //test all levels of error
    error_manager::logError("test INFO", target::ErrorLevel::INFO);
    error_manager::logError("test WARNING", target::ErrorLevel::WARNING);
    error_manager::logError("test ERROR", target::ErrorLevel::ERROR);
    error_manager::logError("test FATAL", target::ErrorLevel::FATAL);
    error_manager::logError("test DEBUG", target::ErrorLevel::DEBUG);
}

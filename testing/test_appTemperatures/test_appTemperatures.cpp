#include "mbed.h"
#include "mc_can.h"
#include "app_bmsTemperature.h"
#include "target_bms.h"



int main()
{
    target::Bms & my_target = target::Bms::getInstance();
    APP::CellTemperature my_app(my_target.getLtc6811().first,
                                my_target.getEvtCan(),
                                1,
                                10); // 100ms (10 ticks)

    MC::can_setMode(MC::CanMode::NORMAL, true);

    my_app.init();

    while(1)
    {
        my_app.run();
        wait(0.1);
    }


}


/*
 * file: test_task_manager.cpp
 * purpose: Creates a sample application and runs a blink app.
 */

#include "app_intf.h"
#include "mbed.h"
#include "target_nucleo_f302r8.h"
#include "task_manager.h"


class Blink : public APP::ApplicationIntf
{

public:

    Blink(DEV::LED & led, unsigned int tick_period) : my_led(led), my_run_period(tick_period)
    {}
    ~Blink() {}

    void init() override;

    uint8_t run() override;

    unsigned int getRunSchedule() override;

private:

    DEV::LED & my_led;
    unsigned int const my_run_period;
};


void Blink::init()
{
    return;
}


uint8_t Blink::run()
{
    my_led.toggle();
    return 0;
}


unsigned int Blink::getRunSchedule()
{
    return my_run_period;
}


int main()
{
    target::nucleo_f302r8 & nucleo = target::nucleo_f302r8::getInstance();
    DEV::LED & blinking_led = nucleo.getLED();
    IO::uartIntf & uart_driver = nucleo.getUART();
    // use the blink app
    // give it an led from the Bms
    Blink blink_application = Blink(blinking_led, 100);
    Blink blink_faster_application = Blink(blinking_led, 10);
    APP::ApplicationIntf * test_apps[2] = {&blink_application,
                                           &blink_faster_application};
    TaskManager<2> my_task_master = TaskManager<2>(test_apps);
    my_task_master.init();
    uart_driver.printf("Starting Task Manager Test");

    while (1)
    {
        my_task_master.run();
        wait_ms(10);
    }
}

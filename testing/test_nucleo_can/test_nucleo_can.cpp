/*
 * file: test_nucleo_can.cpp
 * purpose: Hardware test for the IO layer CAN driver on the Nucleo F302.
 */

#include "mbed.h"
#include "target_nucleo_f302r8.h"
#include "evt_protocols_common.h"
// Only included so it can be put in loopback mode
#include "mc_can.h"

namespace
{

uint8_t fakeData[] = {0xDE, 0xAD, 0xBE, 0xEF};
uint32_t id = 1337;
uint8_t dlc = 4;


uint8_t responseData[8];
// Dummy filter to accept all CAN messages
MC::CanFilter my_filter((id << 3),
                        0,
                        (0x000 << 3),
                        0,
                        MC::CanFilterPosition::POS_0,
                        MC::CanRxMailbox::RX_0,
                        MC::CanFilterMode::ID_MASK_32_BIT);

}

void printMessageContents(PROTOCOL::CanMessage * responseMsg, IO::CanIntf & my_can_driver, IO::uartIntf & my_uart_driver);

int main()
{
    // Initialize
    target::nucleo_f302r8 & my_target = target::nucleo_f302r8::getInstance();
    IO::CanIntf & my_can_driver = my_target.getCAN();
    IO::uartIntf & my_uart_driver = my_target.getUART();

    // Set into loopback mode for purposes of bench test
    MC::can_setMode(MC::CanMode::NORMAL, true);
    MC::can_configFilter(my_filter);

    my_uart_driver.baud(115200);

    PROTOCOL::CanMessage testMessage(id, fakeData, dlc);

    PROTOCOL::CanMessage * responseMsg;

    while (true)
    {

        my_uart_driver.printf("Press S to send or R to receive.\r\n");

        char command = my_uart_driver.getc();

        if (('s' == command) ||
            ('S' == command) ||
            ('r' == command) ||
            ('R' == command))
        {
            my_uart_driver.printf(" 1. Message ID 1337\r\n");
            my_uart_driver.printf(" 2. Message ID 1338\r\n");
            my_uart_driver.printf(" 3. Message ID 1339\r\n");
        }
        else
        {
            my_uart_driver.printf("Command Error.\r\n");
            continue;
        }


        char messageSelection = my_uart_driver.getc();

        switch (messageSelection)
        {
            case '1':

                if (command == 's' || command == 'S')
                {
                    testMessage.id = 1337;
                    my_can_driver.transmit(testMessage);
                    my_uart_driver.printf("\r\nTransmission occurred.\r\n\r\n");
                }
                else if (command == 'r' || command == 'R')
                {
                    responseMsg = my_can_driver.receive(1337);
                    printMessageContents(responseMsg, my_can_driver, my_uart_driver);
                }

                break;

            case '2':
                if (command == 's' || command == 'S')
                {
                    testMessage.id = 1338;
                    my_can_driver.transmit(testMessage);
                    my_uart_driver.printf("\r\nTransmission occurred.\r\n\r\n");
                }
                else if (command == 'r' || command == 'R')
                {
                    responseMsg = my_can_driver.receive(1338);
                    printMessageContents(responseMsg, my_can_driver, my_uart_driver);
                }

                break;

            case '3':
                if (command == 's' || command == 'S')
                {
                    testMessage.id = 1339;
                    my_can_driver.transmit(testMessage);
                    my_uart_driver.printf("\r\nTransmission occurred.\r\n\r\n");
                }
                else if (command == 'r' || command == 'R')
                {
                    responseMsg = my_can_driver.receive(1339);
                    printMessageContents(responseMsg, my_can_driver, my_uart_driver);
                }

                break;

            default:
                my_uart_driver.printf("\r\nCommand Error\r\n\r\n");
        } // switch (messageSelection)
    } // while (true)
} // main()

void printMessageContents(PROTOCOL::CanMessage * responseMsg, IO::CanIntf & my_can_driver, IO::uartIntf & my_uart_driver)
{
    if (nullptr != responseMsg)
    {
        my_uart_driver.printf("\r\nMessage received!\r\n");
        my_uart_driver.printf("    ID : %u\n\r", responseMsg->id);
        my_uart_driver.printf("    DLC: %u\n\r", responseMsg->dataLengthCode);

        for(uint8_t i = 0; i < responseMsg->dataLengthCode; i++)
            my_uart_driver.printf("    data[%u]: %x\n\r", i, responseMsg->packet[i]);
    }
    else
    {
        my_uart_driver.printf("\r\nReceive error\r\n\r\n");
    }
}

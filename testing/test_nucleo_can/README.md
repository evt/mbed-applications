# Testing IO CAN on Nucleo

## Hardware Requirements
* STM32F302 Nucleo board
* Female-to-Female Jumper
* Saleae Logic Analyzer / Oscilloscope
* USB/TTL FTDI Cable

1. Hook up the male-to-male wire to pins B8 and B9.
2. Connect your USB adapter to pins C10 and C11.
3. Set the protocol of your logic analyzer to CAN at 250 kbps.
4. Verify that the CANTX pin is writing data.
    1. Message identifier: 1337 (0x539)
    2. Data: 0xDE, 0xAD, 0xBE, 0xEF
5. Check the UART output.
    1. A message "Transmission occurred." indicates transmission is working.

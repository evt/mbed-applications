/*
 * file: test_gpio_toggle_time.cpp
 * This file is to test the time it takes to toggle a gpio pin
 */

#include "io_gpio_mbed_adapter.h"

int main()
{

    auto gpio = IO::GpioMbedAdapter::getInstance<IO::PIN::MC_PC8>(); //GPIO on nucleo
    gpio.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    gpio.writePin(IO::GpioIntf::STATE::LOW);
    
    while(1)
    {
        gpio.writePin(IO::GpioIntf::STATE::LOW);
        gpio.writePin(IO::GpioIntf::STATE::HIGH);

    }
}

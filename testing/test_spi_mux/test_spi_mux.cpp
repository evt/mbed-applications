/*
 * file: test_spi_mux.cpp
 * purpose: Tests blinking an LED on the STM32 Nucleo development board using
 *          the Mbed peripheral adapters.
 */

#include "mbed.h"
#include "io_spi_mbed_adapter.h"
#include "dev_sn74cb3q3253.h"
#include "io_gpio_mbed_adapter.h"
#include "io_gpio_intf.h"
#include "target_sgm.h"

int main()
{
    target::Sgm &my_target = target::Sgm::getInstance();
    DEV::Sn74cb3q3253 my_sn74cb3q3253 = my_target.getMUX();

    IO::SpiIntf &my_spi = my_target.getSPI();


    IO::GpioIntf &chipSelect = my_target.getChipSelect();

    while(1)
    {
        uint8_t test1[] = "Test 1";
        my_sn74cb3q3253.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT0);

        my_spi.setChipSelectLow(chipSelect);
        my_spi.write(test1, 7);
        my_spi.setChipSelectHigh(chipSelect);

        wait_ms(250);

        uint8_t test2[] = "Test 2";
        my_sn74cb3q3253.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT1);

        my_spi.setChipSelectLow(chipSelect);
        my_spi.write(test2, 7);
        my_spi.setChipSelectHigh(chipSelect);

        wait_ms(250);

        uint8_t test3[] = "Test 3";
        my_sn74cb3q3253.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT2);

        my_spi.setChipSelectLow(chipSelect);
        my_spi.write(test3, 7);
        my_spi.setChipSelectHigh(chipSelect);

        wait_ms(250);

        uint8_t test4[] = "Test 4";
        my_sn74cb3q3253.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT3);

        my_spi.setChipSelectLow(chipSelect);
        my_spi.write(test4, 7);
        my_spi.setChipSelectHigh(chipSelect);

        wait_ms(250);
    }
}

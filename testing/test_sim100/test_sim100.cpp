#include "dev_sendyne_sim100.h"
#include "target_tms.h"
#include "io_uart_intf.h"
#include "mc_can.h"
#include "dev_led.h"


/**
 * Handles printing the status structure to the uart interface
 * 
 * @param status The Status Structure to display on uart
 * @param uartIntf The uart interface used to display the structure
 **/
void printStatus(DEV::Sim100::Status & status, IO::uartIntf & uartIntf)
{
    uartIntf.printf("Printing Status...\n\r");
    uartIntf.printf("Hardware Error: %d\n\r", status.hardwareError);
    uartIntf.printf("No New Estimates: %d\n\r", status.noNewEstimates);
    uartIntf.printf("High Uncertainty: %d\n\r", status.highUncertainty);
    uartIntf.printf("High Battery Voltage: %d\n\r", status.highBatteryVoltage);
    uartIntf.printf("Low Battery Voltage: %d\n\r", status.lowBatteryVoltage);
    uartIntf.printf("Isolation Warning: %d\n\r", status.isoltationWarning);
    uartIntf.printf("Isolation Fault: %d\n\r", status.isolationFault);
}


int main()
{
    // Get interfaces needed for the test
    target::Tms & my_target = target::Tms::getInstance();
    IO::CanIntf & canIntf = my_target.getCAN();
    IO::uartIntf & uartIntf = my_target.getUART();
    DEV::LED & my_LED = my_target.getLED();
    my_LED.toggle();

    // Setup CAN
    uint32_t canResponseId = 0xA100100;
    // Only accept messages that match the Sendyne ID 
    MC::CanFilter my_filter((canResponseId << 3), (canResponseId & 0x1FFFE000) >> 13, 0xFFF8, 0xFFFF, MC::CanFilterPosition::POS_0, MC::CanRxMailbox::RX_0, MC::CanFilterMode::ID_MASK_32_BIT);
    MC::can_configFilter(my_filter);

    // Setup for testing the Sendyne with structures to populate with data
    DEV::Sim100 my_sim(canIntf);
    DEV::Sim100::IsolationState isolationState;
    DEV::Sim100::IsolationResistances isolationResistances;
    DEV::Sim100::IsolationCapacitances isolationCapacitances;
    DEV::Sim100::IsolationVoltages isolationVoltages;
    DEV::Sim100::BatteryVoltages batteryVoltages;
    DEV::Sim100::ErrorFlags errorFlags;
    char partName[17];

    // The input that will be used for the command
    char input;

    while(true)
    {
        uartIntf.printf(">> ");
        input = uartIntf.getc();
        uartIntf.putc(input);
        uartIntf.printf("\n\r\n");

        switch(input)
        {
            case 'i':
            case 'I':
                my_sim.readIsolationState(isolationState);
                printStatus(isolationState.status, uartIntf);
                uartIntf.printf("Electric Isolation: %d\n\r", isolationState.electricIsolation);
                uartIntf.printf("Electric Uncertainty: %d\n\r", isolationState.isolationUncertainty);
                uartIntf.printf("Energy Stored: %d\n\r", isolationState.energyStored);
                uartIntf.printf("Stored Uncertainty: %d\n\r", isolationState.storedUncertainty);
                break;
            case 'r':
            case 'R':
                uartIntf.printf("Getting Isolation Resistances...\n\r");
                my_sim.readIsolationResistances(isolationResistances);
                printStatus(isolationCapacitances.status, uartIntf);
                uartIntf.printf("Resistance P: %d\n\r", isolationResistances.resistanceP);
                uartIntf.printf("Resistance P Uncertainty: %d\n\r", isolationResistances.resistancePUncertainty);
                uartIntf.printf("Resistance N: %d\n\r", isolationResistances.resistanceN);
                uartIntf.printf("Resistance N Uncertainty: %d\n\r", isolationResistances.resistanceNUncertainty);
                break;
            case 'c':
            case 'C':
                uartIntf.printf("Getting Isolation Capacitances...\n\r");
                my_sim.readIsolationCapacitances(isolationCapacitances);
                printStatus(isolationCapacitances.status, uartIntf);
                uartIntf.printf("Capacitance P: %d\n\r", isolationCapacitances.capacitanceP);
                uartIntf.printf("Capacitance P Uncertainty: %d\n\r", isolationCapacitances.capacitancePUncertainty);
                uartIntf.printf("Capacitance N: %d\n\r", isolationCapacitances.capacitanceN);
                uartIntf.printf("Capacitance N Uncertainty: %d\n\r", isolationCapacitances.capacitanceNUncertainty);
                break;
            case 'v':
            case 'V':
                uartIntf.printf("Getting Isolation Voltages...\n\r");
                my_sim.readIsolationVoltages(isolationVoltages);
                printStatus(isolationVoltages.status, uartIntf);
                uartIntf.printf("Isolation Voltage P: %d\n\r", isolationVoltages.voltageP);
                uartIntf.printf("Isolation Voltage P Uncertainty: %d\n\r", isolationVoltages.voltagePUncertainty);
                uartIntf.printf("Isolation Voltage N: %d\n\r", isolationVoltages.voltageN);
                uartIntf.printf("Isoltion Voltage N Uncertainity: %d\n\r", isolationVoltages.voltageNUncertainty);
                break;
            case 'b':
            case 'B':
                uartIntf.printf("Getting Battery Voltages...\n\r");
                my_sim.readBatteryVoltages(batteryVoltages);
                printStatus(batteryVoltages.status, uartIntf);
                uartIntf.printf("Battery Voltage: %d\n\r", batteryVoltages.batteryVoltage);
                uartIntf.printf("Battery Voltage Uncertainty: %d\n\r", batteryVoltages.batteryVoltageUncertainty);
                uartIntf.printf("Battery Voltage Max: %d\n\r", batteryVoltages.batteryVoltageMax);
                uartIntf.printf("Battery Voltage Max Uncertainty: %d\n\r", batteryVoltages.batteryVoltageMaxUncertainty);
                break;
            case 'p':
            case 'P':
                uartIntf.printf("Getting Part Name...\n\r");
                my_sim.readPartName(partName);
                for(int i = 0; i < 12; i ++) {
                    uartIntf.printf("Val: %c\n\r", partName[i]);
                }
                uartIntf.printf("Part Name: %s\n\r", partName);
                break;
            case 'e':
            case 'E':
                uartIntf.printf("Getting Error Flags...\n\r");
                my_sim.readErrorFlags(errorFlags);
                printStatus(errorFlags.status, uartIntf);
                uartIntf.printf("VX2 Broken Connections: %d\n\r", errorFlags.vx2Broken);
                uartIntf.printf("VX1 Broken Connections: %d\n\r", errorFlags.vx1Broken);
                uartIntf.printf("Chassis Connection Broken: %d\n\r", errorFlags.chassisConnectionBroken);
                uartIntf.printf("VX1 VX2 Reversed: %d\n\r", errorFlags.vx1Vx2Reversed);
                uartIntf.printf("Power Supply Out of Range: %d\n\r", errorFlags.powerSupplyOutOfRange);
                break;
            case '\n':
                break;
            default:
                uartIntf.printf("Invalid Command\n\r");
        }
    }
}
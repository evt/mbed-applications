# Test Plan for Dev-stwd100 Driver

## Features
* Keeps watchdog from timing out and also times out the watchdog to reset the MC

## Required Hardware for Test
* Saleae or other logic analyzer
* TMS
* STM32 Programmer

## Additional Required Software
* STM32CubeProgrammer
* Saleae's "Logic" if testing with Saleae

## Steps to Complete the Test using the TMS
1. Connect probe of logic analyzer to pin on WDI pin on on the TMS WD header
2. (Optional) Connect another probe to the WDO on the watchdog
3. Make sure the code in the test file is using PA8 as the wdi
4. Compile
5. Program the board through JTAG using the STM Programmer
6. Run the logic analyzer and you should see it pet the GPIO every couple of seconds and then see the WDO get toggled on the third WDI pet
/*
 * file: test_stwd100.cpp
 * This file is to test the watchdog on the TMS
 */

#include "dev_stwd100.h"
#include "io_gpio_mbed_adapter.h"
#include "mbed.h"

int main()
{

    auto wdi = IO::GpioMbedAdapter::getInstance<IO::PIN::MC_PA8>(); // WDI on TMS

    DEV::Stwd100 watchdog = DEV::Stwd100(wdi);  // creates the genereic mux using the selects

    while(1)
    {

        for (int i = 0; i < 3; i++) // Resets the timer at normal intervals 3 times
        {
            watchdog.pet();
            wait(1);    
        }
        wait(2);    // Should cause watchdog to timeout 
    }
}

/*
 * file: test_f3xx_flash.cpp
 *
 * Test for the IO-layer wrapper for STM32F3xx_HAL_FLASH.
 *
 */

#include "io_flash_f3xx_hal_adapter.h"
#include "target_nucleo_f302r8.h"
#include "io_uart_mbed_adapter.h"

int main()
{ 
    auto &my_stm = target::nucleo_f302r8::getInstance();
    auto & my_uart = my_stm.getUART();
    my_uart.baud(115200);

    auto & my_flash = my_stm.getFlash();
    
    my_flash.erasePage(PAGE31);
    my_flash.erasePage(PAGE30);

    uint32_t address1 = PAGE31;
    uint32_t address2 = PAGE31+sizeof(uint64_t);
    uint32_t address3 = PAGE31+sizeof(uint64_t)+sizeof(uint32_t);

    my_uart.printf("Writing doubleword to Page31...\r\n");
    my_flash.write(static_cast<uint32_t>(IO::FlashF3xxHalAdapter::ProgramMode::DOUBLEWORD), 0xDEADBEEF12345678, address1);
    wait_ms(.01);
    my_uart.printf("Writing word to Page31...\r\n");
    my_flash.write(static_cast<uint32_t>(IO::FlashF3xxHalAdapter::ProgramMode::WORD), 0xFACE9876, address2);
    wait_ms(.01);
    my_uart.printf("Writing halfword to Page31...\r\n\r\n");
    my_flash.write(static_cast<uint32_t>(IO::FlashF3xxHalAdapter::ProgramMode::HALFWORD), 0xFEED, address3);
    wait_ms(.01);

    my_uart.printf("Reading doubleword from Page31:\r\n");
    // printf doesnt see mto be able to print 64 bit
    my_uart.printf("%X", my_flash.read(reinterpret_cast<uint32_t *>(address1+sizeof(uint32_t))));
    my_uart.printf("%X\r\n\n", my_flash.read(reinterpret_cast<uint64_t *>(address1)));
    wait_ms(.01);

    my_uart.printf("Reading word from Page31:\r\n");
    my_uart.printf("%X\r\n\n", my_flash.read(reinterpret_cast<uint32_t *>(address2)));
    wait_ms(.01);
    
    my_uart.printf("Reading halfword from Page31:\r\n");
    my_uart.printf("%X\r\n\n", my_flash.read(reinterpret_cast<uint16_t *>(address3)));
    wait_ms(.01);

    address1 = PAGE30;
    address2 = PAGE30+sizeof(uint64_t);
    address3 = PAGE30+sizeof(uint64_t)+sizeof(uint32_t);

    my_uart.printf("Swapping Page31 and Page30:\r\n\r\n");
    my_flash.swapPage(PAGE30, PAGE31);

    my_uart.printf("Reading doubleword from Page30:\r\n");
    // printf doesnt see mto be able to print 64 bit
    my_uart.printf("%X", my_flash.read(reinterpret_cast<uint32_t *>(address1+sizeof(uint32_t))));
    my_uart.printf("%X\r\n\n", my_flash.read(reinterpret_cast<uint64_t *>(address1)));
    wait_ms(.01);

    my_uart.printf("Reading word from Page30:\r\n");
    my_uart.printf("%X\r\n\n", my_flash.read(reinterpret_cast<uint32_t *>(address2)));
    wait_ms(.01);
    
    my_uart.printf("Reading halfword from Page30:\r\n");
    my_uart.printf("%X\r\n\n", my_flash.read(reinterpret_cast<uint16_t *>(address3)));
    wait_ms(.01);

    my_uart.printf("Done.\r\n\r\n");

}

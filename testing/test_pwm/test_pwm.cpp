/*
 * file: test_pwm.cpp
 * purpose: simulate a pwm for different values of duty cycle and period
 */


#include <cstdint>
#include "mbed.h"
#include "io_pwm_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"

int main()
{
    constexpr int SIM_TIME = 3;
    // Configurable PWM pin 
    constexpr auto PWM_PIN = IO::PIN::MC_PA6;
    auto &my_pwm = IO::pwmMbedAdapter::getInstance<PWM_PIN>();

    //just cycles through different duty cycles with different periods
    while (true)
    {
        for (int i = 0; i < 5; i++)
        {
            switch (i)
            {
                case 0:
                    my_pwm.setPeriodMicroSeconds(100);
                    my_pwm.setDutyCycle(0.0);
                    break;

                case 1:
                    my_pwm.setPeriodMicroSeconds(30);
                    my_pwm.setDutyCycle(0.25);
                    break;

                case 2:
                    my_pwm.setPeriodMicroSeconds(40);
                    my_pwm.setDutyCycle(0.50);
                    break;

                case 3:
                    my_pwm.setPeriodMicroSeconds(100);
                    my_pwm.setDutyCycle(0.75);
                    break;

                case 4:
                    my_pwm.setPeriodMicroSeconds(200);
                    my_pwm.setDutyCycle(1.0);
                    break;

                default:
                    break;
            }
            wait(SIM_TIME);
        }
    }
}

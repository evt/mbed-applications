# Test Plan for io_pwm Driver

## Features
* Tests the I/O wrapper for a PWM

## Required Hardware for Test
* STM32 Nucleo F302R8
* Oscilliscope or other digital analyzer (Saleae was used)

## Additional Required Software
* Saleae's "Logic" if using a Saleae

## Steps to Complete the Test
1. Probe MC_PA6
2. Flash the board
3. Read the pin
4. To see full simulation, run for about 25 seconds.
5. Waveform should start low for ~100ms (0% duty cycle), then, the period 
   should be the smallest at 25% duty cycle, and increase per 25% duty cycle
   until it gets to 100% duty cycle (high signal) where the period is 200ms.
   For each different duty cycle/period, it should simulate for ~3 seconds   (arbitrary).
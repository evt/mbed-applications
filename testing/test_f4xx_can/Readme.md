# Test Plan for io_can_f4xx Driver

## Features
* Tests the I/O wrapper for the STM32F4xx HAL CAN driver

## Required Hardware for Test
* STM32 NUCLEO_F446RE
* Serial USART to USB adapter
* Assorted jumper wires

## Additional Required Software
* Minicom, gtkterm, putty or your favourite serial terminal emulator
* Saleae Logic (Optional)

## Steps to Complete Test
1. Connect the USART to USB adapter to USART Tx/Rx (PA0/PA1) on the Nucleo. Configure a serial terminal with 115200 8N1 baud settings.
2. Set the CAN interface to use loopback mode
3. Build the test with 'python mbed_build.py test_f4xx_can'
4. Flash the Nucleo with test_f4xx_can.bin.
5. Verify that a welcome message is printed to the terminal.
6. For each of the CAN interfaces, transmit one message of each available ID.
7. Read the messages from the receive buffer and verify the data is correct.
8. Try sending and receiving random CAN messages from the terminal and verify their reception.
9. Verify that reading from an empty slot (or reading from the same slot twice consecutively) produces an error message.

/*
 * file: test_f4xx_can.cpp
 * purpose: Functional test for the io_can_f4xx_hal_adapter
 */

#include "target_nucleo_f446re.h"
#include "io_can_intf.h"
#include "io_uart_intf.h"
#include "evt_protocols_common.h"
#include "mbed.h"

namespace
{

uint32_t id_1 = 0x1;
uint8_t dlc_1 = 8;
uint8_t payload_1[8] = {0xDE, 0xAD, 0xBE, 0xEF, 0x00, 0x11, 0x22, 0x33};

uint32_t id_2 = 0x2;
uint8_t dlc_2 = 5;
uint8_t payload_2[5] = {0xAA, 0x55, 0xAA, 0x55, 0xAA};

uint32_t id_3 = 0x3;
uint8_t dlc_3 = 5;
uint8_t payload_3[5] = {0x12, 0x34, 0x56, 0x78, 0x90};

uint32_t id_4 = 0x4;
uint8_t dlc_4 = 4;
uint8_t payload_4[4] = {0xD1, 0x5E, 0xA5, 0xE5};

} // anonymous namespace

void printMainMenu(IO::uartIntf & uart);
void printTxSubmenu(IO::uartIntf & uart);
void printRxSubmenu(IO::uartIntf & uart);
void printMessageContents(PROTOCOL::CanMessage * responseMsg, IO::uartIntf & my_uart_driver);

int main()
{
    target::nucleo_f446re & target = target::nucleo_f446re::getInstance();
    IO::CanIntf & can1 = target.getCAN1();
    IO::CanIntf & can2 = target.getCAN2();

    IO::uartIntf & uart = target.getUART();

    uart.baud(115200);

    PROTOCOL::CanMessage txMessage;
    PROTOCOL::CanMessage * rxMessage;
    IO::CanIntf * currentCAN;

    uart.printf("\r\n============================================\r\n");
    uart.printf("\r\n\r\nWelcome to the test for IO F4xx CAN!\r\n\r\n");
    uart.printf("\r\n============================================\r\n");
    while(true)
    {

        printMainMenu(uart);
        char tmp1 = uart.getc();

        switch(tmp1)
        {
            case '1':
                printTxSubmenu(uart);
                currentCAN = &can1;
                break;
            case '2':
                printRxSubmenu(uart);
                currentCAN = &can1;
                break;
            case '3':
                printTxSubmenu(uart);
                currentCAN = &can2;
                break;
            case '4':
                printRxSubmenu(uart);
                currentCAN = &can2;
                break;
            default:
                uart.printf("\r\nInvalid selection.\r\n");
                continue;
                break;
        }

        char tmp2 = uart.getc();

        if ((tmp1 == '1') || (tmp1 == '3'))
        {
            switch(tmp2)
            {
                case '1':
                    txMessage.id = id_1;
                    txMessage.dataLengthCode = dlc_1;
                    std::memcpy(txMessage.packet, payload_1, sizeof(payload_1));
                    currentCAN->transmit(txMessage);
                    break;
                case '2':
                    txMessage.id = id_2;
                    txMessage.dataLengthCode = dlc_2;
                    std::memcpy(txMessage.packet, payload_2, sizeof(payload_2));
                    currentCAN->transmit(txMessage);
                    break;
                case '3':
                    txMessage.id = id_3;
                    txMessage.dataLengthCode = dlc_3;
                    std::memcpy(txMessage.packet, payload_3, sizeof(payload_3));
                    currentCAN->transmit(txMessage);
                    break;
                case '4':
                    txMessage.id = id_4;
                    txMessage.dataLengthCode = dlc_4;
                    std::memcpy(txMessage.packet, payload_4, sizeof(payload_4));
                    currentCAN->transmit(txMessage);
                    break;
                default:
                    uart.printf("\r\nInvalid selection.\r\n");
                    continue;
                    break;
            }
            uart.printf("\r\nTransmitted a message!\r\n");
        }
        else
        {
            switch(tmp2)
            {
                case '1':
                    rxMessage = currentCAN->receive(id_1);
                    break;
                case '2':
                    rxMessage = currentCAN->receive(id_2);
                    break;
                case '3':
                    rxMessage = currentCAN->receive(id_3);
                    break;
                case '4':
                    rxMessage = currentCAN->receive(id_4);
                    break;
                default:
                    uart.printf("\r\nInvalid selection.\r\n");
                    continue;
                    break;
            }
            printMessageContents(rxMessage, uart);
        }

        wait_us(10000);
    }
}

void printMessageContents(PROTOCOL::CanMessage * responseMsg, IO::uartIntf & my_uart_driver)
{
    if (nullptr != responseMsg)
    {
        my_uart_driver.printf("\r\nMessage received!\r\n");
        my_uart_driver.printf("    ID : %u\n\r", responseMsg->id);
        my_uart_driver.printf("    DLC: %u\n\r", responseMsg->dataLengthCode);

        for(uint8_t i = 0; i < responseMsg->dataLengthCode; i++)
            my_uart_driver.printf("    data[%u]: %x\n\r", i, responseMsg->packet[i]);
    }
    else
    {
        my_uart_driver.printf("\r\nReceive error: Message either doesn't exist in the Rx buffer or ID was invalid.\r\n\r\n");
    }
}

void printMainMenu(IO::uartIntf & uart)
{
    uart.printf("\r\nChoose one of the following:\r\n\r\n");
    uart.printf("1. Transmit a message from CAN1\r\n");
    uart.printf("2. Receive a message from CAN1\r\n");
    uart.printf("3. Transmit a message from CAN2\r\n");
    uart.printf("4. Receive a message from CAN2\r\n");
}

void printTxSubmenu(IO::uartIntf & uart)
{
    uart.printf("\r\nPick one of the following messages to send:\r\n\r\n");
    uart.printf("1. ID: 1 | Data: DE AD BE EF 00 11 22 33\r\n");
    uart.printf("2. ID: 2 | Data: AA 55 AA 55 AA\r\n");
    uart.printf("3. ID: 3 | Data: 12 34 56 78 90\r\n");
    uart.printf("4. ID: 4 | Data: D1 5E A5 E5\r\n");
}

void printRxSubmenu(IO::uartIntf & uart)
{
    uart.printf("\r\nPick one of the following messages to receive:\r\n\r\n");
    uart.printf("1. ID 1\r\n");
    uart.printf("2. ID 2\r\n");
    uart.printf("3. ID 3\r\n");
    uart.printf("4. ID 4\r\n");
}
#include "mbed.h"
#include "target_nucleo_f446re.h"
#include "dev_led.h"

int main()
{
    target::nucleo_f446re &my_target = target::nucleo_f446re::getInstance();

    DEV::LED my_led = my_target.getLED();

    auto toggle_lambda = [& my_led]()
    {
        static bool state = false;
        state = !state;
        my_led.setCurrentState(static_cast<IO::GpioIntf::STATE>(state));
        return;
    };

    while (true)
    {
        toggle_lambda();
        wait(0.5);
    }
}

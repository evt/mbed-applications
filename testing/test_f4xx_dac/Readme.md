# Test Plan for F4xx DAC

## Features
* HAL Adapter for the STM32F4xx DAC peripheral

## Required Hardware for Test
* STM32 Nucleo F446RE
* Oscilliscope or other analyzer (Saleae was used)

## Additional Required Software
* Saleae Logic (if using Saleae)

## Steps to Complete the Test
1. Compile the test program with the target "test_f4xx_dac" and program the Nucleo
2. Probe pin PA4 and (optionally) PA5 on the Nucleo with the oscilloscope or Saleae in ANALOG mode
3. Observe on pin PA4 a steady and cyclic ramp-up and ramp-down from 0 to 3.3V and vice versa.
4. The LED on the Nucleo should be "breathing". On the Saleae this should be a steady rise and fall to/from ~1.8V.
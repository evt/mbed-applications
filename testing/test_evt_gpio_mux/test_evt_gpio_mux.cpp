/*
 * file: test_evt_gpio_mux.cpp
 * This file is to test the generic multiplexor by driving gpio pins on the nucleo
 */

#include "dev_evt_gpio_mux.h"
#include "io_gpio_intf.h"
#include "io_gpio_mbed_adapter.h"

int main()
{

    constexpr unsigned int NUMBER_OF_MUX_SELS = 4;      // Number of selects you want to test, change this for testing

    // These pins correlate to unused pins on the nucleo where MUX3 is the MSb
    auto MUX3 = IO::GpioMbedAdapter::getInstance<IO::PIN::MC_PC8>();
    auto MUX2 = IO::GpioMbedAdapter::getInstance<IO::PIN::MC_PC6>();
    auto MUX1 = IO::GpioMbedAdapter::getInstance<IO::PIN::MC_PB2>();
    auto MUX0 = IO::GpioMbedAdapter::getInstance<IO::PIN::MC_PA5>();

    IO::GpioIntf * my_sels[NUMBER_OF_MUX_SELS] = {&MUX0, &MUX1, &MUX2, &MUX3}; // creates the array of muxes, also change this according to NUMBER_OF_MUX_SELS

    DEV::EvtGpioMux<NUMBER_OF_MUX_SELS> my_mux = DEV::EvtGpioMux<NUMBER_OF_MUX_SELS>(my_sels);  // creates the genereic mux using the selects

    while(1)
    {
        for (int i = 0; i < 1<<NUMBER_OF_MUX_SELS; i++) // loops for 2 to the n times, the number of mux outputs
        {
            my_mux.choosePort(i);   // selects which port to choose, telling the mux how to drive the selects
            wait_ms(250);
        }
    }
}

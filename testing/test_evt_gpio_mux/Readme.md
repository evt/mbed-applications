# Test Plan for Dev_evt_gpio_mux Driver

## Features
* Generic implementation of a multiplexer for use on any device

## Required Hardware for Test
* STM32 Nucleo F302R8
* Oscilliscope or other digital analyzer (Saleae was used)

## Additional Required Software
* Saleae's "Logic" if using a Saleae

## Steps to Complete the Test
1. Modify the number of selects variable in the test file for desired amount of selects
3. Probe PC8, PC6, PB2, and PC5 on the nucleo
2. Modify the mySels array variable in the test file to take in the desired selects
4. Make sure the order of the selects the array takes is in a logical order for reading the values (e.g. PC8, PC6, PB2, PC5 is MSb to LSb)
5. Run the logic analyzer and it will run through all possible combinations for the specified amount of selects.
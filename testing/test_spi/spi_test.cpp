/*
 * file: spi_test.cpp
 * purpose: perform sample read and write sequences to test io_spi_mbed_adapter module
 */


#include <cstdint>
#include "mbed.h"
#include "target_nucleo_f302r8.h"
#include "io_spi_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"


int main()
{

    // GPIO pins
	constexpr IO::PIN CS_PIN = IO::PIN::MC_PB12;
    constexpr IO::PIN TEST_PIN = IO::PIN::MC_PC9;

    // SPI parameters
    constexpr IO::SpiMbedAdapter::BusMode BUS_MODE = IO::SpiMbedAdapter::BusMode::MODE_00;
    constexpr IO::SpiMbedAdapter::BitOrder BIT_ORDER = IO::SpiMbedAdapter::BitOrder::DEFAULT_FIRST;
		
    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();
    IO::SpiIntf &spiModule = my_target.getSPI();

    IO::GpioMbedAdapter &chipSelect = IO::GpioMbedAdapter::getInstance<CS_PIN>();
    IO::GpioMbedAdapter &testPin = IO::GpioMbedAdapter::getInstance<TEST_PIN>();

	uint8_t test_message[] = "SPI TEST MESSAGE"; // Length 16
	uint8_t receive_message[5];					 // Length 5

	// Configure SPI bus
	spiModule.configureSpi(BUS_MODE, BIT_ORDER);
	
	// Configure chip select as ouptput and write high
	chipSelect.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
	chipSelect.writePin(IO::GpioIntf::STATE::HIGH);

	testPin.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
	testPin.writePin(IO::GpioIntf::STATE::LOW);
	testPin.writePin(IO::GpioIntf::STATE::HIGH);

	// 1 second message pulse
	while(true)
	{
		// Toggle CS Low
		spiModule.setChipSelectLow(chipSelect);

		// Start the transfer
		spiModule.write(test_message, 16);

		// Dummy read to test driving the clock
		spiModule.read(receive_message, 5);

		// Toggle CS High
		spiModule.setChipSelectHigh(chipSelect);

		wait(1);

	}


}

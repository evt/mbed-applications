# Testing for the SPI Mbed Adapter

## Features
* Toggle chip select to initiate transfer to/from slave
* Read and write to/from slave device

## Hardware required to test (Until BMS boards get populated)
* STM32F302R8 Nucleo Development Board
* Oscilloscope / digital logic analyzer

## Steps to test:
1. MOSI, MISO, SCLK, and CS are pins PB_15, PB_14, PB_13, and PB_12
on the Nucleo board, respectively. Probe these with 7, 6, 5, and 4 on
the digital logic analyzer (you can use others, this is the default setup).
2. Make sure the Serial mode is configured for SPI, 8 bits, and MSB first on the analyzer
3. Set up a trigger for the falling edge of CS
4. Build with this command: *python mbed_build.py -t test_spi*
5. Flash the Nucleo with the appropriate *test_spi.bin* file
6. The board should be sending 1 second pulses of SPI data. Activate the trigger.
7. The first 16 bytes written by the Nucleo should correspond to the ASCII for "SPI TEST MESSAGE"
8. The next 5 bytes should exhibit read behavior: writing all zeros for the sake of driving
the clock signal. It should look like 5 distinct groups of 8 clock pulses each. This will 
trigger the slave device to send its data. Dummy length of 5 bytes is used for now, until
a real slave device is used (i.e. until the BMS gets populated).
9. CS should be high at the end of the transmission

**It should be noted that this is the test setup just to see bus behavior,
this will be updated once the BMS's are populated and tests with the LTC6811
as a slave device can be done**
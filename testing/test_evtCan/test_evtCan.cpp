
#include "target_nucleo_f302r8.h"

int main()
{
    char temp;

    target::nucleo_f302r8 &my_target = target::nucleo_f302r8::getInstance();

    IO::EvtCan &can = my_target.getEvtCan();
    IO::uartIntf &uartDriver = my_target.getUART();

    // Create some structs to test with
    IO::EvtCan::BMS_cellVoltage cellVolt(1, 0xDEAD, 2, 0xBEEF);
    IO::EvtCan::BMS_cellTemperature cellTemp(3, 0xCAFE, 4, 0xBABE);
    IO::EvtCan::BMS_packVoltage packVolt(0xCAB005E5);
    IO::EvtCan::BMS_packCurrent packCur(0x01234567, 0x89ABCDEF);

    IO::EvtCan::IMU_linearAcceleration lia(-1, 2, -3);
    IO::EvtCan::IMU_gravityVector grv(4, -5, 6);
    IO::EvtCan::IMU_quaternionPosition qua(-7, 8, -9, 10);

    IO::EvtCan::SGM_strain strain(5, 0x0B501E7E0);

    uartDriver.puts("Starting CAN messages!\r\n");

    can.sendMessage(cellVolt);
    can.sendMessage(cellTemp);
    can.sendMessage(packVolt);
    can.sendMessage(packCur);

    can.sendMessage(lia);
    can.sendMessage(grv);
    can.sendMessage(qua);

    can.sendMessage(strain);

    can.sendError(IO::EvtCan::Board::BMS, target::ErrorLevel::WARNING,
                    "Error Message 1", 15);
    can.sendError(IO::EvtCan::Board::IMU, target::ErrorLevel::ERROR,
                    "Err0r Me55Age 2", 15);
    can.sendError(IO::EvtCan::Board::SGM, target::ErrorLevel::FATAL,
                    "Error Message 3", 15);

    uartDriver.printf("CAN Messages complete!\r\n");

    for (;;)
    {
    }
}

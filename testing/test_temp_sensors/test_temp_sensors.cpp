#include "mbed.h"
#include "mc_can.h"
#include "app_tmsTempSensors.h"
#include "target_tms.h"
#include "io_uart_intf.h"

int main() 
{
    target::Tms & my_target = target::Tms::getInstance();
    APP::TemperatureSensors my_app(my_target.getEvtCan(), my_target.getADC(), my_target.getMUX(), 1);
    IO::uartIntf & uartIntf = my_target.getUART(); 

    MC::can_setMode(MC::CanMode::NORMAL, true);

    my_app.init();

    while(1)
    {
        my_app.run();
        wait(0.5);
    }

}

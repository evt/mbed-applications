/*
 * file: test_imu.cpp
 * purpose: 
 */

#include "app_imu.h"
#include "mbed.h"
#include "target_bmu.h"
#include "mc_can.h"

int main()
{
    target::Bmu & my_target = target::Bmu::getInstance();
    IO::CanIntf & can_network = my_target.getCAN();
    DEV::LED & my_led = my_target.getLED();
    APP::Imu my_app(my_target.getBno055(), my_target.getEvtCan());

    my_app.init();

    // always
    while (true)
    {
        MC::can_setMode(MC::CanMode::NORMAL, true);
        my_led.toggle();
        // runs app every 100 miliseconds
        my_app.run();
        wait(0.1);
    }
}
"""
Handles building the EVT project into target specific binaries for running on each board.
"""
from __future__ import print_function
import argparse
import os
import sys
import subprocess
import evt_targets


PROFILES = {
    'debug': os.path.join('custom-build-files', 'debug.json'),
    'develop': os.path.join('custom-build-files', 'develop.json'),
    'release': os.path.join('custom-build-files', 'release.json')
}

DEFAULT_TOOLCHAIN = 'GCC_ARM'


def main():
    """
    Builds a compile command from some arguments and a dictionary of targets.
    :return:
    """
    parser = argparse.ArgumentParser(description='Builds the long form command for compiling.')
    parser.add_argument('target', help='The executable to be built.')
    parser.add_argument('-p', '--profile', help='The profile JSON to use for configuring'
                        + 'compiler options.')
    parser.add_argument('-m', '--platform', help='The hardware platform to build for. Beware, '
                        + 'code might not work on unsupported platforms.')
    parser.add_argument('-c', '--clean', action='store_true', help='Do a completely clean build.')
    my_arguments = parser.parse_args()

    print()

    if my_arguments.profile is None or my_arguments.profile.lower() not in PROFILES:
        my_profile = PROFILES['debug']
        if my_arguments.profile is not None:
            print(my_arguments.profile + " is not a profile option.")
        print("Using " + my_profile + '.')
    else:
        my_profile = PROFILES[my_arguments.profile.lower()]

    # Determine which target to build and build it
    if my_arguments.target in evt_targets.EVT_TARGETS:
        my_target = my_arguments.target

        # Determine which board to use
        if my_arguments.platform is None:
            my_board = evt_targets.EVT_TARGETS[my_target]['hw_target']
        else:
            my_board = my_arguments.platform

        build(my_target, my_profile, my_board, my_arguments.clean)

    elif my_arguments.target == 'all':
        for my_target in evt_targets.EVT_TARGETS:
            print("Building: " + my_target)

            # Determine which board to use
            if my_arguments.platform is None:
                my_board = evt_targets.EVT_TARGETS[my_target]['hw_target']
            else:
                my_board = my_arguments.platform

            res = build(my_target, my_profile, my_board, my_arguments.clean, quiet=True)

            # Check to see if a build failed
            if not res[0]:
                print(res[1])
                print("Build of " + my_target + " failed. Exiting...\n")
                sys.exit(1)

        print("\nAll builds successful!")

    else:
        print("No target definition for " + my_arguments.target)
        my_target = None
        return

    print()


def build(my_target, my_profile, my_board, clean, quiet=False):
    """
    Run the actual compilation command.
    """
    success = False
    error = ""

    command_format = "mbed compile --build BUILD -m {} --profile {} -N {}"
    my_command = command_format.format(my_board, str(my_profile), my_target)

    if clean:
        my_command += " -c"

    for source in evt_targets.EVT_TARGETS[my_target]['sources']:
        my_command += " --source " + str(source)

    print(my_command)
    process = subprocess.Popen(my_command.split(), stderr=subprocess.PIPE,
                               stdout=subprocess.PIPE, universal_newlines=True)
    output = process.communicate()[0]

    if not quiet:
        print(output)

    return (process.returncode == 0, process.returncode)


if __name__ == "__main__":
    main()

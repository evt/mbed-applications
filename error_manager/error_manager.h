/*
 * file: error_manager.h
 * purpose: interface for error_manager
 */

#ifndef ERROR_MANAGER
#define ERROR_MANAGER
#include "target_base.h"

class error_manager
{
public:

    static void logError(const char *msg, target::ErrorLevel level);

    //constructor and destructor
    static void setTarget(target::base * target);

private:
    error_manager();
    ~error_manager();

    //target with error
    static target::base * theTarget;

};

#endif

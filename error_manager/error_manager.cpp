/*
 * file: error_manager.cpp
 * purpose: sends error from a target to the target to output problem.
 */

#include "error_manager.h"

target::base * error_manager::theTarget;

/*passes a target to the error manager*/
error_manager::error_manager()
{
    theTarget = nullptr;
}

error_manager::~error_manager()
{
    //empty destructor
}

/*just passes information to the targets logError*/
void error_manager::logError(const char *msg, target::ErrorLevel level)
{
    if (theTarget != nullptr)
    {
        theTarget->logError(msg, level);
    }
}

void error_manager::setTarget(target::base * target)
{
    theTarget = target;
}

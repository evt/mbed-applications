/*
 * file: target_bmu.h
 * purpose: Temporary thing.
 */

#ifndef TARGET_BMU_H
#define TARGET_BMU_H

#include <utility>
#include <array>
#include "target_base.h"
#include "io_can_intf.h"
#include "io_gpio_intf.h"
#include "io_i2c_intf.h"
#include "io_uart_intf.h"
#include "io_spi_intf.h"
#include "io_evtCan.h"
#include "dev_led.h"
#include "dev_ltc6811.h"
#include "dev_bno055.h"
#include "io_flash_intf.h"

namespace target
{

class Bmu : public base
{

public:

    static Bmu & getInstance();

    IO::i2cIntf & getI2C();
    IO::uartIntf & getUART();
    IO::SpiIntf & getSPI();
    IO::CanIntf & getCAN();
    IO::EvtCan & getEvtCan();
    IO::FlashIntf & getFlash();

    DEV::LED & getLED();
    std::pair<DEV::Ltc6811 *, uint8_t> getLtc6811();
    DEV::bno055 & getBno055();

    void logError(const char *msg, ErrorLevel level) override;

private:

    Bmu();
    ~Bmu();

    IO::i2cIntf * my_i2c;
    IO::uartIntf * my_uart;
    IO::SpiIntf * my_spi;
    IO::CanIntf * my_can;
    IO::EvtCan * my_evtCan;
    IO::FlashIntf * my_flash;
    
    DEV::LED * my_led;
    DEV::Ltc6811 * my_ltc6811;
    DEV::bno055 * my_bno055;

};

} // namespace target

#endif // TARGET_BMU_H
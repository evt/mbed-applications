
#ifndef TARGET_NUCLEO_F446RE_H
#define TARGET_NUCLEO_F446RE_H

#include "target_base.h"

/*
*   Forward Declarations
*/

namespace IO
{
    class EvtCan;
    class dev_led;
    class CanIntf;
    class i2cIntf;
    class uartIntf;
    class SpiIntf;
    class RtcIntf;
    class DacIntf;
}

namespace DEV
{
    class LED;
}

namespace target
{

class nucleo_f446re : public base
{
public:

    static nucleo_f446re & getInstance();
    
    IO::i2cIntf & getI2C();
    IO::SpiIntf & getSPI();
    IO::uartIntf & getUART();
    IO::CanIntf & getCAN1();
    IO::CanIntf & getCAN2();
    IO::EvtCan & getEvtCan();
    IO::RtcIntf & getRTC();
    IO::DacIntf & getDAC();

    DEV::LED & getLED();

    void logError(const char *msg, ErrorLevel level) override;

private:

    nucleo_f446re();
    ~nucleo_f446re();

    IO::i2cIntf * my_i2c;
    IO::SpiIntf * my_spi;
    IO::uartIntf * my_uart;
    IO::CanIntf * my_can1;
    IO::CanIntf * my_can2;
    IO::EvtCan * my_evtCan;
    IO::RtcIntf * my_rtc;
    IO::DacIntf * my_dac;

    DEV::LED * my_led;


};

} // namespace target

#endif // TARGET_GUB_H

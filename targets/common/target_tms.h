 /*
 * file: target_tms.h
 * Purpose: Header for Thermal Management System
 */

#ifndef TARGET_TMS_H
#define TARGET_TMS_H

#include <utility>
#include <array>
#include "target_base.h"
#include "dev_evt_gpio_mux.h"

/*
 * Forward Declarations
 */
namespace IO
{
class CanIntf;
class EvtCan;
class AdcIntf;
class GpioIntf;
class i2cIntf;
class PwmIntf;
class SpiIntf;
class uartIntf;
class AdcIntf;
class FlashIntf;
} // namespace IO

namespace DEV
{
class LED;
class Stwd100;
} // namespace DEV

namespace target
{

class Tms : public base
{

public:

    static Tms & getInstance();

    // IO Layer Getters
    IO::CanIntf & getCAN();
    IO::EvtCan & getEvtCan();
    IO::AdcIntf & getADC();
    IO::i2cIntf & getI2C();
    IO::PwmIntf & getPWM1();
    IO::PwmIntf & getPWM2();
    IO::uartIntf & getUART();
    IO::FlashIntf & getFlash();

    //DEV Layer Getters
    DEV::LED & getLED();
    DEV::EvtGpioMux<3> & getMUX();
    DEV::Stwd100 & getWatchdog();

    void logError(const char *msg, ErrorLevel level) override;

private:

    Tms();
    ~Tms();

    IO::CanIntf * my_can;
    IO::EvtCan * my_evtCan;
    IO::AdcIntf * my_adc;
    IO::i2cIntf * my_i2c;
    IO::PwmIntf * my_pwm1;
    IO::PwmIntf * my_pwm2;
    IO::uartIntf * my_uart;
    IO::FlashIntf * my_flash;
    DEV::LED * my_led;
    DEV::EvtGpioMux<3> * my_mux;
    DEV::Stwd100 * my_watchdog;
};

} // namespace target

#endif // TARGET_TMS_H

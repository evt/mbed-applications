/*
 * file: target_gub.h
 */

#include "target_base.h"

// Forward declarations
namespace IO
{
    class EvtCan;
    class dev_led;
    class CanIntf;
    class i2cIntf;
    class uartIntf;
    class SpiIntf;
    class RtcIntf;
}

namespace DEV
{
    class LED;
    class Sim100;
}

namespace target
{

class Gub : public base
{
public:

    static Gub & getInstance();

    IO::i2cIntf & getI2C();
    IO::SpiIntf & getSPI();
    IO::uartIntf & getUART();
    IO::CanIntf & getCAN1();
    IO::CanIntf & getCAN2();
    IO::EvtCan & getEvtCan();
    IO::RtcIntf & getRTC();

    DEV::LED & getLED();
    DEV::Sim100 & getGFD();

    void logError(const char *msg, ErrorLevel level) override;

private:

    Gub();
    ~Gub();

    IO::i2cIntf * my_i2c;
    IO::SpiIntf * my_spi;
    IO::uartIntf * my_uart;
    IO::CanIntf * my_can1;
    IO::CanIntf * my_can2;
    IO::EvtCan * my_evtCan;
    IO::RtcIntf * my_rtc;

    DEV::LED * my_led;
    DEV::Sim100 * my_sim100;


};

} // namespace target

/*
*file: target_lvss.h
*Purpose: Header for LVSS <--> GUB comms
*/

#ifndef TARGET_LVSS_H
#define TARGET_LVSS_H

#include <utility>
#include <array>
#include "target_base.h"
#include "io_gpio_mbed_adapter.h"
/*
*   Forward Declarations
*/

namespace IO
{
    class CanIntf;
    class EvtCan;
    class i2cIntf;
    class PwmIntf;
    class uartIntf;
    class FlashIntf;
} //namespace IO

namespace DEV
{
    class LED;
} // namespace DEV

namespace target
{
    
class Lvss : public base
{
public:
    
    static Lvss & getInstance();

    //IO Layer Getters
    IO::CanIntf & getCAN();
    IO::EvtCan & getEvtCan();
    IO::i2cIntf & getI2C();  
    IO::uartIntf & getUART();
    IO::GpioMbedAdapter & getSTMCRL();
    IO::GpioMbedAdapter & getIGNITION_STATE();
    IO::GpioMbedAdapter & getAPS();
    IO::FlashIntf & getFlash();

    //DEV Layer Getters
    //These devices aren't implemented so they can't be
    //used within compilation.
    #if 0
    DEV::Ltc2992 & getLtc2992();
    DEV::Ina226 & getIna226();
    #endif
    std::pair<DEV::LED *, uint8_t> getLED();
     
    void logError(const char *msg, ErrorLevel level) override;

private:

    Lvss();
    ~Lvss();

    IO::CanIntf * my_can;
    IO::EvtCan * my_evtCan;
    IO::i2cIntf * my_i2c;
    IO::uartIntf * my_uart;      
    DEV::LED * my_ledArray;
    IO::GpioMbedAdapter * my_stmcrl;
    IO::GpioMbedAdapter * my_ignition_state;
    IO::GpioMbedAdapter * my_aps;
    IO::FlashIntf * my_flash;
    
    //Not implemented
    #if 0
    DEV::Ltc2992 * my_ltc2992;
    DEV::Ina226 * my_ina226
    #endif
};
} // namespace target

#endif // TARGET_LVSS_H



#ifndef TARGET_IMU_H
#define TARGET_IMU_H

#include "target_base.h"
#include "io_can_intf.h"
#include "io_gpio_intf.h"
#include "io_i2c_intf.h"
#include "io_uart_intf.h"
#include "dev_led.h"
#include "dev_bno055.h"
#include "io_evtCan.h"
#include "io_flash_intf.h"

namespace target
{

class Imu : base
{
public: 

    static Imu & getInstance();

    // IO layer getters
    IO::i2cIntf & getI2C();
    IO::uartIntf & getUART();
    IO::CanIntf & getCAN();
    IO::EvtCan & getEvtCan();
    IO::FlashIntf & getFlash();

    // DEV layer getters
    DEV::LED & getLED();
    DEV::bno055 & getBno055();

    void logError(const char *msg, ErrorLevel level) override;

private:
    // Private constructor and destructor
    Imu();
    ~Imu();

    IO::i2cIntf * my_i2c;
    IO::uartIntf * my_uart;
    IO::CanIntf * my_can;
    IO::EvtCan * my_evtCan;
    IO::FlashIntf * my_flash;
    DEV::LED * my_led;
    DEV::bno055 * my_bno055;


};

}

#endif  // TARGET_IMU_H


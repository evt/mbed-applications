
#ifndef TARGET_BASE_H
#define TARGET_BASE_H

namespace target
{

// Error levels for error logging.
enum class ErrorLevel
{
    INFO = 0,
    WARNING = 1,
    ERROR = 2,
    FATAL = 3,
    DEBUG = 4,
};

class base
{
public: 

    #if 0 
    enum class ErrorSource
    {
        IO_I2C,
        IO_UART,
        IO_SPI,
        IO_GPIO
    };
    #endif

    virtual void logError(const char *msg, ErrorLevel level) = 0;

protected:
    // Private constructor and destructor
    base() { }
    virtual ~base() { }
};

}
#endif  // TARGET_BASE


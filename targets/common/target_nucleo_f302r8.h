
#ifndef TARGET_NUCLEO_F302R8_H
#define TARGET_NUCLEO_F302R8_H

#include "target_base.h"
#include "io_can_intf.h"
#include "io_evtCan.h"
#include "io_gpio_intf.h"
#include "io_i2c_intf.h"
#include "io_uart_intf.h"
#include "io_spi_intf.h"
#include "io_flash_intf.h"
#include "io_dac_intf.h"
#include "dev_led.h"

namespace target
{

class nucleo_f302r8 : public base
{
public: 

    static nucleo_f302r8 & getInstance();

    // IO layer getters
    IO::i2cIntf & getI2C();
    IO::uartIntf & getUART();
    IO::SpiIntf & getSPI();
    IO::CanIntf & getCAN();
    IO::EvtCan & getEvtCan();
    IO::FlashIntf & getFlash();
    IO::DacIntf & getDAC();

    // DEV layer getters
    DEV::LED & getLED();

    void logError(const char *msg, ErrorLevel level) override;

private:
    // Private constructor and destructor
    nucleo_f302r8();
    ~nucleo_f302r8();

    IO::i2cIntf * my_i2c;
    IO::uartIntf * my_uart;
    IO::SpiIntf * my_spi;
    IO::CanIntf * my_can;
    IO::EvtCan * my_evtCan;
    IO::FlashIntf * my_flash;
    IO::DacIntf * my_dac;

    DEV::LED * my_led;

};

}

#endif  // TARGET_NUCLEO_F302R8


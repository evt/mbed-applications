 /*
 * file: target_Sgm.h
 */

#ifndef TARGET_SGM_H
#define TARGET_SGM_H

#include <utility>
#include <array>
#include "target_base.h"
#include "io_can_intf.h"
#include "io_evtCan.h"
#include "io_gpio_intf.h"
#include "io_spi_intf.h"
#include "io_flash_intf.h"
#include "dev_led.h"
#include "dev_ltc2440.h"
#include "dev_sn74cb3q3253.h"

namespace target
{

class Sgm : public base
{

public:

    static Sgm & getInstance();

    IO::SpiIntf & getSPI();
    IO::CanIntf & getCAN();
    IO::EvtCan & getEvtCan();
    IO::FlashIntf & getFlash();
    DEV::LED & getHeartbeatLED();
    DEV::LED & getLED1();
    DEV::LED & getLED2();
    DEV::LED & getLED();
    DEV::Sn74cb3q3253 & getMUX();

    std::pair<DEV::Ltc2440 *, uint8_t> getLtc2440();
    IO::GpioIntf& getChipSelect();

    void logError(const char *msg, ErrorLevel level) override;

private:

    Sgm();
    ~Sgm();

    IO::SpiIntf * my_spi;
    IO::CanIntf * my_can;
    IO::EvtCan * my_evtCan;
    IO::FlashIntf * my_flash;
    DEV::LED * my_heartbeat_led;
    DEV::LED * my_led1;
    DEV::LED * my_led2;
    DEV::Ltc2440 * my_ltc2440s;
    DEV::Sn74cb3q3253 * my_mux;
};

} // namespace target

#endif // TARGET_SGM_H

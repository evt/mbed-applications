/*
 * file: target_gub.cpp
 */

#include "target_gub.h"
#include "io_i2c_mbed_adapter.h"
#include "io_uart_mbed_adapter.h"
#include "io_spi_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"
#include "io_can_f4xx_hal_adapter.h"
#include "io_rtc_f4xx_hal_adapter.h"
#include "io_pin_names.h"
#include "error_manager.h"
#include "dev_led.h"
#include "io_evtCan.h"

namespace
{
    // Pin defines
    // I2C
    constexpr PinName DEFAULT_SDA = PB_4;
    constexpr PinName DEFAULT_SCL = PB_10;

    // CAN1
    constexpr uint8_t CAN1_PORT_ID = 1;
    constexpr IO::PIN DEFAULT_CANTX = IO::PIN::MC_PB9;
    constexpr IO::PIN DEFAULT_CANRX = IO::PIN::MC_PB8;

    //CAN2
    constexpr uint8_t CAN2_PORT_ID = 2;
    constexpr IO::PIN DEFAULT_CANTX2 = IO::PIN::MC_PB13;
    constexpr IO::PIN DEFAULT_CANRX2 = IO::PIN::MC_PB12;

    //SPI
    constexpr PinName DEFAULT_MOSI = PA_7;
    constexpr PinName DEFAULT_MISO = PA_6;
    constexpr PinName DEFAULT_SCK = PC_7;

    //SDIO
    constexpr PinName DEFAULT_D0 = PC_8;
    constexpr PinName DEFAULT_D1 = PB_0;
    constexpr PinName DEFAULT_D2 = PB_1;
    constexpr PinName DEFAULT_D3 = PC_11;
    constexpr PinName DEFAULT_CMD = PD_2;
    constexpr PinName DEFAULT_CK = PB_2;

    //USB
    constexpr PinName DEFAULT_DP = PA_12;
    constexpr PinName DEFAULT_DM = PA_11;

    //UART
    constexpr PinName DEFAULT_TX = PA_9;
    constexpr PinName DEFAULT_RX = PA_10;

    //LED
    constexpr IO::PIN LED_PIN = IO::PIN::MC_PA5;

    constexpr uint8_t NUM_CAN_IDS = 4;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {1,2,3,4};
}

namespace target
{

Gub::Gub() :
    my_i2c(nullptr),
    my_spi(nullptr),
    my_uart(nullptr),
    my_can1(nullptr),
    my_can2(nullptr),
    my_evtCan(nullptr),
    my_led(nullptr)
{
    error_manager::setTarget(this);
}

Gub::~Gub()
{
    // Empty destructor
}

Gub & Gub::getInstance()
{
    static Gub instance;
    return instance;
}

IO::i2cIntf & Gub::getI2C()
{
    if (my_i2c == nullptr)
    {
        my_i2c = &IO::i2cMbedAdapter::getInstance<DEFAULT_SDA, DEFAULT_SCL>();
    }
    return *my_i2c;
}

IO::CanIntf & Gub::getCAN1()
{
    if (my_can1 == nullptr)
    {
        my_can1 = &IO::CanF4xxHalAdapter::getInstance<DEFAULT_CANTX, DEFAULT_CANRX, NUM_CAN_IDS, CAN1_PORT_ID>(CAN_ID_LIST);
    }
    return *my_can1;
}

IO::CanIntf & Gub::getCAN2()
{
    if (my_can2 == nullptr)
    {
        my_can2 = &IO::CanF4xxHalAdapter::getInstance<DEFAULT_CANTX2, DEFAULT_CANRX2, NUM_CAN_IDS, CAN2_PORT_ID>(CAN_ID_LIST);
    }
    return *my_can2;
}

IO::SpiIntf & Gub::getSPI()
{
    if (my_spi == nullptr)
    {
        my_spi = &IO::SpiMbedAdapter::getInstance<DEFAULT_MOSI, DEFAULT_MISO, DEFAULT_SCK>();
    }
    return *my_spi;
}

IO::uartIntf & Gub::getUART()
{
    if(my_uart == nullptr)
    {
        my_uart = &IO::uartMbedAdapter::getInstance<DEFAULT_TX, DEFAULT_RX>();
    }
    return *my_uart;
}

IO::EvtCan & Gub::getEvtCan()
{
    static IO::EvtCan _evtCan(getEvtCan());
    if(my_evtCan == nullptr)
    {
        my_evtCan = &_evtCan;
    }
    return *my_evtCan;
}

IO::RtcIntf & Gub::getRTC()
{
    if (nullptr == my_rtc)
    {
        my_rtc = &IO::RtcF4xxHalAdapter::getInstance();
    }
    return *my_rtc;
}

DEV::LED & Gub::getLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_PIN>());
    my_led = &instance;
    return *my_led;
}

DEV::Sim100 & Gub::getGFD()
{
    static DEV::Sim100 instance(getCAN2());
    my_sim100 = &instance;
    return *my_sim100;
}

void Gub::logError(const char *msg, ErrorLevel level)
{
    return;
}

}

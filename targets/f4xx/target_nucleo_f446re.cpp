
#include "target_nucleo_f446re.h"
#include "io_i2c_f4xx_hal_adapter.h"
#include "io_uart_f4xx_hal_adapter.h"
#include "io_spi_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"
#include "io_can_f4xx_hal_adapter.h"
#include "io_rtc_f4xx_hal_adapter.h"
#include "io_dac_f4xx_hal_adapter.h"
#include "io_pin_names.h"
#include "error_manager.h"
#include "dev_led.h"
#include "io_evtCan.h"

namespace
{
    // Pin defines
    // I2C
    constexpr IO::PIN DEFAULT_SDA = IO::PIN::MC_PB9;
    constexpr IO::PIN DEFAULT_SCL = IO::PIN::MC_PB8;
    constexpr unsigned DEFAULT_I2C_FREQ = 100000;
    constexpr int DEFAULT_I2C_MODULE = 1;

    // CAN1
    constexpr uint8_t CAN1_PORT_ID = 1;
    constexpr IO::PIN DEFAULT_CANTX = IO::PIN::MC_PB9;
    constexpr IO::PIN DEFAULT_CANRX = IO::PIN::MC_PB8;
    constexpr uint32_t CAN_BAUD_RATE(500000);
    constexpr unsigned int PCLK_FREQUENCY(36000000u);
    
    //CAN2
    constexpr uint8_t CAN2_PORT_ID = 2;
    constexpr IO::PIN DEFAULT_CANTX2 = IO::PIN::MC_PB13;
    constexpr IO::PIN DEFAULT_CANRX2 = IO::PIN::MC_PB12;

    //SPI
    constexpr PinName DEFAULT_MOSI = PA_7;
    constexpr PinName DEFAULT_MISO = PA_6;
    constexpr PinName DEFAULT_SCK = PC_7;

    //SDIO
    constexpr PinName DEFAULT_D0 = PC_8;
    constexpr PinName DEFAULT_D1 = PB_0;
    constexpr PinName DEFAULT_D2 = PB_1;
    constexpr PinName DEFAULT_D3 = PC_11;
    constexpr PinName DEFAULT_CMD = PD_2;
    constexpr PinName DEFAULT_CK = PB_2;

    //USB
    constexpr PinName DEFAULT_DP = PA_12;
    constexpr PinName DEFAULT_DM = PA_11;

    //UART
    constexpr IO::PIN DEFAULT_TX = IO::PIN::MC_PA2; //For UART over USB
    constexpr IO::PIN DEFAULT_RX = IO::PIN::MC_PA3; //For UART over USB

    //DAC
    constexpr uint8_t NUM_DAC_CHANNELS = 2;
    constexpr IO::PIN DAC_PIN1 = IO::PIN::MC_PA4;
    constexpr IO::PIN DAC_PIN2 = IO::PIN::MC_PA5; // CONFLICTS WITH LED PIN, NO WORKAROUND FOR THIS

    //LED
    constexpr IO::PIN LED_PIN = IO::PIN::MC_PA5;

    constexpr uint8_t NUM_CAN_IDS = 4;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {1,2,3,4};
}

namespace target
{

nucleo_f446re::nucleo_f446re() :
    my_i2c(nullptr),
    my_spi(nullptr),
    my_uart(nullptr),
    my_can1(nullptr),
    my_can2(nullptr),
    my_evtCan(nullptr),
    my_led(nullptr)
    //my_sdio TODO
    //my_usbotg TODO
{
    error_manager::setTarget(this);
}

nucleo_f446re::~nucleo_f446re()
{
    // Empty destructor
}

nucleo_f446re & nucleo_f446re::getInstance()
{
    static nucleo_f446re instance;
    return instance;
}

IO::i2cIntf & nucleo_f446re::getI2C()
{
    if (my_i2c == nullptr)
    {
        my_i2c = &IO::I2cF4xxHalAdapter::getInstance<DEFAULT_SDA, DEFAULT_SCL, DEFAULT_I2C_FREQ, DEFAULT_I2C_MODULE>();
    }
    return *my_i2c;
}

IO::CanIntf & nucleo_f446re::getCAN1()
{
    if (my_can1 == nullptr)
    {
        my_can1 = &IO::CanF4xxHalAdapter::getInstance<DEFAULT_CANTX, DEFAULT_CANRX, NUM_CAN_IDS, CAN1_PORT_ID>(CAN_ID_LIST);
    }
    return *my_can1;
}

IO::CanIntf & nucleo_f446re::getCAN2()
{
    if (my_can2 == nullptr)
    {
        my_can2 = &IO::CanF4xxHalAdapter::getInstance<DEFAULT_CANTX2, DEFAULT_CANRX2, NUM_CAN_IDS, CAN2_PORT_ID>(CAN_ID_LIST);
    }
    return *my_can2;
}

IO::SpiIntf & nucleo_f446re::getSPI()
{
    if (my_spi == nullptr)
    {
        my_spi = &IO::SpiMbedAdapter::getInstance<DEFAULT_MOSI, DEFAULT_MISO, DEFAULT_SCK>();
    }
    return *my_spi;
}

IO::uartIntf & nucleo_f446re::getUART()
{
    if(my_uart == nullptr)
    {
        my_uart = &IO::UartF4xxHalAdapter::getInstance<DEFAULT_TX, DEFAULT_RX>();
    }
    return *my_uart;
}

IO::EvtCan & nucleo_f446re::getEvtCan()
{
    static IO::EvtCan _evtCan(getEvtCan());
    if(my_evtCan == nullptr)
    {
        my_evtCan = &_evtCan;
    }
    return *my_evtCan;
}

IO::RtcIntf & nucleo_f446re::getRTC()
{
    if (nullptr == my_rtc)
    {
        my_rtc = &IO::RtcF4xxHalAdapter::getInstance();
    }
    return *my_rtc;
}

IO::DacIntf & nucleo_f446re::getDAC()
{
    if (nullptr == my_dac)
    {
        my_dac = &IO::DacF4xxHalAdapter::getInstance<NUM_DAC_CHANNELS>({DAC_PIN1, DAC_PIN2});
    }
    return *my_dac;
}

DEV::LED & nucleo_f446re::getLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_PIN>());
    my_led = &instance;
    return *my_led;
}

void nucleo_f446re::logError(const char *msg, ErrorLevel level)
{
    return;
}

}

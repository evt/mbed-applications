
#include "target_imu.h"
#include "io_can_f302x8_adapter.h"
#include "io_i2c_mbed_adapter.h"
#include "io_uart_mbed_adapter.h"
#include "io_spi_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"
#include "io_flash_f3xx_hal_adapter.h"
#include "error_manager.h"

namespace
{
    // Pin defines
    constexpr PinName DEFAULT_SDA = PB_7;
    constexpr PinName DEFAULT_SCL = PB_6;

    constexpr IO::PIN BNO055_PS0 = IO::PIN::MC_PB2;
    constexpr IO::PIN BNO055_PS1 = IO::PIN::MC_PB1;

    constexpr PinName DEFAULT_TX = PB_10;
    constexpr PinName DEFAULT_RX = PB_11;

    constexpr IO::PIN LED_PIN = IO::PIN::MC_PB15; 

    constexpr MC::Pin DEFAULT_CANRX(MC::Pin::A11);
    constexpr MC::Pin DEFAULT_CANTX(MC::Pin::A12);
    constexpr uint32_t CAN_BAUD_RATE(500000);
    constexpr unsigned int PCLK1_FREQUENCY(32000000u);

    // Subscribe to specific CAN IDs (CAN_RX)
    constexpr uint8_t NUM_CAN_IDS = 1;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {0};

    // Default calibration settings for BNO055
    // BNO055 does not save calibration settings when power cycled.
    // Perform a calibration procedure and update these values
    constexpr static DEV::bno055::CalibrationInfo defaultCal {
                            /* ACCEL_X */         0,
                            /* ACCEL_Y */         -14,
                            /* ACCEL_Z */         1,
                            /* MAG_X */           -258,
                            /* MAG_Y */           51,
                            /* MAG_Z */           51,
                            /* GYRO_X */          1,
                            /* GYRO_Y */          -1,
                            /* GYRO_Z */          -1,
                            /* ACCEL_RADIUS */    1000,
                            /* MAG_RADIUS */      268
                                                  };

    // Byte 0 - Remap axis orientation
    // Byte 1 - Remap axis direction (For each bit: 0 = Positive, 1 = Negative)
    constexpr uint8_t BNO055_AXIS_REMAP[2] = {0x21, 0x00};
}

namespace target
{

Imu::Imu() :
    my_i2c(nullptr),
    my_uart(nullptr),
    my_can(nullptr),
    my_evtCan(nullptr),
    my_led(nullptr),
    my_bno055(nullptr)
{
    error_manager::setTarget(this);
}

Imu::~Imu()
{
    // Empty destructor
}

Imu & Imu::getInstance()
{
    static Imu instance;
    return instance;
}

IO::i2cIntf & Imu::getI2C()
{
    if (my_i2c == nullptr)
    {
        my_i2c = &IO::i2cMbedAdapter::getInstance<DEFAULT_SDA, DEFAULT_SCL>();
    }
    return *my_i2c;
}

IO::uartIntf & Imu::getUART()
{
    if (my_uart == nullptr)
    {
        my_uart = &IO::uartMbedAdapter::getInstance<DEFAULT_TX, DEFAULT_RX>();
    }
    return *my_uart;
}

IO::CanIntf & Imu::getCAN()
{
    if (nullptr == my_can)
    {
        my_can = &IO::CanF302x8Adapter::getInstance<DEFAULT_CANRX, DEFAULT_CANTX, NUM_CAN_IDS, PCLK1_FREQUENCY, CAN_BAUD_RATE>(CAN_ID_LIST);
    }
    return *my_can;
}

IO::EvtCan & Imu::getEvtCan()
{
    if (nullptr == my_evtCan)
    {
        static IO::EvtCan instance(getCAN());
        my_evtCan = &instance;
    }
    return *my_evtCan;
}

IO::FlashIntf & Imu::getFlash()
{

    if (my_flash == nullptr)
    {
        my_flash = &IO::FlashF3xxHalAdapter::getInstance();
    }
    return *my_flash;
}

DEV::LED & Imu::getLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_PIN>());
    my_led = &instance;
    return *my_led;
}

DEV::bno055 & Imu::getBno055()
{
    static DEV::bno055 instance(getI2C(),
            IO::GpioMbedAdapter::getInstance<BNO055_PS0>(),
            IO::GpioMbedAdapter::getInstance<BNO055_PS1>());

    instance.loadCalibration(defaultCal);
    my_bno055 = &instance;
    return *my_bno055;
}

void Imu::logError(const char *msg, ErrorLevel level)
{
    const int MAX_ERROR_LENGTH = 80;
    IO::EvtCan & can = getEvtCan();

    // Send the error message over CAN
    can.sendError(IO::EvtCan::Board::IMU, level, msg, MAX_ERROR_LENGTH);
}

}


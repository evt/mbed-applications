
#include "target_nucleo_f302r8.h"
#include "io_can_f302x8_adapter.h"
#include "io_i2c_f3xx_hal_adapter.h"
#include "io_uart_f3xx_hal_adapter.h"
#include "io_spi_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"
#include "io_flash_f3xx_hal_adapter.h"
#include "io_dac_f3xx_hal_adapter.h"
#include "error_manager.h"

namespace
{
    // Pin defines
    constexpr IO::PIN DEFAULT_SDA = IO::PIN::MC_PB9;
    constexpr IO::PIN DEFAULT_SCL = IO::PIN::MC_PB8;
    constexpr unsigned DEFAULT_I2C_FREQ = 100000;
    constexpr int DEFAULT_I2C_MODULE = 1;

    constexpr PinName DEFAULT_MOSI = PB_15;
    constexpr PinName DEFAULT_MISO = PB_14;
    constexpr PinName DEFAULT_SCLK = PB_13;

    constexpr IO::PIN DEFAULT_TX = IO::PIN::MC_PA2; //For UART over USB
    constexpr IO::PIN DEFAULT_RX = IO::PIN::MC_PA3; //For UART over USB

    constexpr IO::PIN DAC_PIN = IO::PIN::MC_PA4;

    constexpr IO::PIN LED_PIN = IO::PIN::MC_PB13; 

    constexpr MC::Pin DEFAULT_CANRX(MC::Pin::B8);
    constexpr MC::Pin DEFAULT_CANTX(MC::Pin::B9);
    constexpr uint32_t CAN_BAUD_RATE(500000);
    constexpr unsigned int PCLK1_FREQUENCY(36000000u);

    // Subscribe to specific CAN IDs (CAN_RX)
    constexpr uint8_t NUM_CAN_IDS = 3;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {1337, 1338, 1339};
}

namespace target
{

nucleo_f302r8::nucleo_f302r8() :
    my_i2c(nullptr),
    my_uart(nullptr),
    my_spi(nullptr),
    my_can(nullptr),
    my_evtCan(nullptr),
    my_flash(nullptr),
    my_dac(nullptr),
    my_led(nullptr)
{
    error_manager::setTarget(this);
}

nucleo_f302r8::~nucleo_f302r8()
{
    // Empty destructor
}

nucleo_f302r8 & nucleo_f302r8::getInstance()
{
    static nucleo_f302r8 instance;
    return instance;
}

IO::i2cIntf & nucleo_f302r8::getI2C()
{

    if (my_i2c == nullptr)
    {
        my_i2c = &IO::I2cF3xxHalAdapter::getInstance<DEFAULT_SDA, DEFAULT_SCL, DEFAULT_I2C_FREQ, DEFAULT_I2C_MODULE>();
    }
    return *my_i2c;
}

IO::FlashIntf & nucleo_f302r8::getFlash()
{

    if (my_flash == nullptr)
    {
        my_flash = &IO::FlashF3xxHalAdapter::getInstance();
    }
    return *my_flash;
}

IO::SpiIntf & nucleo_f302r8::getSPI()
{
    if (my_spi == nullptr)
    {
        my_spi = &IO::SpiMbedAdapter::getInstance<DEFAULT_MOSI, DEFAULT_MISO, DEFAULT_SCLK>();
    }
    return *my_spi;
}

IO::uartIntf & nucleo_f302r8::getUART()
{
    if (my_uart == nullptr)
    {
        my_uart = &IO::UartF3xxHalAdapter::getInstance<DEFAULT_TX, DEFAULT_RX>();
    }
    return *my_uart;
}

IO::CanIntf & nucleo_f302r8::getCAN()
{
    if (nullptr == my_can)
    {
        my_can = &IO::CanF302x8Adapter::getInstance<DEFAULT_CANRX, DEFAULT_CANTX, NUM_CAN_IDS, PCLK1_FREQUENCY, CAN_BAUD_RATE>(CAN_ID_LIST);
    }
    return *my_can;
}

IO::EvtCan & nucleo_f302r8::getEvtCan()
{
    static IO::EvtCan _evtCan(getCAN());
    if (nullptr == my_evtCan)
    {
        my_evtCan = &_evtCan;
    }
    return *my_evtCan;
}

IO::DacIntf & nucleo_f302r8::getDAC()
{
    if (nullptr == my_dac)
    {
        my_dac = &IO::DacF3xxHalAdapter::getInstance<DAC_PIN>();
    }
    return *my_dac;
}

DEV::LED & nucleo_f302r8::getLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_PIN>());
    my_led = &instance;
    return *my_led;
}

void nucleo_f302r8::logError(const char *msg, ErrorLevel level)
{
    //UART and LED to display error
     IO::uartIntf & uart = getUART();
    DEV::LED & led = getLED();

    uart.printf("test");

    //checks level of error and displays to UART and the LED accordingly
    switch (level)
    {
    
        case ErrorLevel::INFO :
            uart.printf("Level: Info, Message: %s\n\r", msg);
            
            //blinks LED 5 times
            for (int i=0; i<5 ;i++)
            {
                led.toggle();
                wait(2.0);
                led.toggle();
                wait(2.0);
            }
            break;

        case ErrorLevel::WARNING :
            uart.printf("Level: Warning, Message: %s\n\r", msg);
            for (int i=0; i<5 ;i++)
            {
                led.toggle();
                wait(1.0);
                led.toggle();
                wait(1.0);
            }
            break;

        case ErrorLevel::ERROR :
            uart.printf("Level: Error, Message: %s\n\r", msg);
            for (int i=0; i<10 ;i++)
            {
                led.toggle();
                wait(0.5);
                led.toggle();
                wait(0.5);
            }
            break;

        case ErrorLevel::FATAL :
            uart.printf("Level: Fatal, Message: %s\n\r", msg);
            for (int i=0; i<10 ;i++)
            {
                led.toggle();
                wait(0.1);
                led.toggle();
                wait(0.1);
            }
            break;

        case ErrorLevel::DEBUG :
            uart.printf("Level: Debug, Message: %s\n\r", msg);
            
            //blinks LED 5 times
            for (int i=0; i<5 ;i++)
            {
                led.toggle();
                wait(1.0);
                led.toggle();
                wait(1.0);
                led.toggle();
                wait(0.2);
                led.toggle();
                wait(0.2);
            }
            break;

        default :
            break;            
    }
}

}


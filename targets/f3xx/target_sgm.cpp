#include "target_sgm.h"
#include "io_can_f302x8_adapter.h"
#include "io_spi_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"
#include "io_flash_f3xx_hal_adapter.h"
#include "mc_can.h"

namespace
{
    // Pin defines
    constexpr PinName DEFAULT_SDA = PB_9;
    constexpr PinName DEFAULT_SCL = PB_8;

    constexpr PinName DEFAULT_MOSI = PB_15;
    constexpr PinName DEFAULT_MISO = PB_14;
    constexpr PinName DEFAULT_SCLK = PB_13;
    constexpr IO::PIN DEFAULT_CS = IO::PIN::MC_PB12;

    constexpr IO::PIN HEARTBEAT_LED = IO::PIN::MC_PB11; 
    constexpr IO::PIN LED_1 = IO::PIN::MC_PB9; 
    constexpr IO::PIN LED_2 = IO::PIN::MC_PB8; 

    constexpr IO::PIN MUX0 = IO::PIN::MC_PA1;
    constexpr IO::PIN MUX1 = IO::PIN::MC_PA2;

    constexpr uint8_t NUM_SLAVE_BOARDS = 4u;
    constexpr uint8_t SLAVE0_ADDRESS = 0u;
    constexpr uint8_t SLAVE1_ADDRESS = 1u;
    constexpr uint8_t SLAVE2_ADDRESS = 2u;
    constexpr uint8_t SLAVE3_ADDRESS = 3u;

    constexpr MC::Pin DEFAULT_CANRX(MC::Pin::A11);
    constexpr MC::Pin DEFAULT_CANTX(MC::Pin::A12);
    constexpr uint32_t CAN_BAUD_RATE(500000);
    constexpr unsigned int PCLK1_FREQUENCY(32000000u);

    // Subscribe to specific CAN IDs (CAN_RX)
    constexpr uint8_t NUM_CAN_IDS = 1;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {0};
}

namespace target
{

Sgm::Sgm() : my_spi(nullptr), my_can(nullptr), my_evtCan(nullptr), my_heartbeat_led(nullptr), my_led1(nullptr), my_led2(nullptr), my_mux(nullptr)

{
    // Empty constructor
}

Sgm::~Sgm()
{
    // Empty destructor
}

Sgm & Sgm::getInstance()
{
    static Sgm instance;
    return instance;
}


IO::SpiIntf & Sgm::getSPI()
{
    if (nullptr == my_spi)
    {
        my_spi = &IO::SpiMbedAdapter::getInstance<DEFAULT_MOSI, DEFAULT_MISO, DEFAULT_SCLK>();
    }
    return *my_spi;
}


IO::CanIntf & Sgm::getCAN()
{
    if (nullptr == my_can)
    {
        my_can = &IO::CanF302x8Adapter::getInstance<DEFAULT_CANRX, DEFAULT_CANTX, NUM_CAN_IDS, PCLK1_FREQUENCY, CAN_BAUD_RATE>(CAN_ID_LIST);
    }
    return *my_can;
}

IO::EvtCan & Sgm::getEvtCan()
{
    if (nullptr == my_evtCan)
    {
        static IO::EvtCan instance(getCAN());
        my_evtCan = &instance;
    }
    return *my_evtCan;
}

IO::FlashIntf & Sgm::getFlash()
{

    if (my_flash == nullptr)
    {
        my_flash = &IO::FlashF3xxHalAdapter::getInstance();
    }
    return *my_flash;
}

DEV::LED & Sgm::getHeartbeatLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<HEARTBEAT_LED>());
    my_heartbeat_led = &instance;
    return *my_heartbeat_led;
}

DEV::LED & Sgm::getLED1()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_1>());
    my_led1 = &instance;
    return *my_led1;
}

DEV::LED & Sgm::getLED2()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_2>());
    my_led2 = &instance;
    return *my_led2;
}

DEV::LED & Sgm::getLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_1>());
    my_led1 = &instance;
    return *my_led1;
}

DEV::Sn74cb3q3253 & Sgm::getMUX()
{
    static DEV::Sn74cb3q3253 instance(IO::GpioMbedAdapter::getInstance<MUX1>(), IO::GpioMbedAdapter::getInstance<MUX0>());
    my_mux = &instance;
    return *my_mux;
}

IO::GpioIntf& Sgm::getChipSelect()
{
    static IO::GpioIntf & cs = IO::GpioMbedAdapter::getInstance<DEFAULT_CS>();
    cs.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);
    return cs;
}


std::pair<DEV::Ltc2440 *, uint8_t> Sgm::getLtc2440()
{
    static IO::GpioIntf & cs = IO::GpioMbedAdapter::getInstance<DEFAULT_CS>();
    cs.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);

    static std::array<DEV::Ltc2440, NUM_SLAVE_BOARDS> ltcArray = {
            DEV::Ltc2440(getSPI(), cs),
            DEV::Ltc2440(getSPI(), cs),
            DEV::Ltc2440(getSPI(), cs),
            DEV::Ltc2440(getSPI(), cs),
    };
    
    my_ltc2440s = ltcArray.data();
    
    return std::make_pair(my_ltc2440s, NUM_SLAVE_BOARDS);
}

void Sgm::logError(const char *msg, ErrorLevel level)
{
    const int MAX_ERROR_LENGTH = 80;
    IO::EvtCan & can = getEvtCan();

    // Send the error message over CAN
    can.sendError(IO::EvtCan::Board::SGM, level, msg, MAX_ERROR_LENGTH);
}

} // namespace target

 /*
 * file: target_tms.cpp
 * Purpose: Thermal Management System target file
 */

#include "target_tms.h"
#include "error_manager.h"
#include "mc_can.h"

#include "dev_evt_gpio_mux.h"
#include "dev_led.h"
#include "dev_stwd100.h"
#include "io_can_f302x8_adapter.h"
#include "io_evtCan.h"
#include "io_adc_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"
#include "io_i2c_mbed_adapter.h"
#include "io_pwm_mbed_adapter.h"
#include "io_uart_mbed_adapter.h"
#include "io_flash_f3xx_hal_adapter.h"
#include "io_adc_f3xx_hal_adapter.h"
#include "io_flash_intf.h"

namespace
{
    // Pin defines
    // CAN
    constexpr uint32_t CAN_BAUD_RATE(500000);
    constexpr unsigned int PCLK1_FREQUENCY(32000000u);
    constexpr MC::Pin DEFAULT_CANRX(MC::Pin::A11);
    constexpr MC::Pin DEFAULT_CANTX(MC::Pin::A12);


    // I2C
    constexpr PinName DEFAULT_SCL = PB_6;
    constexpr PinName DEFAULT_SDA = PB_7;

    // PWM
    constexpr IO::PIN PWM1 = IO::PIN::MC_PB15;
    constexpr IO::PIN PWM2 = IO::PIN::MC_PA5;

    // UART
    constexpr PinName DEFAULT_TX = PB_10;
    constexpr PinName DEFAULT_RX = PB_11;

    // LED
    constexpr IO::PIN LED = IO::PIN::MC_PA10; 

    // MUX
    constexpr IO::PIN MUX0 = IO::PIN::MC_PB9;
    constexpr IO::PIN MUX1 = IO::PIN::MC_PB8;
    constexpr IO::PIN MUX2 = IO::PIN::MC_PB2;

    // Watchdog
    constexpr IO::PIN WDI = IO::PIN::MC_PA8;
    constexpr IO::PIN WDE = IO::PIN::MC_PA9;
    
    // ADC Pins for Temperature and Current Sense
    constexpr IO::PIN TEMPERATURE_IN = IO::PIN::MC_PA4;
    constexpr IO::PIN CURRENT_SENSE1 = IO::PIN::MC_PA0;
    constexpr IO::PIN CURRENT_SENSE2 = IO::PIN::MC_PA1;

    // Flow Meter Data
    constexpr IO::PIN FLOW_DATA1 = IO::PIN::MC_PB0;
    constexpr IO::PIN FLOW_DATA2 = IO::PIN::MC_PB1;

    // Subscribe to specific CAN IDs (CAN_RX)
    constexpr uint8_t NUM_CAN_IDS = 1;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {0};

    constexpr uint8_t NUM_ADC_CHANNELS = 3;
    constexpr IO::PIN ADC_CHANNEL_LIST[NUM_ADC_CHANNELS] = {TEMPERATURE_IN, CURRENT_SENSE1, CURRENT_SENSE2};
}

namespace target
{

Tms::Tms() : my_can(nullptr),
             my_evtCan(nullptr),
             my_adc(nullptr),
             my_i2c(nullptr),
             my_pwm1(nullptr),
             my_pwm2(nullptr),
             my_uart(nullptr),
             my_led(nullptr),
             my_mux(nullptr),
             my_watchdog(nullptr)
{
    error_manager::setTarget(this);
}

Tms::~Tms()
{
    // Empty destructor
}

Tms & Tms::getInstance()
{
    static Tms instance;
    return instance;
}

IO::CanIntf & Tms::getCAN()
{
    if (nullptr == my_can)
    {
        my_can = &IO::CanF302x8Adapter::getInstance<DEFAULT_CANRX, DEFAULT_CANTX, NUM_CAN_IDS, PCLK1_FREQUENCY, CAN_BAUD_RATE>(CAN_ID_LIST);
    }
    return *my_can;
}

IO::EvtCan & Tms::getEvtCan()
{
    if (nullptr == my_evtCan)
    {
        static IO::EvtCan instance(getCAN());
        my_evtCan = &instance;
    }
    return *my_evtCan;
}

IO::i2cIntf & Tms::getI2C()
{
    if (my_i2c == nullptr)
    {
        my_i2c = &IO::i2cMbedAdapter::getInstance<DEFAULT_SDA, DEFAULT_SCL>();
    }
    return *my_i2c;
}

IO::PwmIntf & Tms::getPWM1()
{
    if (my_pwm1 == nullptr)
    {
        my_pwm1 = &IO::pwmMbedAdapter::getInstance<PWM1>();
    }
    return *my_pwm1;
}

IO::PwmIntf & Tms::getPWM2()
{
    if (my_pwm2 == nullptr)
    {
        my_pwm2 = &IO::pwmMbedAdapter::getInstance<PWM2>();
    }
    return *my_pwm2;
}

IO::uartIntf & Tms::getUART()
{
    if (my_uart == nullptr)
    {
        my_uart = &IO::uartMbedAdapter::getInstance<DEFAULT_TX, DEFAULT_RX>();
    }
    return *my_uart;
}

IO::AdcIntf & Tms::getADC()
{
    if (my_adc == nullptr)
    {
        my_adc = &IO::AdcF3xxHalAdapter::getInstance<NUM_ADC_CHANNELS>(ADC_CHANNEL_LIST);
    }

    return *my_adc;
}

IO::FlashIntf & Tms::getFlash()
{

    if (my_flash == nullptr)
    {
        my_flash = &IO::FlashF3xxHalAdapter::getInstance();
    }
    return *my_flash;
}

DEV::LED & Tms::getLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED>());
    my_led = &instance;
    return *my_led;
}

DEV::EvtGpioMux<3> & Tms::getMUX()
{
    IO::GpioIntf * my_sels[3] = {&IO::GpioMbedAdapter::getInstance<MUX2>(), 
                                  &IO::GpioMbedAdapter::getInstance<MUX1>(), 
                                  &IO::GpioMbedAdapter::getInstance<MUX0>()};
    static DEV::EvtGpioMux<3> instance(my_sels);
    my_mux = &instance;
    return *my_mux;
}

DEV::Stwd100 & Tms::getWatchdog()
{
    static DEV::Stwd100 instance(IO::GpioMbedAdapter::getInstance<WDI>());
    my_watchdog = &instance;
    return *my_watchdog;
}

void Tms::logError(const char *msg, ErrorLevel level)
{
    const int MAX_ERROR_LENGTH = 80;
    IO::EvtCan & can = getEvtCan();

    // Send the error message over CAN
    can.sendError(IO::EvtCan::Board::TMS, level, msg, MAX_ERROR_LENGTH);
}

} // namespace target

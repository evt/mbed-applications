// PA9 - TX
// PA10 - RX

#include "target_bms.h"
#include "io_can_f302x8_adapter.h"
#include "io_evtCan.h"
#include "io_i2c_mbed_adapter.h"
#include "io_uart_mbed_adapter.h"
#include "io_spi_mbed_adapter.h"
#include "io_gpio_mbed_adapter.h"
#include "io_flash_f3xx_hal_adapter.h"
#include "error_manager.h"
#include "mc_can.h"

namespace
{
    // Pin defines
    constexpr PinName DEFAULT_SDA = PB_9;
    constexpr PinName DEFAULT_SCL = PB_8;

    constexpr PinName DEFAULT_MOSI = PB_15;
    constexpr PinName DEFAULT_MISO = PB_14;
    constexpr PinName DEFAULT_SCLK = PB_13;

    constexpr PinName DEFAULT_TX = PA_9;
    constexpr PinName DEFAULT_RX = PA_10;

    constexpr IO::PIN LED_PIN = IO::PIN::MC_PA1;
    constexpr IO::PIN DEFAULT_CHIP_SELECT = IO::PIN::MC_PB12;

    constexpr MC::Pin DEFAULT_CANRX(MC::Pin::A11);
    constexpr MC::Pin DEFAULT_CANTX(MC::Pin::A12);
    constexpr uint32_t CAN_BAUD_RATE(500000);
    constexpr unsigned int PCLK1_FREQUENCY(32000000u);
    
    constexpr uint8_t NUM_SLAVE_BOARDS = 10u;

    // THESE NEED TO CHANGE BASED ON WHAT GETS PUT ON THE BIKE
    constexpr uint8_t SLAVE0_ADDRESS = 0u;
    constexpr uint8_t SLAVE1_ADDRESS = 1u;
    constexpr uint8_t SLAVE2_ADDRESS = 2u;
    constexpr uint8_t SLAVE3_ADDRESS = 3u;
    constexpr uint8_t SLAVE4_ADDRESS = 4u;
    constexpr uint8_t SLAVE5_ADDRESS = 5u;
    constexpr uint8_t SLAVE6_ADDRESS = 6u;
    constexpr uint8_t SLAVE7_ADDRESS = 7u;
    constexpr uint8_t SLAVE8_ADDRESS = 8u;
    constexpr uint8_t SLAVE9_ADDRESS = 9u;

    constexpr uint8_t SLAVE0_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE1_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE2_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE3_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE4_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE5_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE6_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE7_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE8_NUM_CELLS = 10u;
    constexpr uint8_t SLAVE9_NUM_CELLS = 10u;

    // Subscribe to specific CAN IDs (CAN_RX)
    constexpr uint8_t NUM_CAN_IDS = 1;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {0};

}

namespace target
{

Bms::Bms() : my_i2c(nullptr), my_uart(nullptr), my_spi(nullptr), my_can(nullptr), my_led(nullptr)
{
    error_manager::setTarget(this);
}

Bms::~Bms()
{
    // Empty destructor
}

Bms & Bms::getInstance()
{
    static Bms instance;
    return instance;
}

IO::i2cIntf & Bms::getI2C()
{
    if (nullptr == my_i2c)
    {
        my_i2c = &IO::i2cMbedAdapter::getInstance<DEFAULT_SDA, DEFAULT_SCL>();
    }
    return *my_i2c;
}

IO::SpiIntf & Bms::getSPI()
{
    if (nullptr == my_spi)
    {
        my_spi = &IO::SpiMbedAdapter::getInstance<DEFAULT_MOSI, DEFAULT_MISO, DEFAULT_SCLK>();
    }
    return *my_spi;
}

IO::uartIntf & Bms::getUART()
{
    if (nullptr == my_uart)
    {
        my_uart = &IO::uartMbedAdapter::getInstance<DEFAULT_TX, DEFAULT_RX>();
    }
    return *my_uart;
}

IO::CanIntf & Bms::getCAN()
{
    if (nullptr == my_can)
    {
        my_can = &IO::CanF302x8Adapter::getInstance<DEFAULT_CANRX, DEFAULT_CANTX, NUM_CAN_IDS, PCLK1_FREQUENCY, CAN_BAUD_RATE>(CAN_ID_LIST);
    }
    return *my_can;
}

IO::EvtCan & Bms::getEvtCan()
{
    if (nullptr == my_evtCan)
    {
        static IO::EvtCan instance(getCAN());
        my_evtCan = &instance;
    }
    return *my_evtCan;
}

IO::FlashIntf & Bms::getFlash()
{

    if (my_flash == nullptr)
    {
        my_flash = &IO::FlashF3xxHalAdapter::getInstance();
    }
    return *my_flash;
}

DEV::LED & Bms::getLED()
{
    static DEV::LED instance(IO::GpioMbedAdapter::getInstance<LED_PIN>());
    my_led = &instance;
    return *my_led;
}

std::pair<DEV::Ltc6811 *, uint8_t> Bms::getLtc6811()
{
    static IO::GpioIntf & ltcChipSelect = IO::GpioMbedAdapter::getInstance<DEFAULT_CHIP_SELECT>();
    ltcChipSelect.setPinDirection(IO::GpioIntf::DIRECTION::OUTPUT);

    static std::array<DEV::Ltc6811, NUM_SLAVE_BOARDS> ltcArray = {
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE0_ADDRESS, SLAVE0_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE1_ADDRESS, SLAVE1_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE2_ADDRESS, SLAVE2_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE3_ADDRESS, SLAVE3_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE4_ADDRESS, SLAVE4_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE5_ADDRESS, SLAVE5_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE6_ADDRESS, SLAVE6_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE7_ADDRESS, SLAVE7_NUM_CELLS),            
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE8_ADDRESS, SLAVE8_NUM_CELLS),
            DEV::Ltc6811(getSPI(), ltcChipSelect, SLAVE9_ADDRESS, SLAVE9_NUM_CELLS),
    };
    my_ltc6811 = ltcArray.data();
    
    return std::make_pair(my_ltc6811, NUM_SLAVE_BOARDS);
}

void Bms::logError(const char *msg, ErrorLevel level)
{
    const int MAX_ERROR_LENGTH = 80;
    IO::EvtCan & can = getEvtCan();

    // Send the error message over CAN
    can.sendError(IO::EvtCan::Board::BMS, level, msg, MAX_ERROR_LENGTH);
}

} // namespace target

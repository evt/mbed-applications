/*
*file: target_lvss.cpp
*Purpose: LVSS comms with GUB target file
*/

#include "target_lvss.h"
#include "dev_led.h"
#include "error_manager.h"
#include "io_can_f302x8_adapter.h"
#include "io_evtCan.h"
#include "io_i2c_mbed_adapter.h"
#include "io_uart_mbed_adapter.h"
#include "io_flash_f3xx_hal_adapter.h"
#include "mc_pin.h"

namespace
{
    //Pin define    
    constexpr PinName DEFAULT_SDA = PB_9;
    constexpr PinName DEFAULT_SCL = PB_8;
    constexpr PinName DEFAULT_TX = PA_11;
    constexpr PinName DEFAULT_RX = PA_12;
    constexpr IO::PIN STMCRL = IO::PIN::MC_PB0;
    constexpr IO::PIN IGNITION_STATE = IO::PIN::MC_PB1;
    constexpr IO::PIN APS = IO::PIN::MC_PB2;
    
    // CAN
    constexpr IO::PIN LED_PIN = IO::PIN::MC_PA1;

    //LEDs
    constexpr IO::PIN LED_PIN0 = IO::PIN::MC_PB10;
    constexpr IO::PIN LED_PIN1 = IO::PIN::MC_PB11;
    constexpr IO::PIN LED_PIN2 = IO::PIN::MC_PB12;
    constexpr IO::PIN LED_PIN3 = IO::PIN::MC_PB13;
    constexpr IO::PIN LED_PIN4 = IO::PIN::MC_PB14;
    constexpr uint8_t NUM_LEDS = 5;

    constexpr uint32_t CAN_BAUD_RATE(500000);
    constexpr unsigned int PCLK1_FREQUENCY(32000000u);

    constexpr MC::Pin DEFAULT_CANRX(MC::Pin::A11);
    constexpr MC::Pin DEFAULT_CANTX(MC::Pin::A12);

    // Subscribe to specific CAN IDs (CAN_RX)
    constexpr uint8_t NUM_CAN_IDS = 1;
    constexpr uint32_t CAN_ID_LIST[NUM_CAN_IDS] = {0};
}

namespace target
{
Lvss::Lvss() : my_can(nullptr),
               my_i2c(nullptr),
               my_uart(nullptr),
               my_ledArray(nullptr),
               my_stmcrl(nullptr),
               my_ignition_state(nullptr),
               my_aps(nullptr)
{
    error_manager::setTarget(this);
}

Lvss::~Lvss()
{
    //Empty destructor
}

Lvss & Lvss::getInstance()
{
    static Lvss instance;
    return instance;
}

IO::i2cIntf & Lvss::getI2C()
{
    if(nullptr == my_i2c)
    {
        my_i2c = &IO::i2cMbedAdapter::getInstance<DEFAULT_SDA, DEFAULT_SCL>();
    }
    return *my_i2c;
}

IO::uartIntf & Lvss::getUART()
{
    if (nullptr == my_uart)
    {
        my_uart = &IO::uartMbedAdapter::getInstance<DEFAULT_TX, DEFAULT_RX>();
    }
    return *my_uart;
}

IO::CanIntf & Lvss::getCAN()
{
    if (nullptr == my_can)
    {
        my_can = &IO::CanF302x8Adapter::getInstance<DEFAULT_CANRX, DEFAULT_CANTX, NUM_CAN_IDS, PCLK1_FREQUENCY, CAN_BAUD_RATE>(CAN_ID_LIST);
    }
    return *my_can;
}

IO::EvtCan & Lvss::getEvtCan()
{
    if (nullptr == my_evtCan)
    {
        static IO::EvtCan instance(getCAN());
        my_evtCan = &instance;
    }
    return *my_evtCan;
}

IO::FlashIntf & Lvss::getFlash()
{

    if (my_flash == nullptr)
    {
        my_flash = &IO::FlashF3xxHalAdapter::getInstance();
    }
    return *my_flash;
}

IO::GpioMbedAdapter & Lvss::getSTMCRL()
{
    if (nullptr == my_stmcrl)
    {
        my_stmcrl = &IO::GpioMbedAdapter::getInstance<STMCRL>();
    }
    return *my_stmcrl;
}

IO::GpioMbedAdapter & Lvss::getIGNITION_STATE()
{
    if (nullptr == my_ignition_state)
    {
        my_ignition_state = &IO::GpioMbedAdapter::getInstance<IGNITION_STATE>();
    }
    return *my_ignition_state;
}

IO::GpioMbedAdapter & Lvss::getAPS()
{
    if (nullptr == my_aps)
    {
        my_aps = &IO::GpioMbedAdapter::getInstance<APS>();
    }
    return *my_aps;
}

//LTC2992 & INA226 stubs
// Not implemented yet and therefore not recognized as a device
#if 0
DEV::Ltc2992 & getLtc2992()
{
    static DEV::Ltc2992 instance(getI2C());
    my_ltc2992 = &instance;
    return *my_ltc2992;
}
    
DEV::Ina226 & getIna226()
{
    static DEV::Ina226 instance(getI2C());
    my_Ina = &instance;
    return *my_Ina;
}
#endif

std::pair<DEV::LED *, uint8_t> Lvss::getLED()
{
  static std::array<DEV::LED, NUM_LEDS> ledArray = {
            DEV::LED(IO::GpioMbedAdapter::getInstance<LED_PIN0>()),
            DEV::LED(IO::GpioMbedAdapter::getInstance<LED_PIN1>()),
            DEV::LED(IO::GpioMbedAdapter::getInstance<LED_PIN2>()),
            DEV::LED(IO::GpioMbedAdapter::getInstance<LED_PIN3>()),
            DEV::LED(IO::GpioMbedAdapter::getInstance<LED_PIN4>())
    };
    my_ledArray = ledArray.data();
    
    return std::make_pair(my_ledArray, NUM_LEDS);
}

void Lvss::logError(const char *msg, ErrorLevel level)
{
    const int MAX_ERROR_LENGTH = 80;
    IO::EvtCan & can = getEvtCan();

    // Send the error message over CAN
    can.sendError(IO::EvtCan::Board::LVSS, level, msg, MAX_ERROR_LENGTH);
}
} // namespace target
/**
 * file: io_flash_f3xx_hal_adapter.h
 * purpose: header file for the f3xx's HAL flash adapter
 *
 * @author Dylan Arrabito dxa9503@rit.edu
 */

#pragma once

#include "io_flash_intf.h"
#include "stm32f3xx_hal.h"
#include "stm32f3xx_hal_flash.h"
#include "stm32f3xx_hal_flash_ex.h"

constexpr uint32_t PAGE31 = 0x0800F000;  // Last page in f302x8
constexpr auto PAGE30 = PAGE31 - FLASH_PAGE_SIZE;
constexpr int PAGES_30_31_WRITE_STATUS = OB_WRP_PAGES30TO31; // write protection for pages 30-31
constexpr uint8_t OPTION_DATA_WRITE_COMPLETE = 123;

namespace IO
{

class FlashF3xxHalAdapter : public FlashIntf
{
public:

    /* which option to program */
    enum class OptionByte
    {
        WRITE_PROTECTION = OPTIONBYTE_WRP,
        READ_PROTECTION  = OPTIONBYTE_RDP,
        USER             = OPTIONBYTE_USER,
        DATA             = OPTIONBYTE_DATA,
    };

    /* protection options for read and write */
    enum class Protection
    {
        DISABLE_WRITE_PROTECTION = OB_WRPSTATE_DISABLE,
        ENABLE_WRITE_PROTECTION  = OB_WRPSTATE_ENABLE,
        READ_PROTECTION_LEVEL_0  = OB_RDP_LEVEL_0,
        READ_PROTECTION_LEVEL_1  = OB_RDP_LEVEL_1,
        READ_PROTECTION_LEVEL_2  = OB_RDP_LEVEL_2,  // Warning: once level 2 is selected, it
                                                    // is no longer possible to select 0 or 1
    };

    enum class OptionByteAddress
    {
        DATA0 = OB_DATA_ADDRESS_DATA0,
        DATA1 = OB_DATA_ADDRESS_DATA1,
    };

    /* how flash should be programmed */
    enum class ProgramMode
    {
        HALFWORD   = FLASH_TYPEPROGRAM_HALFWORD,    // 16-bit
        WORD       = FLASH_TYPEPROGRAM_WORD,        // 32-bit
        DOUBLEWORD = FLASH_TYPEPROGRAM_DOUBLEWORD,  // 64-bit
    };
    
    typedef struct
    {
        HAL_StatusTypeDef halStatus;    // HAL_OK       = 0x00U,
                                        // HAL_ERROR    = 0x01U,
                                        // HAL_BUSY     = 0x02U,
                                        // HAL_TIMEOUT  = 0x03

        uint32_t pageError;        // 0xFFFFFFFF if pages are successfully erased
    } FlashStatus;


    inline static FlashF3xxHalAdapter & getInstance();

    uint64_t read(uint64_t *address) override;
    uint32_t read(uint32_t *address) override;
    uint16_t read(uint16_t *address) override;

    void write(uint32_t mode, uint64_t data, uint32_t address = PAGE31) override;
    void writePage(uint32_t address, uint32_t *data) override;

    void erasePage(uint32_t pageAddress = PAGE31) override;

    void swapPage(uint32_t destPage, uint32_t srcPage) override;

    HAL_StatusTypeDef getStatus();

private:

    FlashF3xxHalAdapter();
    ~FlashF3xxHalAdapter();


    // TODO: Make this public if we need to erase more than 1 page at once
    void eraseFlash(uint32_t pageAddress, uint32_t numOfPages);
    
    void programOptionBytes();

    FLASH_EraseInitTypeDef flashErase;  // controls how flash will be erased
    FLASH_OBProgramInitTypeDef optionProgram;   // controls various options of flash programming
    FlashStatus flashStatus;    // holds the status of hal/flash operations
    uint32_t pageBuffer[FLASH_PAGE_SIZE];
};

/* just get a static reference to the flash adapter */
inline FlashF3xxHalAdapter& FlashF3xxHalAdapter::getInstance()
{
    static FlashF3xxHalAdapter instance;

    return instance;
}

} // namespace IO
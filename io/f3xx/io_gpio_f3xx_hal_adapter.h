/*
 * file: io_gpio_f3xx_hal_adapter.h
 * purpose: Interface for GPIOs for STM32F3xx microcontrollers
 */

#pragma once

#include "io_gpio_intf.h"
#include "io_pin_names.h"
#include "evt_types.h"

namespace IO
{

class GpioF3xxHalAdapter : public GpioIntf
{
public:

    enum class TriggerEdge
    {
        RISING = 1,
        FALLING = 2,
        RISING_FALLING = 3
    };

    /*
     * getInstance()
     *
     * Returns a single instance of GpioF3xxHalAdapter
     */
    template<PIN pin> static GpioF3xxHalAdapter& getInstance();

    /*
     * setPinDirection()
     *
     * Changes the direction of the pin to either output or input
     *
     * Param: direction: The desired direction of the pin
     */
    void setPinDirection(GpioIntf::DIRECTION dir);


    /*
     * writePin()
     *
     * Sets the output level of the pin to high or low
     *
     * Param: state: Desired output state
     */
    void writePin(GpioIntf::STATE state);

    /*
     * readPin()
     *
     * Returns the current state of the GPIO pin (high or low)
     * If the GPIO is configured as an output it returns the
     * most recent set state
     */
    GpioIntf::STATE readPin(void);

    void registerIrq(TriggerEdge edge, void_function_ptr_t irqHandler);

    GpioF3xxHalAdapter() = delete;

private:

    constexpr static uint32_t GPIO_TRIGGER_INTERRUPT_BASE = 0x10010000U;

    // Private constructor/destructor
    explicit GpioF3xxHalAdapter(PIN pin);
    ~GpioF3xxHalAdapter();

    PIN my_pin;
    uint16_t my_halPin;     // Pin number based on bit position used for the HAL (bit 0 = pin0, bit 1 = pin1, etc...)
    GPIO_TypeDef * my_port; // GPIO port (A, B, C, D, F)

}; // class GpioF3xxHalAdatper

template<PIN pin>
GpioF3xxHalAdapter& GpioF3xxHalAdapter::getInstance()
{
    static GpioF3xxHalAdapter instance(pin);

    return instance;
};

} // namespace IO
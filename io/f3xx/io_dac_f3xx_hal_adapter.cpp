/*
 * file: io_dac_f3xx_hal_adapter.h
 * purpose: Contains implementation for using the DAC peripheral
 * controlled by the HAL
 */

#include "io_dac_f3xx_hal_adapter.h"
#include "stm32f3xx_hal_dac.h"

namespace IO
{

DacF3xxHalAdapter::DacF3xxHalAdapter(IO::PIN pin) : my_pin(pin)
{
    // Configure the pin(s)
    if (!(__HAL_RCC_DAC1_IS_CLK_ENABLED()))
        __HAL_RCC_DAC1_CLK_ENABLE();

    GPIO_InitTypeDef gpioInit;

    gpioInit.Pin = 1 << (static_cast<uint8_t>(pin) & 0x0F);
    gpioInit.Mode = GPIO_MODE_ANALOG;
    gpioInit.Pull = GPIO_NOPULL;

    // TODO: Support multiple channel DACs
    //       STM32f302x8 only has one channel, so we're leaving it like this for now
    __HAL_RCC_GPIOA_CLK_ENABLE();
    HAL_GPIO_Init(GPIOA, &gpioInit);

    // Initialize the peripheral
    my_hdac.Instance = DAC1;
    HAL_DAC_Init(&my_hdac);

    // Add the channel(s)
    DAC_ChannelConfTypeDef config;
    config.DAC_Trigger      = DAC_TRIGGER_NONE;
    config.DAC_OutputBuffer = DAC_OUTPUTBUFFER_DISABLE;
    HAL_DAC_ConfigChannel(&my_hdac, &config, DAC_CHANNEL_1);

    // Go!
    HAL_DAC_Start(&my_hdac, DAC_CHANNEL_1);
}

DacF3xxHalAdapter::~DacF3xxHalAdapter()
{
    // Empty destructor
}

void DacF3xxHalAdapter::write(float voltage)
{
    uint32_t data = static_cast<uint32_t>((voltage/3.3) * DAC_DATA_MAX);

    if (data > DAC_DATA_MAX)
        data = DAC_DATA_MAX;

    HAL_DAC_SetValue(&my_hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, data);
}

void DacF3xxHalAdapter::write(float voltage, uint8_t channel)
{
    write(voltage); // There's only one channel
}

} // namespace IO
/*
 * file: io_can_f302x8_adapter.h
 * purpose: Implements the standard CAN interface for the STM32F302x8 microcontroller.
 */

#ifndef IO_CAN_F302X8_ADAPTER_H
#define IO_CAN_F302X8_ADAPTER_H

#include <map>
#include "io_can_intf.h"
#include "mc_can.h"


/*
 * Forward Declarations
 */
namespace MC
{
enum class Pin;
} // namespace MC

namespace IO
{

class CanF302x8Adapter : public CanIntf
{

public:

    /*
     * Gets the target's instance of the CanF302x8Adapter.
     *
     * Returns a reference to a singleton instance of the CAN driver.
     */
    template<MC::Pin CANRX, MC::Pin CANTX, size_t numIds, unsigned int pclk1, unsigned int frequency=500000>
    static CanF302x8Adapter & getInstance(const uint32_t (&idList)[numIds]);

    /*
     * Transmits a single CAN message.
     *
     * message - Message to transmit: contains ID, data and dataLengthCode.
     */
    bool transmit(PROTOCOL::CanMessage const & message) override;

    /*
     * Looks for a CAN message with a specific ID in the rxBuffer.
     * Marks message as read if successful.
     *
     * canId - The ID of the message to search for
     *
     * return - PROTCOL::CanMessage * - Pointer to the message contents. Returns nullptr if it does not exist.
     */
    PROTOCOL::CanMessage * receive(uint32_t canId) override;

    uint8_t receiveBufferDepth() override;

    void insertCustomRxCallback(void_function_ptr_t callback, uint8_t port = 1) override;

    CanLog * getRxLog() override;

    /*
     * Definition for the elements that comprise the CAN rxBuffer
     * Contains flags to indicate messages were read or missed
     */
    struct CanRxBufferElement
    {

        // Laying groundwork for a "network manager" to log errors based on missed CAN messages...
        bool read;
        bool missed;

        PROTOCOL::CanMessage message;
        CanRxBufferElement() : read(true), missed(false)
        {
            // Empty constructor
        }

    };

private:

    // Lookup table that maps CAN IDs to their respective indices in rxBuffer
    std::map<uint32_t, uint32_t> my_rxLookupTable;

    // Buffer to hold incoming CAN messages. Each elements contains the message and 2 status flags
    // See the definition for CanRxBufferElement.
    CanRxBufferElement * my_rxBuffer;

    // Number of IDs to subscribe to
    uint8_t my_numIds;

    // Keep the constructor and destructor private to enforce the singleton.
    CanF302x8Adapter();
    ~CanF302x8Adapter();

    /*
     * ISR to place a newly received message in the rxBuffer.
     * If a message is in the buffer and was not read, it is overwritten and marked as "missed"
     */
    void receiveISR();
};


template<MC::Pin CANRX, MC::Pin CANTX, size_t numIds, unsigned int pclk1, unsigned int frequency /* = 500000 */>
CanF302x8Adapter & CanF302x8Adapter::getInstance(const uint32_t (&idList)[numIds])
{
    static CanF302x8Adapter instance;
    static CanRxBufferElement rxBuffer[numIds];
    static std::map<uint32_t, uint32_t> lookupTable;

    instance.my_rxBuffer = rxBuffer;
    instance.my_numIds = numIds;
    instance.my_rxLookupTable = lookupTable;

    for(unsigned int i = 0; i < numIds; i++)
        instance.my_rxLookupTable.insert({idList[i], i});

    MC::can_initializeGpioPins(CANRX, CANTX);
    MC::can_initialize(frequency, pclk1);
    MC::can_enableInterrupt(MC::CanIrq::RECEIVE, [](){ instance.receiveISR(); });

    return instance;
}

} // namespace IO

#endif // IO_CAN_F302X8_ADAPTER_H
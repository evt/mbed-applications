#include "io_rtc_f3xx_hal_adapter.h"
#include <stdio.h>
#include <string.h>

namespace
{
    RTC_HandleTypeDef halRtc;
    RTC_DateTypeDef rtcDate;
    RTC_TimeTypeDef rtcTime;
}

namespace IO
{
RtcF3xxHalAdapter::RtcF3xxHalAdapter()
{
    uint32_t status;

    halRtc.Instance             = RTC;

    halRtc.Init.HourFormat      = RTC_HOURFORMAT_24;
    halRtc.Init.AsynchPrediv    = ASYNC_PREDIV;
    halRtc.Init.SynchPrediv     = SYNC_PREDIV;
    halRtc.Init.OutPut          = RTC_OUTPUT_DISABLE;
    halRtc.Init.OutPutType      = RTC_OUTPUT_TYPE_PUSHPULL;
    halRtc.Init.OutPutPolarity  = RTC_OUTPUT_POLARITY_HIGH;

    __HAL_RCC_PWR_CLK_ENABLE();

    HAL_PWR_EnableBkUpAccess();

    status = HAL_RTCEx_BKUPRead(&halRtc, RTC_STATUS_REG);
    // If the HAL RTC was already in initialized
    if(status == RTC_STATUS_OK)
    {
        initInternalClock();
        HAL_RTC_WaitForSynchro(&halRtc);
        __HAL_RCC_CLEAR_RESET_FLAGS();
    }
    else
    {
        initInternalClock();
        HAL_RTC_Init(&halRtc);
        HAL_RTCEx_BKUPWrite(&halRtc, RTC_STATUS_REG, RTC_STATUS_OK);
    }
}

void RtcF3xxHalAdapter::initInternalClock()
{
    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    // Initialize the peripherials
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;

    // Set the clock to LSE
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.LSEState = RCC_LSE_ON;
    PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;

    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);

    __HAL_RCC_RTC_ENABLE();
}

void RtcF3xxHalAdapter::initTime(Time::Date & date, Time::Clock & clock)
{
    // RTC struct for the date
    rtcDate.WeekDay = date.day;
    rtcDate.Month   = static_cast<uint8_t>(date.month);
    rtcDate.Date    = date.day;
    rtcDate.Year    = date.year;

    // RTC struct for the time
    rtcTime.Hours           = clock.hours;
    rtcTime.Minutes         = clock.minutes;
    rtcTime.TimeFormat      = RTC_HOURFORMAT_24;
    rtcTime.Seconds         = clock.seconds;
    rtcTime.SubSeconds      = clock.subseconds;
    rtcTime.DayLightSaving  = RTC_DAYLIGHTSAVING_NONE;
    rtcTime.StoreOperation  = RTC_STOREOPERATION_RESET;

    HAL_RTC_SetDate(&halRtc, &rtcDate, RTC_FORMAT_BIN);
    HAL_RTC_SetTime(&halRtc, &rtcTime, RTC_FORMAT_BIN);
}

void RtcF3xxHalAdapter::readDate(Time::Date & date)
{
    HAL_RTC_GetDate(&halRtc, &rtcDate, RTC_FORMAT_BIN);
    date.weekDay = static_cast<Time::WeekDay>(rtcDate.WeekDay);
    date.month   = static_cast<Time::Month>(rtcDate.Month);
    date.day     = rtcDate.Date;
    date.year    = rtcDate.Year;
}

void RtcF3xxHalAdapter::readClock(Time::Clock & clock)
{
    HAL_RTC_GetTime(&halRtc, &rtcTime, RTC_FORMAT_BIN);
    clock.hours      = rtcTime.Hours;
    clock.minutes    = rtcTime.Minutes;
    clock.seconds    = rtcTime.Seconds;
    clock.subseconds = rtcTime.SubSeconds;
}

void RtcF3xxHalAdapter::getTimestamp(char * timestamp)
{
    Time::Clock clock;
    Time::Date date;

    readClock(clock);
    readDate(date);

    sprintf(timestamp, "%d-%d-%d %d:%d:%d.%d", date.year, static_cast<uint8_t>(date.month), static_cast<uint8_t>(date.day),
                                               clock.hours, clock.minutes, clock.seconds, clock.subseconds); 
}

RtcF3xxHalAdapter & RtcF3xxHalAdapter::getInstance()
{
    static RtcF3xxHalAdapter instance;
    return instance;
}

RtcF3xxHalAdapter::~RtcF3xxHalAdapter()
{
    // Empty
}
}
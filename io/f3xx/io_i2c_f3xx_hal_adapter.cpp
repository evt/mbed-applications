#include "io_i2c_f3xx_hal_adapter.h"

namespace IO
{

I2cF3xxHalAdapter::I2cF3xxHalAdapter(IO::PIN i2c_sda, IO::PIN i2c_scl, unsigned freq, uint8_t portId)
{
    GPIO_InitTypeDef gpioInit;

    switch (portId)
    {
        case 1:
            my_i2c.Instance = I2C1;

            if(!(__HAL_RCC_I2C1_IS_CLK_ENABLED()))
                __HAL_RCC_I2C1_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF4_I2C1;

            break;
        case 2:
            my_i2c.Instance = I2C2;

            if(!(__HAL_RCC_I2C2_IS_CLK_ENABLED()))
                __HAL_RCC_I2C2_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF4_I2C2;

            break;
        case 3:
            my_i2c.Instance = I2C3;

            if(!(__HAL_RCC_I2C3_IS_CLK_ENABLED()))
                __HAL_RCC_I2C3_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF2_I2C3;

            break;
        default:
            break;
    }

    IO::PIN myPins[2]   = {i2c_sda, i2c_scl};

    gpioInit.Pin        = static_cast<uint32_t>(1 << (static_cast<uint32_t>(myPins[0]) & 0x0F)) |
                        static_cast<uint32_t>(1 << (static_cast<uint32_t>(myPins[1]) & 0x0F));
    
    gpioInit.Mode       = GPIO_MODE_AF_OD;
    gpioInit.Pull       = GPIO_NOPULL;
    gpioInit.Speed      = GPIO_SPEED_FREQ_HIGH;

    for(uint8_t i = 0; i < 2; i++)
    {
        switch((static_cast<uint8_t>(myPins[i]) & 0xF0) >> 4)
        {
            case 0x0:
                __HAL_RCC_GPIOA_CLK_ENABLE();
                HAL_GPIO_Init(GPIOA, &gpioInit);
                break;
            case 0x1:
                __HAL_RCC_GPIOB_CLK_ENABLE();
                HAL_GPIO_Init(GPIOB, &gpioInit);
                break;
            case 0x2:
                __HAL_RCC_GPIOC_CLK_ENABLE();
                HAL_GPIO_Init(GPIOC, &gpioInit);
                break;
            case 0x3:
                __HAL_RCC_GPIOD_CLK_ENABLE();
                HAL_GPIO_Init(GPIOD, &gpioInit);
                break;
            default:
                break;
        }
    }

    my_i2c.Init.Timing           = 0x00310309; // 8MHz = 0x00310309; 16MHz = 0x10320309; 48MHz = 0x50330309
                                               // Timing Register Layout(Bits): [PRESC(4)][RESERVED(4)][SCLDEL(4)][SDADEL(4)][SCLH(8)][SCLL(8)]
    my_i2c.Init.OwnAddress1      = 0;
    my_i2c.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
    my_i2c.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
    my_i2c.Init.OwnAddress2      = 0;
    my_i2c.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    my_i2c.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
    my_i2c.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;

    HAL_I2C_Init(&my_i2c);
}

I2cF3xxHalAdapter::~I2cF3xxHalAdapter()
{
    // Empty destructor
}

void I2cF3xxHalAdapter::write(uint8_t addr, char *data, int length)
{
    HAL_I2C_Master_Transmit(&my_i2c, addr << 1, (uint8_t *) data, length, DEFAULT_I2C_TIMEOUT);
}

void I2cF3xxHalAdapter::read(uint8_t addr, char *data, int length)
{
    HAL_I2C_Master_Receive(&my_i2c, addr << 1, (uint8_t *) data, length, DEFAULT_I2C_TIMEOUT);
}
} // namespace IO
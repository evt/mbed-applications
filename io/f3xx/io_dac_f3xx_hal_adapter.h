/*
 * file: io_dac_f3xx_hal_adapter.h
 * purpose: Contains class definition for using the DAC peripheral
 * contrlled by the HAL
 */

#pragma once

#include "io_dac_intf.h"
#include "io_pin_names.h"
#include "stm32f3xx_hal_dac.h"
#include "stm32f3xx_hal_dac_ex.h"


namespace IO
{

class DacF3xxHalAdapter : public DacIntf
{

public:

    template<IO::PIN pin>
    static DacF3xxHalAdapter & getInstance();

    /*
     * write()
     *
     * Set the output voltage on the first configured channel
     *
     * param: voltage - Desired output voltage
     */
    void write(float voltage);

    /*
     * write()
     *
     * Set the output voltage on the specified channel
     * IMPLEMENTED ONLY TO SATISFY IO_DAC_INTF
     *
     * param: voltage - Desired output voltage
     *        channel - Channel number (has no effect)
     */
    void write(float voltage, uint8_t channel);

private:

    // Private constructor and destructor
    DacF3xxHalAdapter(IO::PIN pin);
    ~DacF3xxHalAdapter();

    DAC_HandleTypeDef my_hdac;
    IO::PIN my_pin;

    constexpr static uint32_t DAC_DATA_MAX = 0xFFF; // 12-bit dac

}; // class DacF3xxHalAdapter

template<IO::PIN pin>
DacF3xxHalAdapter & DacF3xxHalAdapter::getInstance()
{
    // TODO: Add support for more than just a single-channel DAC
    //       STM32F302x8 only has one option so we're leaving it like this
    static_assert(pin == IO::PIN::MC_PA4, "ERROR: Invalid pin.");
    static DacF3xxHalAdapter instance(pin);
    return instance;
}

} // namespace IO
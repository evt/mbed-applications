#include "io_i2c_intf.h"
#include "stm32f3xx_hal.h"
#include "io_pin_names.h"

namespace IO
{
class I2cF3xxHalAdapter : public i2cIntf
{
public:

    template<IO::PIN i2c_sda, IO::PIN i2c_scl, unsigned int freq, uint8_t portId>
    static I2cF3xxHalAdapter & getInstance();

    /** 
    *  write - Writes a string of data to an I2C slave.
    *
    *  Parameters:
    *      addr    - The unshifted 7-bit address of the I2C slave that will
    *                  be written to.
    *      data    - The pointer to the array of bytes that will be written
    *                  to the slave.
    *      length  - The number of bytes that will be written to the slave. 
    */
    void write(uint8_t addr, char *data, int length);


    /**
    *  read - Reads a string of data from an I2C slave.
    * 
    *  Parameters:
    *      addr    - The unshifted 7-bit address of the I2C slave that will
    *                  be read from.
    *      data    - The pointer to the buffer that will be used to store the
    *                  data recieved from the slave.
    *      length  - The number of bytes that will be read from the slave.
    */
    void read(uint8_t addr, char *data, int length);

private:

    I2C_HandleTypeDef my_i2c;

    // Private constructor and destructor
    I2cF3xxHalAdapter(IO::PIN i2c_sda, IO::PIN i2c_scl, unsigned int freq, uint8_t portId);
    ~I2cF3xxHalAdapter();

    constexpr static uint32_t DEFAULT_I2C_FREQ = 100000;
    constexpr static uint32_t DEFAULT_I2C_TIMEOUT = 100;
};

template <IO::PIN i2c_sda, IO::PIN i2c_scl, unsigned int freq, uint8_t portId>
I2cF3xxHalAdapter & I2cF3xxHalAdapter::getInstance()
{
    static I2cF3xxHalAdapter instance(i2c_sda, i2c_scl, freq, portId);
    return instance;
}
} // namespace IO
/*
 * file: io_flash_f3xx_hal_adapter.cpp
 * purpose: f3xx's HAL flash adapter which provides functions for interfacing
 *          with the f3xx's (specifically the f302x6/8 atm) to configure,
 *          read, and write to flash memory
 * 
 * @author Dylan Arrabito dxa9503@rit.edu
 */

#include "io_flash_f3xx_hal_adapter.h"
#include <string.h>


namespace IO
{

/*
 * @brief   Contructor for the flash HAL adapter which erases and configures
 *          the option bytes to use flash
 */
FlashF3xxHalAdapter::FlashF3xxHalAdapter()
{
    // check if custom config is in effect
    if(*reinterpret_cast<uint8_t *>(static_cast<uint32_t>(OptionByteAddress::DATA0)) != OPTION_DATA_WRITE_COMPLETE)
    {
        flashStatus.halStatus = HAL_FLASHEx_OBErase();  // erase any previous option byte configs

        /* Disable read and write protection for interfacing with flash */
        optionProgram.OptionType = static_cast<uint32_t>(OptionByte::WRITE_PROTECTION);
        optionProgram.WRPState   = static_cast<uint32_t>(Protection::DISABLE_WRITE_PROTECTION);
        programOptionBytes();

        optionProgram.OptionType = static_cast<uint32_t>(OptionByte::READ_PROTECTION);
        optionProgram.RDPLevel   = static_cast<uint32_t>(Protection::READ_PROTECTION_LEVEL_0);
        programOptionBytes();


        optionProgram.OptionType  = static_cast<uint32_t>(OptionByte::DATA);
        optionProgram.DATAAddress = static_cast<uint32_t>(OptionByteAddress::DATA0);
        optionProgram.DATAData    = OPTION_DATA_WRITE_COMPLETE;
        programOptionBytes();

       HAL_FLASH_OB_Launch();  // apply changes    
    }
}

/**
 * @brief Enable read and write protection the previously programmed option bytes
 * 
 */
FlashF3xxHalAdapter::~FlashF3xxHalAdapter()
{
    #if 0
    HAL_FLASHEx_OBErase();  // erase previous option byte configs

    optionProgram.OptionType = static_cast<uint32_t>(OptionByte::WRITE_PROTECTION);
    optionProgram.WRPState   = static_cast<uint32_t>(Protection::ENABLE_WRITE_PROTECTION);
    programOptionBytes();

    optionProgram.OptionType = static_cast<uint32_t>(OptionByte::READ_PROTECTION);
    optionProgram.RDPLevel   = static_cast<uint32_t>(Protection::READ_PROTECTION_LEVEL_2);
    programOptionBytes();

    optionProgram.OptionType  = static_cast<uint32_t>(OptionByte::DATA);
    optionProgram.DATAAddress = static_cast<uint32_t>(OptionByteAddress::DATA0);
    optionProgram.DATAData    = 0;
    #endif
}

/**
 * @brief Get status of last hal operation
 * 
 * @return HAL_StatusTypeDef 
 */
HAL_StatusTypeDef FlashF3xxHalAdapter::getStatus()
{
    return flashStatus.halStatus;
}

/**
 * @brief  Simply reads from an address in flash memory  
 * 
 * @param  address: address to read from
 * 
 * @return *address: data read from @param address
 */
uint64_t FlashF3xxHalAdapter::read(uint64_t *address)
{
    return *address;
}
uint32_t FlashF3xxHalAdapter::read(uint32_t *address)
{
    return *address;
}
uint16_t FlashF3xxHalAdapter::read(uint16_t *address)
{
    return *address;
}


/**
 * @brief Writes data to a specified address in flash
 * 
 * @param mode @ref ProgramMode: how to program, doubleword, word, or halfword
 * @param data what to program
 * @param address where to program
 */
void FlashF3xxHalAdapter::write(uint32_t mode, uint64_t data, uint32_t address)
{
    HAL_FLASH_Unlock(); // unlock flash interface
    HAL_FLASH_Program(mode, address, data);  // write to flash
    HAL_FLASH_Lock();   // lock up
}

void FlashF3xxHalAdapter::writePage(uint32_t address, uint32_t *data)
{
    for(int i = 0; i < FLASH_PAGE_SIZE; i+=sizeof(address))
    {
        write(static_cast<uint32_t>(ProgramMode::WORD), data[i/sizeof(address)], address+i);
    }
}

/**
 * @brief erases the page at the specified address in flash
 * 
 * @param pageAddress the page to erase
 */
void FlashF3xxHalAdapter::erasePage(uint32_t pageAddress)
{
    eraseFlash(pageAddress, 1);   // erase page/pages
}

void FlashF3xxHalAdapter::swapPage(uint32_t destPage, uint32_t srcPage)
{
    memcpy(pageBuffer, reinterpret_cast<uint32_t *>(srcPage), FLASH_PAGE_SIZE);
    erasePage(srcPage);
    erasePage(destPage);
    writePage(destPage, pageBuffer);
}

/**
 * @brief erases multiple pages in flash
 * 
 * @param pageAddress page to start erase at
 * @param numOfPages how many pages to erase
 */
void FlashF3xxHalAdapter::eraseFlash(uint32_t pageAddress, uint32_t numOfPages)
{
    flashErase.TypeErase   = FLASH_TYPEERASE_PAGES;  // how to perform erase operation
    flashErase.PageAddress = pageAddress;           // address of starting page to erase
    flashErase.NbPages     = numOfPages;            // how many pages to erase

    HAL_FLASH_Unlock(); // unlock the flash interface
    flashStatus.halStatus = HAL_FLASHEx_Erase(&flashErase, &flashStatus.pageError);  // perform flash erase
    HAL_FLASH_Lock();   // lock up
}

/**
 * @brief programs the option bytes specified by flashStatus
 * 
 */
void FlashF3xxHalAdapter::programOptionBytes()
{
    HAL_FLASH_Unlock();     // unlock flash interface
    HAL_FLASH_OB_Unlock();  // unlock option bytes

    flashStatus.halStatus = HAL_FLASHEx_OBProgram(&optionProgram);
    
    HAL_FLASH_Lock();       // lock up
    HAL_FLASH_OB_Lock();    // lock up
}

} // namespace IO

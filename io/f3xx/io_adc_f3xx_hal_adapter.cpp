/*
 * file: io_adc_f302x8_hal_adapter.cpp
 *
 *
 */

#include "io_adc_f3xx_hal_adapter.h"
#include "stm32f3xx_hal_adc.h"

namespace
{

ADC_HandleTypeDef halAdc;
DMA_HandleTypeDef halDma;

}

namespace IO
{

AdcF3xxHalAdapter::AdcF3xxHalAdapter(uint8_t numChannels)
{

    halAdc.Instance = ADC1;
    halAdc.Init.ClockPrescaler        = ADC_CLOCKPRESCALER_PCLK_DIV4; // TODO: Need a good value for this...seems to work for now
    halAdc.Init.Resolution            = ADC_RESOLUTION_12B;           // 12-bit resolution
    halAdc.Init.DataAlign             = ADC_DATAALIGN_RIGHT;          // LSB of data is located at bit 0
    halAdc.Init.ScanConvMode          = ENABLE;                       // Conversions performed in sequence across all channels
    halAdc.Init.EOCSelection          = DISABLE;                      // No end-of-conversion flag
    halAdc.Init.LowPowerAutoWait      = DISABLE;                      // Don't transition to low-power state when not in use
    halAdc.Init.ContinuousConvMode    = ENABLE;                       // Keep converting continuously after a trigger occurs (opposite of single mode / one conversion only)
    halAdc.Init.NbrOfConversion       = numChannels;                  // Number of channels we're going to convert
    halAdc.Init.DiscontinuousConvMode = DISABLE;                      // Perform all conversions in one complete sequence (as opposed to multiple discrete sequences)
    halAdc.Init.NbrOfDiscConversion   = 1;                            // Number of subdivisions for the main conversion sequence
    halAdc.Init.ExternalTrigConv      = ADC_SOFTWARE_START;           // Trigger manually with software
    halAdc.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;// Don't care since we're using ADC_SOFTWARE_START as our trigger
    halAdc.Init.DMAContinuousRequests = ENABLE;                       // Keep going once a full DMA transfer is done (opposite of single-shot mode)
    halAdc.Init.Overrun               = ADC_OVR_DATA_OVERWRITTEN;     // Overwrite the data in the DMA buffer upon overrun

    __HAL_RCC_ADC1_CLK_ENABLE();
    HAL_ADC_Init(&halAdc);
}

void AdcF3xxHalAdapter::addChannel(IO::PIN pin)
{
    static uint8_t rank = 1;
    GPIO_InitTypeDef gpioInit;
    ADC_ChannelConfTypeDef adcChannel;

    // Configure GPIO for ADC
    gpioInit.Pin  = (static_cast<uint8_t>(pin) & 0x0F) + 1;
    gpioInit.Mode = GPIO_MODE_ANALOG;
    gpioInit.Pull = GPIO_NOPULL;

    // Which GPIO bank?
    switch ((static_cast<uint8_t>(pin) & 0xF0) >> 4)
    {
        case 0x0:
            __HAL_RCC_GPIOA_CLK_ENABLE();
            HAL_GPIO_Init(GPIOA, &gpioInit);
            break;
        case 0x1:
            __HAL_RCC_GPIOB_CLK_ENABLE();
            HAL_GPIO_Init(GPIOB, &gpioInit);
            break;
        case 0x2:
            __HAL_RCC_GPIOC_CLK_ENABLE();
            HAL_GPIO_Init(GPIOC, &gpioInit);
            break;
        default:
            break; // Should never get here
    }

    switch (pin)
    {
        case IO::PIN::MC_PA0:
            adcChannel.Channel = ADC_CHANNEL_1;
            break;
        case IO::PIN::MC_PA1:
            adcChannel.Channel = ADC_CHANNEL_2;
            break;
        case IO::PIN::MC_PA2:
            adcChannel.Channel = ADC_CHANNEL_3;
            break;
        case IO::PIN::MC_PA3:
            adcChannel.Channel = ADC_CHANNEL_4;
            break;
        case IO::PIN::MC_PA4:
            adcChannel.Channel = ADC_CHANNEL_5;
            break;
        case IO::PIN::MC_PC0:
            adcChannel.Channel = ADC_CHANNEL_6;
            break;
        case IO::PIN::MC_PC1:
            adcChannel.Channel = ADC_CHANNEL_7;
            break;
        case IO::PIN::MC_PC2:
            adcChannel.Channel = ADC_CHANNEL_8;
            break;
        case IO::PIN::MC_PC3:
            adcChannel.Channel = ADC_CHANNEL_9;
            break;
        case IO::PIN::MC_PA6:
            adcChannel.Channel = ADC_CHANNEL_10;
            break;
        case IO::PIN::MC_PB0:
            adcChannel.Channel = ADC_CHANNEL_11;
            break;
        case IO::PIN::MC_PB1:
            adcChannel.Channel = ADC_CHANNEL_12;
            break;
        case IO::PIN::MC_PB13:
            adcChannel.Channel = ADC_CHANNEL_13;
            break;
        case IO::PIN::MC_PB11:
            adcChannel.Channel = ADC_CHANNEL_14;
            break;
        case IO::PIN::MC_PA7:
            adcChannel.Channel = ADC_CHANNEL_15;
            break;
        default:
            break; // Should never get here

    }

    // Subtract 1 because rank starts at 1
    my_channels[rank-1] = pin;

    adcChannel.Rank = rank++;
    adcChannel.SamplingTime = ADC_SAMPLETIME_601CYCLES_5;
    adcChannel.Offset = 0;

    HAL_ADC_ConfigChannel(&halAdc, &adcChannel);

    startDMA();
}

void AdcF3xxHalAdapter::startDMA()
{

    HAL_ADC_Stop(&halAdc);

    __HAL_RCC_DMA1_CLK_ENABLE();

    halDma.Instance                 = DMA1_Channel1;
    halDma.Init.Direction           = DMA_PERIPH_TO_MEMORY;    // Data going from the ADC peripheral to a buffer in memory
    halDma.Init.PeriphInc           = DMA_PINC_DISABLE;        // Disable peripheral pointer auto-increment
    halDma.Init.MemInc              = DMA_MINC_ENABLE;         // Enable memory pointer auto-increment
    halDma.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD; // 16-bit data alignment (the ADC runs in 12-bit mode)
    halDma.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;
    halDma.Init.Mode                = DMA_CIRCULAR;            // Wrap around to the beginning of the buffer when we reach the end
    halDma.Init.Priority            = DMA_PRIORITY_VERY_HIGH;

    HAL_DMA_Init(&halDma);
    HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

    __HAL_LINKDMA(&halAdc, DMA_Handle, halDma);

    HAL_ADC_Start_DMA(&halAdc, (uint32_t *)my_buffer, my_bufferLength);

}

void AdcF3xxHalAdapter::stopDMA()
{
    HAL_ADC_Stop_DMA(&halAdc);
}

float AdcF3xxHalAdapter::read()
{
    return static_cast<float>(my_buffer[0] / 4095.0); // 4095 is max value for a 12-bit ADC
}

float AdcF3xxHalAdapter::readChannel(IO::PIN pin)
{
    uint8_t channelNum = 0;
    // TODO: Don't use naive search. Probably OK for now since the number of channels on any given board is small
    while(my_channels[channelNum] != pin)
        channelNum++;
    return static_cast<float>(my_buffer[channelNum] / 4095.0); // 4095 is max value for a 12-bit ADC
}

uint16_t AdcF3xxHalAdapter::readChannelRaw(IO::PIN pin)
{
    uint8_t channelNum = 0;
    // TODO: Don't use naive search. Probably OK for now since the number of channels on any given board is small
    while(my_channels[channelNum] != pin)
        channelNum++;
    return my_buffer[channelNum];
}

uint16_t * AdcF3xxHalAdapter::getBuffer()
{
    return this->my_buffer;
}

} // namespace IO

extern "C"
{
    void DMA1_Channel1_IRQHandler()
    {
        HAL_DMA_IRQHandler(&halDma);
    }

    void ADC_IRQHandler()
    {
        HAL_ADC_IRQHandler(&halAdc);
    }
}

/*
 * file: io_can_f302x8_adapter.cpp
 * purpose: Implementation of logic for managing the CAN protocol transmission.
 */

#include "io_can_f302x8_adapter.h"
#include "error_manager.h"
#include <cstring>


namespace IO
{

CanF302x8Adapter::CanF302x8Adapter()
{
    // Empty constructor
}

CanF302x8Adapter::~CanF302x8Adapter()
{
    // Empty destructor
}

bool CanF302x8Adapter::transmit(PROTOCOL::CanMessage const & message)
{

    for (uint8_t i = 0; i < static_cast<uint8_t>(MC::CanTxMailbox::NUMBER_OF_MAILBOXES); ++i)
    {
        // Check for the first valid mailbox
        bool status = MC::can_write(&message, static_cast<MC::CanTxMailbox>(i));
        if (status)
        {
            // Write into the mailbox was successful

            // Busy wait until we detect that message transmit was a success
            while (!MC::can_previousTransmitSuccessful(static_cast<MC::CanTxMailbox>(i)));
            // Check for an error
            uint32_t error = MC::can_getErrorState();
            if (error)
            {
                char error_str[64];
                sprintf(error_str, "Error during transmit: %lu", error);
                error_manager::logError(error_str, target::ErrorLevel::ERROR);
            }
            return !error;
        }
    }
    // No mailboxes are free, will have to wait
    // TODO: Add in an internal buffer and make CAN asynchronous
    return false;
}


PROTOCOL::CanMessage * CanF302x8Adapter::receive(uint32_t canId)
{
    auto search = this->my_rxLookupTable.find(canId);
    if (search == this->my_rxLookupTable.end())
        return nullptr; // Couldn't find it in the table. This shouldn't happen if our hardware filters are good.

    uint32_t bufferIndex = search->second;

    // If we already read it
    if (this->my_rxBuffer[bufferIndex].read == true)
        return nullptr;

    this->my_rxBuffer[bufferIndex].read = true;

    return &(this->my_rxBuffer[search->second].message);
}


void CanF302x8Adapter::receiveISR()
{
    PROTOCOL::CanMessage receivedMessage;

    for (uint8_t i = 0; i < static_cast<uint8_t>(MC::CanRxMailbox::NUMBER_OF_MAILBOXES); ++i)
    {
        bool status = MC::can_read(&receivedMessage, static_cast<MC::CanRxMailbox>(i));
        if (status)
            break; // Read a message out successfully
    }

    auto search = this->my_rxLookupTable.find(receivedMessage.id);
    if (search == this->my_rxLookupTable.end())
        return; // Couldn't find it in the table. This shouldn't happen if our hardware filters are good.

    uint32_t bufferIndex = search->second;

    // If a message is there that we haven't read, mark it as missed
    if ((this->my_rxBuffer[bufferIndex].read == false))
        this->my_rxBuffer[bufferIndex].missed = true;

    this->my_rxBuffer[bufferIndex].read = false;

    std::memcpy(&(this->my_rxBuffer[bufferIndex].message),
                &(receivedMessage),
                sizeof(PROTOCOL::CanMessage));

}

uint8_t CanF302x8Adapter::receiveBufferDepth()
{
    // TODO: Add in receive buffer
    return 0;
}

void CanF302x8Adapter::insertCustomRxCallback(void_function_ptr_t callback, uint8_t port)
{
    // Placeholder
}

CanLog * CanF302x8Adapter::getRxLog()
{
    // Placeholder
    return nullptr;
}

} // namespace IO

/*
 * file: io_adc_intf.h
 * purpose: Defines a pure virtual interface for ADC class definitions to
 *          inherit from.
 */

#pragma once

namespace IO
{

class AdcIntf
{
protected:

    // Empty constructor
    AdcIntf() {}

public:

    // Empty destructor
    ~AdcIntf() {}

    /*
     * Reads the input voltage in volts on the analog-to-digital converter.
     */
    virtual float read() = 0;

}; // class AdcIntf

} // namespace IO
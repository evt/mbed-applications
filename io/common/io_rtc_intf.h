#pragma once

#include "evt_time.h"

namespace IO
{   

class RtcIntf
{
public:
    virtual void readDate(Time::Date & date) = 0;

    virtual void readClock(Time::Clock & clock) = 0;

    virtual void getTimestamp(char * timestamp) = 0;

    virtual void initTime(Time::Date & date, Time::Clock & clock) = 0;
};
}
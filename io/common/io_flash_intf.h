/*
 * file: io_flash_intf.h
 * purpose: Defines a pure virtual interface for Flash class definitions to
 *          inherit from.
 * 
 * @author Dylan Arrabito dxa9503@rit.edu
 */

#pragma once

#include <cstdint>

namespace IO
{

class FlashIntf
{
protected:

    // Empty constructor
    FlashIntf() {}

public:

    // Empty destructor
    ~FlashIntf() {}

    //TODO: What else should a flash interface provide?

    /* Halfword, word, and doubleword reads pssoible */
    virtual uint64_t read(uint64_t *address) = 0;
    virtual uint32_t read(uint32_t *address) = 0;
    virtual uint16_t read(uint16_t *address) = 0;
    
    virtual void write(uint32_t programMode, uint64_t data, uint32_t address) = 0;
    virtual void writePage(uint32_t address, uint32_t *data) = 0;

    virtual void erasePage(uint32_t pageAddress) = 0;

    virtual void swapPage(uint32_t destPage, uint32_t srcPage) = 0;
}; // class FlashIntf

} // namespace IO
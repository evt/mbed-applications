
#ifndef IO_EVT_CAN_H
#define IO_EVT_CAN_H

#include <cstdint>
#include "dev_sendyne_sim100.h"

/*
 * Forward Declarations
 */
namespace target
{
enum class ErrorLevel;
} // namespace target

namespace dev
{
    struct Status;
}

namespace IO
{

/*
 * Forward Declarations
 */
class CanIntf;

class EvtCan
{
public:

    // The different boards that produce CAN messages.
    // The numbers for each board correspond with the CAN ID
    // of their error_start message, since that is what this
    // enum is used for.
    enum class Board
    {
        SIM = 12u, /*1100*/
        TMS = 16u,
        BMS = 160u,
        LVSS = 170u,
        IMU = 544u,
        SGM = 672u
    };

    // Base message
    struct MessageBase
    {
        MessageBase() {}
        virtual ~MessageBase() {}

        // getData - get a pointer to the CAN message's data
        virtual uint8_t * getData() = 0;

        // getCanId - Return the appropriate CAN ID for a message
        //              involving this struct.
        virtual uint32_t getCanId() = 0;

        // getDataLen - Get the length of this CAN message's data
        virtual int getDataLen() = 0;
    };

    /*------- Low Voltage Sub-System -------*/
    struct LVSS_errorCodes : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 64;

        LVSS_errorCodes(uint8_t errorCodes);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct LVSS_powerOnOffCommand : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 65;

        LVSS_powerOnOffCommand(uint8_t cmd);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct LVSS_gubHandshake : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 66;

        LVSS_gubHandshake(uint8_t cmd);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct LVSS_highVoltagePower : public MessageBase
    {
        static constexpr int DATA_LENGTH = 2;
        static constexpr uint32_t CAN_ID = 67;

        LVSS_highVoltagePower(uint16_t wattsHV);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct LVSS_lowVoltagePower : public MessageBase
    {
        static constexpr int DATA_LENGTH = 4;
        static constexpr uint32_t CAN_ID = 68;

        LVSS_lowVoltagePower(uint16_t milliWatts12V, uint16_t milliWatts5V);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    /*------- Gateway LTE Utility Board -------*/
    struct GUB_requestBoardStatus : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 128;

        GUB_requestBoardStatus(uint8_t request);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct GUB_shutdownCmd : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 129;

        GUB_shutdownCmd(uint8_t cmd);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct GUB_lvssHandshake : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 130;

        GUB_lvssHandshake(uint8_t cmd);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    /*------- Thermal Management System -------*/
    struct TMS_errorCodes : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 192;

        TMS_errorCodes(uint8_t errorCodes);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];

    };

    struct TMS_hsdCurrent : public MessageBase
    {
        static constexpr int DATA_LENGTH = 4;
        static constexpr uint32_t CAN_ID = 193;

        TMS_hsdCurrent(uint16_t current1, uint16_t current2);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct TMS_temperatureSensors : public MessageBase
    {
        static constexpr int DATA_LENGTH = 6;
        static constexpr uint32_t CAN_ID = 194;

        TMS_temperatureSensors(uint8_t id1, uint16_t temperature1, uint8_t id2, uint16_t temperature2);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct TMS_liquidFlowRate : public MessageBase
    {
        static constexpr int DATA_LENGTH = 4;
        static constexpr uint32_t CAN_ID = 195;

        TMS_liquidFlowRate(uint16_t pumpFlow1, uint16_t pumpFlow2);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    //----- Battery Management System -----//
    struct BMS_errorCodes : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 256;

        BMS_errorCodes(uint8_t errorCodes);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct BMS_cellVoltage : public MessageBase
    {
        static constexpr int DATA_LENGTH = 6;
        static constexpr uint32_t CAN_ID = 257;

        BMS_cellVoltage(uint8_t id1, uint16_t voltage1, uint8_t id2, uint16_t voltage2);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct BMS_cellTemperature : public MessageBase
    {
        static constexpr int DATA_LENGTH = 6;
        static constexpr uint32_t CAN_ID = 258;

        BMS_cellTemperature(uint8_t id1, uint16_t temp1, uint8_t id2, uint16_t temp2);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct BMS_packVoltage : public MessageBase
    {
        static constexpr int DATA_LENGTH = 4;
        static constexpr uint32_t CAN_ID = 259;

        BMS_packVoltage(uint32_t voltage);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct BMS_packCurrent : public MessageBase
    {
        static constexpr int DATA_LENGTH = 8;
        static constexpr uint32_t CAN_ID = 260;

        BMS_packCurrent(uint32_t current1, uint32_t current2);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    //----- Inertial Measurement Unit -----//
    struct IMU_errorCodes : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 320;

        IMU_errorCodes(uint8_t errorCodes);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];

    };

    struct IMU_linearAcceleration : public MessageBase
    {
        static constexpr int DATA_LENGTH = 6;
        static constexpr uint32_t CAN_ID = 321;

        IMU_linearAcceleration(int16_t X, int16_t Y, int16_t Z);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct IMU_gravityVector : public MessageBase
    {
        static constexpr int DATA_LENGTH = 6;
        static constexpr uint32_t CAN_ID = 322;

        IMU_gravityVector(int16_t X, int16_t Y, int16_t Z);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct IMU_quaternionPosition : public MessageBase
    {
        static constexpr int DATA_LENGTH = 8;
        static constexpr uint32_t CAN_ID = 323;

        IMU_quaternionPosition(int16_t W, int16_t X, int16_t Y, int16_t Z);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct IMU_calibrationStatus : public MessageBase
    {
        static constexpr int DATA_LENGTH = 4;
        static constexpr uint32_t CAN_ID = 324;

        IMU_calibrationStatus(uint8_t sysCal, uint8_t accelCal, uint8_t magCal, uint8_t gyroCal);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    //----- Strain Gauge Module -----//
    struct SGM_errorCodes : public MessageBase
    {
        static constexpr int DATA_LENGTH = 1;
        static constexpr uint32_t CAN_ID = 384;

        SGM_errorCodes(uint8_t errorCodes);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    struct SGM_strain : public MessageBase
    {
        static constexpr int DATA_LENGTH = 5;
        static constexpr uint32_t CAN_ID = 385;

        SGM_strain(uint8_t gauge, uint32_t strain);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return CAN_ID; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint8_t data[DATA_LENGTH];
    };

    //----- Error Message Stuff ----//
    struct ErrorBase : public MessageBase
    {
        ErrorBase() {}
        virtual ~ErrorBase() {}
    };

    struct ERR_start : public ErrorBase
    {
        static constexpr int DATA_LENGTH = 1;

        ERR_start(uint32_t id, uint8_t level);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return my_id; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint32_t my_id;
        uint8_t data[DATA_LENGTH];
    };

    struct ERR_message : public ErrorBase
    {
        static constexpr int DATA_LENGTH = 8;

        ERR_message(uint32_t id, const char * msg);
        inline uint8_t * getData() { return data; }
        inline uint32_t getCanId() { return my_id; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint32_t my_id;
        uint8_t data[DATA_LENGTH];
    };

    struct ERR_end : public ErrorBase
    {
        static constexpr int DATA_LENGTH = 0;

        ERR_end(uint32_t id);
        inline uint8_t * getData() { return nullptr; }
        inline uint32_t getCanId() { return my_id; }
        inline int getDataLen() { return DATA_LENGTH; }

        private:
        uint32_t my_id;
    };

    //----- Ground Fault Message Stuff ----//
    struct SIMBase : public MessageBase
    {
      SIMBase(){}
      virtual ~SIMBase(){}

      static constexpr uint32_t CAN_ID = 168820993; //0xA100101
      static constexpr int DATA_LENGTH = 8;
      //Get the Opcode of the CAN message from the ground fault detecting device.
      virtual uint8_t getOpcode() = 0;
      uint8_t SIM_getStatus(DEV::Sim100::Status);

      inline uint32_t getCanId() { return CAN_ID; } 
    };

    struct SIM_IsolationState : public SIMBase
    {
      static constexpr uint8_t OP_CODE = 224; //0xE0

      SIM_IsolationState(DEV::Sim100::Status status, uint16_t electricIsolation,
        uint8_t electricUncertainty, uint16_t energyStored, uint8_t energyUncertainty);
      inline uint8_t * getData(){return data;}
      inline uint8_t getOpcode(){return OP_CODE;}
      inline int getDataLen(){return DATA_LENGTH;}

      private:
      uint8_t data[DATA_LENGTH];

    };

    struct SIM_IsolationResistance : public SIMBase
    {
      static constexpr uint8_t OP_CODE = 225; //0xE1

      SIM_IsolationResistance(DEV::Sim100::Status status, uint16_t resistanceP,
        uint8_t RpUncertainty, uint16_t resistanceN, uint8_t RnUncertainty);
      inline uint8_t * getData(){return data;}
      inline uint8_t getOpcode(){return OP_CODE;}
      inline int getDataLen(){return DATA_LENGTH;}

      private:
      uint8_t data[DATA_LENGTH];
    };

    struct SIM_IsolationCapacitance : public SIMBase
    {
      static constexpr uint8_t OP_CODE = 226; //0xE2

      SIM_IsolationCapacitance(DEV::Sim100::Status status, uint16_t capacitanceP,
        uint8_t CpUncertainty, uint16_t capacitanceN, uint8_t CnUncertainty);

      inline uint8_t * getData(){return data;}
      inline uint8_t getOpcode(){return OP_CODE;}
      inline int getDataLen(){return DATA_LENGTH;}

      private:
      uint8_t data[DATA_LENGTH];
    };

    struct SIM_Voltages : public SIMBase
    {
      static constexpr uint8_t OP_CODE = 227; //0xE3

      SIM_Voltages(DEV::Sim100::Status status, uint16_t voltageP,
        uint8_t VpUncertainty, uint16_t voltageN, uint8_t VnUncertainty);
      inline uint8_t * getData(){return data;}
      inline uint8_t getOpcode(){return OP_CODE;}
      inline int getDataLen(){return DATA_LENGTH;}

      private:
      uint8_t data[DATA_LENGTH];
    };

    struct SIM_BatteryVoltages : public SIMBase
    {
      static constexpr uint8_t OP_CODE = 228; //0xE4

      SIM_BatteryVoltages(DEV::Sim100::Status status, uint16_t voltageB,
        uint8_t VbUncertainty, uint16_t voltageBmax, uint8_t VbMaxUncertainty);
      inline uint8_t * getData(){return data;}
      inline uint8_t getOpcode(){return OP_CODE;}
      inline int getDataLen(){return DATA_LENGTH;}

      private:
      uint8_t data[DATA_LENGTH];
    };

    struct SIM_ErrorFlags : public SIMBase
    {
      static constexpr uint8_t OP_CODE = 229; //0xE5

      SIM_ErrorFlags(DEV::Sim100::Status status, DEV::Sim100::ErrorFlags errorFlags);
      uint8_t getErrorFlags(DEV::Sim100::ErrorFlags errorFlags);
      static constexpr int DATA_LENGTH = 3;
      inline uint8_t * getData(){return data;}
      inline uint8_t getOpcode(){return OP_CODE;}
      inline int getDataLen(){return DATA_LENGTH;}

      private:
      uint8_t data[DATA_LENGTH];
    };



    EvtCan(CanIntf & can);
    ~EvtCan();

    // sendMessage -    Send a message over CAN.
    //
    //  msg -           A reference to a populated message struct.
    void sendMessage(MessageBase & msg);

    // sendError -      Send an error message over CAN.
    //
    //  Board -         The board that is sending the error message.
    //  level -         The severity of the error.
    //  msg -           The string message describing the error.
    //  n -             The length of the string message.
    void sendError(Board source, target::ErrorLevel level, const char * msg, int n);

private:
    CanIntf & my_can;

}; // class evtCan

} // namespace IO

#endif // IO_EVT_CAN_H

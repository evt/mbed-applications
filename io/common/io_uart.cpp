/*
 * file: io_uart.cpp
 * @author Dan Popp ddp5730@rit.edu
 *
 * This file will hold shared implementations of the uart_intf between all chips
 */

#include "io_uart_intf.h"
#include <string.h>

namespace IO {

    char *uartIntf::gets(char *buf, int size) {
        char ret = '\0';
        int buf_index = 0;
        memset(buf, 0, size);

        while (ret != '\r' && ret != '\n' && ret != '\4') { // '\4' is EOF
            ret = getc();

            // Check if backspace key is entered
            if (ret == '\b') {
                if (buf_index > 0 && buf_index < size) {
                    buf[--buf_index] = '\0';
                    putc('\b');
                    putc(' ');
                    putc('\b');
                }
            }
                // Check if character should be written to buffer
            else if (buf_index < (size - 1) && ret != '\r' && ret != '\n' && ret != '\4') {
                buf[buf_index++] = ret;
                putc(ret);
            }
        }

        return buf;
    }

}


/*
 * file: io_dac_intf.h
 * purpose: Defines a pure virtual interface for DAC class definitions to
 *          inherit from.
 */

#pragma once

#include <cstdint>

namespace IO
{

class DacIntf
{
protected:

    // Empty constructor
    DacIntf() {}

public:

    // Empty destructor
    ~DacIntf() {}

    /*
     * Sets the voltage of the analog output pin
     */
    virtual void write(float voltage) = 0;
    virtual void write(float voltage, uint8_t channel) = 0;

}; // class DacIntf

} // namespace IO
/*
 * file: io_can_intf.h
 * purpose: Define an interface for CAN driver classes.
 */

#ifndef IO_CAN_INTF_H
#define IO_CAN_INTF_H


#include <cstdint>
#include "evt_types.h"
#include "evt_protocols_common.h"


namespace IO
{

class CanIntf
{
public:

    /*
     * Constructor and destructor
     */
    CanIntf() {}
    virtual ~CanIntf() {}

    /*
     * Queues a CAN message for transmission.
     *
     * message - The data to be transmitted.
     *
     * Returns: Whether the message was successfully queued or not. 
     */
    virtual bool transmit(PROTOCOL::CanMessage const & message) = 0;

    /*
     * Looks for a CAN message with a specific ID in the rxBuffer.
     * Marks message as read if successful.
     *
     * canId - The ID of the message to search for
     *
     * return - PROTCOL::CanMessage * - Pointer to the message contents. Returns nullptr if it does not exist.
     */
    virtual PROTOCOL::CanMessage * receive(uint32_t canId) = 0;

    /*
     * Checks if the receive buffer is empty.
     *
     * Returns: True if the buffer is empty, false otherwise.
     */
    virtual uint8_t receiveBufferDepth() = 0;

    /*
     * insertCustomRxCallback()
     *
     * Want to do something else besides store the message in the Rx buffer? Use this.
     *
     * callback - Pointer to custom callback function
     */
    virtual void insertCustomRxCallback(void_function_ptr_t callback, uint8_t port = 1) = 0;

    virtual CanLog * getRxLog() = 0;

};

} // namespace IO

#endif // IO_CAN_INTF_H
/*
 * file: io_uart_intf.h
 * purpose: Define the interface for UART.
 */

#ifndef IO_UART_INTF_H
#define IO_UART_INTF_H

namespace IO
{

class uartIntf
{
public:

    enum class Parity
    {
        NONE,
        ODD,
        EVEN,
        MASK,
        SPACE
    };

    // Empty constructor
    uartIntf() {}
    // Empty destructor
    virtual ~uartIntf() {}

    /** 
     *  baud - Sets the baud rate of the uart module.
     *
     *  Parameters:
     *      baudrate  - The new baudrate for the uart module.
     */
    virtual void baud(int baudrate) = 0;

    /** 
     *  format - Sets the transmission format of the uart module.
     *
     *  Parameters:
     *      bits - Number of bits in a word (from 5 to 8).
     *      Parity - The form of parity checking used.
     *      stopBits - The number of stop bits (1 or 2).
     */
    virtual void format(int bits=8, Parity parityType=Parity::NONE, int stopBits=1) = 0;

    /** 
     *  sendBreak - Generates a serial break condition.
     */
    virtual void sendBreak() = 0;

    /** 
     *  readable - Determines if the uart module has characters that are ready to be read.
     *
     *  Returns:
     *      true if there is a character to read. false otherwise.
     */
    virtual bool readable() = 0;

    /** 
     *  writeable - Determines if the uart module has space to write a character.
     *
     *  Returns:
     *      true if there is space in the uart module to write a character. false otherwise.
     */
    virtual bool writeable() = 0;
    
    /** 
     *  putc - Print a single character to the uart module.
     *
     *  Parameters:
     *      c - The character to print over uart.
     */
    virtual void putc(char c) = 0;

    /** 
     *  puts - Print a string of characters to the uart module.
     *
     *  Parameters:
     *      s - The pointer to the start of a null-terminated string.
     */
    virtual void puts(const char *s) = 0;

    /** 
     *  getc - Get a single character from the uart module. (Blocking)
     *
     *  Returns:
     *      The a character from the uart module.
     */
    virtual char getc() = 0;

    /** 
     *  gets - Get a string of characters from the uart module, stopping when a NL, CR, or EOF is
     *          encountered. (Blocking)
     *
     *  Parameters:
     *      buf - A pointer the che buffer to read characters into.
     *      size - The maximum number of characters to read.
     *
     *  Returns:
     *      'buf' on success or NULL on error.
     */
    char *gets(char *buf, int size);

    /** 
     *  puts - Print a string of characters to the uart module.
     *
     *  Parameters:
     *      format - The string to print over uart, including any format characters.
     *      ... - The other args that will be printed as part of 'format'.
     *
     *  NOTE:
     *      This function has poor portability (and probably performance), so avoid it when convienent.
     */
    virtual void printf(const char *format, ...) = 0;

    

}; // class UART_INTF

} // namespace IO
#endif //IO_UART_INTF_H

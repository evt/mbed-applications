
#include "io_evtCan.h"
#include "io_can_intf.h"

namespace IO
{

EvtCan::BMS_cellVoltage::BMS_cellVoltage(uint8_t id1, uint16_t voltage1, uint8_t id2, uint16_t voltage2)
{
    data[0] = id1;
    data[1] = static_cast<uint8_t>((voltage1 & 0x00ff) >> 0);
    data[2] = static_cast<uint8_t>((voltage1 & 0xff00) >> 8);
    data[3] = id2;
    data[4] = static_cast<uint8_t>((voltage2 & 0x00ff) >> 0);
    data[5] = static_cast<uint8_t>((voltage2 & 0xff00) >> 8);
}
EvtCan::BMS_cellTemperature::BMS_cellTemperature(uint8_t id1, uint16_t temp1, uint8_t id2, uint16_t temp2)
{
    data[0] = id1;
    data[1] = static_cast<uint8_t>((temp1 & 0x00ff) >> 0);
    data[2] = static_cast<uint8_t>((temp1 & 0xff00) >> 8);
    data[3] = id2;
    data[4] = static_cast<uint8_t>((temp2 & 0x00ff) >> 0);
    data[5] = static_cast<uint8_t>((temp2 & 0xff00) >> 8);
}
EvtCan::BMS_packVoltage::BMS_packVoltage(uint32_t voltage)
{
    data[0] = static_cast<uint8_t>((voltage & 0x000000ff) >> 0);
    data[1] = static_cast<uint8_t>((voltage & 0x0000ff00) >> 8);
    data[2] = static_cast<uint8_t>((voltage & 0x00ff0000) >> 16);
    data[3] = static_cast<uint8_t>((voltage & 0xff000000) >> 24);
}
EvtCan::TMS_temperatureSensors::TMS_temperatureSensors(uint8_t id1, uint16_t tempVoltage1, uint8_t id2, uint16_t tempVoltage2)
{
    data[0] = id1;
    data[1] = static_cast<uint8_t>((tempVoltage1 & 0x00ff) >> 0);
    data[2] = static_cast<uint8_t>((tempVoltage1 & 0xff00) >> 8);
    data[3] = id2;
    data[4] = static_cast<uint8_t>((tempVoltage2 & 0x00ff) >> 0);
    data[5] = static_cast<uint8_t>((tempVoltage2 & 0xff00) >> 8);
}
EvtCan::BMS_packCurrent::BMS_packCurrent(uint32_t current1, uint32_t current2)
{
    data[0] = static_cast<uint8_t>((current1 & 0x000000ff) >> 0);
    data[1] = static_cast<uint8_t>((current1 & 0x0000ff00) >> 8);
    data[2] = static_cast<uint8_t>((current1 & 0x00ff0000) >> 16);
    data[3] = static_cast<uint8_t>((current1 & 0xff000000) >> 24);

    data[4] = static_cast<uint8_t>((current2 & 0x000000ff) >> 0);
    data[5] = static_cast<uint8_t>((current2 & 0x0000ff00) >> 8);
    data[6] = static_cast<uint8_t>((current2 & 0x00ff0000) >> 16);
    data[7] = static_cast<uint8_t>((current2 & 0xff000000) >> 24);
}

EvtCan::IMU_linearAcceleration::IMU_linearAcceleration(int16_t X, int16_t Y, int16_t Z)
{
    data[0] = static_cast<uint8_t>((X & 0x00ff) >> 0);
    data[1] = static_cast<uint8_t>((X & 0xff00) >> 8);
    data[2] = static_cast<uint8_t>((Y & 0x00ff) >> 0);
    data[3] = static_cast<uint8_t>((Y & 0xff00) >> 8);
    data[4] = static_cast<uint8_t>((Z & 0x00ff) >> 0);
    data[5] = static_cast<uint8_t>((Z & 0xff00) >> 8);
}
EvtCan::IMU_gravityVector::IMU_gravityVector(int16_t X, int16_t Y, int16_t Z)
{
    data[0] = static_cast<uint8_t>((X & 0x00ff) >> 0);
    data[1] = static_cast<uint8_t>((X & 0xff00) >> 8);
    data[2] = static_cast<uint8_t>((Y & 0x00ff) >> 0);
    data[3] = static_cast<uint8_t>((Y & 0xff00) >> 8);
    data[4] = static_cast<uint8_t>((Z & 0x00ff) >> 0);
    data[5] = static_cast<uint8_t>((Z & 0xff00) >> 8);
}
EvtCan::IMU_quaternionPosition::IMU_quaternionPosition(int16_t W, int16_t X, int16_t Y, int16_t Z)
{
    data[0] = static_cast<uint8_t>((W & 0x00ff) >> 0);
    data[1] = static_cast<uint8_t>((W & 0xff00) >> 8);
    data[2] = static_cast<uint8_t>((X & 0x00ff) >> 0);
    data[3] = static_cast<uint8_t>((X & 0xff00) >> 8);
    data[4] = static_cast<uint8_t>((Y & 0x00ff) >> 0);
    data[5] = static_cast<uint8_t>((Y & 0xff00) >> 8);
    data[6] = static_cast<uint8_t>((Z & 0x00ff) >> 0);
    data[7] = static_cast<uint8_t>((Z & 0xff00) >> 8);
}
EvtCan::IMU_calibrationStatus::IMU_calibrationStatus(uint8_t sysCal, uint8_t accelCal, uint8_t magCal, uint8_t gyroCal)
{
    data[0] = sysCal;
    data[1] = accelCal;
    data[2] = magCal;
    data[3] = gyroCal;
}

EvtCan::SGM_strain::SGM_strain(uint8_t gauge, uint32_t strain)
{
    data[0] = gauge;
    data[1] = static_cast<uint8_t>((strain & 0x000000ff) >>  0);
    data[2] = static_cast<uint8_t>((strain & 0x0000ff00) >>  8);
    data[3] = static_cast<uint8_t>((strain & 0x00ff0000) >> 16);
    data[4] = static_cast<uint8_t>((strain & 0xff000000) >> 24);
}

uint8_t EvtCan::SIMBase::SIM_getStatus(DEV::Sim100::Status status)
{
    uint8_t result = 0;
    result = (((status.hardwareError & 0x1) << 7) | result);
    result = (((status.noNewEstimates & 0x1) << 6) | result);
    result = (((status.highUncertainty & 0x1) << 5) | result);
    result = (((status.highBatteryVoltage & 0x1) << 3) | result);
    result = (((status.lowBatteryVoltage & 0x1) << 2) | result);
    result = (((status.isolationFault & 0x1) << 1) | result);
    result = (((status.isoltationWarning & 0x1) << 1) | result);
    return result;
}

EvtCan::SIM_IsolationState::SIM_IsolationState(DEV::Sim100::Status status, uint16_t electricIsolation,
  uint8_t electricUncertainty, uint16_t energyStored, uint8_t energyUncertainty)
{
    data[0] = getOpcode();
    data[1] = SIM_getStatus(status);
    data[2] = static_cast<uint8_t>((electricIsolation & 0xff00) >> 8);
    data[3] = static_cast<uint8_t>((electricIsolation & 0x00ff) >> 0);
    data[4] = electricUncertainty;
    data[5] = static_cast<uint8_t>((energyStored & 0xff00) >> 8);
    data[6] = static_cast<uint8_t>((energyStored & 0x00ff) >> 0);
    data[7] = energyUncertainty;
}

EvtCan::SIM_IsolationResistance::SIM_IsolationResistance(DEV::Sim100::Status status, uint16_t resistanceP,
  uint8_t RpUncertainty, uint16_t resistanceN, uint8_t RnUncertainty)
{
    data[0] = getOpcode();
    data[1] = SIM_getStatus(status);
    data[2] = static_cast<uint8_t>((resistanceP & 0xff00) >> 8);
    data[3] = static_cast<uint8_t>((resistanceP & 0x00ff) >> 0);
    data[4] = RpUncertainty;
    data[5] = static_cast<uint8_t>((resistanceN & 0xff00) >> 8);
    data[6] = static_cast<uint8_t>((resistanceN & 0x00ff) >> 0);
    data[7] = RnUncertainty;
}

EvtCan::SIM_IsolationCapacitance::SIM_IsolationCapacitance(DEV::Sim100::Status status, uint16_t capacitanceP,
  uint8_t CpUncertainty, uint16_t capacitanceN, uint8_t CnUncertainty)
{
    data[0] = getOpcode();
    data[1] = SIM_getStatus(status);
    data[2] = static_cast<uint8_t>((capacitanceP & 0xff00) >> 8);
    data[3] = static_cast<uint8_t>((capacitanceP & 0x00ff) >> 0);
    data[4] = CpUncertainty;
    data[5] = static_cast<uint8_t>((capacitanceN & 0xff00) >> 8);
    data[6] = static_cast<uint8_t>((capacitanceN & 0x00ff) >> 0);
    data[7] = CnUncertainty;
}

EvtCan::SIM_Voltages::SIM_Voltages(DEV::Sim100::Status status, uint16_t voltageP,
  uint8_t VpUncertainty, uint16_t voltageN, uint8_t VnUncertainty)
{
    data[0] = getOpcode();
    data[1] = SIM_getStatus(status);
    data[2] = static_cast<uint8_t>((voltageP & 0xff00) >> 8);
    data[3] = static_cast<uint8_t>((voltageP & 0x00ff) >> 0);
    data[4] = VpUncertainty;
    data[5] = static_cast<uint8_t>((voltageN & 0xff00) >> 8);
    data[6] = static_cast<uint8_t>((voltageN & 0x00ff) >> 0);
    data[7] = VnUncertainty;
}

EvtCan::SIM_BatteryVoltages::SIM_BatteryVoltages(DEV::Sim100::Status status, uint16_t voltageB,
  uint8_t VbUncertainty, uint16_t voltageBmax, uint8_t VbMaxUncertainty)
{
    data[0] = getOpcode();
    data[1] = SIM_getStatus(status);
    data[2] = static_cast<uint8_t>((voltageB & 0xff00) >> 8);
    data[3] = static_cast<uint8_t>((voltageB & 0x00ff) >> 0);
    data[4] = VbUncertainty;
    data[5] = static_cast<uint8_t>((voltageBmax & 0xff00) >> 8);
    data[6] = static_cast<uint8_t>((voltageBmax & 0x00ff) >> 0);
    data[7] = VbMaxUncertainty;
}

EvtCan::SIM_ErrorFlags::SIM_ErrorFlags(DEV::Sim100::Status status, DEV::Sim100::ErrorFlags errorFlags)
{
    data[0] = getOpcode();
    data[1] = SIM_getStatus(status);
    data[2] = getErrorFlags(errorFlags);
}

uint8_t EvtCan::SIM_ErrorFlags::getErrorFlags(DEV::Sim100::ErrorFlags errorFlags)
{
    uint8_t result = 0;
    result = (((errorFlags.vx2Broken  & 0x1) << 7) | result);
    result = (((errorFlags.vx1Broken  & 0x1) << 6) | result);
    result = (((errorFlags.chassisConnectionBroken   & 0x1) << 5) | result);
    result = (((errorFlags.vx1Vx2Reversed  & 0x1) << 4) | result);
    result = (((errorFlags.voltageOutOfSpecs  & 0x1) << 3) | result);
    result = (((errorFlags.powerSupplyOutOfRange & 0x1) << 2) | result);
    return result;
}
  
EvtCan::ERR_start::ERR_start(uint32_t id, uint8_t level) : my_id(id)
{
    data[0] = level;
}
EvtCan::ERR_message::ERR_message(uint32_t id, const char * msg) : my_id(id)
{
    bool end_hit = false;

    for (int i = 0; i != DATA_LENGTH; ++i)
    {
        // If we've already hit the end, pad the data with null characters
        if (end_hit)
        {
            data[i] = '\0';
        }
        // Check to see if we hit the end of a null-terminated string
        else if (msg[i] == '\0')
        {
            end_hit = true;
            data[i] = '\0';
        }
        else
        {
            data[i] = msg[i];
        }
    }
}
EvtCan::ERR_end::ERR_end(uint32_t id) : my_id(id)
{
    // Empty Constructor
}

EvtCan::EvtCan(CanIntf& can) : my_can(can)
{
    // Empty Constructor
}

EvtCan::~EvtCan()
{
    // Empty Destructor
}

void EvtCan::sendMessage(MessageBase & msg)
{
    PROTOCOL::CanMessage canMsg(msg.getCanId(), msg.getData(), msg.getDataLen());
    my_can.transmit(canMsg);
}

void EvtCan::sendError(EvtCan::Board source, target::ErrorLevel level, const char * msg, int n)
{
    uint16_t canId = static_cast<uint16_t>(source);

    ERR_start startMsg(canId, static_cast<uint8_t>(level));
    sendMessage(startMsg);

    for (int i = 0; i < n; i += 8)
    {
        bool end_hit = false;

        ERR_message errMsg(canId+1, msg+i);
        sendMessage(errMsg);

        // Check to see if the set of bytes we just sent
        // contains a null-terminator.
        for (int j = 0; j != 8; ++j)
        {
            if (msg[i+j] == '\0')
            {
                end_hit = true;
                break;
            }
        }

        // See if we've hit the end
        if (end_hit)
        {
            break;
        }
    }

    ERR_end endMsg(canId+2);
    sendMessage(endMsg);
}

}

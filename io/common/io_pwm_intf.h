/*
 * file: io_pwm_intf.h
 * purpose: Define the interface for pulse width modulator
 */

#ifndef IO_PWM_INTF
#define IO_PWM_INTF

namespace IO
{

class PwmIntf
{

public:

    // Empty initializer
    PwmIntf() {}
    // Empty destructor
    virtual ~PwmIntf() {}

    // Interface for setting the duty cycle of the PWM
    virtual void setDutyCycle(float dutyCycle) = 0;

    // Interface for setting the period of the PWM
    virtual void setPeriodMicroSeconds(float periodUs) = 0;

    // Interface for getting the current duty cycle of the PWM
    virtual float getDutyCycle(void) = 0;

    // Interface for getting the period of the PWM
    virtual float getPeriodMicroSeconds(void) = 0;

};

}   // namespace IO

#endif  // IO_PWM_INTF

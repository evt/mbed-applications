/*
 * file: io_spi_intf.h
 * purpose: Define an interface for SPI driver classes to follow.
 */

#ifndef IO_SPI_INTF_H
#define IO_SPI_INTF_H

#include "io_gpio_intf.h"
#include <cstdint>

namespace IO
{

class SpiIntf
{
public:

    enum class BusMode
    {
        MODE_DEFAULT    = 0u,
        MODE_00         = 0u,
        MODE_01         = 1u,
        MODE_10         = 2u,
        MODE_11         = 3u,
    };

    enum class BitOrder
    {
        DEFAULT_FIRST   = 0u,
        MSB_FIRST       = 0u,
        LSB_FIRST       = 1u,
    };

    /*
     *Constructor and destructor
     *
     */
    SpiIntf() {}
    virtual ~SpiIntf() {}

    /* 
     * configureSpi()
     *
     * Configures parameters for bus mode and bit order. Bit order
     * support for mbed is ONLY MSB first. Bus mode is defined as
     * follows:
     *
     *   Mode   |   Clock Polarity  |  Phase
     *  =====================================
     *    0     |         0         |   0
     *    1     |         0         |   1
     *    2     |         1         |   0
     *    3     |         1         |   1
     *
     *
     */
    virtual void configureSpi(BusMode busMode, BitOrder bitOrder) = 0;

    /*
     * read()
     *
     * "Read" length bytes from the bus. Writes zeros to the
     *  bus to drive the clock for the slave to send the data.
     *
     */
    virtual void read(uint8_t * rxMessage, unsigned int length = 1) = 0;

    /*
     * write()
     *
     * Writes length bytes to the bus / slave device
     */
    virtual void write(uint8_t * txMessage, unsigned int length = 1) = 0;

    /*
     * setChipSelectHigh()
     *
     * Sets the GPIO used for chip select to the state GpioIntf::STATE::HIGH
     * Call when finished with a transfer to release control of the bus.
     */
    virtual void setChipSelectHigh(GpioIntf &cs) = 0;

    /*
     * setChipSelectLow()
     *
     * Sets the GPIO used for chip slect to the state GpioIntf::STATE::LOW
     * Call to initiate a transfer to take control of the bus as master.
     */
    virtual void setChipSelectLow(GpioIntf &cs) = 0;

};

} // namespace IO

#endif  // IO_SPI_INTF_H

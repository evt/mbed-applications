/*
 * file: io_can_f4xx_hal_adapter.h
 * purpose: Implements an interface for the STM32F4xx family of microcontrollers
 */
#pragma once

#include <map>
#include "io_can_intf.h"
#include "io_pin_names.h"
#include "evt_protocols_common.h"
#include "evt_types.h"

namespace IO
{
class CanF4xxHalAdapter : public CanIntf
{
public:

    /*
     * getInstance<>()
     *
     * Returns a reference to a single CAN instance.
     */
    template<PIN can_tx, PIN can_rx, size_t numIds, uint8_t canPortId>
    static CanF4xxHalAdapter & getInstance(const uint32_t (&idList)[numIds]);


    /*
     * transmit()
     *
     * Sends a single CAN message on the bus
     *
     * Param: message - Reference to outgoing message
     *
     * Return: bool for success indication
     */
    bool transmit(PROTOCOL::CanMessage const & message);

    /*
     * receive()
     *
     * Returns a pointer to an incoming CAN message with the corresponding CAN ID
     *
     * Param: canId - ID of the incoming message
     *
     * Return: Pointer to the CAN message's location in the receive buffer. nullptr if it doesn't exist.
     */
    PROTOCOL::CanMessage * receive(uint32_t canId);

    /*
     * receiveBufferDepth()
     *
     * Returns the amount of CAN messages present in the STM32 CANRx FIFO.
     */
    uint8_t receiveBufferDepth();

    /*
     * initHalReceiveBuffer()
     *
     * Passes pointers of the object's lookup table and receive buffers to
     * module-level variables used by the HAL callbacks that have external C linkage.
     */
    void initHalReceiveBuffer();

    /*
     * insertCustomRxCallback()
     *
     * Want to do something else besides store the message in the Rx buffer? Use this.
     *
     * callback - Pointer to custom callback function
     */
    void insertCustomRxCallback(void_function_ptr_t callback, uint8_t port = 1);

    CanLog * getRxLog();

    /*
     * Definition for the elements that comprise the CAN rxBuffer
     * Contains flags to indicate messages were read or missed
     */
    struct CanRxBufferElement
    {

        // Laying groundwork for a "network manager" to log errors based on missed CAN messages...
        bool read;
        bool missed;

        PROTOCOL::CanMessage message;
        CanRxBufferElement() : read(true), missed(false)
        {
            // Empty constructor
        }

    };

private:

    // Used for indicating to the HAL that filters 14-27 belong to the slave CAN instance (CAN2)
    // Implies that filters 0-13 belong to the master CAN instance (CAN1)
    constexpr static uint8_t SLAVE_FILTER_START = 14;

    std::map<uint32_t, uint32_t> my_rxLookupTable;
    CanRxBufferElement * my_rxBuffer;
    uint32_t my_numIds;

    // STM32F4xx microcontrollers can have up to 2 CAN IP blocks
    uint8_t my_canPortId;
    CAN_HandleTypeDef * my_canPort;

    // Empty constructor/destructor
    CanF4xxHalAdapter(PIN can_tx, PIN can_rx, uint8_t canPortId);
    ~CanF4xxHalAdapter();

};


template<PIN can_tx, PIN can_rx, size_t numIds, uint8_t canPortId>
CanF4xxHalAdapter & CanF4xxHalAdapter::getInstance(const uint32_t (&idList)[numIds])
{
    static_assert((canPortId == 1) || (canPortId == 2), "Error: Invalid CAN port ID. Must be 1 or 2.");

    static CanF4xxHalAdapter instance(can_tx, can_rx, canPortId);
    static CanRxBufferElement rxBuffer[numIds];
    static std::map<uint32_t, uint32_t> lookupTable;

    instance.my_rxBuffer = rxBuffer;
    instance.my_numIds = numIds;
    instance.my_rxLookupTable = lookupTable;
    instance.my_canPortId = canPortId;

    for(unsigned int i = 0; i < numIds; i++)
        instance.my_rxLookupTable.insert({idList[i], i});

    instance.initHalReceiveBuffer();

    return instance;
}

/*
 * storeCanMessage()
 *
 * Free-function to store a CAN message into the receive buffer used by the CAN adapter. Used by the HAL CAN Rx callbacks.
 *
 * Params: lookupTable - Pointer to lookup table used to find the location if the buffer to store the message based on ID
 *         buffer - Pointer to the received buffer used to to store the message
 *         message - Pointer to the incoming CAN message
 */
void storeCanMessage(std::map<uint32_t, uint32_t> * lookupTable, CanF4xxHalAdapter::CanRxBufferElement * rxBuffer, PROTOCOL::CanMessage * rxMessage);

} // namespace IO
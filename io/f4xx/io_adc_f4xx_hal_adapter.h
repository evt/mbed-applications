#pragma once

#include "io_adc_intf.h"
#include "io_gpio_mbed_adapter.h"
#include "stm32f4xx_hal_adc.h"
#include "stm32f4xx_hal_adc_ex.h"
#include "stm32f4xx.h"

namespace IO
{

class AdcF4xxHalAdapter : public AdcIntf
{
public:

    template<uint8_t numChannels, uint32_t dmaBufferSize = numChannels>
    static AdcF4xxHalAdapter & getInstance(const IO::PIN (&channelList)[numChannels]);


    /*
     * Adds and configures a new ADC channel
     *
     * pin - The microcontroller pin for the corresponding channel
     */
    void addChannel(IO::PIN pin);

    /*
     * Enables direct data transfer between the ADC peripheral
     * and a buffer in memory
     */
    void startDMA();

    /*
     * Disables direct data transfer between the ADC peripheral
     * and a buffer in memory
     */
    void stopDMA();


    /*
     * Implemented here for the sake of keeping IO::AdcIntf happy...
     *
     * Returns the value of the 1st configured ADC channel.
     */
    float read();

    /*
     * Read a single value from an ADC channel
     * Range is from 0.0 to 1.0 as a percentage of VDD
     *
     * pin - The pin to read from
     */
    float readChannel(IO::PIN pin);

    /*
     * Read a single raw value from an ADC channel
     * Range is from 0 to (2^n -1) for an n-bit ADC.
     * The F302x8 supports up to 12 bits, which is configured by default.
     *
     * pin - The pin to read from
     */
    uint16_t readChannelRaw(IO::PIN pin);

    /*
     * Returns a pointer to the DMA buffer
     */
    uint16_t * getBuffer();

private:
    static constexpr uint8_t MAX_NUM_CHANNELS = 15;

    // Constructor
    AdcF4xxHalAdapter(uint8_t numChannels);

    // Empty destructor
    ~AdcF4xxHalAdapter() {};

    uint16_t * my_buffer;
    uint16_t my_bufferLength;

    IO::PIN * my_channels;

    PIN my_pin;


};

template<uint8_t numChannels, uint32_t dmaBufferSize = numChannels>
AdcF4xxHalAdapter & AdcF4xxHalAdapter::getInstance(const IO::PIN (&channelList)[numChannels])
{
    static_assert(numChannels <= MAX_NUM_CHANNELS, "Error: Cannot configure ADC for more than 14 channels!");
    static_assert(numChannels > 0, "Error: Cannot configure ADC with 0 channels!");

    static AdcF4xxHalAdapter instance(numChannels);
    static uint16_t dmaBuffer[dmaBufferSize];
    static IO::PIN channels[numChannels];

    instance.my_buffer = dmaBuffer;
    instance.my_bufferLength = dmaBufferSize;
    instance.my_channels = channels;

    for (uint8_t i = 0; i < numChannels; i++)
        instance.addChannel(channelList[i]);

    return instance;
}

} // namespace IO

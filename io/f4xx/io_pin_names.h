/*
 * file: io_pin_names.h
 * purpose: Pin defines
 */

#pragma once

#include "mbed.h"

namespace IO
{
    enum class PIN
    {
        // Below are the microcontroller pins
        MC_PA0  =(int)PA_0,
        MC_PA1  =(int)PA_1,
        MC_PA2  =(int)PA_2,
        MC_PA3  =(int)PA_3,
        MC_PA4  =(int)PA_4,
        MC_PA5  =(int)PA_5,
        MC_PA6  =(int)PA_6,
        MC_PA7  =(int)PA_7,
        MC_PA8  =(int)PA_8,
        MC_PA9  =(int)PA_9,
        MC_PA10 =(int)PA_10,
        MC_PA11 =(int)PA_11,
        MC_PA12 =(int)PA_12,
        MC_PA13 =(int)PA_13,
        MC_PA14 =(int)PA_14,
        MC_PA15 =(int)PA_15,
        MC_PB0  =(int)PB_0,
        MC_PB1  =(int)PB_1,
        MC_PB2  =(int)PB_2,
        MC_PB3  =(int)PB_3,
        MC_PB4  =(int)PB_4,
        MC_PB5  =(int)PB_5,
        MC_PB6  =(int)PB_6,
        MC_PB7  =(int)PB_7,
        MC_PB8  =(int)PB_8,
        MC_PB9  =(int)PB_9,
        MC_PB10 =(int)PB_10,
        MC_PB12 =(int)PB_12,
        MC_PB13 =(int)PB_13,
        MC_PB14 =(int)PB_14,
        MC_PB15 =(int)PB_15,
        MC_PC0  =(int)PC_0,
        MC_PC1  =(int)PC_1,
        MC_PC2  =(int)PC_2,
        MC_PC3  =(int)PC_3,
        MC_PC4  =(int)PC_4,
        MC_PC5  =(int)PC_5,
        MC_PC6  =(int)PC_6,
        MC_PC7  =(int)PC_7,
        MC_PC8  =(int)PC_8,
        MC_PC9  =(int)PC_9,
        MC_PC10 =(int)PC_10,
        MC_PC11 =(int)PC_11,
        MC_PC12 =(int)PC_12,
        MC_PC13 =(int)PC_13,
        MC_PC14 =(int)PC_14,
        MC_PC15 =(int)PC_15,
        MC_PD2  =(int)PD_2,
        MC_PH0  =(int)PH_0,
        MC_PH1  =(int)PH_1,
        // Below are the mappings to the Arduino Uno R3 header naming
        R3_D0  = MC_PA3,
        R3_D1  = MC_PA2,
        R3_D2  = MC_PA10,
        R3_D3  = MC_PB3,
        R3_D4  = MC_PB5,
        R3_D5  = MC_PB4,
        R3_D6  = MC_PB10,
        R3_D7  = MC_PA8,
        R3_D8  = MC_PA9,
        R3_D9  = MC_PC7,
        R3_D10 = MC_PB6,
        R3_D11 = MC_PA7,
        R3_D12 = MC_PA6,
        R3_D13 = MC_PA5,
        R3_D14 = MC_PB9,
        R3_D15 = MC_PB8,

    };
}
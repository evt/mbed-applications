#include "io_uart_intf.h"
#include "stm32f4xx_hal.h"
#include "io_pin_names.h"

#include <map>

namespace IO
{

class UartF4xxHalAdapter : public uartIntf
{
public:

    /**
     *  getInstance - Returns the system's instance if UartF4xxHalAdapter
     * 
     *  Returns:
     *      A reference to to an instance of the uart module.
     */
    template <IO::PIN uart_tx, IO::PIN uart_rx>
    static UartF4xxHalAdapter & getInstance();

    /**
     *  baud - Sets the baud rate of the uart module.
     * 
     *  Parameters:
     *      baudrate - The new baudrate for the uart module.
     */
    void baud(int baudrate);

    /**
     *  format - Sets the transmission format of the uart module.
     * 
     *  Parameters:
     *      bits - Number of bits in a word (from 5 to 8).
     *      Parity - The form of parity checking used.
     *      stopBits - The number of stop bits (1 or 2).
     */
    void format(int bits=8, Parity parityType=Parity::NONE, int stopBits=1);

    /**
     *  sendBreak - Generates a serial break condition.
     */
    void sendBreak();

    /**
     *  readable - Determines if the uart module has characters that are ready to be read.
     * 
     *  Returns:
     *      true if there is a character to read. false otherwise.
     */
    bool readable();

    /**
     *  writeable - Determines if the uart module has space to write a character.
     * 
     *  Returns:
     *      true if there is space in the uart module to write a character. false otherwise.
     */
    bool writeable();

    /**
     *  putc - Print a single character to the uart module.
     * 
     *  Parameters:
     *      c - The character to print over uart.
     */
    void putc(char c);

    /**
     *  puts - Print a single character to the uart module.
     * 
     *  Parameters:
     *      c - The character to print over uart.
     */
    void puts(const char *s);

    /**
     *  getc - Get a single character from the uart module. (Blocking)
     * 
     *  Returns:
     *      The a character from the uart module.
     */
    char getc();

    /**
     *  printf - Print a string of characters to the uart module.
     * 
     *  Parameters:
     *      format - The string to print over uart, including any format characters.
     *      ... - The other args that will be printed as part of 'format'. 
     */
    void printf(const char *format, ...);

private:

    UART_HandleTypeDef my_uart;

    // Private constructors and destructor
    UartF4xxHalAdapter(IO::PIN uart_tx, IO::PIN uart_rx, uint8_t portId);
    ~UartF4xxHalAdapter();

    // Pin validation
    constexpr static bool checkPinsUart(IO::PIN TX, IO::PIN RX);

    constexpr static uint32_t DEFAULT_BAUDRATE = 115200;
    constexpr static uint32_t DEFAULT_TIMEOUT = 100;
};

template <IO::PIN uart_tx, IO::PIN uart_rx>
UartF4xxHalAdapter & UartF4xxHalAdapter::getInstance()
{
    static_assert(checkPinsUart(uart_tx, uart_rx), "Pins do not map to the same UART/USART module.");

    uint8_t portId;
    switch(uart_tx)
    {
        case IO::PIN::MC_PA9:
        case IO::PIN::MC_PB6:
            portId = 1;
            break;
        case IO::PIN::MC_PA2:
            portId = 2;
            break;
        case IO::PIN::MC_PB10:
            portId = 3;
            break;
        case IO::PIN::MC_PA0:
            portId = 4;
            break;
        case IO::PIN::MC_PC10: //This pin can map to two different modules...

            switch (uart_rx)
            {
            case IO::PIN::MC_PC5:
                portId = 3;
                break;
            case IO::PIN::MC_PA1:
                portId = 4;
                break;
            case IO::PIN::MC_PC11:
                portId = 3; //Defaulting to USART3 in the case of PC10 & PC11...
                break;
            default:
                break;
            }

            break;
        case IO::PIN::MC_PC12:
            portId = 5;
            break;
        case IO::PIN::MC_PC6:
            portId = 6;
            break;
        default:
            break;
    }

    static UartF4xxHalAdapter instance(uart_tx, uart_rx, portId);
    return instance;
}

constexpr bool UartF4xxHalAdapter::checkPinsUart(IO::PIN TX, IO::PIN RX)
{
    constexpr IO::PIN TxPins[] = {               // Module: Indices
            IO::PIN::MC_PA9, IO::PIN::MC_PB6,    // USART1_TX: 0,1
            IO::PIN::MC_PA2,                     // USART2_TX: 2
            IO::PIN::MC_PB10, IO::PIN::MC_PC10,  // USART3_TX: 3,4
            IO::PIN::MC_PA0, IO::PIN::MC_PC10,   // UART4_TX:  5,6
            IO::PIN::MC_PC12,                    // UART5_TX:  7
            IO::PIN::MC_PC6                      // USART6_TX: 8
    };

    constexpr IO::PIN RxPins[] = {               // Module: Indices
            IO::PIN::MC_PA10, IO::PIN::MC_PB7,   // USART1_RX: 0,1
            IO::PIN::MC_PA3,                     // USART2_RX: 2
            IO::PIN::MC_PC5, IO::PIN::MC_PC11,   // USART3_RX: 3,4
            IO::PIN::MC_PA1, IO::PIN::MC_PC11,   // UART4_RX:  5,6
            IO::PIN::MC_PD2,                     // UART5_RX:  7
            IO::PIN::MC_PC7                      // USART6_RX: 8
    };

    int txIndex = -1;
    int rxIndex = -1;

    bool validPins = false;
    bool sameModule = false;

    // Make sure the TX pin is valid and determine which module it belongs to.
    for(auto iter = std::cbegin(TxPins); iter != std::cend(TxPins); ++iter)
    {
        if(*iter == TX)
        {
            txIndex = iter - std::cbegin(TxPins);
            break;
        }
    }

    // Make sure the RX pin is valid and determine which module it belongs to.
    for(auto iter = std::cbegin(RxPins); iter != std::cend(RxPins); ++iter)
    {
        if(*iter == RX)
        {
            rxIndex = iter - std::cbegin(RxPins);
            break;
        }
    }

    // Make sure that all the pins were found in the arrays
    validPins = ((txIndex != -1) && (rxIndex != -1));

    // Make sure that all the pins belong to the same module
    if((txIndex == 0 || txIndex == 1) && (rxIndex == 0 || rxIndex == 1))
    {
        sameModule = true;
    }
    else if((txIndex == 2) && (rxIndex == 2))
    {
        sameModule = true;
    }
    else if((txIndex == 3 || txIndex == 4) && (rxIndex == 3 || rxIndex == 4))
    {
        sameModule = true;
    }
    else if((txIndex == 5 || txIndex == 6) && (rxIndex == 5 || rxIndex == 6))
    {
        sameModule = true;
    }
    else if((txIndex == 7) && (rxIndex == 7))
    {
        sameModule = true;
    }
    else if((txIndex == 8) && (rxIndex == 8))
    {
        sameModule = true;
    }

    return (validPins && sameModule);
}
}
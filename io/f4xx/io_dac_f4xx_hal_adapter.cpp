/*
 * file: io_dac_f4xx_hal_adapter.h
 * purpose: Contains implementation for using the DAC peripheral
 * controlled by the HAL
 */

#include "io_dac_f4xx_hal_adapter.h"
#include "io_pin_names.h"
#include "stm32f4xx_hal_dac.h"

namespace IO
{

DacF4xxHalAdapter::DacF4xxHalAdapter()
{
    // Pretty basic stuff here
    if (!(__HAL_RCC_DAC_IS_CLK_ENABLED()))
        __HAL_RCC_DAC_CLK_ENABLE();

    my_hdac.Instance = DAC1;
    HAL_DAC_Init(&my_hdac);

}

DacF4xxHalAdapter::~DacF4xxHalAdapter()
{
    // Empty destructor
}

void DacF4xxHalAdapter::addChannel(IO::PIN pin)
{
    GPIO_InitTypeDef gpioInit;
    DAC_ChannelConfTypeDef config;

    gpioInit.Pin = 1 << (static_cast<uint8_t>(pin) & 0x0F);
    gpioInit.Mode = GPIO_MODE_ANALOG;
    gpioInit.Pull = GPIO_NOPULL;

    config.DAC_Trigger      = DAC_TRIGGER_NONE;
    config.DAC_OutputBuffer = DAC_OUTPUTBUFFER_DISABLE;

    // We're assuming the user knows that the DAC pins are only available on PA4 and PA5
    if (!(__HAL_RCC_GPIOA_IS_CLK_ENABLED()))
        __HAL_RCC_GPIOA_CLK_ENABLE();

    HAL_GPIO_Init(GPIOA, &gpioInit);

    switch(pin)
    {
        case IO::PIN::MC_PA4:
            HAL_DAC_ConfigChannel(&my_hdac, &config, DAC_CHANNEL_1);
            HAL_DAC_Start(&my_hdac, DAC_CHANNEL_1);
            break;
        case IO::PIN::MC_PA5:
            HAL_DAC_ConfigChannel(&my_hdac, &config, DAC_CHANNEL_2);
            HAL_DAC_Start(&my_hdac, DAC_CHANNEL_2);
            break;
        default:
            break; // Should never get here
    }
}

void DacF4xxHalAdapter::write(float voltage)
{
    this->write(voltage, 1); // Default to channel 1
}

void DacF4xxHalAdapter::write(float voltage, uint8_t channel)
{
    uint32_t data = static_cast<uint32_t>((voltage/3.3) * DAC_DATA_MAX);

    if (data > DAC_DATA_MAX)
        data = DAC_DATA_MAX;

    switch(channel)
    {
        case 1:
            HAL_DAC_SetValue(&my_hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, data);
            break;
        case 2:
            HAL_DAC_SetValue(&my_hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, data);
            break;
        default:
            break;
    }
}

} // namespace IO
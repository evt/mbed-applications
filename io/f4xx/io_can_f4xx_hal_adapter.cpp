/*
 * file: io_can_f4xx_hal_adapter.cpp
 * purpose: Implements an interface for the STM32F4xx family of microcontrollers
 */
#include "io_can_f4xx_hal_adapter.h"
#include "stm32f4xx_hal_can.h"
#include <cstring>

namespace
{

CAN_HandleTypeDef hcan1;
CAN_HandleTypeDef hcan2;

// These are here because I can't grant direct access to private members of CanF4xxHalAdapter
// when using external HAL callbacks
IO::CanF4xxHalAdapter::CanRxBufferElement * rxBuffer1;
IO::CanF4xxHalAdapter::CanRxBufferElement * rxBuffer2;
std::map<uint32_t, uint32_t> * lookupTable1;
std::map<uint32_t, uint32_t> * lookupTable2;

// Structure for timestamped, in-order log of all messages on the bus
CanLog canLog1;
CanLog canLog2;

void_function_ptr_t CUSTOM_CALLBACKS[2] = {0};

}

extern "C" void CAN1_RX0_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1);
}

extern "C" void CAN2_RX0_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2);
}

extern "C" void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
    CAN_RxHeaderTypeDef rxHeader;
    uint8_t data[PROTOCOL::CAN_MESSAGE_MAX_LENGTH];

    HAL_CAN_GetRxMessage(hcan, 0, &rxHeader, data);

    PROTOCOL::CanMessage receivedMessage(rxHeader.ExtId, data, rxHeader.DLC);

    if (hcan->Instance == CAN1)
    {
        IO::storeCanMessage(lookupTable1, rxBuffer1, &receivedMessage);

        // Which buffer are we using?
        if (canLog1.context == 0)
        {
            std::memcpy(&canLog1.messages_A[canLog1.index], &receivedMessage, sizeof(PROTOCOL::CanMessage));
        }
        else if (canLog1.context == 1)
        {
            std::memcpy(&canLog1.messages_B[canLog1.index], &receivedMessage, sizeof(PROTOCOL::CanMessage));
        }

        if (nullptr != CUSTOM_CALLBACKS[0])
            CUSTOM_CALLBACKS[0]();

        // Move along.
        canLog1.index++;

        // Context switch to alternate buffer.
        if (canLog1.index == CAN_BUFFER_LENGTH)
        {

            if (canLog1.context == 0)
                canLog1.isFull_A = true;
            else if (canLog1.context == 1)
                canLog1.isFull_B = true;

            canLog1.context = (canLog1.context + 1) % 2;
            canLog1.index = 0;
        }

    }
    else if (hcan->Instance == CAN2)
    {
        IO::storeCanMessage(lookupTable2, rxBuffer2, &receivedMessage);

        // Which buffer are we using?
        if (canLog2.context == 0)
        {
            std::memcpy(&canLog2.messages_A[canLog2.index], &receivedMessage, sizeof(PROTOCOL::CanMessage));
        }
        else if (canLog2.context == 1)
        {
            std::memcpy(&canLog2.messages_B[canLog2.index], &receivedMessage, sizeof(PROTOCOL::CanMessage));
        }

        if (nullptr != CUSTOM_CALLBACKS[1])
            CUSTOM_CALLBACKS[1]();

        // Move along.
        canLog2.index++;

        // Context switch to alternate buffer.
        if (canLog2.index == CAN_BUFFER_LENGTH)
        {

            if (canLog2.context == 0)
                canLog2.isFull_A = true;
            else if (canLog2.context == 1)
                canLog2.isFull_B = true;

            canLog2.context = (canLog2.context + 1) % 2;
            canLog2.index = 0;
        }
    }
}

namespace IO
{

CanF4xxHalAdapter::CanF4xxHalAdapter(IO::PIN can_tx, IO::PIN can_rx, uint8_t canPortId) : my_canPortId(canPortId)
{
    if (my_canPortId == 1)
    {
        my_canPort = &hcan1;
        my_canPort->Instance = CAN1;
    }
    else if (my_canPortId == 2)
    {
        my_canPort = &hcan2;
        my_canPort->Instance = CAN2;
    }

    /* CAN2 is a "slave" relative to CAN1, cannot run without CAN1 being init'ed in hardware */
    if (!(__HAL_RCC_CAN1_IS_CLK_ENABLED()))
        __HAL_RCC_CAN1_CLK_ENABLE();
    if (!(__HAL_RCC_CAN2_IS_CLK_ENABLED()) && (my_canPortId == 2))
        __HAL_RCC_CAN2_CLK_ENABLE();

    /*
     * Configure pins
     * - Alternate function open drain
     * - Internal pullup enabled
     * - "Very high" toggle speed
     */
    GPIO_InitTypeDef gpioInit;
    PIN myPins[2] = {can_tx, can_rx};

    gpioInit.Pin = static_cast<uint32_t>(1 << (static_cast<uint32_t>(myPins[0]) & 0x0F)) |
                   static_cast<uint32_t>(1 << (static_cast<uint32_t>(myPins[1]) & 0x0F));
    gpioInit.Mode = GPIO_MODE_AF_OD;
    gpioInit.Pull = GPIO_PULLUP;
    gpioInit.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    if (canPortId == 1)
        gpioInit.Alternate = GPIO_AF9_CAN1;
    else
        gpioInit.Alternate = GPIO_AF9_CAN2;

    for (uint8_t i = 0; i < 2; i++)
    {
        switch((static_cast<uint8_t>(myPins[i]) & 0xF0) >> 4)
        {
            case 0x0:
                __HAL_RCC_GPIOA_CLK_ENABLE();
                HAL_GPIO_Init(GPIOA, &gpioInit);
                break;
            case 0x1:
                __HAL_RCC_GPIOB_CLK_ENABLE();
                HAL_GPIO_Init(GPIOB, &gpioInit);
                break;
            case 0x2:
                __HAL_RCC_GPIOC_CLK_ENABLE();
                HAL_GPIO_Init(GPIOC, &gpioInit);
                break;
            case 0x3:
                __HAL_RCC_GPIOD_CLK_ENABLE();
                HAL_GPIO_Init(GPIOC, &gpioInit);
                break;
            case 0x7:
                __HAL_RCC_GPIOH_CLK_ENABLE();
                HAL_GPIO_Init(GPIOC, &gpioInit);
                break;
            default:
                break; // Should never get here
        }
    }

    /* Initialize the CAN peripheral to 500kbps */
    my_canPort->Init.Prescaler            = 5;
    my_canPort->Init.Mode                 = CAN_MODE_NORMAL;
    my_canPort->Init.SyncJumpWidth        = CAN_SJW_1TQ;
    my_canPort->Init.TimeSeg1             = CAN_BS1_14TQ;
    my_canPort->Init.TimeSeg2             = CAN_BS2_3TQ;
    my_canPort->Init.TimeTriggeredMode    = DISABLE;
    my_canPort->Init.AutoBusOff           = DISABLE;
    my_canPort->Init.AutoWakeUp           = DISABLE;
    my_canPort->Init.AutoRetransmission   = DISABLE;
    my_canPort->Init.ReceiveFifoLocked    = DISABLE;
    my_canPort->Init.TransmitFifoPriority = DISABLE;

    HAL_CAN_Init(my_canPort);

    /* Enable CAN_Rx Interrupts */
    HAL_CAN_ActivateNotification(my_canPort,  CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_RX_FIFO1_MSG_PENDING);
    if (my_canPortId == 1)
    {
        NVIC_SetVector(CAN1_RX0_IRQn, (uint32_t)&CAN1_RX0_IRQHandler);
        HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
    }
    else if (my_canPortId == 2)
    {
        NVIC_SetVector(CAN2_RX0_IRQn, (uint32_t)&CAN2_RX0_IRQHandler);
        HAL_NVIC_EnableIRQ(CAN2_RX0_IRQn);
    }

    /* By default - filter that accepts all incoming messages */
    CAN_FilterTypeDef defaultFilter;
    defaultFilter.FilterIdHigh         = 0;
    defaultFilter.FilterIdLow          = 0;
    defaultFilter.FilterMaskIdHigh     = 0;
    defaultFilter.FilterMaskIdLow      = 0;
    defaultFilter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
    defaultFilter.FilterBank           = (SLAVE_FILTER_START * (my_canPortId-1)); // Apply to filter bank 0 if master, 14 if slave.
    defaultFilter.FilterMode           = CAN_FILTERMODE_IDMASK;
    defaultFilter.FilterScale          = CAN_FILTERSCALE_32BIT;
    defaultFilter.FilterActivation     = ENABLE;
    defaultFilter.SlaveStartFilterBank = SLAVE_FILTER_START; // Allocate filters for each of the master and slave (14 each)

    HAL_CAN_ConfigFilter(my_canPort, &defaultFilter);

    /* Start your engines! */
    HAL_CAN_Start(my_canPort);
}

CanF4xxHalAdapter::~CanF4xxHalAdapter()
{
    // Empty destructor
}

bool CanF4xxHalAdapter::transmit(PROTOCOL::CanMessage const & message)
{
    CAN_TxHeaderTypeDef txHeader;
    uint8_t payload[PROTOCOL::CAN_MESSAGE_MAX_LENGTH];

    uint32_t mailbox = CAN_TX_MAILBOX0;

    txHeader.ExtId              = message.id;
    txHeader.IDE                = CAN_ID_EXT;
    txHeader.RTR                = CAN_RTR_DATA;
    txHeader.DLC                = message.dataLengthCode;
    txHeader.TransmitGlobalTime = DISABLE;

    std::memcpy(payload, message.packet, sizeof(payload));

    HAL_CAN_AddTxMessage(this->my_canPort, &txHeader, payload, &mailbox);

    return true;
}

PROTOCOL::CanMessage * CanF4xxHalAdapter::receive(uint32_t canId)
{
    auto search = this->my_rxLookupTable.find(canId);
    if (search == this->my_rxLookupTable.end())
        return nullptr; // Couldn't find it in the table. This shouldn't happen if our hardware filters are good.

    uint32_t bufferIndex = search->second;

    // If we already read it
    if (this->my_rxBuffer[bufferIndex].read == true)
        return nullptr;

    this->my_rxBuffer[bufferIndex].read = true;

    return &(this->my_rxBuffer[search->second].message);
}

uint8_t CanF4xxHalAdapter::receiveBufferDepth()
{
    switch(this->my_canPortId)
    {
        case 1:
            return HAL_CAN_GetRxFifoFillLevel(&hcan1, 0);
            break;
        case 2:
            return HAL_CAN_GetRxFifoFillLevel(&hcan2, 0);
            break;
        default:
            return 0;
            break; // Should never get here
    }
}

void CanF4xxHalAdapter::initHalReceiveBuffer()
{
    switch(this->my_canPortId)
    {
        case 1:
            rxBuffer1 = this->my_rxBuffer;
            lookupTable1 = &(this->my_rxLookupTable);
            break;
        case 2:
            rxBuffer2 = this->my_rxBuffer;
            lookupTable2 = &(this->my_rxLookupTable);
            break;
        default:
            break; // Should never get here
    }
}

void CanF4xxHalAdapter::insertCustomRxCallback(void_function_ptr_t callback, uint8_t port)
{
    CUSTOM_CALLBACKS[port-1] = callback;
}

CanLog * CanF4xxHalAdapter::getRxLog()
{
    if (this->my_canPortId == 1)
        return &canLog1;
    else if (this->my_canPortId == 2)
        return &canLog2;
}

void storeCanMessage(std::map<uint32_t, uint32_t> * lookupTable, CanF4xxHalAdapter::CanRxBufferElement * rxBuffer, PROTOCOL::CanMessage * rxMessage)
{
    auto search = lookupTable->find(rxMessage->id);
    if (search == lookupTable->end())
        return; // Couldn't find it in the table. This shouldn't happen if our hardware filters are good.

    uint32_t bufferIndex = search->second;

    // If a message is there that we haven't read, mark it as missed
    if ((rxBuffer[bufferIndex].read == false))
         rxBuffer[bufferIndex].missed = true;

    rxBuffer[bufferIndex].read = false;

    std::memcpy(&(rxBuffer[bufferIndex].message),
                rxMessage,
                sizeof(PROTOCOL::CanMessage));
}

} // namespace IO


/*
 * file: io_gpio_f4xx_hal_adapter.cpp
 * purpose: Interface for GPIOs for STM32F4xx microcontrollers
 */

#include "io_gpio_f4xx_hal_adapter.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_ll_gpio.h"
#include "evt_types.h"

namespace
{
void_function_ptr_t INTERRUPT_HANDLERS[16] = {0};
}

extern "C" void EXTI0_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}
extern "C" void EXTI1_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}
extern "C" void EXTI2_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}
extern "C" void EXTI3_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}
extern "C" void EXTI4_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
}
extern "C" void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_6);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
}

extern "C" void EXTI15_10_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_12);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
}

extern "C" void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    uint16_t tmpPin = GPIO_Pin;
    uint8_t count = 0;

    while((tmpPin & 0x0001) == 0)
    {
        tmpPin = tmpPin >> 1;
        count++;
    }
    if (nullptr != INTERRUPT_HANDLERS[count])
    {
        INTERRUPT_HANDLERS[count]();
    }
}
namespace IO
{

GpioF4xxHalAdapter::GpioF4xxHalAdapter(PIN pin) : my_pin(pin)
{
    GPIO_InitTypeDef gpioInit;

    this->my_halPin = 1 << (static_cast<uint16_t>(this->my_pin) & 0x0F);
    switch((static_cast<uint8_t>(pin) & 0xF0) >> 4)
    {
        case 0x0:
            this->my_port = GPIOA;
            __HAL_RCC_GPIOA_CLK_ENABLE();
            break;
        case 0x1:
            this->my_port = GPIOB;
            __HAL_RCC_GPIOB_CLK_ENABLE();
            break;
        case 0x2:
            this->my_port = GPIOC;
            __HAL_RCC_GPIOC_CLK_ENABLE();
            break;
        case 0x3:
            this->my_port = GPIOD;
            __HAL_RCC_GPIOD_CLK_ENABLE();
            break;
        case 0x4:
            this->my_port = GPIOE;
            __HAL_RCC_GPIOE_CLK_ENABLE();
            break;
        case 0x5:
            this->my_port = GPIOF;
            __HAL_RCC_GPIOF_CLK_ENABLE();
            break;
        case 0x6:
            this->my_port = GPIOG;
            __HAL_RCC_GPIOG_CLK_ENABLE();
            break;
        case 0x7:
            this->my_port = GPIOH;
            __HAL_RCC_GPIOH_CLK_ENABLE();
            break;
        default:
            break; // Should never get here
    }

    gpioInit.Pin = this->my_halPin;
    gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
    gpioInit.Pull = GPIO_NOPULL;
    gpioInit.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(this->my_port, &gpioInit);
    this->writePin(GpioIntf::STATE::LOW); // Output set low by default
}

GpioF4xxHalAdapter::~GpioF4xxHalAdapter()
{
    // Empty destructor
}

void GpioF4xxHalAdapter::setPinDirection(GpioIntf::DIRECTION direction)
{
    LL_GPIO_SetPinMode(this->my_port, static_cast<uint32_t>(this->my_halPin), static_cast<uint32_t>(direction));
}

void GpioF4xxHalAdapter::writePin(GpioIntf::STATE state)
{
    HAL_GPIO_WritePin(this->my_port, this->my_halPin, static_cast<GPIO_PinState>(state));
}

GpioIntf::STATE GpioF4xxHalAdapter::readPin()
{
    return static_cast<GpioIntf::STATE>(HAL_GPIO_ReadPin(this->my_port, this->my_halPin));
}

void GpioF4xxHalAdapter::registerIrq(TriggerEdge edge, void_function_ptr_t irqHandler)
{
    GPIO_InitTypeDef gpioInit;

    gpioInit.Pin = this->my_halPin;
    gpioInit.Mode = GPIO_TRIGGER_INTERRUPT_BASE | (static_cast<uint32_t>(edge) << 20);
    gpioInit.Pull = GPIO_NOPULL;
    gpioInit.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(this->my_port, &gpioInit);

    INTERRUPT_HANDLERS[(static_cast<uint8_t>(this->my_pin) & 0x0F)] = irqHandler;

    switch(this->my_halPin)
    {
        case GPIO_PIN_0:
            NVIC_SetVector(EXTI0_IRQn, (uint32_t)&EXTI0_IRQHandler);
            HAL_NVIC_EnableIRQ(EXTI0_IRQn);
            break;
        case GPIO_PIN_1:
            NVIC_SetVector(EXTI1_IRQn, (uint32_t)&EXTI1_IRQHandler);
            HAL_NVIC_EnableIRQ(EXTI1_IRQn);
            break;
        case GPIO_PIN_2:
            NVIC_SetVector(EXTI2_IRQn, (uint32_t)&EXTI2_IRQHandler);
            HAL_NVIC_EnableIRQ(EXTI2_IRQn);
            break;
        case GPIO_PIN_3:
            NVIC_SetVector(EXTI3_IRQn, (uint32_t)&EXTI3_IRQHandler);
            HAL_NVIC_EnableIRQ(EXTI3_IRQn);
            break;
        case GPIO_PIN_4:
            NVIC_SetVector(EXTI4_IRQn, (uint32_t)&EXTI4_IRQHandler);
            HAL_NVIC_EnableIRQ(EXTI4_IRQn);
            break;
        case GPIO_PIN_5:
        case GPIO_PIN_6:
        case GPIO_PIN_7:
        case GPIO_PIN_8:
        case GPIO_PIN_9:
            NVIC_SetVector(EXTI9_5_IRQn, (uint32_t)&EXTI9_5_IRQHandler);
            HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
            break;
        case GPIO_PIN_10:
        case GPIO_PIN_11:
        case GPIO_PIN_12:
        case GPIO_PIN_13:
        case GPIO_PIN_14:
        case GPIO_PIN_15:
            NVIC_SetVector(EXTI15_10_IRQn, (uint32_t)&EXTI15_10_IRQHandler);
            HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
            break;

        default:
            break; // Shouldn't get here
    }
}

} // namespace IO
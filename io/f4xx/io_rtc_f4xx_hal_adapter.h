#pragma once

#include "io_rtc_intf.h"
#include "stm32f4xx_hal.h"
#include "evt_time.h"

namespace IO
{

class RtcF4xxHalAdapter : public RtcIntf
{
public:
    /**
     * Creates a static instance of the f4xx hal adapter with the given date
     * and time as the starting date and time for the RTC
     * @param date the start date to use for the RTC
     * @param time the start time to use for the RTC
     */
    static RtcF4xxHalAdapter & getInstance();

    /**
     * Read the time from the HAL RTC and store the result in the given date
     * @param date the date value to modify with the read date
     */ 
    void readDate(Time::Date & date);

    /**
     * Read the time from the HAL RTC and store the result in the given time
     * NOTE: Calling read clock will lock the shadow register and will cause 
     * subsequent readings to not update. Get date has to be called AFTER
     * each call of read clock to unlock the shadow register
     * @param time the time value to modify with the read time
     */ 
    void readClock(Time::Clock & clock);

    /**
     * Read the time from the RTC and set the passed in character array to the
     * time in the format MM-DD-YY HH:MM:SS.sss
     * @param timestamp char array to fill with the timestamp value
     */
    void getTimestamp(char * timestamp);

    /**
     * Initialize the RTC date and time with the passed in values
     * @param date the date to set the RTC to
     * @param clock the value of the clock to set the RTC to
     */  
    void initTime(Time::Date & date, Time::Clock & clock);

private:
    /**
     * Used to initialize the RTC with given parameters
     */ 
    void initInternalClock();

    RtcF4xxHalAdapter();
    ~RtcF4xxHalAdapter();

    constexpr static uint8_t SYNC_PREDIV        = 255;
    constexpr static uint8_t ASYNC_PREDIV       = 127;
    constexpr static uint16_t RTC_STATUS_OK     = 0x4321;
    constexpr static uint16_t RTC_STATUS_REG    = RTC_BKP_DR19;
};
}
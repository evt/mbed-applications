#include "io_uart_f4xx_hal_adapter.h"
#include <string.h>
#include <stdio.h>

namespace IO
{

UartF4xxHalAdapter::UartF4xxHalAdapter(IO::PIN uart_tx, IO::PIN uart_rx, uint8_t portId)
{
    GPIO_InitTypeDef gpioInit;
    
    switch (portId)
    {
        case 1:
            my_uart.Instance = USART1;

            if(!(__HAL_RCC_USART1_IS_CLK_ENABLED()))
                __HAL_RCC_USART1_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF7_USART1;

            break;
        case 2:
            my_uart.Instance = USART2;

            if (!(__HAL_RCC_USART2_IS_CLK_ENABLED()))
                __HAL_RCC_USART2_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF7_USART2;
        
            break;
        case 3:
            my_uart.Instance = USART3;

            if(!(__HAL_RCC_USART3_IS_CLK_ENABLED()))
                __HAL_RCC_USART3_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF7_USART3;

            break;
        case 4:
            my_uart.Instance = UART4;

            if(!(__HAL_RCC_UART4_IS_CLK_ENABLED()))
                __HAL_RCC_UART4_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF8_UART4;

            break;
        case 5:
            my_uart.Instance = UART5;

            if(!(__HAL_RCC_UART5_IS_CLK_ENABLED()))
                __HAL_RCC_UART5_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF8_UART5;

            break;
        case 6:
            my_uart.Instance = USART6;

            if(!(__HAL_RCC_USART6_IS_CLK_ENABLED()))
                __HAL_RCC_USART6_CLK_ENABLE();

            gpioInit.Alternate = GPIO_AF8_USART6;

            break;
        default:
            break;
    }

    PIN myPins[2] = {uart_tx, uart_rx};

    gpioInit.Pin = static_cast<uint32_t>(1 << (static_cast<uint32_t>(myPins[0]) & 0x0F)) |
                    static_cast<uint32_t>(1 << (static_cast<uint32_t>(myPins[1]) & 0x0F));
    gpioInit.Mode = GPIO_MODE_AF_PP;
    gpioInit.Pull = GPIO_NOPULL;
    gpioInit.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

    for (uint8_t i = 0; i < 2; i++)
    {
        switch((static_cast<uint8_t>(myPins[i]) & 0xF0) >> 4)
        {
            case 0x0:
                __HAL_RCC_GPIOA_CLK_ENABLE();
                HAL_GPIO_Init(GPIOA, &gpioInit);
                break;
            case 0x1:
                __HAL_RCC_GPIOB_CLK_ENABLE();
                HAL_GPIO_Init(GPIOB, &gpioInit);
                break;
            case 0x2:
                __HAL_RCC_GPIOC_CLK_ENABLE();
                HAL_GPIO_Init(GPIOC, &gpioInit);
                break;
            case 0x3:
                __HAL_RCC_GPIOD_CLK_ENABLE();
                HAL_GPIO_Init(GPIOD, &gpioInit);
                break;
            default:
                break;
        }
    }
    
    my_uart.Init.BaudRate     = DEFAULT_BAUDRATE;
    my_uart.Init.WordLength   = UART_WORDLENGTH_8B;
    my_uart.Init.StopBits     = UART_STOPBITS_1;
    my_uart.Init.Parity       = UART_PARITY_NONE;
    my_uart.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
    my_uart.Init.Mode         = UART_MODE_TX_RX;
    my_uart.Init.OverSampling = UART_OVERSAMPLING_16;

    HAL_UART_Init(&my_uart);
}

UartF4xxHalAdapter::~UartF4xxHalAdapter()
{
    // Empty
}

void UartF4xxHalAdapter::baud(int baudrate)
{
    my_uart.Init.BaudRate     = baudrate;
}

void UartF4xxHalAdapter::format(int bits, Parity parityType, int stopBits)
{
    my_uart.Init.WordLength   = bits;
    my_uart.Init.Parity       = (uint32_t)parityType;
    my_uart.Init.StopBits     = stopBits;
}

void UartF4xxHalAdapter::sendBreak()
{
    HAL_LIN_SendBreak(&my_uart);
}

bool UartF4xxHalAdapter::readable()
{
    return my_uart.pRxBuffPtr != NULL;
}

bool UartF4xxHalAdapter::writeable()
{
    return my_uart.pTxBuffPtr == NULL;
}

void UartF4xxHalAdapter::putc(char c)
{
    HAL_UART_Transmit(&my_uart, (uint8_t*)(&c), 1, DEFAULT_TIMEOUT);
}

void UartF4xxHalAdapter::puts(const char *s)
{
    char buf[100];
    strncpy(buf, s, 99);
    HAL_UART_Transmit(&my_uart, (uint8_t*)buf, strlen(buf), DEFAULT_TIMEOUT);
}

char UartF4xxHalAdapter::getc()
{
    char c;
    HAL_UART_Receive(&my_uart, (uint8_t*)(&c), 1, HAL_MAX_DELAY);
    return c;
}

void UartF4xxHalAdapter::printf(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    
    char string[200];
    if(0 < vsprintf(string, format, args))
    {
        HAL_UART_Transmit(&my_uart, (uint8_t*)string, strlen(string), DEFAULT_TIMEOUT);
    }

    va_end(args);
}
} // namespace IO
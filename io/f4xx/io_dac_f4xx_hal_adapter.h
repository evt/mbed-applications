/*
 * file: io_dac_f4xx_hal_adapter.h
 * purpose: Contains class definition for using the DAC peripheral
 * contrlled by the HAL
 */

#pragma once

#include "io_dac_intf.h"
#include "io_pin_names.h"
#include "stm32f4xx_hal_dac.h"
#include "stm32f4xx_hal_dac_ex.h"


namespace IO
{

class DacF4xxHalAdapter : public DacIntf
{

public:

    template<uint8_t numChannels>
    static DacF4xxHalAdapter & getInstance(const IO::PIN (&channelList)[numChannels]);

    /*
     * write()
     *
     * Set the output voltage on the first channel
     *
     * param: voltage - Desired output voltage
     */
    void write(float voltage);

    /*
     * write()
     *
     * Set the output voltage on the specified channel
     *
     * param: voltage - Desired output voltage
     *        channel - Channel number (valid inputs are 1 and 2)
     */
    void write(float voltage, uint8_t channel);

private:

    // Private constructor and destructor
    DacF4xxHalAdapter();
    ~DacF4xxHalAdapter();

    /*
     * addChannel()
     *
     * Configures and adds a new channel to the DAC peripheral.
     * For internal use only (in the template).
     *
     * param: pin - Pin on which the DAC channel will operate
     */
    void addChannel(IO::PIN pin);

    DAC_HandleTypeDef my_hdac;
    IO::PIN * my_channels;

    constexpr static uint32_t DAC_DATA_MAX = 0xFFF; // 12-bit dac
    constexpr static uint8_t MAX_NUM_CHANNELS = 2;

}; // class DacF4xxHalAdapter

template<uint8_t numChannels>
DacF4xxHalAdapter & DacF4xxHalAdapter::getInstance(const IO::PIN (&channelList)[numChannels])
{
    static_assert(numChannels <= MAX_NUM_CHANNELS, "Error: Cannot configure DAC for more than 2 channels!");
    static_assert(numChannels > 0, "Error: Cannot configure DAC with 0 channels!");

    static DacF4xxHalAdapter instance;
    static IO::PIN channels[numChannels];

    instance.my_channels = channels;

    for (uint8_t i = 0; i < numChannels; i++)
        instance.addChannel(channelList[i]);

    return instance;
}

} // namespace IO
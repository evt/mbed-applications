/*
 * file: io_uart_mbed_adapter.cpp
 * purpose: Implementation of the UART driver using Mbed framework.
 */

#include "io_uart_mbed_adapter.h"

namespace IO
{

constexpr unsigned DEFAULT_BAUD = 9600;
constexpr PinName DEFAULT_TX = PC_10;
constexpr PinName DEFAULT_RX = PC_11;

uartMbedAdapter::uartMbedAdapter() : my_mbedUART(nullptr)
{
    // Empty Constructor
}

uartMbedAdapter::~uartMbedAdapter()
{
    // Empty Destructor
}

void uartMbedAdapter::baud(int baudrate)
{
    my_mbedUART->baud(baudrate);
} 
void uartMbedAdapter::format(int bits/* = 8 */,
        Parity parityType/* = Parity::NONE */, int stopBits /* = 1*/)
{
    mbed::SerialBase::Parity mbedParity = mbed::SerialBase::None;
    switch (parityType)
    {
        case Parity::NONE:
            mbedParity = mbed::SerialBase::None;
            break;
        case Parity::ODD:
            mbedParity = mbed::SerialBase::Odd;
            break;
        case Parity::EVEN:
            mbedParity = mbed::SerialBase::Even;
            break;
        case Parity::MASK:
            mbedParity = mbed::SerialBase::Forced1;
            break;
        case Parity::SPACE:
            mbedParity = mbed::SerialBase::Forced0;
            break;
    }

    my_mbedUART->format(bits, mbedParity, stopBits);
}
bool uartMbedAdapter::readable()
{
    return my_mbedUART->readable();
}
bool uartMbedAdapter::writeable()
{
    return my_mbedUART->writeable();
}
    
void uartMbedAdapter::putc(char c)
{
    my_mbedUART->putc((int) c);
}
void uartMbedAdapter::puts(const char *s)
{
    my_mbedUART->puts(s);
}
char uartMbedAdapter::getc()
{
    return ((char) my_mbedUART->getc());
}

void uartMbedAdapter::printf(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    my_mbedUART->vprintf(format, args);
    va_end(args);
}

void uartMbedAdapter::sendBreak()
{
    my_mbedUART->send_break();
}
} // namespace IO

/*
 * file: io_adc_mbed_adapter.cpp
 * purpose: Implementation getting the voltage of an analog input pin as a floating point value
 */

#include "AnalogIn.h"
#include "io_adc_mbed_adapter.h"


namespace IO
{

namespace
{

/*
 * Takes a ADC measurement in [0.0, 1.0] interval and returns voltage in volts.
 *
 * rawValue: The input proportion to Vdd voltage.
 *
 * Returns: The measurement of the ADC in volts.
 */
constexpr float MICROCONTROLLER_VDD(3.3);

inline float convertRawADCToVoltage(float rawValue)
{
    return MICROCONTROLLER_VDD * rawValue;
}

} // anonymous namespace

AdcMbedAdapter::AdcMbedAdapter(PIN pin) : my_adc_driver((PinName) pin)
{
    // Empty constructor
}


AdcMbedAdapter::~AdcMbedAdapter()
{
    // Empty destructor
}


float AdcMbedAdapter::read()
{   
    return convertRawADCToVoltage(my_adc_driver.read());
}

} // namespace IO
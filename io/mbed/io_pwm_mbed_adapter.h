/*
 * file: io_pwm_mbed_adapter.h
 * purpose: Inheritance of the PWM interface for use with Mbed.
 */

#ifndef IO_PWM_MBED_ADAPTER_H
#define IO_PWM_MBED_ADAPTER_H


#include "io_pwm_intf.h"
#include "io_gpio_mbed_adapter.h"
#include "mbed.h"

namespace IO
{

class pwmMbedAdapter : public PwmIntf
{

public:

    // Takes in a pin that will be the PWM.  periodUs is default period in microseconds and dutycycle is default dutyCycle
    template<PIN pin> static pwmMbedAdapter& getInstance(float periodUs = 20000, float dutyCycle = 0);

    void setDutyCycle(float dutyCycle) override;

    void setPeriodMicroSeconds(float periodUs) override;

    float getDutyCycle(void) override;

    float getPeriodMicroSeconds(void) override;

    pwmMbedAdapter() = delete;
    ~pwmMbedAdapter();

private:

    // Takes pin of PWM and the period in microseconds and dufycycle
    explicit pwmMbedAdapter(PIN pin, float periodUs, float dutyCycle);

    mbed::PwmOut my_pwm;
    float my_periodUs;
    float my_duty_cycle;
};

// Templates need to be defined in the header.
template<PIN pin>
pwmMbedAdapter& pwmMbedAdapter::getInstance(float periodUs, float dutyCycle)
{
    static pwmMbedAdapter staticPin(pin, periodUs, dutyCycle);

    return staticPin;
}
}   // namespace IO

#endif  // IO_PWM_MBED_ADAPTER_H

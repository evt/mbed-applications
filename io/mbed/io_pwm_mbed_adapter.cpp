/*
 * file: io_pwm_mbed_adapter.cpp
 * purpose: Implementation of the PWM driver using Mbed framework.
 */

#include "io_pwm_mbed_adapter.h"


namespace IO
{


// Initializes the period in microseconds and duty cycle in that order
pwmMbedAdapter::pwmMbedAdapter(PIN pin, float periodUs, float dutyCycle) : 
    my_pwm(static_cast<PinName>(pin)), my_periodUs(periodUs), my_duty_cycle(dutyCycle)
{
    my_pwm.write(dutyCycle);
    my_pwm.period_us(periodUs);
}


pwmMbedAdapter::~pwmMbedAdapter()
{
    // Empty destructor
}


// Calls mbeds PWM write function which sets the duty cycle
void pwmMbedAdapter::setDutyCycle(float dutyCycle)
{
    my_pwm.write(dutyCycle);
    this->my_duty_cycle = dutyCycle;
}


// Calls mbeds PWM period in milliseconds
void pwmMbedAdapter::setPeriodMicroSeconds(float periodUs)
{
    my_pwm.period_us(static_cast<int>(periodUs));
    this->my_periodUs = periodUs;
}


// Gets the current duty cycle
float pwmMbedAdapter::getDutyCycle(void)
{
    return this->my_duty_cycle;
}


// Gets the current period in milliseconds
float pwmMbedAdapter::getPeriodMicroSeconds(void)
{
    return this->my_periodUs;
}

}   // namespace IO

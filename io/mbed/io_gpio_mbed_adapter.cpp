/*
 * file: io_gpio_mbed_adapter.cpp
 * purpose: Implementation of the GPIO driver using Mbed framework.
 */

#include "io_gpio_mbed_adapter.h"


namespace IO
{

GpioMbedAdapter::GpioMbedAdapter(PIN pin) : my_pin((PinName)pin)
{
    // Empty constructor
}


GpioMbedAdapter::~GpioMbedAdapter()
{
    // Empty destructor
}


void GpioMbedAdapter::setPinDirection(DIRECTION direction)
{
    switch (direction)
    {
        case DIRECTION::INPUT:
        {
            this->my_pin.input();
            break;
        }
        case DIRECTION::OUTPUT:
        {
            this->my_pin.output();
            break;
        }
        default:
        {
            break;
        }
    }
}


void GpioMbedAdapter::writePin(STATE state)
{
    switch (state)
    {
        case STATE::LOW:
        {
            this->my_pin = 0;
            break;
        }
        case STATE::HIGH:
        {
            this->my_pin = 1;
            break;
        }
        default:
        {
            break;
        }
    }
}


GpioMbedAdapter::STATE GpioMbedAdapter::readPin(void)
{
    if (0 == this->my_pin.read())
    {
        return STATE::LOW;
    }
    else
    {
        return STATE::HIGH;
    }
}

GpioMbedAdapter& GpioMbedAdapter::operator=(GpioMbedAdapter& other)
{
    my_pin = other.my_pin;
    return *this;
}

}   // namespace IO

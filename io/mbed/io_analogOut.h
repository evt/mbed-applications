/*
 * file: io_analogOut.h
 * purpose: Set the voltage of an analog output pin as a uint
 */ 

#ifndef IO_ANALOGOUT
#define IO_ANALOGOUT

#include "mbed.h"
#include <cstdint>

namespace IO
{
class analogOut
{
public:

    /**
    * Set the output voltage, specified as a percentage (uint16_t)
    * 
    * Parameters:
    * value - uint16_t that is casted to an unsigned short when calling the mbed write method
    * returns the value specificied as a voltage
    */
    void write(uint16_t value);

    template<PinName pin> static analogOut& getInstance();

private:
    // Private constructor and destructor
    analogOut();
    ~analogOut();
    // Member variable
    mbed::AnalogOut *myAnalogOut;
};

template<PinName pin>
analogOut& analogOut::getInstance()
{
    static analogOut instance;
    static mbed::AnalogOut analogOut (pin);
    instance.myAnalogOut = &analogOut;
    return instance;
}

}   // namespace IO

#endif // IO_ANALOGOUT_H
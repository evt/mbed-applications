/*
 * file: io_i2c_mbed_adapter.cpp
 * purpose: Implementation of the I2C driver using Mbed framework.
 */

#include "io_i2c_mbed_adapter.h"

namespace IO
{

constexpr PinName DEFAULT_SDA = PB_9;
constexpr PinName DEFAULT_SCL = PB_8;
constexpr unsigned DEFAULT_FREQ = 100000;

i2cMbedAdapter::i2cMbedAdapter() : my_mbedI2C(nullptr)
{
    // Empty Constructor
}

i2cMbedAdapter::~i2cMbedAdapter()
{
    // Empty Destructor
}

void i2cMbedAdapter::write(uint8_t addr, char *data, int length)
{
    // pass through to the mbed write function
    my_mbedI2C->write((int) (addr << 1), data, length);
}
void i2cMbedAdapter::read(uint8_t addr, char *data, int length)
{
    // pass through to the mbed read function
    my_mbedI2C->read((int) (addr << 1), data, length);
}
} // namespace IO


/*
 * file: io_i2c_mbed_adapter.h
 * purpose: Inheritance of the I2C interface for use with Mbed.
 */

#ifndef IO_I2C_MBED_ADAPTER_H
#define IO_I2C_MBED_ADAPTER_H

#include "io_i2c_intf.h"
#include "mbed.h"

namespace IO
{

class i2cMbedAdapter : public i2cIntf
{
public:

    /**
     *  getInstance - Returns the system's instance of i2cMbedAdapter
     *
     *  Returns:
     *      A reference to an instance of the i2c module.
     */
    template<PinName SDA, PinName SCL, unsigned freq=100000> static i2cMbedAdapter& getInstance();

    void write(uint8_t addr, char *data, int length) override;
    void read(uint8_t addr, char *data, int length) override;

private:
    // Private constructor and destructor
    i2cMbedAdapter();
    ~i2cMbedAdapter();

    I2C *my_mbedI2C;

}; // class I2C_MBED_ADAPTER

template<PinName SDA, PinName SCL, unsigned freq /* =100000 */>
i2cMbedAdapter& i2cMbedAdapter::getInstance()
{
    static i2cMbedAdapter instance;
    static mbed::I2C mbedI2C(SDA, SCL);
    if (instance.my_mbedI2C == nullptr)
    {
        instance.my_mbedI2C = &mbedI2C;
        mbedI2C.frequency(freq);
    }

    return instance;
}

} // namespace IO
#endif // IO_I2C_MBED_ADAPTER_H

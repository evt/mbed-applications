/*
 * file: io_analogOut.cpp
 * purpose: Implementation setting the voltage of an analog output pin as a uint
 */

#include "io_analogOut.h"
#include "AnalogOut.h"

namespace IO
{
    
analogOut::analogOut()
{
    // Default constructor
}


analogOut::~analogOut()
{
    // Empty destructor
}


/*
 * Wraps around the mbed API write method
 * Calls the mbed method and then static_casts the value to unsigned short
 * value is specified in millivolts
 * value is then static casted to a float and divided by 1000
 */ 
void analogOut::write(uint16_t value)
{
    float convertValueToVolts = static_cast<float>(value)/1000;
    myAnalogOut->write(static_cast<unsigned short>(convertValueToVolts));
}


} // namespace IO
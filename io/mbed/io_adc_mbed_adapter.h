/*
 * file: io_adc_mbed_adapter.h
 * purpose: Get the voltage of an analog intput pin as a uint
 */ 

#ifndef IO_ADC_MBED_ADAPTER_H
#define IO_ADC_MBED_ADAPTER_H

#include <cstdint>
#include "mbed.h"
#include "io_adc_intf.h"
#include "io_gpio_mbed_adapter.h"

namespace IO
{

class AdcMbedAdapter : public AdcIntf
{
public:

    /*
     * Gets a reference to a static singleton instance of an ADC adapter for
     * the provided GPIO pin.
     *
     * pin: The microcontroller pin that the ADC peripheral is connected to.
     */
    template<PIN pin>
    static AdcMbedAdapter& getInstance();

    /*
     * Destructor for the adapter.
     */
    ~AdcMbedAdapter();

    /*
     * AdcIntf methods
     */
    float read() override;

private:
    /*
     * Private constructor
     */
    AdcMbedAdapter(PIN pin);

    // Mbed subdriver the interface is being adapted onto
    mbed::AnalogIn my_adc_driver;
    
};

template<PIN pin>
AdcMbedAdapter& AdcMbedAdapter::getInstance()
{
    static AdcMbedAdapter instance(pin);
    return instance;
}


}   // namespace IO

#endif // IO_ADC_MBED_ADAPTER_H
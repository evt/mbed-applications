/*
 * file: io_spi_mbed_adapter.h
 * purpose: Implements the SPI interface using the mbed framework SPI api calls.
 */

#ifndef IO_SPI_MBED_ADAPTER_H
#define IO_SPI_MBED_ADAPTER_H

#include <array>
#include <iterator>
#include "mbed.h"
#include "io_spi_intf.h"

namespace IO
{

class SpiMbedAdapter : public SpiIntf
{
public:

    template<PinName MOSI, PinName MISO, PinName SCLK> static SpiMbedAdapter& getInstance();

    void configureSpi(BusMode busMode, BitOrder bitOrder) override;

    void read(uint8_t * rxMessage, unsigned int length = 1) override;
    void write(uint8_t * txMessage, unsigned int length = 1) override;
    void setChipSelectHigh(GpioIntf &cs) override;
    void setChipSelectLow(GpioIntf &cs) override;

private:
    // Private constructor and destructor
    SpiMbedAdapter();
    ~SpiMbedAdapter();

    // Pin validation functions
    static constexpr bool checkPinsSpi(PinName MOSI, PinName MISO, PinName SCLK);
    static constexpr bool checkPinsUnique(PinName MOSI, PinName MISO, PinName SCLK);

    mbed::SPI *my_mbedSPI;
    BusMode my_busMode;

};

// Templates need to be defined in the header.
template<PinName MOSI, PinName MISO, PinName SCLK>
SpiMbedAdapter& SpiMbedAdapter::getInstance()
{
    #if 0
    static_assert(checkPinsSpi(MOSI, MISO, SCLK),
            "The SPI pins specified are either invalid or belong to different SPI modules.");
    #endif // 0

    // Create the SPI module to return as a reference.
    static SpiMbedAdapter staticSpi;

    // Create the underlying mbed SPI module
    static mbed::SPI spi2(MOSI, MISO, SCLK);

    if (staticSpi.my_mbedSPI == nullptr)
    {
        staticSpi.my_mbedSPI = &spi2;
        staticSpi.my_mbedSPI->format(8,0);
    }

    return staticSpi;
}


// Check to make sure that the specified pins are all capable of performing their roles
// and check that they all belong to the same module.
#if 0
constexpr bool SpiMbedAdapter::checkPinsSpi(PinName MOSI, PinName MISO, PinName SCLK)
{
    constexpr PinName mosiPins[4] = {
                        PinName::PB_15, PinName::PA_11, // SPI2_MOSI
                        PinName::PC_12, PinName::PB_5   // SPI3_MOSI
                        };

    constexpr PinName misoPins[4] = {
                        PinName::PB_14, PinName::PA_10, // SPI2_MISO
                        PinName::PC_11, PinName::PB_4   // SPI3_MISO
                        };

    constexpr PinName sclkPins[4] = {
                        PinName::PF_1,  PinName::PB_13, // SPI2_SCLK
                        PinName::PC_10, PinName::PB_3   // SPI3_SCLK
                        };

    // NOTE: This isn't needed, but I thought it would be good to add for reference.
    /*
    constexpr PinName sselPins[4] = {
                        PinName::PF_0,  PinName::PB_12, // SPI2_SSEL
                        PinName::PA_4,  PinName::PA_15  // SPI3_SSEL
                        };
    */

    int mosiIndex = -1;
    int misoIndex = -1;
    int sclkIndex = -1;

    bool validPins = false;
    bool sameModule = false;

    // Make sure the MOSI pin is valid and determine which module it belongs to.
    for (auto iter = std::cbegin(mosiPins); iter != std::cend(mosiPins); ++iter)
    {
        if (*iter == MOSI)
        {
            mosiIndex = iter - std::cbegin(mosiPins);
            break;
        }
    }

    // Make sure the MISO pin is valid and determine which module it belongs to.
    for (auto iter = std::cbegin(misoPins); iter != std::cend(misoPins); ++iter)
    {
        if (*iter == MISO)
        {
            misoIndex = iter - std::cbegin(misoPins);
            break;
        }
    }

    // Make sure the SCLK pin is valid and determine which module it belongs to.
    for (auto iter = std::cbegin(sclkPins); iter != std::cend(sclkPins); ++iter)
    {
        if (*iter == SCLK)
        {
            sclkIndex = iter - std::cbegin(sclkPins);
            break;
        }
    }

    // Make sure that all the pins were found in the arrays
    validPins = ((mosiIndex != -1) && (misoIndex != -1) && (sclkIndex != -1));

    // Make sure that all of the pins belong to the same SPI module
    // NOTE: Indices 0 and 1 are for SPI2, indicies 2 and 3 are for SPI3.
    sameModule = (((mosiIndex < 2) == (misoIndex < 2)) == (sclkIndex < 2));

    return (validPins && sameModule);
}

#endif // 0, we need to fix our build server to implement full C++14

constexpr bool SpiMbedAdapter::checkPinsUnique(PinName MOSI, PinName MISO, PinName SCLK)
{
    // We aren't ready for this one yet.
    return true;
}

} // namespace IO

#endif  // IO_SPI_MBED_ADAPTER_H

/*
 * file: io_spi_mbed_adapter.cpp
 * purpose: Implementation of the SPI interface using the mbed framework.
 */

#include "io_spi_mbed_adapter.h"

namespace IO
{

SpiMbedAdapter::SpiMbedAdapter() 
    : my_mbedSPI(nullptr),
      my_busMode(BusMode::MODE_DEFAULT)
{
}


SpiMbedAdapter::~SpiMbedAdapter()
{
    // empty destructor
}


void SpiMbedAdapter::configureSpi(BusMode busMode, BitOrder bitOrder)
{
    this->my_busMode = busMode;

    my_mbedSPI->format(8, (int)busMode);

    return;
}


void SpiMbedAdapter::read(uint8_t * rxMessage, unsigned int length /* = 1 */)
{
    for (unsigned int i = 0; i < length; i++)
    {
        // send 0's to drive the clock and get output
        *(rxMessage + i) = my_mbedSPI->write(0x00);
    }

    return;
}


void SpiMbedAdapter::write(uint8_t * txMessage, unsigned int length /* = 1 */)
{
    for (unsigned int i = 0; i < length; i++)
    {
        // write txMessage + i
        my_mbedSPI->write(*(txMessage + i));
    }

    return;
}

void SpiMbedAdapter::setChipSelectHigh(GpioIntf &cs)
{

    cs.writePin(GpioIntf::STATE::HIGH);

    return;
}

void SpiMbedAdapter::setChipSelectLow(GpioIntf &cs)
{

    cs.writePin(GpioIntf::STATE::LOW);

    return;
}
} // namespace IO

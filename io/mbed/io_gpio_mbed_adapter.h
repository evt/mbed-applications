/*
 * file: io_gpio_mbed_adapter.h
 * purpose: Inheritance of the GPIO interface for use with Mbed.
 */

#ifndef IO_GPIO_MBED_ADAPTER_H
#define IO_GPIO_MBED_ADAPTER_H


#include "io_gpio_intf.h"
#include "io_pin_names.h"

namespace IO
{

class GpioMbedAdapter : public GpioIntf
{

public:

    template<PIN pin> static GpioMbedAdapter& getInstance();

    void setPinDirection(DIRECTION direction) override;

    void writePin(STATE state) override;

    STATE readPin(void) override;

    GpioMbedAdapter& operator=(GpioMbedAdapter& other);

    GpioMbedAdapter() = delete;
    ~GpioMbedAdapter();

private:

    explicit GpioMbedAdapter(PIN pin);
    DigitalInOut my_pin;

};

// Templates need to be defined in the header.
template<PIN pin>
GpioMbedAdapter& GpioMbedAdapter::getInstance()
{
    static GpioMbedAdapter staticPin(pin);

    return staticPin;
}

}   // namespace IO

#endif  // IO_GPIO_MBED_ADAPTER_H

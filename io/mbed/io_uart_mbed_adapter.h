
/*
 * file: io_uart_mbed_adapter.h
 * purpose: Inheritance of the UART interface for use with Mbed.
 */

#ifndef IO_UART_MBED_ADAPTER_H
#define IO_UART_MBED_ADAPTER_H

#include "io_uart_intf.h"
#include "mbed.h"

namespace IO
{

class uartMbedAdapter : public uartIntf
{
public:
    
    /**
     *  getInstance - Returns the system's instance of uartMbedAdapter
     *
     *  Returns:
     *      A reference to an instance of the uart module.
     */
    template<PinName tx, PinName rx, unsigned baud=9600>static uartMbedAdapter& getInstance();

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    void baud(int baudrate) override;

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    void format(int bits=8, Parity parityType=Parity::NONE, int stopBits=1) override;

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    bool readable() override;

    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    bool writeable() override;
    
    /*
     *  //////////////////////////////
     *  //WARNING: UNTESTED FUNCTION//
     *  //////////////////////////////
     */
    void sendBreak() override;

    void putc(char c) override;
    void puts(const char *s) override;
    char getc() override;

    void printf(const char *format, ...) override;

private:

    // Private constructors and destructor
    uartMbedAdapter();
    ~uartMbedAdapter();

    mbed::Serial *my_mbedUART;

}; // class UART_MBED_ADAPTER

template<PinName tx, PinName rx, unsigned baud /* =9600 */>
uartMbedAdapter& uartMbedAdapter::getInstance()
{
    static uartMbedAdapter instance;
    static mbed::Serial mbedUART(tx, rx, baud);

    if (instance.my_mbedUART == nullptr)
    {
        instance.my_mbedUART = &mbedUART;
    }

    return instance;
}

} // namespace IO
#endif // IO_UART_MBED_ADAPTER_H

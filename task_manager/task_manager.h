/*
 * file: task_manager.h
 * purpose: Interface to the task manager application which drives functionality
 *          of the entire firmware.
 */

#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H


#include <array>
#include <cstdint>
#include "app_intf.h"




template <unsigned int NUMBER_OF_APPS>
class TaskManager
{
public:

    // Constructor
    TaskManager(APP::ApplicationIntf ** applications)
    {
        for (unsigned int i = 0; i < NUMBER_OF_APPS; ++i)
        {
            my_applications[i] = AppData(applications[i], applications[i]->getRunSchedule());
        }
    }
    // Destructor
    ~TaskManager() {}

    // Calls initialization code for all modules.
    void init()
    {
        for (unsigned int i = 0; i < my_applications.size(); ++i)
        {
            if (nullptr != my_applications[i].app)
            {
                my_applications[i].app->init();
            }
        }
    }

    // Runs a cycle of the state machines for the applications and drivers.
    void run()
    {
        for (unsigned int i = 0; i < my_applications.size(); ++i)
        {
            if (nullptr != my_applications[i].app)
            {
                // If it has no time left, reset the timer and run it
                if (0 == my_applications[i].timeLeft)
                {
                    my_applications[i].timeLeft = my_applications[i].timePeriod;
                    my_applications[i].app->run();
                }
                // Decrement the app's time left
                my_applications[i].timeLeft--;
            }
        }
    }

private:

    struct AppData
    {
        APP::ApplicationIntf * app;
        unsigned int timePeriod;
        unsigned int timeLeft;

        AppData(APP::ApplicationIntf * _apps, unsigned int _timePeriod)
         : app(_apps), timePeriod(_timePeriod), timeLeft(_timePeriod)
        {}

        AppData() : app(nullptr), timePeriod(0), timeLeft(0) {}
    };

    std::array<AppData, NUMBER_OF_APPS> my_applications;    
};

#endif  // TASK_MANAGER_H

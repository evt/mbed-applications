# EVT mBed Applications
This repository is the main branch of development for EVT's 32-bit ARM microcontroller applications. This repository will host source code for:

1. Battery Management System (HW Rev 4.0+)
2. Inertial Measurement Unit (HW Rev 3.0+)
3. Thermal Management System (HW Rev 1.0+)
4. Gateway Utility Board (HW Rev 1.0+) **
5. Strain Gauge Module (HW Rev 1.0+)
6. Low Voltage Sub-System (HW Rev 2.0+)

These applications are developed using ARM's mbed framework for the STM32F302 (**and STM32F446) microcontroller(s), which run on each of the boards above.
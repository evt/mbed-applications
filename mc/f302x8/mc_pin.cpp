/*
 * file: mc_pin.cpp
 * purpose: Define some GPIO utility functions.
 */

#include "mc_pin.h"


namespace MC
{

GPIO_TypeDef * getGpioPort(Pin pin)
{
    // Filter out the top nibble
    switch (static_cast<uint8_t>(pin) >> 4)
    {
        case 0xA:
            return GPIOA;
        case 0xB:
            return GPIOB;
        case 0xC:
            return GPIOC;
        case 0xF:
            return GPIOF;
        default:
            return nullptr;
    }
}

} // namespace MC
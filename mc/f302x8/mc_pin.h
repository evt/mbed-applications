/*
 * file: mc_pin.h
 * purpose: Defines pins available on the STM32F302.
 */


#ifndef MC_PIN_H
#define MC_PIN_H


#include "stm32f3xx.h"


namespace MC
{

enum class Pin 
{
    A0  = 0xA0,
    A1  = 0xA1,
    A2  = 0xA2,
    A3  = 0xA3,
    A4  = 0xA4,
    A5  = 0xA5,
    A6  = 0xA6,
    A7  = 0xA7,
    A8  = 0xA8,
    A9  = 0xA9,
    A10 = 0xAA,
    A11 = 0xAB,
    A12 = 0xAC,
    A13 = 0xAD,
    A14 = 0xAE,
    A15 = 0xAF,
    B0  = 0xB0,
    B1  = 0xB1,
    B2  = 0xB2,
    B3  = 0xB3,
    B4  = 0xB4,
    B5  = 0xB5,
    B6  = 0xB6,
    B7  = 0xB7,
    B8  = 0xB8,
    B9  = 0xB9,
    B10 = 0xBA,
    B11 = 0xBB,
    B12 = 0xBC,
    B13 = 0xBD,
    B14 = 0xBE,
    B15 = 0xBF,
    C13 = 0xCD,
    C14 = 0xCE,
    C15 = 0xCF,
    F0  = 0xF0,
    F1  = 0xF1,
};

/*
 * TODO: Find a way to make this run at compile time. Currently this cannot
 *       be a constexpr function because GPIO_TypeDef is a struct containing
 *       volatile members, making it a non-literal type.
 *
 * See this gist: https://gist.github.com/ssnover95/7cf939d0ba56ea7d871994494d1139ea
 */
GPIO_TypeDef * getGpioPort(Pin pin);

} // namespace MC


#endif // MC_PIN_H
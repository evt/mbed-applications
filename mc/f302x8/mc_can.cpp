/*
 * file: mc_can.cpp
 * purpose: Configure bits in the CAN peripheral.
 */

#include <cstring>
#include "mc_can.h"
#include "mc_pin.h"

namespace MC
{

namespace
{
CanMode my_mode = CanMode::SLEEP;
CAN_TypeDef * const MY_CAN_PORT = CAN1;
unsigned int my_PCLK1_FREQUENCY;

/* 
 * The following table is used to program bit_timing. It is an adjustment of the sample
 * point by synchronizing on the start-bit edge and resynchronizing on the following edges.
 * This table has the sampling points as close to 75% as possible (most commonly used).
 * The first value is TSEG1, the second TSEG2.
 */
uint8_t const timing_pts[23][2] = {
    {0x0, 0x0},      // 2,  50%
    {0x1, 0x0},      // 3,  67%
    {0x2, 0x0},      // 4,  75%
    {0x3, 0x0},      // 5,  80%
    {0x3, 0x1},      // 6,  67%
    {0x4, 0x1},      // 7,  71%
    {0x5, 0x1},      // 8,  75%
    {0x6, 0x1},      // 9,  78%
    {0x6, 0x2},      // 10, 70%
    {0x7, 0x2},      // 11, 73%
    {0x8, 0x2},      // 12, 75%
    {0x9, 0x2},      // 13, 77%
    {0x9, 0x3},      // 14, 71%
    {0xA, 0x3},      // 15, 73%
    {0xB, 0x3},      // 16, 75%
    {0xC, 0x3},      // 17, 76%
    {0xD, 0x3},      // 18, 78%
    {0xD, 0x4},      // 19, 74%
    {0xE, 0x4},      // 20, 75%
    {0xF, 0x4},      // 21, 76%
    {0xF, 0x5},      // 22, 73%
    {0xF, 0x6},      // 23, 70%
    {0xF, 0x7},      // 24, 67%
};

/*
 * An array of handler functions registered in position by the CanIrq enum. The
 * interrupt handlers check their position's value here for a registered callback
 * and execute it if it exists.
 */
void_function_ptr_t INTERRUPT_HANDLERS[3] = {0};

/*
 * Calculates the bits for the time quanta registers from the clock frequency and CAN baud rate.
 *
 * TODO: This should be possible to calculate at compile time.
 */
uint32_t calculateBitTiming(unsigned int peripheral_clock, unsigned int can_frequency, uint8_t psjw)
{
    uint32_t const bit_width(peripheral_clock / can_frequency);
    uint32_t baud_rate_prescaler(bit_width / 0x18);

    while (baud_rate_prescaler < (bit_width / 4))
    {
        baud_rate_prescaler++;
        /* bits initialized above to increase scope */
        for (uint8_t bits = 22; bits > 0; --bits)
        {
            uint32_t calculated_bit = (bits + 3) * (baud_rate_prescaler + 1);
            if (bit_width == calculated_bit)
            {
                return ((timing_pts[bits][1] << CAN_BTR_TS2_Pos) & CAN_BTR_TS2)
                     | ((timing_pts[bits][0] << CAN_BTR_TS1_Pos) & CAN_BTR_TS1)
                     | ((psjw                << CAN_BTR_SJW_Pos) & CAN_BTR_SJW)
                     | ((baud_rate_prescaler << CAN_BTR_BRP_Pos) & CAN_BTR_BRP);
            }
        }
    }
    
    return 0xFFFFFFFF;
}

} // namespace


/*
 * Initialize the hardware for transmission and reception of CAN messages.
 */
void can_initialize(unsigned int frequency, unsigned int PCLK1_FREQUENCY)
{
    // Setup the CAN registers
    RCC->APB1ENR |= RCC_APB1ENR_CANEN;
    // Reset the peripheral
    MY_CAN_PORT->MCR |= CAN_MCR_RESET;
    // Set to init mode
    can_setMode(CanMode::INIT);

    // Enable Automatic Bus-Off Management, Automatic Wakeup Mode, and 
    //        Chronological Transmission
    MY_CAN_PORT->MCR |= CAN_MCR_ABOM | CAN_MCR_AWUM | CAN_MCR_TXFP;
    // Disable Silent Mode
    MY_CAN_PORT->BTR &= ~CAN_BTR_SILM;
    // Reset error register
    MY_CAN_PORT->ESR = 0x0;

    // Enable transmit and receive
    MY_CAN_PORT->MSR |= (CAN_MSR_RXM | CAN_MSR_TXM);

    // Set the frequency
    my_PCLK1_FREQUENCY = PCLK1_FREQUENCY;
    can_setFrequency(frequency);
}

/*
 * Initialize two GPIO pins for CAN functions.
 */
void can_initializeGpioPins(MC::Pin canrx, MC::Pin cantx)
{
    // Setup GPIO pins
    GPIO_TypeDef * gpioPort = getGpioPort(canrx);

    // Turn on the right clock
    switch (static_cast<uint8_t>(canrx) >> 4)
    {
        case 0xA:
            RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
            break;
        case 0xB:
            RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
            break;
        case 0xC:
            RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
            break;
        case 0XF:
            RCC->AHBENR |= RCC_AHBENR_GPIOFEN;
            break;
    }

    uint8_t const canRxPinNumber = static_cast<uint8_t>(canrx) & 0x0F;

    // Clear the CANRX pin MODER setting and set to Alternate Function Mode (0b10)
    gpioPort->MODER &= ~(GPIO_MODER_MODER0 << (2 * canRxPinNumber));
    gpioPort->MODER |= (GPIO_MODER_MODER0_1 << (2 * canRxPinNumber));
    // Set the CANRX pin for High Speed (0b11)
    gpioPort->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR0 << 2 * canRxPinNumber);
    // Set the CANRX pin for Push-Pull (0b0)
    gpioPort->OTYPER &= ~(GPIO_OTYPER_OT_0 << canRxPinNumber);
    // Enable pull-ups for the CANRX pin (0b01)
    gpioPort->PUPDR &= ~(GPIO_PUPDR_PUPDR0 << 2 * canRxPinNumber);
    gpioPort->PUPDR |= (GPIO_PUPDR_PUPDR0_0 << 2 * canRxPinNumber);
    // Alternate function for CAN_RX on PB8 is AF9
    gpioPort->AFR[1] &= ~(GPIO_AFRH_AFRH0 << ((canRxPinNumber - 8) * 4));
    gpioPort->AFR[1] |= (0x9 << ((canRxPinNumber - 8) * 4));

    uint8_t const canTxPinNumber = static_cast<uint8_t>(cantx) & 0x0F;

    // Clear the CANTX pin MODER setting and set to Alternate Function Mode (0b10)
    gpioPort->MODER &= ~(GPIO_MODER_MODER0 << (2 * canTxPinNumber));
    gpioPort->MODER |= (GPIO_MODER_MODER0_1 << (2 * canTxPinNumber));
    // Set the CANTX pin for High Speed (0b11)
    gpioPort->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR0 << 2 * canTxPinNumber);
    // Set the CANTX pin for Push-Pull (0b0)
    gpioPort->OTYPER &= ~(GPIO_OTYPER_OT_0 << canTxPinNumber);
    // Enable pull-ups for the CANTX pin (0b01)
    gpioPort->PUPDR &= ~(GPIO_PUPDR_PUPDR0 << 2 * canTxPinNumber);
    gpioPort->PUPDR |= (GPIO_PUPDR_PUPDR0_0 << 2 * canTxPinNumber);
    // Alternate function for CAN_TX on PB9 is AF9
    gpioPort->AFR[1] &= ~(GPIO_AFRH_AFRH0 << ((canTxPinNumber - 8) * 4));
    gpioPort->AFR[1] |= (0x9 << ((canTxPinNumber - 8) * 4));
}

/*
 * Sets the mode for the hardware.
 */
void can_setMode(CanMode new_mode, bool loopback /* = false */)
{
    switch (new_mode)
    {
        case CanMode::SLEEP:
            MY_CAN_PORT->MCR |= CAN_MCR_SLEEP;
            // Wait for acknowledgement from hardware
            // SLAK = 1, INAK = 0
            while (((MY_CAN_PORT->MSR & CAN_MSR_INAK) != 0)
                   || ((MY_CAN_PORT->MSR & CAN_MSR_SLAK) == 0)) {} 
            break;
        case CanMode::INIT:
            MY_CAN_PORT->MCR |= CAN_MCR_INRQ;
            if (my_mode == CanMode::SLEEP)
            {
                // Clear the sleep flag to wakeup peripheral
                MY_CAN_PORT->MCR &= ~CAN_MCR_SLEEP;
            }
            // Wait for the acknowledgement from hardware
            // INAK = 1 indicates initialization mode
            while (((MY_CAN_PORT->MSR & CAN_MSR_INAK) == 0)
                   || ((MY_CAN_PORT->MSR & CAN_MSR_SLAK) != 0)) {}
            break;
        case CanMode::NORMAL:
            can_setMode(CanMode::INIT, loopback);
            if (loopback)
            {
                // Enable loopback mode
                MY_CAN_PORT->BTR |= CAN_BTR_LBKM;
            }
            else
            {
                // Disable loopback mode
                MY_CAN_PORT->BTR &= ~CAN_BTR_LBKM;
            }
            MY_CAN_PORT->MCR &= ~(CAN_MCR_INRQ);
            // Wait for acknowledgement from hardware
            // SLAK = 0, INAK = 0
            while (((MY_CAN_PORT->MSR & CAN_MSR_SLAK) != 0)
                   || ((MY_CAN_PORT->MSR & CAN_MSR_INAK) != 0)) {}
            break;
        case CanMode::UNKNOWN:
            new_mode = can_getMode();
            break;
    }

    my_mode = new_mode;
}

/*
 * Configure in-hardware CAN message reception filtering
 */
bool can_configFilter(CanFilter & filter)
{
    uint32_t filterBitPosition = (1 << static_cast<uint32_t>(filter.position));
    uint32_t filterPosition = 0;

    if (filter.position == CanFilterPosition::NUMBER_OF_POSITIONS)
        return false;

    filterPosition = static_cast<uint8_t>(filter.position);

    // Initialization mode for this filter
    MY_CAN_PORT->FMR |= (uint32_t)CAN_FMR_FINIT;

    // Deactivate filter to configure it
    MY_CAN_PORT->FA1R &= ~(filterBitPosition);

    switch(filter.mode)
    {
        case CanFilterMode::ID_MASK_32_BIT:
            // 32-bit mode
            MY_CAN_PORT->FS1R |= filterBitPosition;

            // ID-Mask Mode
            MY_CAN_PORT->FM1R &= ~(filterBitPosition);

            // FR1 = ID to be masked
            // FR2 = Bitmask: 1 = 'Must match', 0 = 'Don't care'
            MY_CAN_PORT->sFilterRegister[filterPosition].FR1 = \
                ((static_cast<uint32_t>(filter.filterIdHigh) << 16) |
                  static_cast<uint32_t>(filter.filterIdLow));

            MY_CAN_PORT->sFilterRegister[filterPosition].FR2 = \
                ((static_cast<uint32_t>(filter.filterIdMaskHigh) << 16) |
                  static_cast<uint32_t>(filter.filterIdMaskLow));

            break;

        case CanFilterMode::ID_LIST_32_BIT:
            // 32-bit mode
            MY_CAN_PORT->FS1R |= filterBitPosition;

            // ID-List Mode
            MY_CAN_PORT->FM1R |= filterBitPosition;

            // FR1 = First ID to be filtered
            // FR2 = Second ID to be filtered
            MY_CAN_PORT->sFilterRegister[filterPosition].FR1 = \
                ((static_cast<uint32_t>(filter.filterIdHigh) << 16) |
                  static_cast<uint32_t>(filter.filterIdLow));

            MY_CAN_PORT->sFilterRegister[filterPosition].FR2 = \
                ((static_cast<uint32_t>(filter.filterIdMaskHigh) << 16) |
                  static_cast<uint32_t>(filter.filterIdMaskLow));

            break;

        case CanFilterMode::ID_MASK_16_BIT:
            // 16-bit mode
            MY_CAN_PORT->FS1R &= ~(filterBitPosition);

            // ID-Mask Mode
            MY_CAN_PORT->FM1R &= ~(filterBitPosition);

            // FR1 = 1st ID and mask pair. For the mask: 1 = 'Must match', 0 = 'Don't care'
            // FR2 = 2nd ID and mask pair. For the mask: 1 = 'Must match', 0 = 'Don't care'
            MY_CAN_PORT->sFilterRegister[filterPosition].FR1 = \
                ((static_cast<uint32_t>(filter.filterIdMaskLow) << 16U) |
                  static_cast<uint32_t>(filter.filterIdLow));

            MY_CAN_PORT->sFilterRegister[filterPosition].FR2 = \
                ((static_cast<uint32_t>(filter.filterIdMaskHigh) << 16U) |
                  static_cast<uint32_t>(filter.filterIdHigh));

            break;

        case CanFilterMode::ID_LIST_16_BIT:
            // 16-bit mode
            MY_CAN_PORT->FS1R &= ~(filterBitPosition);

            // ID-List Mode
            MY_CAN_PORT->FM1R |= filterBitPosition;

            // FR1 - 1st and 2nd IDs to be filtered
            // FR2 - 3rd and 4th IDs to be filtered
            MY_CAN_PORT->sFilterRegister[filterPosition].FR1 = \
                ((static_cast<uint32_t>(filter.filterIdMaskLow) << 16U) |
                  static_cast<uint32_t>(filter.filterIdLow));

            MY_CAN_PORT->sFilterRegister[filterPosition].FR2 = \
                ((static_cast<uint32_t>(filter.filterIdMaskHigh) << 16U) |
                  static_cast<uint32_t>(filter.filterIdHigh));

            break;

        default:
            return false;
            break; // Should never get here

    } // switch(filter.mode)

    // Filter applies to which FIFO?
    switch(filter.fifoNumber)
    {
        case CanRxMailbox::RX_0:
            MY_CAN_PORT->FFA1R &= ~filterBitPosition;
            break;
        case CanRxMailbox::RX_1:
            MY_CAN_PORT->FFA1R |= filterBitPosition;
            break;
        case CanRxMailbox::NUMBER_OF_MAILBOXES:
        default:
            return false;
    }
    // Enable the filter
    MY_CAN_PORT->FA1R |= (filterBitPosition);

    // Leave filter init mode
    MY_CAN_PORT->FMR &= ~(CAN_FMR_FINIT);

    return true;
}

/*
 * Sets the CAN baud rate.
 */
bool can_setFrequency(unsigned int frequency)
{
    uint32_t btr_value = calculateBitTiming(my_PCLK1_FREQUENCY, frequency, 1u);
    bool status = false;

    // Verify valid BTR value
    if (0xFFFFFFFF != btr_value)
    {
        status = true;
        // Set to init mode in order to alter frequency
        can_setMode(CanMode::INIT);
        // Clear the fields exclusively related to frequency
        MY_CAN_PORT->BTR &= ~(CAN_BTR_TS2 | CAN_BTR_TS1 | CAN_BTR_SJW | CAN_BTR_BRP);
        // Then set the value
        MY_CAN_PORT->BTR |= btr_value;
        // Now set to normal mode
        can_setMode(CanMode::NORMAL);
    }

    return status;
}

/*
 * Reads a CAN message from the specified RX mailbox.
 */
bool can_read(PROTOCOL::CanMessage * received_message, CanRxMailbox mailbox)
{
    // First check that there is a message available to read from buffer
    switch (mailbox)
    {
        case CanRxMailbox::RX_0:
            if (0 == (MY_CAN_PORT->RF0R & CAN_RF0R_FMP0))
            {
                return false;
            }
            break;
        case CanRxMailbox::RX_1:
            if (0 == (MY_CAN_PORT->RF1R & CAN_RF1R_FMP1))
            {
                return false;
            }
            break;
        case CanRxMailbox::NUMBER_OF_MAILBOXES:
            return false;
    }

    // Verified that there is a message in the requested mailbox
    uint8_t mailbox_index = static_cast<uint8_t>(mailbox);

    if (MY_CAN_PORT->sFIFOMailBox[mailbox_index].RIR & CAN_RI0R_IDE)
    {
        // Extended ID
        received_message->id = (MY_CAN_PORT->sFIFOMailBox[mailbox_index].RIR & (CAN_RI0R_EXID | CAN_RI0R_STID)) >> CAN_RI0R_EXID_Pos;
    }
    else
    {
        // Standard ID
        received_message->id = (MY_CAN_PORT->sFIFOMailBox[mailbox_index].RIR & CAN_RI0R_STID) >> CAN_RI0R_STID_Pos;
    }
    received_message->dataLengthCode = (MY_CAN_PORT->sFIFOMailBox[mailbox_index].RDTR & CAN_RDT0R_DLC) >> CAN_RDT0R_DLC_Pos;
    uint32_t data_low = MY_CAN_PORT->sFIFOMailBox[mailbox_index].RDLR;
    uint32_t data_high = MY_CAN_PORT->sFIFOMailBox[mailbox_index].RDHR;

    std::memcpy(received_message->packet, &data_low, 4);
    std::memcpy(received_message->packet + 4, &data_high, 4);

    switch (mailbox)
    {
        case CanRxMailbox::RX_0:
            MY_CAN_PORT->RF0R |= CAN_RF0R_RFOM0;
            break;
        case CanRxMailbox::RX_1:
            MY_CAN_PORT->RF1R |= CAN_RF1R_RFOM1;
            break;
        case CanRxMailbox::NUMBER_OF_MAILBOXES:
            return false;
    }

    return true;
}


/*
 * Queues a CAN message for transmission in a mailbox.
 */
bool can_write(PROTOCOL::CanMessage const * const message_to_transmit, CanTxMailbox mailbox)
{
    uint32_t data_high, data_low;
    std::memcpy(&data_low, message_to_transmit->packet, 4);
    std::memcpy(&data_high, message_to_transmit->packet + 4, 4);
    uint8_t mailbox_index = static_cast<uint8_t>(mailbox);

    // Check the transmit mailbox is available
    if (0 == (MY_CAN_PORT->TSR & (CAN_TSR_TME0_Msk << mailbox_index)))
    {
        return false;
    }

    // Clear the transmit request bit just in case
    MY_CAN_PORT->sTxMailBox[mailbox_index].TIR &= ~CAN_TI0R_TXRQ;
    // Clear the Remote Transmit Request
    MY_CAN_PORT->sTxMailBox[mailbox_index].TIR &= ~CAN_TI0R_RTR;
    // Load the CAN id
    MY_CAN_PORT->sTxMailBox[mailbox_index].TIR &= ~CAN_TI0R_EXID;
    MY_CAN_PORT->sTxMailBox[mailbox_index].TIR &= ~CAN_TI0R_STID;
    MY_CAN_PORT->sTxMailBox[mailbox_index].TIR |= (message_to_transmit->id << CAN_TI0R_EXID_Pos);
    // Load the DLC
    MY_CAN_PORT->sTxMailBox[mailbox_index].TDTR &= ~CAN_TDT0R_DLC;
    MY_CAN_PORT->sTxMailBox[mailbox_index].TDTR |= (message_to_transmit->dataLengthCode << CAN_TDT0R_DLC_Pos);
    // Load the data
    MY_CAN_PORT->sTxMailBox[mailbox_index].TDLR = data_low;
    MY_CAN_PORT->sTxMailBox[mailbox_index].TDHR = data_high;
    // Configure for extended ID
    MY_CAN_PORT->sTxMailBox[mailbox_index].TIR |= CAN_TI0R_IDE_Msk;
    // Set the transmit request
    MY_CAN_PORT->sTxMailBox[mailbox_index].TIR |= CAN_TI0R_TXRQ;

    return true;
}


/*
 * Checks the current value of the CAN Error Status Register.
 */
uint32_t can_getErrorState()
{
    return MY_CAN_PORT->ESR;
}


/*
 * Gets the current state of the CAN peripheral.
 */
MC::CanMode can_getMode()
{
    bool INAK = static_cast<bool>(MY_CAN_PORT->MSR & CAN_MSR_INAK >> CAN_MSR_INAK_Pos);
    bool SLAK = static_cast<bool>(MY_CAN_PORT->MSR & CAN_MSR_SLAK >> CAN_MSR_SLAK_Pos);

    if (!INAK && !SLAK)
    {
        return CanMode::NORMAL;
    }
    else if (!INAK && SLAK)
    {
        return CanMode::SLEEP;
    }
    else if (INAK && !SLAK)
    {
        return CanMode::INIT;
    }

    return CanMode::UNKNOWN;
}


/*
 * Checks the transmit mailbox successful transmit flag for a mailbox. This bit
 * is set if the message has been transmitted and no error occurred.
 */
bool can_previousTransmitSuccessful(CanTxMailbox mailbox)
{
    switch (mailbox)
    {
        case CanTxMailbox::TX_0:
            return (0 != (MY_CAN_PORT->TSR & CAN_TSR_TXOK0));
            break;
        case CanTxMailbox::TX_1:
            return (0 != (MY_CAN_PORT->TSR & CAN_TSR_TXOK1));
            break;
        case CanTxMailbox::TX_2:
            return (0 != (MY_CAN_PORT->TSR & CAN_TSR_TXOK2));
            break;
        case CanTxMailbox::NUMBER_OF_MAILBOXES:
            return false;
    }

    return false;
}

} // namespace MC


/*
 * The following functions are overrides of weakly defined functions and the
 * signature cannot be changed!!! They cannot be placed inside a namespace.
 */

extern "C" void USB_LP_CAN_RX0_IRQHandler(void)
{
    void_function_ptr_t func_call = MC::INTERRUPT_HANDLERS[static_cast<uint8_t>(MC::CanIrq::RECEIVE)];
    if (func_call)
    {
        func_call();
    }
}

extern "C" void USB_HP_CAN_TX_IRQHandler(void)
{
    void_function_ptr_t func_call = MC::INTERRUPT_HANDLERS[static_cast<uint8_t>(MC::CanIrq::TRANSMIT)];
    if (func_call)
    {
        func_call();
    }
}

extern "C" void CAN_SCE_IRQHandler(void)
{
    void_function_ptr_t func_call = MC::INTERRUPT_HANDLERS[static_cast<uint8_t>(MC::CanIrq::ERROR)];
    if (func_call)
    {
        func_call();
    }
}


namespace MC
{
/*
 * Enables an interrupt and sets a function to be called when the interrupt
 * occurs.
 *
 * Note that this function must be defined below the handlers above because they
 * are weakly defined by the compiler and appear as undefined at compile time.
 */
void can_enableInterrupt(CanIrq irq, void_function_ptr_t callback)
{
    INTERRUPT_HANDLERS[static_cast<uint8_t>(irq)] = callback;
    IRQn_Type interrupt = (IRQn_Type)0;
    uint32_t vector = 0;

    switch (irq)
    {
        case CanIrq::RECEIVE:
            interrupt = CAN_RX0_IRQn;
            vector = (uint32_t)&CAN_RX0_IRQHandler;
            MY_CAN_PORT->IER |= CAN_IER_FMPIE0;
            break;
        case CanIrq::TRANSMIT:
            interrupt = CAN_TX_IRQn;
            vector = (uint32_t)&CAN_TX_IRQHandler;
            MY_CAN_PORT->IER |= CAN_IER_TMEIE;
            break;
        case CanIrq::ERROR:
            interrupt = CAN_SCE_IRQn;
            vector = (uint32_t)&CAN_SCE_IRQHandler;
            MY_CAN_PORT->IER |= CAN_IER_ERRIE;
            break;
    }

    NVIC_SetVector(interrupt, vector);
    NVIC_EnableIRQ(interrupt);
}

} // namespace MC

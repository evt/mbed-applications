/*
 * file: mc_can.h
 * purpose: Interface to the bare metal CAN registers.
 */


#ifndef MC_CAN_H
#define MC_CAN_H


#include "mc_pin.h"
#include "evt_types.h"
#include "evt_protocols_common.h"


namespace MC
{

/*
 * Different modes of operation that the peripheral can be set to.
 */
enum class CanMode
{
    /*
     * Default mode, not an actual hardware state.
     */
    UNKNOWN     = 0u,
    /*
     * In this mode the peripheral hardware can be configured to set baud rate,
     * enable or disable transmission and reception, etc.
     */
    INIT        = 1u,
    /*
     * In this mode the peripheral can send and receive CAN messages. The peripheral
     * will spend most of its time in Normal mode.
     */
    NORMAL      = 2u,
    /*
     * A low power mode entered when a CAN message hasn't been received for a while.
     */
    SLEEP       = 3u,
};

/*
 * The hardware offers three mailboxes for messages to be queued for transmit.
 */
enum class CanTxMailbox
{
    TX_0                = 0u,
    TX_1                = 1u,
    TX_2                = 2u,
    NUMBER_OF_MAILBOXES = 3u,
};

/*
 * The hardware offers two mailboxes where messages are placed after reception
 * by the hardware for the software to read.
 */
enum class CanRxMailbox
{
    RX_0                = 0u,
    RX_1                = 1u,
    NUMBER_OF_MAILBOXES = 2u,
};

/*
 * There are three different type of interrupts with corresponding handlers.
 */
enum class CanIrq
{
    RECEIVE  = 0u,
    TRANSMIT = 1u,
    ERROR    = 2u,
};

/*
 * The hardware provides the option for 4 different message filter types.
 * See Fig. 385 of the STM32F302xx reference manual for details.
 */
enum class CanFilterMode
{
    ID_MASK_32_BIT = 0u,
    ID_LIST_32_BIT = 1u,
    ID_MASK_16_BIT = 2u,
    ID_LIST_16_BIT = 3u,
};

/*
 * The CAN peripheral offers up to 14 simultaneous filters that can
 * be applied at any given time.
 */
enum class CanFilterPosition
{
    POS_0 = 0u,
    POS_1 = 1u,
    POS_2 = 2u,
    POS_3 = 3u,
    POS_4 = 4u,
    POS_5 = 5u,
    POS_6 = 6u,
    POS_7 = 7u,
    POS_8 = 8u,
    POS_9 = 9u,
    POS_10 = 10u,
    POS_11 = 11u,
    POS_12 = 12u,
    POS_13 = 13u,
    NUMBER_OF_POSITIONS = 14u,
};

/*
 * The hardware offers the option to filter incoming messages so that no
 * resources are wasted by manually filtering messages in software. At least one
 * filter must be implemented to receive any CAN messages.
 */
struct CanFilter
{

    /*
     * In 32-bit MASK mode:
     *
     * filterIdLow = Low 16 bits of the CAN ID to be compared against using a mask
     * filterIdHigh= High 16 bits of the CAN ID to be compared against using a mask
     *
     * In 32-bit LIST mode:
     *
     * filterIdLow = Low 16 bits of the 1st single CAN ID to be accepted
     * filterIdHigh= High 16 bits of the 1st single CAN ID to be accepted
     *
     * In 16-bit MASK mode:
     *
     * filterIdLow = 1st CAN ID to be compared against using a mask
     * filterIdHigh = 2nd CAN ID to be compared against using a mask
     *
     * In 16-bit LIST mode:
     *
     * filterIdLow = 1st single CAN ID to be accepted
     * filterIdHigh = 2nd single CAN ID to be accepted
     */
    uint16_t filterIdLow;
    uint16_t filterIdHigh;

    /*
     * In 32-bit MASK mode:
     *
     * filterIdMaskLow = Low 16 bits of mask to use for a single filter
     * filterIdMaskHigh = High 16 bits of mask to use for a single filter
     *
     * In 32-bit LIST mode:
     *
     * filterIdMaskLow = Low 16 bits of 2nd single CAN ID to be accpeted
     * filterIdMaskHigh = High 16 bits of 2nd single CAN ID to be accepted
     *
     * In 16-bit MASK mode:
     *
     * filterIdMaskLow = 1st 16-bit mask to use for a single filter
     * filterIdMaskHigh = 2nd 16-bit mask to use for a single filter
     *
     * In 16-bit LIST mode:
     *
     * filterIdMaskLow = 3rd single CAN ID to be accepted
     * filterIdMaskHigh = 4th single CAN ID to be accepted
     */
    uint16_t filterIdMaskLow;
    uint16_t filterIdMaskHigh;

    CanFilterPosition position;
    CanRxMailbox  fifoNumber;

    CanFilterMode mode;

    // Constructor - Inspired by the HAL, but this is DISGUSTING
    CanFilter(uint16_t _filterIdLow,
              uint16_t _filterIdHigh,
              uint16_t _filterIdMaskLow,
              uint16_t _filterIdMaskHigh,
              CanFilterPosition _position,
              CanRxMailbox _fifoNumber,
              CanFilterMode _mode) :
                filterIdLow(_filterIdLow),
                filterIdHigh(_filterIdHigh),
                filterIdMaskLow(_filterIdMaskLow),
                filterIdMaskHigh(_filterIdMaskHigh),
                position(_position),
                fifoNumber(_fifoNumber),
                mode(_mode) {}

};

/*
 * Initialize the hardware for transmission and reception of CAN messages.
 *
 * frequency - The baud rate of the CAN bus in bits per second.
 * PCLK1_FREQUENCY - The frequency of the microcontroller's PCLK1.
 */
void can_initialize(unsigned int frequency, unsigned int PCLK1_FREQUENCY);

/*
 * Initialize two GPIO pins for CAN functions.
 *
 * rx - The GPIO pin for CANRX.
 * tx - The GPIO pin for CANTX.
 */
void can_initializeGpioPins(MC::Pin rx, MC::Pin tx);

/*
 * Sets the mode for the hardware.
 *
 * new_mode - The new mode to set the hardware to.
 */
void can_setMode(CanMode new_mode, bool loopback = false);

/*
 * Sets the CAN baud rate.
 *
 * frequency - The baud rate of the CAN bus in bits per second.
 *
 * Returns a boolean state representing the success of the method.
 */
bool can_setFrequency(unsigned int frequency);

/*
 * Reads a CAN message from the specified RX mailbox.
 *
 * id - Pointer to a CAN ID, will be populated by this function
 * dlc - Pointer to length information, will be populated by this function
 * data - Pointer to array of data to be stored
 * mailbox - Which mailbox to read from
 */
bool can_read(PROTOCOL::CanMessage * received_message, CanRxMailbox mailbox);

/*
 * Queues a CAN message for transmission in a mailbox.
 *
 * id - CAN ID of the outgoing message
 * dlc - Number of bytes of message data
 * data - Pointer to array of byte data for the message
 */
bool can_write(PROTOCOL::CanMessage const * const message_to_transmit, CanTxMailbox mailbox);

/*
 * Enables an interrupt and sets a function to be called when the interrupt
 * occurs.
 *
 * irq - The interrupt handler to set.
 * callback - A function to be called with no arguments when the interrupt occurs.
 *
 */
void can_enableInterrupt(CanIrq irq, void_function_ptr_t callback);


/*
 * Checks the current value of the CAN Error Status Register.
 *
 * Returns a word containing the contents of the CAN error.
 */
uint32_t can_getErrorState();


/*
 * Gets the current state of the CAN peripheral.
 *
 * Returns the CAN mode.
 */
MC::CanMode can_getMode();


/*
 * Checks the transmit mailbox successful transmit flag for a mailbox. This bit
 * is set if the message has been transmitted and no error occurred.
 *
 * mailbox - The transmit mailbox to check.
 *
 * Returns a boolean representing the success of a transmit.
 */
bool can_previousTransmitSuccessful(CanTxMailbox mailbox);

/*
 * Configure in-hardware CAN message reception filtering. Up to
 * 14 distinct filters can be applied
 *
 * filter - Filter configuration to be applied
 *
 * AT LEAST ONE FILTER MUST BE CONFIGUED FOR THE uC TO RECEIVE ANY MESSAGES
 */
bool can_configFilter(CanFilter & filter);

} // namespace MC


#endif // MC_CAN_H
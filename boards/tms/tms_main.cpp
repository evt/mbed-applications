
#include "target_tms.h"
#include "task_manager.h"
#include "app_tmsTempSensors.h"
#include "io_pwm_mbed_adapter.h"
#include "io_pin_names.h"
#include "mbed.h"


int main()
{

    target::Tms & Tms = target::Tms::getInstance();
    APP::TemperatureSensors temp_sensor_app = APP::TemperatureSensors(Tms.getEvtCan(), Tms.getADC(), Tms.getMUX(), 100);
    APP::ApplicationIntf * test_apps[1] = {&temp_sensor_app};
    TaskManager<1> my_task_master = TaskManager<1>(test_apps);

    constexpr auto PWM_PIN1 = IO::PIN::MC_PA3;
    constexpr auto PWM_PIN2 = IO::PIN::MC_PA6;

    auto &my_pwm1 = IO::pwmMbedAdapter::getInstance<PWM_PIN1>();
    auto &my_pwm2 = IO::pwmMbedAdapter::getInstance<PWM_PIN2>();

    my_pwm1.setPeriodMicroSeconds(10000);
    my_pwm2.setPeriodMicroSeconds(10000);
    my_pwm1.setDutyCycle(0.55);
    my_pwm2.setDutyCycle(0.40);

    my_task_master.init();

    // Set up the system ticker.
    Ticker system_ticker;

    bool ready_flag;
    auto flag_lambda = [& ready_flag]()
    {
        ready_flag = true;
    };

    system_ticker.attach(flag_lambda, 0.001);

    while (1)
    {
        while (!ready_flag)
        {
            ; // wait
        }
        ready_flag = false;
        my_task_master.run();

    }

}
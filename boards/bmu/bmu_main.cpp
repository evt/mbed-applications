
#include "mbed.h"
#include "target_bmu.h"
#include "task_manager.h"
#include "error_manager.h"
#include "app_intf.h"
#include "app_imu.h"
#include "app_bmsTemperature.h"
#include "app_cellVoltage.h"


int main()
{
    target::Bmu & Bmu = target::Bmu::getInstance();

    APP::CellTemperature temperature_app = APP::CellTemperature(Bmu.getLtc6811().first, Bmu.getEvtCan(), Bmu.getLtc6811().second, 100);
    APP::CellVoltage voltage_app = APP::CellVoltage(Bmu.getLtc6811().first, Bmu.getEvtCan(), Bmu.getLtc6811().second, 1);
    APP::Imu imu_app = APP::Imu(Bmu.getBno055(), Bmu.getEvtCan());
    APP::ApplicationIntf * test_apps[] = {&voltage_app, &imu_app, &temperature_app};
    TaskManager<3> my_task_master = TaskManager<3>(test_apps);

    my_task_master.init();

    // Set up the system ticker.
    Ticker system_ticker;

    bool ready_flag;
    auto flag_lambda = [& ready_flag]()
    {
        ready_flag = true;
    };

    system_ticker.attach(flag_lambda, 0.010);

    while (1)
    {
        while (!ready_flag)
        {
            ; // wait
        }
        ready_flag = false;
        my_task_master.run();

        if (ready_flag)
        {
            error_manager::logError("Voilated Timing.",
                                    target::ErrorLevel::ERROR);
        }
    }
}

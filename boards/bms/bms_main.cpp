
#include "mbed.h"
#include "target_bms.h"
#include "task_manager.h"
#include "error_manager.h"
#include "app_intf.h"
#include "app_bmsTemperature.h"
#include "app_cellVoltage.h"


int main()
{
    target::Bms & Bms = target::Bms::getInstance();

    APP::CellTemperature temperature_app = APP::CellTemperature(Bms.getLtc6811().first, Bms.getEvtCan(), 4, 100);
    APP::CellVoltage voltage_app = APP::CellVoltage(Bms.getLtc6811().first, Bms.getEvtCan(), Bms.getLtc6811().second, 5);
    APP::ApplicationIntf * test_apps[2] = {&voltage_app, &temperature_app};
    TaskManager<2> my_task_master = TaskManager<2>(test_apps);

    my_task_master.init();

    // Set up the system ticker.
    Ticker system_ticker;

    bool ready_flag;
    auto flag_lambda = [& ready_flag]()
    {
        ready_flag = true;
    };

    system_ticker.attach(flag_lambda, 0.010);

    while (1)
    {
        while (!ready_flag)
        {
            ; // wait
        }
        ready_flag = false;
        my_task_master.run();

        if (ready_flag)
        {
            error_manager::logError("Voilated Timing.",
                                    target::ErrorLevel::ERROR);
        }
    }
}

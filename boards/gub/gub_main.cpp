
#include "target_gub.h"
#include "task_manager.h"
#include "app_gub_logger.h"
#include "app_gfd.h"
#include "io_can_intf.h"
#include "io_rtc_intf.h"
#include "mbed.h"


int main()
{

    target::Gub & Gub = target::Gub::getInstance();
    APP::AppGubLogger gub_app = APP::AppGubLogger(Gub.getCAN1(), Gub.getCAN2(), Gub.getRTC(), 10);
    APP::GroundFault gfd_app = APP::GroundFault(Gub.getGFD(), 100);
    APP::ApplicationIntf * test_apps[2] = {&gub_app, &gfd_app};
    TaskManager<2> my_task_master = TaskManager<2>(test_apps);

    my_task_master.init();

    // Set up the system ticker.
    Ticker system_ticker;

    bool ready_flag;
    auto flag_lambda = [& ready_flag]()
    {
        ready_flag = true;
    };

    system_ticker.attach(flag_lambda, 0.001);

    while (1)
    {
        while (!ready_flag)
        {
            ; // wait
        }
        ready_flag = false;
        my_task_master.run();

    }

}

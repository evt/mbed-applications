
#include "mbed.h"
#include "target_imu.h"
#include "task_manager.h"
#include "app_intf.h"
#include "app_imu.h"
#include "error_manager.h"

int main()
{
    target::Imu & Imu = target::Imu::getInstance();
    APP::Imu imu_app = APP::Imu(Imu.getBno055(), Imu.getEvtCan());
    APP::ApplicationIntf * test_apps[1] = {&imu_app};
    TaskManager<1> my_task_master = TaskManager<1>(test_apps);

    my_task_master.init();

    // Set up the system ticker.
    Ticker system_ticker;

    bool ready_flag;
    auto flag_lambda = [& ready_flag]()
    {
        ready_flag = true;
    };

    system_ticker.attach(flag_lambda, 0.005);

    while (1)
    {
        while (!ready_flag)
        {
            ; // wait
        }
        ready_flag = false;
        my_task_master.run();

        if (ready_flag)
        {
            error_manager::logError("Voilated Timing.",
                                    target::ErrorLevel::ERROR);
        }
    }
}


#include "mbed.h"
#include "target_sgm.h"
#include "task_manager.h"
#include "error_manager.h"
#include "app_intf.h"
#include "app_sgm.h"

int main()
{
    target::Sgm & Sgm = target::Sgm::getInstance();

    APP::Sgm sgm_app = APP::Sgm(Sgm.getEvtCan(), Sgm.getLtc2440().first, Sgm.getMUX(), 1);
    APP::ApplicationIntf * test_apps[] = {&sgm_app};
    TaskManager<1> my_task_master = TaskManager<1>(test_apps);

    my_task_master.init();

    // Set up the system ticker.
    Ticker system_ticker;

    bool ready_flag;
    auto flag_lambda = [& ready_flag]()
    {
        ready_flag = true;
    };

    system_ticker.attach(flag_lambda, 0.010);

    while (1)
    {
        while (!ready_flag)
        {
            ; // wait
        }
        ready_flag = false;
        my_task_master.run();

        if (ready_flag)
        {
            error_manager::logError("Voilated Timing.",
                                    target::ErrorLevel::ERROR);
        }
    }
}

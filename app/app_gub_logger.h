/*
 * file: app_gub_logger.h
 * purpose: Class definition for application used to log CAN messages to the SD card
 */
#include "app_intf.h"
#include "io_can_intf.h"
#include "io_rtc_intf.h"
#include "fatfs.h"
#include "evt_types.h"

namespace APP
{
class AppGubLogger : public ApplicationIntf
{
public:

    void init() override;

    uint8_t run() override;

    unsigned int getRunSchedule() override;

    AppGubLogger(IO::CanIntf & canBus1, IO::CanIntf & canBus2, IO::RtcIntf & rtc, unsigned int runSchedule);
    ~AppGubLogger();

    // DEBUG ONLY
    bool getRxMsg();

private:

    void flushBuffer();
    void generateFileNames();

    IO::CanIntf & my_canBus1;
    IO::CanIntf & my_canBus2;
    IO::RtcIntf & my_rtc;
    unsigned int my_runSchedule;

    char my_fileNameEVT[9];
    char my_fileNameMC[9];


}; // class AppGubLogger
} // namespace APP
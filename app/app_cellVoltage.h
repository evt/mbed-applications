/* app_CellVoltage.h
 * 
 */

#ifndef APP_CELL_VOLTAGE_H
#define APP_CELL_VOLTAGE_H

#include "dev_ltc6811.h"
#include "app_intf.h"

/*
 * Forward Declarations
 */
namespace IO
{
class EvtCan;
} // namespace IO

namespace APP
{

class CellVoltage : public ApplicationIntf
{
public:

    //constructor
    CellVoltage(DEV::Ltc6811 * ltcArray, IO::EvtCan & evtCan,
                uint8_t numLTC, unsigned int runSchedule);
    //destructor
    ~CellVoltage();

    /*
     * init -   Initialize the application.
     */
    void init() override;

    /*
     * run -    Run the application through a single cycle of operation.
     *          For the cell voltage application, this entails collecting
     *          all of the cell voltages from the LTC6811s and sending the
     *          data over CAN.
     *
     * return - An unsigned integer representing the status of the execution.
     *          Zero represents EXIT_SUCCESS.
     */
    uint8_t run() override;

    /*
     * getRunSchedule - Gets the number of system ticks of 10 milliseconds between which the
     *                  application should be run.
     *
     * returns -        The number of system ticks. AKA 1.
     */
    unsigned int getRunSchedule() override;


private:

    //TODO: There should be a way to get this from the ltc
    static constexpr unsigned int CELLS_PER_LTC = 12;

    //normal mode, no discharge, all cells are selected.
    static constexpr DEV::Ltc6811::AdcMode DEFAULT_MODE = DEV::Ltc6811::AdcMode::MODE_NORMAL;
    static constexpr DEV::Ltc6811::DischargePermitted DEFAULT_DISCHARGE_PERMITTED = DEV::Ltc6811::DischargePermitted::NO;
    static constexpr DEV::Ltc6811::CellSelect DEFAULT_CELL_SELECT = DEV::Ltc6811::CellSelect::ALL_CELLS;

    DEV::Ltc6811 * my_ltcArray;
    IO::EvtCan & my_EvtCan;          //  can protocol
    uint8_t my_numLTC;
    unsigned int my_runSchedule;

}; /* CellVoltage */
} /* Namespace APP */

#endif /* APP_CELL_VOLTAGES_H */


#pragma once

#include "app_intf.h"
#include "dev_sendyne_sim100.h"


namespace APP
{
class GroundFault : public ApplicationIntf
{
public:

    GroundFault(DEV::Sim100 &sim100, unsigned int runSchedule);
    ~GroundFault();

    void init() override;

    uint8_t run() override;

    unsigned int getRunSchedule() override;

    DEV::Sim100::IsolationCapacitances *getIsolationCapacitance();
    DEV::Sim100::IsolationResistances *getIsolationResistance();
    DEV::Sim100::IsolationState *getIsolationState();
    DEV::Sim100::IsolationVoltages *getIsolationVoltages();
    DEV::Sim100::BatteryVoltages *getBatteryVoltages();
    DEV::Sim100::ErrorFlags *getErrorFlags();

private:
    DEV::Sim100 &my_sim100;
    unsigned int my_runSchedule;

    DEV::Sim100::IsolationCapacitances isolationCapacitance;
    DEV::Sim100::IsolationResistances isolationResistance;
    DEV::Sim100::IsolationState isolationState;
    DEV::Sim100::IsolationVoltages isolationVoltages;
    DEV::Sim100::BatteryVoltages batteryVoltages;
    DEV::Sim100::ErrorFlags errorFlags;

};
}


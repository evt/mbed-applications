/*
 * file: app_tmsTempSensors.h
 * purpose: Read temperature from various locations on the bike, multiplexed to one ADC channel
 */
#pragma once

#include <cstdint>
#include "app_intf.h"
#include "dev_evt_gpio_mux.h"
#include "io_pin_names.h"

namespace IO
{
class EvtCan;
class AdcIntf;
}

namespace APP
{

class TemperatureSensors : public ApplicationIntf
{

public:

    void init() override;
    uint8_t run() override;
    unsigned int getRunSchedule() override;
    TemperatureSensors(IO::EvtCan & evtCan, IO::AdcIntf & adc, DEV::EvtGpioMux<3> & mux, uint8_t runSchedule);
    ~TemperatureSensors();

private:

    constexpr static uint8_t NUM_THERMISTORS = 8;

    IO::EvtCan& my_EvtCan;
    IO::AdcIntf & my_adc;
    IO::PIN my_channel = IO::PIN::MC_PA4;

    DEV::EvtGpioMux<3> & my_mux;

    uint8_t my_runSchedule;
    uint8_t my_thermistorIDs[NUM_THERMISTORS] = {0,1,2,3,4,5,6,7};


};
    
}

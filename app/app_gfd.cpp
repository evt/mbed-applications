#include "app_gfd.h"

namespace APP
{

GroundFault::GroundFault(DEV::Sim100 &sim100, unsigned int runSchedule) : my_sim100(sim100),
                                                                          my_runSchedule(runSchedule)
{
    // empty constructor
}

GroundFault::~GroundFault()
{
    // Empty
}

void GroundFault::init()
{
    // Empty
}

uint8_t GroundFault::run()
{
    my_sim100.readIsolationCapacitances(isolationCapacitance);
    my_sim100.readIsolationResistances(isolationResistance);
    my_sim100.readIsolationState(isolationState);
    my_sim100.readIsolationVoltages(isolationVoltages);
    my_sim100.readBatteryVoltages(batteryVoltages);
    my_sim100.readErrorFlags(errorFlags);

    return 0;
}

unsigned int GroundFault::getRunSchedule()
{
    return this->my_runSchedule;
}

DEV::Sim100::IsolationCapacitances *GroundFault::getIsolationCapacitance()
{
    return &isolationCapacitance;
}

DEV::Sim100::IsolationResistances *GroundFault::getIsolationResistance()
{
    return &isolationResistance;
}

DEV::Sim100::IsolationState *GroundFault::getIsolationState()
{
    return &isolationState;
}

DEV::Sim100::IsolationVoltages *GroundFault::getIsolationVoltages()
{
    return &isolationVoltages;
}

DEV::Sim100::BatteryVoltages *GroundFault::getBatteryVoltages()
{
    return &batteryVoltages;
}

DEV::Sim100::ErrorFlags *GroundFault::getErrorFlags()
{
    return &errorFlags;
}
}
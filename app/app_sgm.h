/*
 * file: app_sgm.h
 * purpose: Interface for SGM app
 */


#ifndef APP_SGM_H
#define APP_SGM_H

#include <cstdint>
#include "app_intf.h"

/*
 * Forward Declarations
 */
namespace IO
{
class EvtCan;
} // namespace IO

namespace DEV
{
class Ltc2440;
class Sn74cb3q3253;
} // namespace DEV

namespace APP
{

class Sgm : public ApplicationIntf
{

public:

    //function prototypes
    Sgm(IO::EvtCan & evtCan, DEV::Ltc2440 * ltcArray, DEV::Sn74cb3q3253 mux, uint8_t runSchedule); 
    ~Sgm();
    void init();
    uint8_t run();
    unsigned int getRunSchedule();

private:

    IO::EvtCan & my_evtCan;
    DEV::Ltc2440 * my_ltcArray;
    DEV::Sn74cb3q3253 & my_mux;
    uint8_t my_runSchedule;

};

}// namespace APP

#endif  // APP_SGM_H

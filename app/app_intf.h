/*
 * file: app_intf.h
 * purpose: Defines an interface to be used for running apps by the task manager.
 */

#ifndef APPLICATION_INTF_H
#define APPLICATION_INTF_H


#include <cstdint>

namespace APP
{

class ApplicationIntf
{

public:

    ApplicationIntf() {}
    virtual ~ApplicationIntf() {}

    /*
     * Initializes the application.
     */
    virtual void init() = 0;

    /*
     * Runs the application through a cycle of operation.
     *
     * Returns: An unsigned integer representing the status of the execution.
     */
    virtual uint8_t run() = 0;

    /*
     * Gets the number of system ticks of 10 milliseconds between which the
     * application should be run.
     *
     * Returns: The number of system ticks.
     */
    virtual unsigned int getRunSchedule() = 0;

}; // class ApplicationIntf

} // namespace APP


#endif // APPLICATION_INTF_H
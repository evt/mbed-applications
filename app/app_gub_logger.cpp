/*
 * file: app_gub_logger.cpp
 * purpose: Implementation of an SD card CAN message logger
 */
#include "app_gub_logger.h"
#include <stdio.h>
#include <cstring>

namespace
{
bool receivedMessage = false;
IO::RtcIntf * rtcGlobal = nullptr;
unsigned int wbytes;
char fileWriteBuffer[100];

CanLog * canLog1;
CanLog * canLog2;

}

namespace APP
{

AppGubLogger::AppGubLogger(IO::CanIntf & canBus1, IO::CanIntf & canBus2, IO::RtcIntf & rtc, unsigned int runSchedule) :
    my_canBus1(canBus1),
    my_canBus2(canBus2),
    my_rtc(rtc),
    my_runSchedule(runSchedule)
{
    Time::Date initDate = { Time::WeekDay::MONDAY, Time::Month::APRIL, 5, 19 };
    Time::Clock initClock = { 12, 12, 12, 12 };
    my_rtc.initTime(initDate, initClock);
    rtcGlobal = &this->my_rtc;
}

AppGubLogger::~AppGubLogger()
{
    // Empty destructor
}

void AppGubLogger::init()
{
    // Init 3rd party SD drivers and FATFS
    MX_FATFS_Init();

    // Mount filesystem, generate hello message
    f_mount(&SDFatFS, SDPath, 1);
    sprintf(fileWriteBuffer, "app_gub_logger initialized!\n");

    // Find the next available log number
    generateFileNames();

    // Print hello message to each file
    f_open(&SDFile, this->my_fileNameEVT, FA_CREATE_ALWAYS|FA_WRITE);
    f_write(&SDFile, fileWriteBuffer, 29, &wbytes);
    f_close(&SDFile);


    f_open(&SDFile, this->my_fileNameMC, FA_CREATE_ALWAYS|FA_WRITE);
    f_write(&SDFile, fileWriteBuffer, 29, &wbytes);
    f_close(&SDFile);

    // Grab the log buffers from the IO layer
    canLog1 = my_canBus1.getRxLog();
    canLog2 = my_canBus2.getRxLog();

    // Register callback with CAN adapter (EVT CAN is my_canBus2)
    my_canBus1.insertCustomRxCallback([](){
                                            // This callback just gets the timestamp
                                            Time::Date tmpDate;
                                            rtcGlobal->readDate(tmpDate);
                                            if (canLog1->context == 0)
                                                rtcGlobal->readClock(canLog1->timestamp_A[canLog1->index]);
                                            else if (canLog1->context == 1)
                                                rtcGlobal->readClock(canLog1->timestamp_B[canLog1->index]);

                                            receivedMessage = true;
                                            }, 1);

    my_canBus2.insertCustomRxCallback([](){
                                            // This callback just gets the timestamp
                                            Time::Date tmpDate;
                                            rtcGlobal->readDate(tmpDate);
                                            if (canLog2->context == 0)
                                                rtcGlobal->readClock(canLog2->timestamp_A[canLog2->index]);
                                            else if (canLog2->context == 1)
                                                rtcGlobal->readClock(canLog2->timestamp_B[canLog2->index]);

                                            receivedMessage = true;
                                            }, 2);


}

uint8_t AppGubLogger::run()
{
    uint8_t ret = 0;
    int n = -1;
    receivedMessage = false;

    if ((0 != canLog1->context) && (true == canLog1->isFull_A))
    {
        f_open(&SDFile, this->my_fileNameMC, FA_OPEN_APPEND|FA_WRITE);
        for (uint32_t i = 0; i < CAN_BUFFER_LENGTH; i++)
        {
            n = f_printf(&SDFile, "%d:%d:%d.%d,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x\n",
                                    canLog1->timestamp_A[i].hours,
                                    canLog1->timestamp_A[i].minutes,
                                    canLog1->timestamp_A[i].seconds,
                                    canLog1->timestamp_A[i].subseconds,
                                    canLog1->messages_A[i].id,
                                    canLog1->messages_A[i].dataLengthCode,
                                    canLog1->messages_A[i].packet[0],
                                    canLog1->messages_A[i].packet[1],
                                    canLog1->messages_A[i].packet[2],
                                    canLog1->messages_A[i].packet[3],
                                    canLog1->messages_A[i].packet[4],
                                    canLog1->messages_A[i].packet[5],
                                    canLog1->messages_A[i].packet[6],
                                    canLog1->messages_A[i].packet[7]);
            if(-1 == n)
                ret++;
        }
        f_close(&SDFile);
        canLog1->isFull_A = false;
    }

    if ((1 != canLog1->context) && (true == canLog1->isFull_B))
    {
        f_open(&SDFile, this->my_fileNameMC, FA_OPEN_APPEND|FA_WRITE);
        for (uint32_t i = 0; i < CAN_BUFFER_LENGTH; i++)
        {
            n = f_printf(&SDFile, "%d:%d:%d.%d,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x\n",
                                    canLog1->timestamp_B[i].hours,
                                    canLog1->timestamp_B[i].minutes,
                                    canLog1->timestamp_B[i].seconds,
                                    canLog1->timestamp_B[i].subseconds,
                                    canLog1->messages_B[i].id,
                                    canLog1->messages_B[i].dataLengthCode,
                                    canLog1->messages_B[i].packet[0],
                                    canLog1->messages_B[i].packet[1],
                                    canLog1->messages_B[i].packet[2],
                                    canLog1->messages_B[i].packet[3],
                                    canLog1->messages_B[i].packet[4],
                                    canLog1->messages_B[i].packet[5],
                                    canLog1->messages_B[i].packet[6],
                                    canLog1->messages_B[i].packet[7]);
            if(-1 == n)
                ret++;
        }
        f_close(&SDFile);
        canLog1->isFull_B = false;
    }

    if ((0 != canLog2->context) && (true == canLog2->isFull_A))
    {
        f_open(&SDFile, this->my_fileNameEVT, FA_OPEN_APPEND|FA_WRITE);
        for (uint32_t i = 0; i < CAN_BUFFER_LENGTH; i++)
        {
            n = f_printf(&SDFile, "%d:%d:%d.%d,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x\n",
                                    canLog2->timestamp_A[i].hours,
                                    canLog2->timestamp_A[i].minutes,
                                    canLog2->timestamp_A[i].seconds,
                                    canLog2->timestamp_A[i].subseconds,
                                    canLog2->messages_A[i].id,
                                    canLog2->messages_A[i].dataLengthCode,
                                    canLog2->messages_A[i].packet[0],
                                    canLog2->messages_A[i].packet[1],
                                    canLog2->messages_A[i].packet[2],
                                    canLog2->messages_A[i].packet[3],
                                    canLog2->messages_A[i].packet[4],
                                    canLog2->messages_A[i].packet[5],
                                    canLog2->messages_A[i].packet[6],
                                    canLog2->messages_A[i].packet[7]);
            if(-1 == n)
                ret++;
        }
        f_close(&SDFile);
        canLog2->isFull_A = false;
    }

    if ((1 != canLog2->context) && (true == canLog2->isFull_B))
    {
        f_open(&SDFile, this->my_fileNameEVT, FA_OPEN_APPEND|FA_WRITE);
        for (uint32_t i = 0; i < CAN_BUFFER_LENGTH; i++)
        {
            n = f_printf(&SDFile, "%d:%d:%d.%d,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x\n",
                                    canLog2->timestamp_B[i].hours,
                                    canLog2->timestamp_B[i].minutes,
                                    canLog2->timestamp_B[i].seconds,
                                    canLog2->timestamp_B[i].subseconds,
                                    canLog2->messages_B[i].id,
                                    canLog2->messages_B[i].dataLengthCode,
                                    canLog2->messages_B[i].packet[0],
                                    canLog2->messages_B[i].packet[1],
                                    canLog2->messages_B[i].packet[2],
                                    canLog2->messages_B[i].packet[3],
                                    canLog2->messages_B[i].packet[4],
                                    canLog2->messages_B[i].packet[5],
                                    canLog2->messages_B[i].packet[6],
                                    canLog2->messages_B[i].packet[7]);
            if(-1 == n)
                ret++;
        }
        f_close(&SDFile);
        canLog2->isFull_B = false;
    }

    return ret;
}

unsigned int AppGubLogger::getRunSchedule()
{
    return this->my_runSchedule;
}

// For when we shut down and stop logging, flush remaining messages to the SD card
void AppGubLogger::flushBuffer()
{
    // Empty for now
}

void AppGubLogger::generateFileNames()
{
    char fileNameTmp[9] = {0};
    uint16_t logNum = 0;
    unsigned int ret = FR_OK;
    DIRECTORY dir;
    FILINFO filinfo;

    // Find the next number to use for the EVT log
    do
    {
        sprintf(fileNameTmp, "EVT%04u", logNum++);
        ret = f_findfirst(&dir, &filinfo, SDPath, fileNameTmp);
    } while(ret == FR_OK && filinfo.fname[0]);

    memcpy(this->my_fileNameEVT, fileNameTmp, 9);
    
    // Clear out local vars before searching for MC file
    memset(fileNameTmp, 0, 9);
    logNum = 0;

    // Find the next number to use for the MC log
    do
    {
        sprintf(fileNameTmp, "MC%04u", logNum++);
        ret = f_findfirst(&dir, &filinfo, SDPath, fileNameTmp);
    } while(ret == FR_OK && filinfo.fname[0]);

    memcpy(this->my_fileNameMC, fileNameTmp, 9);

}

// DEBUG ONLY
bool AppGubLogger::getRxMsg()
{
    return receivedMessage;
}

} // namespace APP


#ifndef APP_BMS_TEMPERATURE_H
#define APP_BMS_TEMPERATURE_H

#include <cstdint>
#include "app_intf.h"

/*
 * Forward Declarations
 */
namespace IO
{
class EvtCan;
} // namespace IO

namespace DEV
{
class Ltc6811;
} // namespace DEV

namespace APP
{

class CellTemperature : public ApplicationIntf
{
public:

    /*
     * init()
     *
     * Initialize the application
     */
    void init() override;

    /*
     * run()
     *
     * Runs the application to obtain all current cell temperatures
     * from the BMS slave boards. Loops through every multiplexer input
     * across every slave board and performs an ADC read to get the temperature
     * from each thermistor.
     *
     * return - Execution status - 0 means EXIT_SUCCESS 
     *
     */
    uint8_t run() override;


    /*
     * getRunSchedule()
     *
     * Returns the number of system ticks (1 tick = 10ms)
     * required for the execution of the app
     */
    unsigned int getRunSchedule() override;

    CellTemperature(DEV::Ltc6811 * ltc6811Array, IO::EvtCan & evtCan, uint8_t numSlaves, uint8_t runSchedule); 
    ~CellTemperature();

private:

    constexpr static uint8_t SLAVE_ADDR_START = 3u;
    constexpr static uint8_t THERMISTORS_PER_SLAVE = 9u;
    constexpr static uint8_t MAX_RETRIES = 5u;

    DEV::Ltc6811 * my_Ltc6811Array;
    IO::EvtCan& my_EvtCan;

    uint8_t my_numSlaves;
    uint8_t my_runSchedule;
    uint16_t my_thermistorReadings[THERMISTORS_PER_SLAVE]; // 9 thermistors per slave
};

}

#endif //APP_BMS_TEMPERATURE_H

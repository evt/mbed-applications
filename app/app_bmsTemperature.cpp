#include "app_bmsTemperature.h"
#include "error_manager.h"
#include "dev_ltc6811.h"
#include "io_evtCan.h"

namespace APP
{

CellTemperature::CellTemperature(DEV::Ltc6811 * ltc6811Array, IO::EvtCan & protocol, uint8_t numSlaves, uint8_t runSchedule)
                                : my_Ltc6811Array(ltc6811Array), my_EvtCan(protocol),
                                  my_numSlaves(numSlaves), my_runSchedule(runSchedule)
{
    // Empty Constructor
}  

CellTemperature::~CellTemperature()
{
    //Empty Destructor
}

void CellTemperature::init()
{
    DEV::Ltc6811::ConfigRegister defaultConfig;

    defaultConfig.GPIO   = 0x01; // GPIO1 needs to be always 'on' to be able to use it as an ADC input
    defaultConfig.REFON  = 0x01; // Voltage reference always stays on to save time on A/D conversions
    defaultConfig.DTEN   = 0;
    defaultConfig.ADCOPT = 0;
    defaultConfig.VUV    = 0; // May need to define this later
    defaultConfig.VOV    = 0; // May need to define this later
    defaultConfig.DCC    = 0;
    defaultConfig.DCTO   = 0;

    for(uint8_t i = 0; i < this->my_numSlaves; i++)
    {
        my_Ltc6811Array[i].writeConfig(&defaultConfig);
    }

}


uint8_t CellTemperature::run()
{
    uint16_t thermistorVoltages[THERMISTORS_PER_SLAVE];
    uint16_t dummyRead[6];
    uint8_t muxSelect;

    // Slaves with thermistors are in a continuous range from SLAVE_ADDR_START to (SLAVE_ADDR_START + numSlaves)
    // This control logic may change based on how slaves are arranged physically in the battery pack
    for (uint8_t i = SLAVE_ADDR_START; i < (SLAVE_ADDR_START + this->my_numSlaves); i++)
    {
        for(uint8_t j = 0; j <= THERMISTORS_PER_SLAVE; j++)
        {
            // Select new thermistor from the multiplexer
            muxSelect = (j) << 1;
            my_Ltc6811Array[i].toggleGPIO(muxSelect);

            // Start ADC conversions on GPIO 1
            my_Ltc6811Array[i].startADCAux(DEV::Ltc6811::AdcMode::MODE_NORMAL,
                                            DEV::Ltc6811::GpioSelect::GPIO_1);

            // Loop to poll if the ADC is done. Times out after 5 retries
            DEV::Ltc6811::AdcStat status = DEV::Ltc6811::AdcStat::BUSY;
            uint8_t retries = 0;
            do
            {
                status = my_Ltc6811Array[i].pollADC();
                retries++;

            } while((status == DEV::Ltc6811::AdcStat::BUSY) && (retries < MAX_RETRIES));

            // If we exceed maximum retries log an error
            if(retries >= MAX_RETRIES)
            {
                error_manager::logError("ADC timed out. Skipping current input...", target::ErrorLevel::WARNING);
                continue;
            }

            // Read the temperature from the LTC
            my_Ltc6811Array[i].readAuxVoltages(dummyRead);
            thermistorVoltages[j] = dummyRead[0];

            // If we read two cells we can send a can message
            if((j % 2) == 1)
            {
                // If we have an odd number of thermistors, stuff the last message with FF's in the upper half
                if ((THERMISTORS_PER_SLAVE % 2 == 1) && (j == THERMISTORS_PER_SLAVE))
                {
                    IO::EvtCan::BMS_cellTemperature payload((i*THERMISTORS_PER_SLAVE)+j-1,
                                                             thermistorVoltages[j-1],
                                                             static_cast<uint8_t>(0xFF),
                                                             static_cast<uint16_t>(0xFFFF));
                    my_EvtCan.sendMessage(payload);
                }
                else
                {
                    IO::EvtCan::BMS_cellTemperature payload((i*THERMISTORS_PER_SLAVE)+j-1,
                                                             thermistorVoltages[j-1],
                                                            (i*THERMISTORS_PER_SLAVE)+j,
                                                             thermistorVoltages[j]);
                    my_EvtCan.sendMessage(payload);
                }
            }
        }
    }
    
    return 0;
}

unsigned int CellTemperature::getRunSchedule()
{
    return this->my_runSchedule;
}

}

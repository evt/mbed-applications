//
// app_cellVoltage.cpp
// 
//

#include "app_cellVoltage.h"
#include "error_manager.h"
#include "io_evtCan.h"

namespace APP
{

CellVoltage::CellVoltage(DEV::Ltc6811 * ltcArray, IO::EvtCan & evtCan,
            uint8_t numLTC, unsigned int runSchedule) :
            my_ltcArray(ltcArray), my_EvtCan(evtCan),
            my_numLTC(numLTC), my_runSchedule(runSchedule)
{
    // empty constructor
} 

CellVoltage::~CellVoltage()
{
    // empty destructor
}

void CellVoltage::init()
{
    DEV::Ltc6811::ConfigRegister defaultConfig;

    defaultConfig.GPIO   = 0x01; // GPIO1 needs to be always 'on' to be able to use it as an ADC input
    defaultConfig.REFON  = 0x01; // Voltage reference always stays on to save time on A/D conversions
    defaultConfig.DTEN   = 0;
    defaultConfig.ADCOPT = 0;
    defaultConfig.VUV    = 0; // May need to define this later
    defaultConfig.VOV    = 0; // May need to define this later
    defaultConfig.DCC    = 0;
    defaultConfig.DCTO   = 0;

    // Write a default config for all boards
    for (int i = 0; i != my_numLTC; ++i)
    {
        my_ltcArray[i].writeConfig(&defaultConfig);
    }
}

uint8_t CellVoltage::run()
{
    // Array to store the cell voltages
    uint16_t adcVoltages[CELLS_PER_LTC];

    for (int i = 0; i < my_numLTC; i++)
    {
        /* ---------------------------------------------------------- 
         * Sweep through the number of slaves and start the ADC,
         * read the voltages and send over can. Log a warning if
         * the LTC doesn't respond after three attempts.
         * ---------------------------------------------------------*/

        my_ltcArray[i].startADC(DEFAULT_MODE, 
                                DEFAULT_DISCHARGE_PERMITTED,
                                DEFAULT_CELL_SELECT);

        constexpr int MAX_TRIES = 3;
        int tries = 0;
        bool ADCbusy;

        // loop until either the ADC is no longer busy or we time out
        do
        {
            ADCbusy = (my_ltcArray[i].pollADC() == DEV::Ltc6811::AdcStat::BUSY);
            ++tries;

        } while (ADCbusy && (tries < MAX_TRIES));

        // If we timed out log an error.
        if (tries >= MAX_TRIES)
        {
            error_manager::logError("ADC timed out while polling. Skipping...",
                                    target::ErrorLevel::WARNING);
            continue;
        }

        // if a adc has been read, package and send those voltages through
        // evt can
        uint8_t numCells;
        numCells = my_ltcArray[i].getNumCells();
        my_ltcArray[i].readCellVoltages(adcVoltages);

        for (uint8_t cellNum = 0; cellNum < numCells; cellNum += 2 )
        {
            // sweep through the number of cells and package two at a time
            // into a struct that can be sent over EvtCan. 
            // TODO: This has every ltc reporting its first cell as 0.
            IO::EvtCan::BMS_cellVoltage payload(
                                    (i*CELLS_PER_LTC) + cellNum,
                                    adcVoltages[cellNum],
                                    (i*CELLS_PER_LTC) + cellNum + 1,
                                    adcVoltages[cellNum + 1]
                                );
            //send EVT message
            my_EvtCan.sendMessage(payload);

        }
        
    }

    return 0;
}

unsigned int CellVoltage::getRunSchedule()
{
    return my_runSchedule;
}

} /* Namespace APP */


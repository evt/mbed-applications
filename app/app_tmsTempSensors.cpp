/*
 * file: app_tmsTempSensors.cpp
 * purpose: Read temperature from various locations on the bike, multiplexed to one ADC channel
 */
#include "app_tmsTempSensors.h"
#include "io_gpio_intf.h"
#include "io_adc_intf.h"
#include "io_evtCan.h"

constexpr float MICROCONTROLLER_VDD(3300);

inline float convertRawADCToTempVoltage(float rawValue)
{
    return MICROCONTROLLER_VDD * rawValue;
}

namespace APP
{

TemperatureSensors::TemperatureSensors(IO::EvtCan & protocol,
                                       IO::AdcIntf & adc,
                                       DEV::EvtGpioMux<3> & mux,
                                       uint8_t runSchedule) :
                                       my_EvtCan(protocol),
                                       my_adc(adc),
                                       my_mux(mux),
                                       my_runSchedule(runSchedule)
{
    // Empty Constructor
}

TemperatureSensors::~TemperatureSensors()
{
    //Empty Destructor
}

void TemperatureSensors::init()
{
   
}

uint8_t TemperatureSensors::run()
{
    uint16_t arrayTempVoltage[2] = { };
    uint16_t outputTempVoltage;

    for(unsigned int selectedOutput = 0; selectedOutput < NUM_THERMISTORS; selectedOutput+=2)
    {

        my_mux.choosePort(selectedOutput);
        wait(0.01);
        arrayTempVoltage[0] = static_cast<uint16_t>(convertRawADCToTempVoltage(my_adc.read()));

        my_mux.choosePort(selectedOutput+1);
        wait(0.01);
        arrayTempVoltage[1] = static_cast<uint16_t>(convertRawADCToTempVoltage(my_adc.read()));

        IO::EvtCan::TMS_temperatureSensors payload(my_thermistorIDs[selectedOutput],
                                                   arrayTempVoltage[0],
                                                   my_thermistorIDs[selectedOutput+1],
                                                   arrayTempVoltage[1]);
        
        my_EvtCan.sendMessage(payload);

    }

    return 0;

}

unsigned int TemperatureSensors::getRunSchedule()
{
    return this->my_runSchedule;
}

}





/*
 * file: app_imu.h
 * purpose: Interface for IMU
 */


#ifndef APP_IMU_H
#define APP_IMU_H

#include <cstdint>
#include "app_intf.h"

/*
 * Forward Declarations
 */
namespace IO
{
class EvtCan;
} // namespace IO

namespace DEV
{
class bno055;
} // namespace DEV


namespace APP
{

class Imu : public ApplicationIntf
{

public:
    
    //function prototypes
    // constructor
    Imu(DEV::bno055 & bno, IO::EvtCan & evtCan); 
    // destructor 
    ~Imu();
    // initializes application
    void init(); 
    // runs application
    uint8_t run();
    // gets number of system ticks
    unsigned int getRunSchedule();

private:

    DEV::bno055 & my_bno;
    IO::EvtCan & my_evtCan;

};

}// namespace APP

#endif  // APP_IMU_H
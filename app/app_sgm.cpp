/*
 * file: app_sgm.cpp
 * purpose: Software for acquiring information from SGM
 */

#include "app_sgm.h"
#include "dev_ltc2440.h"
#include "dev_sn74cb3q3253.h"
#include "evt_bit_utils.h"
#include "io_evtCan.h"
#include "target_sgm.h"

namespace APP
{   

Sgm::Sgm(IO::EvtCan & evtCan, DEV::Ltc2440 * ltcArray, DEV::Sn74cb3q3253 mux, uint8_t runSchedule) :
        my_evtCan(evtCan), my_ltcArray(ltcArray), my_mux(mux), my_runSchedule(runSchedule)
{
    //empty constructor
}

Sgm::~Sgm()
{
    // empty destructor
}

void Sgm::init()
{
    // empty init
}

/*
 * Runs the application through a cycle of operation.
 *
 * Returns: An unsigned integer representing the status of the execution.
 */
uint8_t Sgm::run()
{
    DEV::Ltc2440::AdcCode adcCode0;
    DEV::Ltc2440::AdcCode adcCode1;
    DEV::Ltc2440::AdcCode adcCode2;
    DEV::Ltc2440::AdcCode adcCode3;

    my_mux.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT0);
    my_ltcArray[0].read(&adcCode0);

    my_mux.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT1);
    my_ltcArray[1].read(&adcCode1);

    my_mux.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT2);
    my_ltcArray[2].read(&adcCode2);

    my_mux.selectSPI(DEV::Sn74cb3q3253::SPI_PORT::PORT3);
    my_ltcArray[3].read(&adcCode3);
    
    IO::EvtCan::SGM_strain strain1(0, adcCode0.value);
    IO::EvtCan::SGM_strain strain2(1, adcCode1.value);
    IO::EvtCan::SGM_strain strain3(2, adcCode2.value);
    IO::EvtCan::SGM_strain strain4(3, adcCode3.value);
    
    // send data through CAN
    this->my_evtCan.sendMessage(strain1);
    this->my_evtCan.sendMessage(strain2);
    this->my_evtCan.sendMessage(strain3);
    this->my_evtCan.sendMessage(strain4);

    return (0);
}

/*
 * Gets the number of system ticks of 10 milliseconds between which the
 * application should be run.
 *
 * Returns: The number of system ticks.
 */
unsigned int Sgm::getRunSchedule()
{
    return this->my_runSchedule;
}
}// namespace APP

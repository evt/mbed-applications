/*
 * file: app_imu.cpp
 * purpose: Software for acquiring information from IMU
 */

#include "app_imu.h"
#include "dev_bno055.h"
#include "io_evtCan.h"

namespace APP
{   
    
// constructor
Imu::Imu(DEV::bno055 & bno, IO::EvtCan & evtCan) : my_bno(bno), my_evtCan(evtCan)
{

}

// destructor
Imu::~Imu()
{
    // empty
}

/*
 * Initializes the application.
 */
void Imu::init()
{
    // empty
}

/*
 * Runs the application through a cycle of operation.
 *
 * Returns: An unsigned integer representing the status of the execution.
 */
uint8_t Imu::run()
{
    uint16_t W;
    uint16_t X;
    uint16_t Y;
    uint16_t Z;
    DEV::bno055::CalibrationStatus calStat;

    // get linear acceleration data
    X = my_bno.getLinearAccelerationX();
    Y = my_bno.getLinearAccelerationY();
    Z  = my_bno.getLinearAccelerationZ();
    IO::EvtCan::IMU_linearAcceleration linearAcceleration(X, Y, Z);

    // get gravity vector data 
    X = my_bno.getGravityVectorX();
    Y = my_bno.getGravityVectorY();
    Z = my_bno. getGravityVectorZ();
    IO::EvtCan::IMU_gravityVector gravityVector(X, Y, Z);

    // get quaternion position data
    W = my_bno.getQuaternionPositionW();
    X = my_bno.getQuaternionPositionX();
    Y = my_bno.getQuaternionPositionY();
    Z = my_bno.getQuaternionPositionZ();
    IO::EvtCan::IMU_quaternionPosition quaternion(W, X, Y, Z);

    // Monitor the sensor calibration status
    my_bno.getCalibrationStatus(calStat);
    IO::EvtCan::IMU_calibrationStatus calibration(calStat.sysCal,
                                                  calStat.accelCal,
                                                  calStat.magCal,
                                                  calStat.gyroCal);

    // send data through CAN
    my_evtCan.sendMessage(linearAcceleration);
    my_evtCan.sendMessage(gravityVector);
    my_evtCan.sendMessage(quaternion);
    my_evtCan.sendMessage(calibration);

    return (0);
}

/*
 * Gets the number of system ticks of 10 milliseconds between which the
 * application should be run.
 *
 * Returns: The number of system ticks.
 */
unsigned int Imu::getRunSchedule()
{
    return (10);
}

}// namespace APP

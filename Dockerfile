FROM ubuntu:18.04

# Install dependencies for running mbed
RUN apt-get update && apt-get install git python3 python3-pip mercurial wget -y

# Install GCC-ARM toolchain
RUN cd /usr/local/bin \
    && wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/7-2018q2/gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2 \
    && tar xvf gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2 \
    && rm gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2

# Set environment variable
ENV GCC_ARM_TOOLS_PATH "/usr/local/bin/gcc-arm-none-eabi-7-2018-q2-update/bin/"

# Directory for mbed repos
RUN mkdir /mbed

# Setup mbed-cli
RUN pip3 install mbed-cli

# Clone mbed repositories
# Basically run mbed deploy without actually running mbed deploy
RUN git clone https://github.com/ARMmbed/mbed-os /mbed/mbed-os
RUN cd /mbed/mbed-os && git checkout 35fa909641fedcad9bbe0c7300d4ccdf15a2b71a


ENTRYPOINT /bin/bash
